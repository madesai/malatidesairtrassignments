
#include <Windows.h>
#include <gl/GL.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment (lib, "opengl32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

int gWindowWidth;
int gWindowHeight;
int gWidth;
int gHeight;
int bDone = false;
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MY_OPENGL_APP");

	//initialization
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = (CS_HREDRAW | CS_VREDRAW | CS_OWNDC);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; //name of call back function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	//register class
	RegisterClassEx(&wndclass);
	//hwnd = CreateWindow()
	gWidth = GetSystemMetrics(SM_CXSCREEN);
	gHeight = GetSystemMetrics(SM_CYSCREEN);
	
	gWindowWidth = gWidth / 2;
	gWindowHeight = gHeight / 2;

	//create window
	int xCordinate = gWindowWidth / 2;
	int yCordinate = gWindowHeight / 2;


	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OpenGL  : ViewPort Alignment "),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		xCordinate,
		yCordinate,
		gWindowWidth,
		gWindowHeight,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
			}
		}
	}

	uninitialize();
	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//	void display(void);
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);
	void MyViewPortTransform(int);
	int keyNum = 0;
	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
		/*	case WM_PAINT:  //remove for double buffer
		display();
		break;*/
		/*case WM_ERASEBKGND:
		return (0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x31:
			MyViewPortTransform(1);
			break;
		case 0x32 :
			MyViewPortTransform(2);
			break;
		case 0x33 : 
			MyViewPortTransform(3);
			break;
		case 0x34 : 
			MyViewPortTransform(4);
			break;
		case 0x35:
			MyViewPortTransform(5);
			break;
		case 0x36:
			MyViewPortTransform(6);
			break;
		case 0x37:
			MyViewPortTransform(7);
			break;
		case 0x38:
			MyViewPortTransform(8);
			break;
		case 0x39:
			MyViewPortTransform(9);
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;
			
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void MyViewPortTransform(int keyNum)
{
	int windowWidth;
	int windowHeight;
	if (gbFullScreen == false)
	{
		windowWidth = GetSystemMetrics(SM_CXSCREEN);
		windowHeight = GetSystemMetrics(SM_CYSCREEN);
	}
	switch(keyNum)
	{
		case 1 :
			if (gbFullScreen)
			glViewport(0, 0, gWidth / 3, gHeight / 3);
			else
			glViewport(0, 0, gWindowWidth / 3, gWindowHeight / 3);
			break;

		case 2:
			//case 2
			if (gbFullScreen)
				glViewport(gWidth - (gWidth / 3), (gHeight - gHeight / 3), gWidth / 3, gHeight / 3);
			else
				glViewport(gWindowWidth - (gWindowWidth / 3), (gWindowHeight - gWindowHeight / 3), gWindowWidth / 3, gWindowHeight / 3);
			break;

		case 3:
			//case 3
			if(gbFullScreen)
			glViewport(0, (gHeight - gHeight / 3), gWidth / 3, gHeight / 3);
			else
				glViewport(0, (gWindowHeight - gWindowHeight / 3), gWindowWidth / 3, gWindowHeight / 3);
			break;

		case 4:
			//case 4
			if(gbFullScreen)
			glViewport(gWidth - gWidth / 3, 0, gWidth / 3, gHeight / 3);
			else
				glViewport(gWindowWidth - gWindowWidth / 3, 0, gWindowWidth / 3, gWindowHeight / 3);
			break;

		case 5:
			//case 5
			if(gbFullScreen)
			glViewport(0, (gHeight - 2 * gHeight / 3), gWidth / 3, gHeight / 3);
			else
				glViewport(0, (gWindowHeight - 2 * gWindowHeight / 3), gWindowWidth / 3, gWindowHeight / 3);
			break;

		case 6:
			//case 6
			if(gbFullScreen)
			glViewport(gWidth - (2 * gWidth / 3), 0, gWidth / 3, gHeight / 3);
			else
				glViewport(gWindowWidth - (2 * gWindowWidth / 3), 0, gWindowWidth / 3, gWindowHeight / 3);
			break;

		case 7:
			//case 7
			if(gbFullScreen)
			glViewport(gWidth - gWidth / 3, gHeight - (2 * gHeight / 3), gWidth / 3, gHeight / 3);
			else
				glViewport(gWindowWidth - gWindowWidth / 3, gWindowHeight - (2 * gWindowHeight / 3), gWindowWidth / 3, gWindowHeight / 3);
			break;

		case 8:
			//case 8
			if(gbFullScreen)
			glViewport(gWidth - 2 * gWidth / 3, gHeight - (gHeight / 3), gWidth / 3, gHeight / 3);
			else
				glViewport(gWindowWidth - 2 * gWindowWidth / 3, gWindowHeight - (gWindowHeight / 3), gWindowWidth / 3, gWindowHeight / 3);
			break;

		case 9:
			//case 9
			if(gbFullScreen)
			glViewport(gWidth - (2 * gWidth / 3), (gHeight - 2 * gHeight / 3), gWidth / 3, gHeight / 3);
			else
				glViewport(gWindowWidth - (2 * gWindowWidth / 3), (gWindowHeight - 2 * gWindowHeight / 3), gWindowWidth / 3, gWindowHeight / 3);
			break;
		}
}
void ToggleFullScreen(void)
{
	MONITORINFO mi;

	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY),
					&mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	resize(gWindowWidth, gWindowHeight); //removed
}

void display(void)
{
	void DrawTriangle();
	glClear(GL_COLOR_BUFFER_BIT);

	//	glFlush();  remove it for double buffer

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	DrawTriangle();
	SwapBuffers(ghdc);
}
void DrawTriangle()
{
	//for triangle
	
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);
	glEnd();
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)(width), (GLsizei)(height));
}

void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

