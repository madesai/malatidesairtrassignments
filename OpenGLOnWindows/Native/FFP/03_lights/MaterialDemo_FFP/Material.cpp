
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include "Material.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "glu32.lib")
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

int gWindowWidth;
int gWindowHeight;

int bDone = false;
bool gbLighting = 0;
GLUquadric *quadric = NULL;

GLfloat angle_light = 0.0f;

GLfloat light_ambient[] = { 0.0f, 0.0f, 0.0f , 0.0f };
GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f , 1.0f };//white light
GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f , 1.0f };
GLfloat light_position[] = { 0.0f, 0.0f, 0.0f , 0.0f };

GLfloat light_model_ambient[] = { 0.2f, 0.2f, 0.2f, 0.2f, 0.0f };//newly added
GLfloat light_model_local_viewer = { 0.0f };//newly added

int bRotate = 0;
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void Update();

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MY_OPENGL_APP");

	//initialization
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = (CS_HREDRAW | CS_VREDRAW | CS_OWNDC);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; //name of call back function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	//register class
	RegisterClassEx(&wndclass);
	//hwnd = CreateWindow()
	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	gWindowWidth = width / 2;
	gWindowHeight = height / 2;

	//create window
	int xCordinate = gWindowWidth / 2;
	int yCordinate = gWindowHeight / 2;


	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OpenGL  : Material Demo"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		xCordinate,
		yCordinate,
		gWindowWidth,
		gWindowHeight,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	
	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
				Update();
			}
		}
	}

	uninitialize();
	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//	void display(void);
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
		/*	case WM_PAINT:  //remove for double buffer
		display();
		break;*/
		/*case WM_ERASEBKGND:
		return (0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;
		case 0x4C: //L
		case 0x6C: //l
			if (gbLighting == 0)
			{
				gbLighting = 1;
				glEnable(GL_LIGHTING);
			}
			else
			{
				gbLighting = 0;
				glDisable(GL_LIGHTING);
			}
			break;
		case 0x78://case x
		case 0x58:
			bRotate = 1;//x
			break;

		case 0x79://y
		case 0x59:
			bRotate = 2;//y
			break;

		case 0x7a://z
		case 0x5a:
			bRotate = 3; //z
			break;
		default:

			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;

	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY),
					&mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient);
	glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	//glLightfv(GL_LIGHT0, GL_POSITION, light_position0);

	glEnable(GL_LIGHT0);

	resize(gWindowWidth, gWindowHeight);
}
void display(void)
{
	void DrawSphere(float x,float y );
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//	glFlush();  //remove it for double buffer

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient1);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse1);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular1);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness1);
	DrawSphere(-6.5f, 4.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse2);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular2);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness2);
	DrawSphere(-6.5f, 2.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient3);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse3);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular3);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness3);
	DrawSphere(-6.5f, 0.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient4);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse4);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular4);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness4);
	DrawSphere(-6.5f, -1.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient5);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse5);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular5);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness5);
	DrawSphere(-6.5f, -3.5f);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient6);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse6);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular6);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness6);
	DrawSphere(-6.5f, -5.5f);

//==========================================================

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient7);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse7);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular7);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness7);
	DrawSphere(-2.5f, 4.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient8);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse8);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular8);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness8);
	DrawSphere(-2.5f, 2.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient9);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse9);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular9);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness9);
	DrawSphere(-2.5f, 0.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient10);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse10);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular10);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness10);
	DrawSphere(-2.5f, -1.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient11);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse11);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular11);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness11);
	DrawSphere(-2.5f, -3.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient12);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse12);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular12);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness12);
	DrawSphere(-2.5f, -5.5f);
//===================================

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient13);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse13);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular13);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness13);
	DrawSphere(2.5f, 4.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient14);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse14);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular14);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness14);
	DrawSphere(2.5f, 2.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient15);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse15);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular15);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness15);
	DrawSphere(2.5f, 0.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient16);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse16);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular16);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness16);
	DrawSphere(2.5f, -1.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient17);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse17);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular17);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness17);
	DrawSphere(2.5f, -3.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient18);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse18);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular18);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness18);
	DrawSphere(2.5f, -5.5f);

//============================================================

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient19);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse19);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular19);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness19);
	DrawSphere(6.5f, 4.5f);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient20);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse20);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular20);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness20);
	DrawSphere(6.5f, 2.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient21);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse21);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular21);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness21);
	DrawSphere(6.5f, 0.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient22);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse22);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular22);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness22);
	DrawSphere(6.5f, -1.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient23);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse23);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular23);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness23);
	DrawSphere(6.5f, -3.5f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient24);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse24);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular24);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shinyness24);
	DrawSphere(6.5f, -5.5f);
	
	SwapBuffers(ghdc);
}
void DrawSphere(float x, float y)
{
	glTranslatef(x, y, -17.0f);
	if (bRotate == 1)
	{
		glRotatef(angle_light, 1, 0, 0);
		light_position[0] = angle_light;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}
	else if (bRotate == 2)
	{
		glRotatef(angle_light, 0, 1, 0);
		light_position[0] = angle_light;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}
	else if (bRotate == 3)
	{
		glRotatef(angle_light, 0, 0, 1);
		light_position[0] = angle_light;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
}
void Update(void)
{
	angle_light = angle_light + 0.1f;
	if (angle_light >= 360.0f)
		angle_light = 0.0f;

}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
}

void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	gluDeleteQuadric(quadric);
}

