
#include <Windows.h>
#include <gl/GL.h>
#include <math.h>
#include<MmSystem.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "Winmm.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

int bDone = false;
float I1WordXAxis = -0.9f;
float NWordYAxis = 1.8f;
float DWordAxis1 = 0.0f;
float DWordAxis2 = 0.0f;
float I2WordYAxis = 1.8f;
float AWordXAxis = +0.75f;
float AFlagXAxis = -2.0f;
float A1FlagXAxis = -2.5f;
float A2FlagXAxis = -2.5f;
float A3FlagXAxis = -2.5f;
int gFlag = 0;
int gStatus = 0;
int gStatus1 = 0;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void Update(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MY_OPENGL_APP");

	//initialization
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = (CS_HREDRAW | CS_VREDRAW | CS_OWNDC);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; //name of call back function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	//register class
	RegisterClassEx(&wndclass);
	//hwnd = CreateWindow()
	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	int windowWidth = width / 2;
	int windowHeight = height / 2;

	//create window
	int xCordinate = windowWidth / 2;
	int yCordinate = windowHeight / 2;


	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OpenGL  : India Dynamic"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		xCordinate,
		yCordinate,
		windowWidth,
		windowHeight,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				Update();
				display();
			}
		}
	}

	uninitialize();
	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//	void display(void);
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;

		break;
		/*	case WM_PAINT:  //remove for double buffer
		display();
		break;*/
		/*case WM_ERASEBKGND:
		return (0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;

	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY),
					&mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA ;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	PlaySound(TEXT("D:\\OpenGL projects\\NATIVE\\10_ShapeAndStaticNDynamicIndia\\DynamicIndia\\SandeseAteHe.wav"), NULL, SND_ASYNC);
	//PlaySound(TEXT("SandeseAteHe.wav"), NULL, SND_ASYNC);

	//resize(WIN_WIDTH, WIN_HEIGHT); //removed
}

void display(void)
{
	void DrawStaticIndia(void);
	void DrawI1Word(void);
	void DrawNWord(void);
	void DrawBlackDWord();
	void DrawI2Word(void);
	void DrawAWord(void);
	void DrawAFlag(void);
	void DrawA1Flag(void);
	void DrawFlexDWord();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//	glFlush();  remove it for double buffer
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(I1WordXAxis, 0.0f, 0.0f);
	DrawI1Word();
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, NWordYAxis, 0.0f);
	DrawNWord();
	if (gStatus == 0)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		DrawBlackDWord();
	}
	if (gStatus == 2)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		DrawFlexDWord();
	}
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, I2WordYAxis, 0.0f);
	DrawI2Word();
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(AWordXAxis, 0.0f, 0.0f);
	DrawAWord();
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(AFlagXAxis, 0.0f, -1.0f);
	DrawA1Flag();

	SwapBuffers(ghdc);
}

void Update(void)
{
	I1WordXAxis= I1WordXAxis + 0.0001f;
	if (I1WordXAxis >= -0.5f)
	{
		I1WordXAxis = -0.5f;
		gFlag = 2;
	}
	if (gFlag == 2)
	{
		NWordYAxis = NWordYAxis - 0.0005f;
		if (NWordYAxis <= 0.0f)
		{
			NWordYAxis = 0.0f;
			gFlag = 3;
		}
	}
	if (gFlag == 3)
	{
		DWordAxis1 = DWordAxis1 + 0.0005;
		if (DWordAxis1 > 1.0f)
		{
			DWordAxis1 = 1.0f;
			gFlag = 4;
		}
		DWordAxis2 = DWordAxis2 + 0.0005;
		if (DWordAxis2 > 0.647f)
			DWordAxis2 = 0.647f;

		gStatus = 2;

		
	}
	if (gFlag == 4)
	{
		I2WordYAxis = I2WordYAxis - 0.0005f;
		if (I2WordYAxis <= 0.0f)
		{
			I2WordYAxis = 0.0f;
			gFlag = 5;
		}
	}
	if (gFlag == 5)
	{
		AWordXAxis = AWordXAxis - 0.0005f;
		if (AWordXAxis <= 0.25f)
		{
			AWordXAxis = 0.25f;
			gFlag = 6;
		}
	}
	if (gFlag == 6)
	{
		AFlagXAxis = AFlagXAxis + 0.0005f;
		if (AFlagXAxis >= 0.25f)
		{
			AFlagXAxis = 0.25f;
			gFlag = 7;
		}
	}
	if (gFlag == 7)
	{
		A1FlagXAxis = A1FlagXAxis + 0.0005f;
		A2FlagXAxis = A2FlagXAxis + 0.0005f;
		A3FlagXAxis = A3FlagXAxis + 0.0005f;
		gStatus1 = 3;
		if (A1FlagXAxis >= 0.33f)
		{
			A1FlagXAxis = 0.33f;
		}
		if (A2FlagXAxis >= 0.325f)
		{
			A2FlagXAxis = 0.325f;
		}
		if (A3FlagXAxis >= 0.315f)
		{
			A3FlagXAxis = 0.315f;
		}
		gFlag = 8;
	}
}

void DrawI1Word(void)
{
glLineWidth(5.0f);
glBegin(GL_LINES);
glColor3f(1.0f, 0.647f, 0.0f);
glVertex3f(-0.25f, 0.75f, 0.0f);
glColor3f(0.0f, 1.0f, 0.0f);
glVertex3f(-0.25f, -0.75f, 0.0f);
glEnd();

}
void DrawNWord(void)
{
glLineWidth(5.0f);
glBegin(GL_LINES);
glColor3f(1.0f, 0.647f, 0.0f);
glVertex3f(-0.60f, 0.75f, 0.0f);
glColor3f(0.0f, 1.0f, 0.0f);
glVertex3f(-0.60f, -0.75f, 0.0f);
glEnd();

glBegin(GL_LINES);
glColor3f(1.0f, 0.647f, 0.0f);
glVertex3f(-0.60f, 0.75f, 0.0f);
glColor3f(0.0f, 1.0f, 0.0f);
glVertex3f(-0.35f, -0.75f, 0.0f);
glEnd();

glBegin(GL_LINES);
glColor3f(1.0f, 0.647f, 0.0f);
glVertex3f(-0.35f, 0.75f, 0.0f);
glColor3f(0.0f, 1.0f, 0.0f);
glVertex3f(-0.35f, -0.75f, 0.0f);
glEnd();
}

void DrawFlexDWord()
{
	glLineWidth(5.0f);
	glBegin(GL_LINES);
	glColor3f(DWordAxis1, DWordAxis2, 0.0f);
	glVertex3f(-0.10f, 0.75f, 0.0f);
	glColor3f(0.0f, DWordAxis1, 0.0f);
	glVertex3f(-0.10f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(DWordAxis1, DWordAxis2, 0.0f);
	glVertex3f(-0.15f, 0.75f, 0.0f);
	glVertex3f(0.10f, 0.75f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(DWordAxis1, DWordAxis2, 0.0f);
	glVertex3f(0.1f, 0.755f, 0.0f);
	glColor3f(0.0f, DWordAxis1, 0.0f);
	glVertex3f(0.1f, -0.755f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.0f, DWordAxis1, 0.0f);
	glVertex3f(-0.15f, -0.75f, 0.0f);
	glVertex3f(0.10f, -0.75f, 0.0f);
	glEnd();

}

void DrawBlackDWord()
{
	glLineWidth(5.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.10f, 0.75f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.10f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.15f, 0.75f, 0.0f);
	glVertex3f(0.10f, 0.75f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.1f, 0.755f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.1f, -0.755f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.15f, -0.75f, 0.0f);
	glVertex3f(0.10f, -0.75f, 0.0f);
	glEnd();

}

void DrawI2Word(void)
{
glLineWidth(5.0f);
glBegin(GL_LINES);
glColor3f(1.0f, 0.647f, 0.0f);
glVertex3f(0.30f, 0.75f, 0.0f);
glColor3f(0.0f, 1.0f, 0.0f);
glVertex3f(0.30f, -0.75f, 0.0f);
glEnd();
}

void DrawAWord(void)
{
	glLineWidth(5.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.647f, 0.0f);
	glVertex3f(0.40f, 0.75f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.25f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.647f, 0.0f);
	glVertex3f(0.40f, 0.75f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.55f, -0.75f, 0.0f);

	glEnd();

}

void DrawA1Flag(void)
{
		glLineWidth(5.0f);
		
		glBegin(GL_LINES);
		glColor3f(1.0f, 0.647f, 0.0f);
		glVertex3f(A1FlagXAxis, 0.05f, 0.0f);
		glVertex3f(0.47f, 0.05f, 0.0f);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(A2FlagXAxis, 0.0f, 0.0f);
		glVertex3f(0.475f, 0.0f, 0.0f);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(A3FlagXAxis, -0.05f, 0.0f);
		glVertex3f(0.48f, -0.05f, 0.0f);
		glEnd();

}
void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

