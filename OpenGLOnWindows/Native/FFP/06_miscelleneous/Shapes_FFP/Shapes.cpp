
#include <Windows.h>
#include <gl/GL.h>
#include <math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment (lib, "opengl32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

int bDone = false;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void Update(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MY_OPENGL_APP");

	//initialization
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = (CS_HREDRAW | CS_VREDRAW | CS_OWNDC);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; //name of call back function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	//register class
	RegisterClassEx(&wndclass);
	//hwnd = CreateWindow()
	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	int windowWidth = width / 2;
	int windowHeight = height / 2;

	//create window
	int xCordinate = windowWidth / 2;
	int yCordinate = windowHeight / 2;


	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OpenGL  : Shapes"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		xCordinate,
		yCordinate,
		windowWidth,
		windowHeight,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				Update();
				display();
			}
		}
	}

	uninitialize();
	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//	void display(void);
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;

		break;
		/*	case WM_PAINT:  //remove for double buffer
		display();
		break;*/
		/*case WM_ERASEBKGND:
		return (0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;

	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY),
					&mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	//resize(WIN_WIDTH, WIN_HEIGHT); //removed
}

void display(void)
{
	void DrawFirstShape();
	void DrawSecondShape();
	void DrawThirdShape();
	void DrawFourthShape();
	void DrawFifthShape();
	void DrawSixthShape();
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//	glFlush();  remove it for double buffer

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.75f, 0.5f, 0.0f);
	DrawFirstShape();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.05f, 0.50f, 0.0f);
	DrawSecondShape();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(+0.75f, 0.50f, 0.0f);
	DrawThirdShape();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.60f, -0.50f, 0.0f);
	DrawFourthShape();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, -0.50f, 0.0f);
	DrawFifthShape();


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.75f, -0.50f, 0.0f);
	DrawSixthShape();

	SwapBuffers(ghdc);
}

void Update(void)
{
}
void DrawFirstShape()
{
	glPointSize(2.0f);

	glBegin(GL_POINTS);
	glVertex3f(-0.15f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_POINTS);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_POINTS);
	glVertex3f(0.15f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_POINTS);
	glVertex3f(0.3f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_POINTS);
	glVertex3f(-0.15f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_POINTS);
	glVertex3f(0.0f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_POINTS);
	glVertex3f(0.15f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_POINTS);
	glVertex3f(0.3f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_POINTS);
	glVertex3f(-0.15f, 0.15f, 0.0f);
	glEnd();

	glBegin(GL_POINTS);
	glVertex3f(0.0f, 0.15f, 0.0f);
	glEnd();

	glBegin(GL_POINTS);
	glVertex3f(0.15f, 0.15f, 0.0f);
	glEnd();

	glBegin(GL_POINTS);
	glVertex3f(0.3f, 0.15f, 0.0f);
	glEnd();

	glBegin(GL_POINTS);
	glVertex3f(-0.15f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_POINTS);
	glVertex3f(0.0f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_POINTS);
	glVertex3f(0.15f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_POINTS);
	glVertex3f(0.3f, 0.3f, 0.0f);
	glEnd();

}

void DrawSecondShape()
{
	glLineWidth(2.0f);

	glBegin(GL_LINES);
	glVertex3f(-0.3f, -0.15f, 0.0f);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.15f, 0.3f, 0.0f);
	glVertex3f(-0.3f, 0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.0f, 0.3f, 0.0f);
	glVertex3f(-0.15f, 0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.15f, 0.3f, 0.0f);
	glVertex3f(-0.0f, 0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.15f, 0.15f, 0.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.0f, 0.15f, 0.0f);
	glVertex3f(-0.15f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.15f, 0.15f, 0.0f);
	glVertex3f(-0.0f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.15f, 0.0f, 0.0f);
	glVertex3f(-0.3f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.0f, 0.0f, 0.0f);
	glVertex3f(-0.15f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.15f, 0.0f, 0.0f);
	glVertex3f(-0.0f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glVertex3f(0.15f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.15f, -0.15f, 0.0f);
	glVertex3f(-0.15f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.0f, -0.15f, 0.0f);
	glVertex3f(-0.0f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glVertex3f(0.15f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.15f, 0.0f);
	glVertex3f(0.15f, 0.15f, 0.0f);
	glEnd();

}

void DrawThirdShape()
{
	glLineWidth(2.0f);
	
	glBegin(GL_LINES);
	glVertex3f(0.15f, -0.15f, 0.0f);
	glVertex3f(0.15f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, -0.15f, 0.0f);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, -0.15f, 0.0f);
	glVertex3f(0.15f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glVertex3f(0.15f, 0.3f, 0.0f);
	glEnd();


	glBegin(GL_LINES);
	glVertex3f(-0.15f, -0.15f, 0.0f);
	glVertex3f(-0.15f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.0f, -0.15f, 0.0f);
	glVertex3f(-0.0f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glVertex3f(0.15f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.15f, 0.0f);
	glVertex3f(0.15f, 0.15f, 0.0f);
	glEnd();
	
}

void DrawFourthShape()
{
	glLineWidth(2.0f);

	glBegin(GL_LINES);
	glVertex3f(0.15f, -0.15f, 0.0f);
	glVertex3f(0.15f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, -0.15f, 0.0f);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.15f, 0.3f, 0.0f);
	glVertex3f(-0.3f, 0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.0f, 0.3f, 0.0f);
	glVertex3f(-0.15f, 0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.15f, 0.3f, 0.0f);
	glVertex3f(-0.0f, 0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.15f, 0.15f, 0.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.0f, 0.15f, 0.0f);
	glVertex3f(-0.15f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.15f, 0.15f, 0.0f);
	glVertex3f(-0.0f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.15f, 0.0f, 0.0f);
	glVertex3f(-0.3f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.0f, 0.0f, 0.0f);
	glVertex3f(-0.15f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.15f, 0.0f, 0.0f);
	glVertex3f(-0.0f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, -0.15f, 0.0f);
	glVertex3f(0.15f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glVertex3f(0.15f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.15f, -0.15f, 0.0f);
	glVertex3f(-0.15f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.0f, -0.15f, 0.0f);
	glVertex3f(-0.0f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glVertex3f(0.15f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.15f, 0.0f);
	glVertex3f(0.15f, 0.15f, 0.0f);
	glEnd();

}

void DrawFifthShape()
{
	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glVertex3f(0.15f, -0.15f, 0.0f);
	glVertex3f(0.15f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, -0.15f, 0.0f);
	glVertex3f(0.15f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glVertex3f(0.15f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, -0.15f, 0.0f);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glVertex3f(0.15f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glVertex3f(0.15f, 0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glVertex3f(0.15f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glVertex3f(-0.15f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glVertex3f(-0.0f, -0.15f, 0.0f);
	glEnd();

}

void DrawSixthShape()
{
	//0.15,-0.3,  -0.15, 0.3
	glLineWidth(2.0f);

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.15f, -0.15f, 0.0f);
	glVertex3f(0.15f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.150f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.15f, 0.15f, 0.0f);
	glVertex3f(0.15f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.150f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.15f, 0.15f, 0.0f);
	glVertex3f(0.15f, 0.3f, 0.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);
	glVertex3f(0.0f, 0.150f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.15f, 0.15f, 0.0f);
	glVertex3f(-0.15f, 0.3f, 0.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);
	glVertex3f(0.0f, 0.150f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.15f, 0.15f, 0.0f);
	glVertex3f(-0.15f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.150f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.15f, -0.15f, 0.0f);
	glVertex3f(-0.15f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.150f, 0.0f);
	glEnd();


	//red
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.15f, 0.15f, 0.0f);
	glVertex3f(-0.15f, 0.3f, 0.0f);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glVertex3f(-0.3f, 0.15f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.15f, 0.15f, 0.0f);
	glVertex3f(-0.15f, 0.0f, 0.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glVertex3f(-0.3f, 0.15f, 0.0f);
	glEnd();


	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.15f, -0.15f, 0.0f);
	glVertex3f(-0.15f, 0.0f, 0.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glVertex3f(-0.3f, -0.15f, 0.0f);
	glEnd();

	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_LINES);
	glVertex3f(-0.15f, -0.15f, 0.0f);
	glVertex3f(-0.15f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.0f, -0.15f, 0.0f);
	glVertex3f(-0.0f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glVertex3f(0.15f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.15f, 0.0f);
	glVertex3f(0.15f, 0.15f, 0.0f);
	glEnd();

}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

