GLfloat material_ambient1[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
GLfloat material_diffuse1[] = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
GLfloat material_specular1[] = { 0.633f, 0.727811f, 0.633f , 1.0f };
GLfloat material_shinyness1 = { 0.6f*128 };

GLfloat material_ambient2[] = { 0.135f, 0.2225f, 0.1575f, 1.0f };
GLfloat material_diffuse2[] = { 0.54f, 0.89f, 0.63f, 1.0f };
GLfloat material_specular2[] = { 0.316228f, 0.316228f, 0.316228f , 1.0f };
GLfloat material_shinyness2 = { 0.1f * 128 };

GLfloat material_ambient3[] = { 0.05375f, 0.05f, 0.06625f, 1.0f };
GLfloat material_diffuse3[] = { 0.18275f, 0.17f, 0.22525f, 1.0f };
GLfloat material_specular3[] = { 0.332741f, 0.328634f, 0.346435f , 1.0f };
GLfloat material_shinyness3 = { 0.3f * 128 };

GLfloat material_ambient4[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
GLfloat material_diffuse4[] = { 1.0f, 0.829f, 0.829f, 1.0f };
GLfloat material_specular4[] = { 0.296648f, 0.296648f, 0.296648f , 1.0f };
GLfloat material_shinyness4 = { 0.088f * 128 };

GLfloat material_ambient5[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
GLfloat material_diffuse5[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
GLfloat material_specular5[] = { 0.727811f, 0.626959f, 0.626959f , 1.0f };
GLfloat material_shinyness5 = { 0.6f * 128 };

GLfloat material_ambient6[] = { 0.1, 0.18725f, 0.1745f, 1.0f };
GLfloat material_diffuse6[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
GLfloat material_specular6[] = { 0.297254f, 0.30829f, 0.306678f , 1.0f };
GLfloat material_shinyness6 = { 0.1f * 128 };

GLfloat material_ambient7[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
GLfloat material_diffuse7[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
GLfloat material_specular7[] = { 0.992157f, 0.941176f, 0.807843f , 1.0f };
GLfloat material_shinyness7 = { 0.2179872f * 128 };

GLfloat material_ambient8[] = { 0.2125f, 0.1275f, 0.054f, 1.0f };
GLfloat material_diffuse8[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
GLfloat material_specular8[] = { 0.393548f, 0.271906f, 0.166721f , 1.0f };
GLfloat material_shinyness8 = { 0.2f * 128 };

//3rd sphere 2nd column
GLfloat material_ambient9[] = { 0.25f, 0.25f, 0.25f, 1.0f };
GLfloat material_diffuse9[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat material_specular9[] = { 0.774597f, 0.774597f, 0.774597f , 1.0f };
GLfloat material_shinyness9 = { 0.6f * 128 };

//4rth sphere 2nd column
GLfloat material_ambient10[] = { 0.19125f, 0.0735f, 0.0225f, 1.0f };
GLfloat material_diffuse10[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
GLfloat material_specular10[] = { 0.256777f, 0.137622f, 0.086014f , 1.0f };
GLfloat material_shinyness10 = { 0.1f * 128 };

//5th Sphere 2nd column
GLfloat material_ambient11[] = { 0.24725f, 0.1995f, 0.0745f, 1.0f };
GLfloat material_diffuse11[] = { 0.75164f, 0.60648f, 0.22648f, 1.0f };
GLfloat material_specular11[] = { 0.628281f, 0.555802f, 0.366065f , 1.0f };
GLfloat material_shinyness11 = { 0.4f * 128 };

//6th Sphere 2nd column
GLfloat material_ambient12[] = { 0.19225f, 0.19225f, 0.19225f, 1.0f };
GLfloat material_diffuse12[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
GLfloat material_specular12[] = { 0.508273f, 0.508273f, 0.508273f , 1.0f };
GLfloat material_shinyness12 = { 0.4f * 128 };

//1st sphere 3rd column
GLfloat material_ambient13[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse13[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat material_specular13[] = { 0.5f, 0.5f, 0.5f , 1.0f };
GLfloat material_shinyness13 = { 0.25f * 128 };

//2nd sphere 3rd column
GLfloat material_ambient14[] = { 0.0f, 0.1f, 0.06f, 1.0f };
GLfloat material_diffuse14[] = { 0.0f, 0.50980392f, 0.50980392f, 1.0f };
GLfloat material_specular14[] = { 0.50196078f, 0.50196078f, 0.50196078f , 1.0f };
GLfloat material_shinyness14 = { 0.25f * 128 };

//3rd sphere in 3rd column
GLfloat material_ambient15[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse15[] = { 0.1f, 0.35f, 0.1f, 1.0f };
GLfloat material_specular15[] = { 0.45f, 0.55f, 0.45f , 1.0f };
GLfloat material_shinyness15 = { 0.25f * 128 };

//4rth sphere in 3rd column
GLfloat material_ambient16[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse16[] = { 0.5f, 0.0f, 0.0f, 1.0f };
GLfloat material_specular16[] = { 0.7f, 0.6f, 0.6f , 1.0f };
GLfloat material_shinyness16 = { 0.25f * 128 };

//5th sphere in 3rd column
GLfloat material_ambient17[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse17[] = { 0.55f, 0.55f, 0.55f, 1.0f };
GLfloat material_specular17[] = { 0.70f, 0.70f, 0.70f , 1.0f };
GLfloat material_shinyness17 = { 0.25f * 128 };

//6th sphere in 3rd column
GLfloat material_ambient18[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse18[] = { 0.5f, 0.5f, 0.0f, 1.0f };
GLfloat material_specular18[] = { 0.6f, 0.6f, 0.5f , 1.0f };
GLfloat material_shinyness18 = { 0.25f * 128 };

//1st sphere in 4rth column
GLfloat material_ambient19[] = { 0.02f, 0.02f, 0.02f, 1.0f };
GLfloat material_diffuse19[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat material_specular19[] = { 0.4f, 0.4f, 0.4f , 1.0f };
GLfloat material_shinyness19 = { 0.078125f * 128 };

//2nd sphere in 4rth column
GLfloat material_ambient20[] = { 0.0f, 0.05f, 0.05f, 1.0f };
GLfloat material_diffuse20[] = { 0.4f, 0.5f, 0.5f, 1.0f };
GLfloat material_specular20[] = { 0.04f, 0.7f, 0.7f , 1.0f };
GLfloat material_shinyness20 = { 0.078125f * 128 };

//3rd sphere in 4rth column
GLfloat material_ambient21[] = { 0.0f, 0.05f, 0.0f, 1.0f };
GLfloat material_diffuse21[] = { 0.4f, 0.5f, 0.4f, 1.0f };
GLfloat material_specular21[] = { 0.04f, 0.7f, 0.04f , 1.0f };
GLfloat material_shinyness21 = { 0.078125f * 128 };

//4rth sphere in 4rth column
GLfloat material_ambient22[] = { 0.05f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse22[] = { 0.5f, 0.4f, 0.4f, 1.0f };
GLfloat material_specular22[] = { 0.7f, 0.04f, 0.04f , 1.0f };
GLfloat material_shinyness22 = { 0.078125f * 128 };

//5th sphere in 4rth column
GLfloat material_ambient23[] = { 0.05f, 0.05f, 0.05f, 1.0f };
GLfloat material_diffuse23[] = { 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat material_specular23[] = { 0.7f, 0.7f, 0.7f , 1.0f };
GLfloat material_shinyness23 = { 0.078125f * 128 };

//6th sphere in 4rth column
GLfloat material_ambient24[] = { 0.05f, 0.05f, 0.0f, 1.0f };
GLfloat material_diffuse24[] = { 0.5f, 0.5f, 0.4f, 1.0f };
GLfloat material_specular24[] = { 0.7f, 0.7f, 0.04f , 1.0f };
GLfloat material_shinyness24 = { 0.078125f * 128 };

GLfloat* material_ambient[24] = { material_ambient1 , material_ambient2 ,material_ambient3 ,material_ambient4 ,material_ambient5 ,material_ambient6 ,
								material_ambient7 , material_ambient8 ,material_ambient9 ,material_ambient10 ,material_ambient11 ,material_ambient12 ,
								material_ambient13 , material_ambient14 ,material_ambient15 ,material_ambient16 ,material_ambient17 ,material_ambient18 ,
								material_ambient19 , material_ambient20 ,material_ambient21 ,material_ambient22 ,material_ambient23 ,material_ambient24 
								};
GLfloat* material_diffuse[24] = { material_diffuse1 , material_diffuse2 , material_diffuse3 , material_diffuse4 ,material_diffuse5 ,material_diffuse6,
									material_diffuse7 , material_diffuse8 , material_diffuse9 , material_diffuse10 ,material_diffuse11 ,material_diffuse12 ,
									material_diffuse13 , material_diffuse14 , material_diffuse15 , material_diffuse16 ,material_diffuse17 ,material_diffuse18 ,
									material_diffuse19 , material_diffuse20 , material_diffuse21 , material_diffuse22 ,material_diffuse23 ,material_diffuse24 };

GLfloat* material_specular[24] = { material_specular1 , material_specular2 ,material_specular3 , material_specular4 , material_specular5 ,material_specular6 , 
								material_specular7 , material_specular8 ,material_specular9 , material_specular10 , material_specular11 ,material_specular12 , 
							material_specular13 , material_specular14 ,material_specular15 , material_specular16 , material_specular17 ,material_specular18 , 
							material_specular19 , material_specular20 ,material_specular21 , material_specular22 , material_specular23 ,material_specular24 };

GLfloat material_shinyness[24] = { material_shinyness1 , material_shinyness2, material_shinyness3,material_shinyness4,material_shinyness5,material_shinyness6,
								material_shinyness7 , material_shinyness8, material_shinyness9,material_shinyness10,material_shinyness11,material_shinyness12,
								material_shinyness13 , material_shinyness14, material_shinyness15,material_shinyness16,material_shinyness17,material_shinyness18,
								material_shinyness19, material_shinyness20, material_shinyness21,material_shinyness22,material_shinyness23,material_shinyness24
							};

float xCoord[24] = { -6.5f , -6.5f , -6.5f , -6.5f , -6.5f ,-6.5f ,
					-2.5f, -2.5f, -2.5f, -2.5f, -2.5f, -2.5f,
					2.5f,  2.5f,  2.5f,  2.5f,  2.5f, 2.5f,
					6.5f, 6.5f, 6.5f, 6.5f, 6.5f, 6.5f};
float yCoord[24] = { 4.5f , 2.5f , 0.5f , -1.5f , -3.5f ,-5.5f ,
					 4.5f , 2.5f , 0.5f , -1.5f , -3.5f ,-5.5f ,
					 4.5f , 2.5f , 0.5f , -1.5f , -3.5f ,-5.5f ,
					 4.5f , 2.5f , 0.5f , -1.5f , -3.5f ,-5.5f  };
/*(-6.5f, 4.5f(-6.5f, 2.5f); (-6.5f, 0.5f(-6.5f, -1.5f(-6.5f, -3.5f); (-6.5f, -5.5f); (-2.5f, 4.5f); (-2.5f, 2.5f);
(-2.5f, 0.5f); (-2.5f, -1.5f); (-2.5f, -3.5f); (-2.5f, -5.5f); 2.5f, 4.5f,2.5f, 2.5f); 2.5f, 0.5f, 2.5f, -1.5f
2.5f, -3.5f,) 2.5f, -5.5f,) 6.5f, 4.5f)6.5f, 2.5f)(6.5f, 0.5f); 6.5f, -1.5f);
(6.5f, -3.5f)(6.5f, -5.5f);
*/
