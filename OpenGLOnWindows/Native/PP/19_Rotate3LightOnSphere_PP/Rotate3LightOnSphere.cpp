#include <windows.h>
#include <stdio.h> // for FILE I/O

#include <gl\glew.h>//use it before other GL.h

#include <gl/GL.h>

#include "vmath.h"
#include "Sphere.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	MALATI_ATTRIBUTE_VERTEX = 0,
	MALATI_ATTRIBUTE_COLOR,
	MALATI_ATTRIBUTE_NORMAL,
	MALATI_ATTRIBUTE_TEXTURE
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObjectPerVertex;
GLuint gVertexShaderObjectPerFragment;
GLuint gFragmentShaderObjectPerVertex;
GLuint gFragmentShaderObjectPerFragment;
GLuint gShaderProgramObjectPerVertex;
GLuint gShaderProgramObjectPerFragment;
GLuint gShaderProgramObject;

GLuint gNumElements;
GLuint gNumVertices;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint gModelMatrixUniform, gViewMatrixUniform, gProjectionMatrixUniform;
GLuint gLd0Uniform, gLa0Uniform, gLs0Uniform;
GLuint gLd1Uniform, gLa1Uniform, gLs1Uniform;
GLuint gLd2Uniform, gLa2Uniform, gLs2Uniform;
GLuint gLightPositionUniform0;
GLuint gLightPositionUniform1;
GLuint gLightPositionUniform2;
GLuint gKdUniform, gKaUniform, gKsUniform;
GLuint gMaterialShininessUniform;

GLuint gLKeyPressedUniform;
GLuint gFKeyPressedUniform;


mat4 gPerspectiveProjectionMatrix;

GLfloat gAngle = 0.0f;

//bool gbAnimate;
bool gbLight;
bool gbFrag;

GLfloat lightAmbient0[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightAmbient1[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightAmbient2[] = { 0.0f,0.0f,0.0f,1.0f };

GLfloat lightDiffuse0[] = { 1.0f,0.0f,0.0f,0.0f };//red
GLfloat lightDiffuse1[] = { 0.0f,1.0f,0.0f,0.0f };//green
GLfloat lightDiffuse2[] = { 0.0f,0.0f,1.0f,0.0f };//blue

GLfloat lightSpecular0[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lightSpecular1[] = { 0.0f,1.0f,0.0f,1.0f };
GLfloat lightSpecular2[] = { 0.0f,0.0f,1.0f,1.0f };

GLfloat lightPosition0[] = { 100.0f,100.0f,100.0f,0.0f };
GLfloat lightPosition1[] = { -100.0f,100.0f,100.0f,0.0f };
GLfloat lightPosition2[] = { 0.0f,0.0f,100.0f,0.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;

GLfloat angle_red = 0.0f;
GLfloat angle_green = 0.0f;
GLfloat angle_blue = 0.0f;

GLboolean bSphere = GL_FALSE;
GLfloat myRotateMatrix[4];
GLfloat myRotateMatrix1[4];
GLfloat myRotateMatrix2[4];
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void Update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	//code
	// create log file
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	//Parallel to glutInitWindowSize(), glutInitWindowPosition() and glutCreateWindow() all three together
	hwnd = CreateWindow(szClassName,
		TEXT("OpenGL Programmable Pipeline Window"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//initialize
	initialize();

	//Message Loop
	while (bDone == false) //Parallel to glutMainLoop();
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// rendring function
			display();
			Update();
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true) //Continuation to glutLeaveMainLoop();
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;
	static bool bIsFKeyPressed = false;

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0) //if 0, the window is active
			gbActiveWindow = true;
		else //if non-zero, the window is not active
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE: //Parallel to glutReshapeFunc();
		resize(LOWORD(lParam), HIWORD(lParam)); //Parallel to glutReshapeFunc(resize);
		break;
	case WM_KEYDOWN: //Parallel to glutKeyboardFunc();
		switch (wParam)
		{
		case VK_ESCAPE: //case 27
			/*if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true; //Parallel to glutLeaveMainLoop();*/
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x46: //for 'f' or 'F'
		case 0x66:
			if (bIsFKeyPressed == false)
			{
				fprintf(gpFile, "Frag apply\n");
				gbFrag = true;
				bIsFKeyPressed = true;
				gbLight = false;
				bIsLKeyPressed = false;
			}
			else
			{
				fprintf(gpFile, "Frag not apply\n");
				gbFrag = false;
				//gbLight = false;
				bIsFKeyPressed = false;
			}
			break;
		case 0x4C://L
		case 0x6C://l
		case 0x56://V
		case 0x76: //v
			if (bIsLKeyPressed == false)
			{
				gbLight = true;
				gbFrag = false;
				bIsLKeyPressed = true;
				bIsFKeyPressed = false;
			}
			else
			{
				gbLight = false;
				//gbFrag = false;
				bIsLKeyPressed = false;
			}
			break;
		case 0x51:
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:  //Parallel to glutMouseFunc();
		break;
	case WM_CLOSE: //Parallel to glutCloseFunc();
		uninitialize(); //Parallel to glutCloseFunc(uninitialize);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

//FUNCTION DEFINITIONS
void initialize(void)
{
	//function prototypes
	void uninitialize(void);
	void resize(int, int);
	void MyLinkProgram(GLuint iPgmObject);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	//Parallel to glutInitDisplayMode()
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	//choose a pixel format which best matches with that of 'pfd'
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//set the pixel format chosen above
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//create OpenGL rendering context
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//make the rendering context created above as current n the current hdc
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "%s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "Shading Lang Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//VERTEX SHADER
	gVertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertextShaderSourceCodePerVertex =
		"#version 400" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \

		"uniform int u_LKeyPressed;" \

		"uniform vec3 u_Ld0;" \
		"uniform vec3 u_Ld1;" \
		"uniform vec3 u_Ld2;" \
		"uniform vec3 u_La0;" \
		"uniform vec3 u_La1;" \
		"uniform vec3 u_La2;" \
		"uniform vec3 u_Ls0;" \
		"uniform vec3 u_Ls1;" \
		"uniform vec3 u_Ls2;" \
		"uniform vec4 u_light_position0;"\
		"uniform vec4 u_light_position1;"\
		"uniform vec4 u_light_position2;"\
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Ks;" \
		"uniform vec3 u_Kd;" \
		"uniform float u_material_shininess;" \

		"out vec3 phong_ads_color0;" \
		"out vec3 phong_ads_color1;" \
		"out vec3 phong_ads_color2;" \
		"void main(void)" \
		"{" \
		"if(u_LKeyPressed == 1)" \
		"{" \
		"vec4 eyeCoordinates =  u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 tnorm = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
		"vec3 light_direction  = normalize(vec3(u_light_position0) - eyeCoordinates.xyz);" \
		"vec3 ambient0 = u_La0 * u_Ka;"\
		"vec3 diffuse_light0 = u_Ld0 * u_Kd * max(dot(light_direction, tnorm), 0.0);" \
		"vec3 reflection_vector0 = reflect(-light_direction, tnorm);"\
		"vec3 viewer_vector0 = normalize(-eyeCoordinates.xyz);" \
		"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, viewer_vector0), 0.0),u_material_shininess);" \
		"phong_ads_color0=ambient0 + diffuse_light0 + specular0;" \

		"vec4 eyeCoordinates1 =  u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 tnorm1 = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
		"vec3 light_direction1  = normalize(vec3(u_light_position1) - eyeCoordinates1.xyz);" \
		"vec3 ambient1 = u_La1 * u_Ka;"\
		"vec3 diffuse_light1 = u_Ld1 * u_Kd * max(dot(light_direction1, tnorm1), 0.0);" \
		"vec3 reflection_vector1 = reflect(-light_direction1, tnorm1);"\
		"vec3 viewer_vector1 = normalize(-eyeCoordinates1.xyz);" \
		"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, viewer_vector1), 0.0),u_material_shininess);" \
		"phong_ads_color1=ambient1 + diffuse_light1 + specular1;" \

		"vec4 eyeCoordinates2 =  u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 tnorm2 = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
		"vec3 light_direction2  = normalize(vec3(u_light_position2) - eyeCoordinates2.xyz);" \
		"vec3 ambient2 = u_La2 * u_Ka;"\
		"vec3 diffuse_light2 = u_Ld2 * u_Kd * max(dot(light_direction2, tnorm2), 0.0);" \
		"vec3 reflection_vector2 = reflect(-light_direction2, tnorm2);"\
		"vec3 viewer_vector2 = normalize(-eyeCoordinates2.xyz);" \
		"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, viewer_vector2), 0.0),u_material_shininess);" \
		"phong_ads_color2=ambient2 + diffuse_light2 + specular2;" \
		"}" \
		"else"\
		"{"\
		"phong_ads_color0 = vec3(1.0, 1.0, 1.0);"\
		"phong_ads_color1 = vec3(1.0, 1.0, 1.0);"\
		"phong_ads_color2 = vec3(1.0, 1.0, 1.0);"\
		"}"\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObjectPerVertex, 1, (const GLchar **)&vertextShaderSourceCodePerVertex, NULL);

	//compile shader
	glCompileShader(gVertexShaderObjectPerVertex);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	///////////////////////////////////PerFragment Vertex Shader///////////////////////////////////////////////
	//VERTEX SHADER
	gVertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertextShaderSourceCodePerFragment =
		"#version 400" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position0;"\
		"uniform vec4 u_light_position1;"\
		"uniform vec4 u_light_position2;"\
		"uniform int u_FKeyPressed;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction0;" \
		"out vec3 light_direction1;" \
		"out vec3 light_direction2;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_FKeyPressed == 1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction0 = vec3(u_light_position0) - eye_coordinates.xyz;" \
		"light_direction1 = vec3(u_light_position1) - eye_coordinates.xyz;" \
		"light_direction2 = vec3(u_light_position2) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"}"\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObjectPerFragment, 1, (const GLchar **)&vertextShaderSourceCodePerFragment, NULL);

	//compile shader
	glCompileShader(gVertexShaderObjectPerFragment);

	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	//char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//FRAGMENT SHADER

	gFragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCodePerVertex =
		"#version 400" \
		"\n" \
		"in vec3 phong_ads_color0;" \
		"in vec3 phong_ads_color1;" \
		"in vec3 phong_ads_color2;" \
		"vec4 FragColor0;"
		"vec4 FragColor1;"
		"vec4 FragColor2;"
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor0 = vec4(phong_ads_color0, 1.0);" \
		"FragColor1 = vec4(phong_ads_color1, 1.0);" \
		"FragColor2 = vec4(phong_ads_color2, 1.0);" \
		"FragColor = FragColor0 + FragColor1 + FragColor2;" \
		"}";

	glShaderSource(gFragmentShaderObjectPerVertex, 1, (const GLchar **)&fragmentShaderSourceCodePerVertex, NULL);

	glCompileShader(gFragmentShaderObjectPerVertex);

	glGetShaderiv(gFragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//////////////////////////////Per Fragment FragShader

	gFragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCodePerFragment =
		"#version 400" \
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction0;" \
		"in vec3 light_direction1;" \
		"in vec3 light_direction2;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La0;" \
		"uniform vec3 u_La1;" \
		"uniform vec3 u_La2;" \
		"uniform vec3 u_Ld0;" \
		"uniform vec3 u_Ld1;" \
		"uniform vec3 u_Ld2;" \
		"uniform vec3 u_Ls0;" \
		"uniform vec3 u_Ls1;" \
		"uniform vec3 u_Ls2;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_FKeyPressed;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color0;" \
		"vec3 phong_ads_color1;" \
		"vec3 phong_ads_color2;" \
		"vec3 phong_ads_color;" \
		"if(u_FKeyPressed==1)" \
		"{" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction0=normalize(light_direction0);" \
		"vec3 ambient0 = u_La0 * u_Ka;" \
		"float tn_dot_ld0 = max(dot(normalized_transformed_normals, normalized_light_direction0),0.0);" \
		"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;" \
		"vec3 reflection_vector0 = reflect(-normalized_light_direction0, normalized_transformed_normals);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color0=ambient0 + diffuse0 + specular0;" \

	
		"vec3 normalized_light_direction1=normalize(light_direction1);" \
		"vec3 ambient1 = u_La1 * u_Ka;" \
		"float tn_dot_ld1 = max(dot(normalized_transformed_normals, normalized_light_direction1),0.0);" \
		"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;" \
		"vec3 reflection_vector1 = reflect(-normalized_light_direction1, normalized_transformed_normals);" \
		"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color1=ambient1 + diffuse1 + specular1;" \

		"vec3 normalized_light_direction2=normalize(light_direction2);" \
		"vec3 ambient2 = u_La2 * u_Ka;" \
		"float tn_dot_ld2 = max(dot(normalized_transformed_normals, normalized_light_direction2),0.0);" \
		"vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;" \
		"vec3 reflection_vector2 = reflect(-normalized_light_direction2, normalized_transformed_normals);" \
		"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color2=ambient2 + diffuse2 + specular2;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color0 = vec3(1.0, 1.0, 1.0);" \
		"phong_ads_color1 = vec3(1.0, 1.0, 1.0);" \
		"phong_ads_color2 = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"phong_ads_color = phong_ads_color0 + phong_ads_color1 + phong_ads_color2;" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";


	glShaderSource(gFragmentShaderObjectPerFragment, 1, (const GLchar **)&fragmentShaderSourceCodePerFragment, NULL);

	glCompileShader(gFragmentShaderObjectPerFragment);

	glGetShaderiv(gFragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Shader Program
	
	gShaderProgramObjectPerVertex = glCreateProgram();

	glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);

	glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	MyLinkProgram(gShaderProgramObjectPerVertex);
	/*gShaderProgramObjectPerFragment = glCreateProgram();

	glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);

	glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	gShaderProgramObject = gShaderProgramObjectPerVertex;

	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	
	gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");

	gLd0Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld0");
	gLd1Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld1");
	gLd2Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld2");

	gLa0Uniform = glGetUniformLocation(gShaderProgramObject, "u_La0");
	gLa1Uniform = glGetUniformLocation(gShaderProgramObject, "u_La1");
	gLa2Uniform = glGetUniformLocation(gShaderProgramObject, "u_La2");

	gLs0Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls0");
	gLs1Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls1");
	gLs2Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls2");

	gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	gKaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");

	gLightPositionUniform0 = glGetUniformLocation(gShaderProgramObject, "u_light_position0");
	gLightPositionUniform1 = glGetUniformLocation(gShaderProgramObject, "u_light_position1");
	gLightPositionUniform2 = glGetUniformLocation(gShaderProgramObject, "u_light_position2");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
	*/
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	//position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//normal
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);


	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	glBindVertexArray(0);


	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // blue

	gPerspectiveProjectionMatrix = mat4::identity();


	gbLight = false;
	gbFrag = false;
	// resize
	resize(WIN_WIDTH, WIN_HEIGHT);
}

void MyLinkProgram(GLuint iPgmObject)
{
	void uninitialize(void);
	gShaderProgramObject = glCreateProgram();

	//glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);

	//glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	gShaderProgramObject = iPgmObject;

	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;
	GLint iInfoLogLength = 0;
	
	char *szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");
	gFKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_FKeyPressed");

	gLd0Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld0");
	gLd1Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld1");
	gLd2Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld2");

	gLa0Uniform = glGetUniformLocation(gShaderProgramObject, "u_La0");
	gLa1Uniform = glGetUniformLocation(gShaderProgramObject, "u_La1");
	gLa2Uniform = glGetUniformLocation(gShaderProgramObject, "u_La2");

	gLs0Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls0");
	gLs1Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls1");
	gLs2Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls2");

	gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	gKaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");

	gLightPositionUniform0 = glGetUniformLocation(gShaderProgramObject, "u_light_position0");
	gLightPositionUniform1 = glGetUniformLocation(gShaderProgramObject, "u_light_position1");
	gLightPositionUniform2 = glGetUniformLocation(gShaderProgramObject, "u_light_position2");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");


}
void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	if (gbLight == true)
	{
		gShaderProgramObjectPerVertex = glCreateProgram();

		glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);

		glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

		MyLinkProgram(gShaderProgramObjectPerVertex);
		glUseProgram(gShaderProgramObjectPerVertex);
		glUniform1i(gLKeyPressedUniform, 1);

		glUniform3fv(gLd0Uniform, 1, lightDiffuse0);//white
		glUniform3fv(gLa0Uniform, 1, lightAmbient0);
		glUniform3fv(gLs0Uniform, 1, lightSpecular0);
		glUniform3fv(gLd1Uniform, 1, lightDiffuse1);//white
		glUniform3fv(gLa1Uniform, 1, lightAmbient1);
		glUniform3fv(gLs1Uniform, 1, lightSpecular1);
		glUniform3fv(gLd2Uniform, 1, lightDiffuse2);//white
		glUniform3fv(gLa2Uniform, 1, lightAmbient2);
		glUniform3fv(gLs2Uniform, 1, lightSpecular2);

		mat4 rotationMatrix = mat4::identity();

		rotationMatrix = vmath::rotate(angle_red, 1.0f, 1.0f, 1.0f);

		/*	myRotateMatrix[0] = rotationMatrix[0][0] * lightPosition0[0] + rotationMatrix[0][1] * lightPosition0[1] + rotationMatrix[0][2] * lightPosition0[2] ;
		myRotateMatrix[1] = rotationMatrix[1][0] * lightPosition0[0] + rotationMatrix[1][1] * lightPosition0[1] + rotationMatrix[1][2] * lightPosition0[2];
		myRotateMatrix[2] = rotationMatrix[2][0] * lightPosition0[0] + rotationMatrix[2][1] * lightPosition0[1] + rotationMatrix[2][2] * lightPosition0[2];
		myRotateMatrix[2] = rotationMatrix[2][0] * lightPosition0[0] + rotationMatrix[2][1] * lightPosition0[1] + rotationMatrix[2][2] * lightPosition0[2];
		*/

		myRotateMatrix[0] = 0.0f;
		myRotateMatrix[1] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix[2] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix[3] = 1.0f;
		glUniform4fv(gLightPositionUniform0, 1, myRotateMatrix);

		myRotateMatrix1[0] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix1[1] = 0.0f;
		myRotateMatrix1[2] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix1[3] = 1.0f;
		glUniform4fv(gLightPositionUniform1, 1, myRotateMatrix1);

		myRotateMatrix2[0] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix2[1] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix2[2] = -2.0f;
		myRotateMatrix2[3] = 1.0f;
		glUniform4fv(gLightPositionUniform2, 1, myRotateMatrix2);



		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

		//	float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };

	}
	else if (gbFrag == true)
	{
		fprintf(gpFile, "Use Program FRAG\n");
		gShaderProgramObjectPerFragment = glCreateProgram();

		glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);

		glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

		MyLinkProgram(gShaderProgramObjectPerFragment);
		glUseProgram(gShaderProgramObjectPerFragment);
		glUniform1i(gFKeyPressedUniform, 1);

		glUniform3fv(gLd0Uniform, 1, lightDiffuse0);//white
		glUniform3fv(gLa0Uniform, 1, lightAmbient0);
		glUniform3fv(gLs0Uniform, 1, lightSpecular0);
		glUniform3fv(gLd1Uniform, 1, lightDiffuse1);//white
		glUniform3fv(gLa1Uniform, 1, lightAmbient1);
		glUniform3fv(gLs1Uniform, 1, lightSpecular1);
		glUniform3fv(gLd2Uniform, 1, lightDiffuse2);//white
		glUniform3fv(gLa2Uniform, 1, lightAmbient2);
		glUniform3fv(gLs2Uniform, 1, lightSpecular2);

		mat4 rotationMatrix = mat4::identity();

		rotationMatrix = vmath::rotate(angle_red, 1.0f, 1.0f, 1.0f);

		/*	myRotateMatrix[0] = rotationMatrix[0][0] * lightPosition0[0] + rotationMatrix[0][1] * lightPosition0[1] + rotationMatrix[0][2] * lightPosition0[2] ;
		myRotateMatrix[1] = rotationMatrix[1][0] * lightPosition0[0] + rotationMatrix[1][1] * lightPosition0[1] + rotationMatrix[1][2] * lightPosition0[2];
		myRotateMatrix[2] = rotationMatrix[2][0] * lightPosition0[0] + rotationMatrix[2][1] * lightPosition0[1] + rotationMatrix[2][2] * lightPosition0[2];
		myRotateMatrix[2] = rotationMatrix[2][0] * lightPosition0[0] + rotationMatrix[2][1] * lightPosition0[1] + rotationMatrix[2][2] * lightPosition0[2];
		*/

		myRotateMatrix[0] = 0.0f;
		myRotateMatrix[1] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix[2] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix[3] = 1.0f;
		glUniform4fv(gLightPositionUniform0, 1, myRotateMatrix);

		myRotateMatrix1[0] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix1[1] = 0.0f;
		myRotateMatrix1[2] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix1[3] = 1.0f;
		glUniform4fv(gLightPositionUniform1, 1, myRotateMatrix1);

		myRotateMatrix2[0] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix2[1] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix2[2] = -2.0f;
		myRotateMatrix2[3] = 1.0f;
		glUniform4fv(gLightPositionUniform2, 1, myRotateMatrix2);



		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
		glUniform1i(gFKeyPressedUniform, 0);
	}

	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();


	modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);

	/*rotationMatrix = vmath::rotate(angle_red, angle_red, angle_red);

	modelMatrix = modelMatrix * rotationMatrix; //imp*/

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);
	//glDrawIndexArrays, glDrawElements
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void Update(void)
{
	angle_red = angle_red + 0.01f;
	if (angle_red >= 360.0f)
		angle_red = 0.0f;

	angle_green = angle_green + 0.1f;
	if (angle_green >= 360.0f)
		angle_green = 0.0f;

	angle_blue = angle_blue + 0.1f;
	if (angle_blue >= 360.0f)
		angle_blue = 0.0f;
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	/*
	if (width <= height)
	{
	gOrthographicProjectionMatrix = ortho(-100.0f, 100.0f, (-100.0f *(height / width)), (100.0f * (height / width)), -100.0f, 100.0f);
	}
	else
	{
	gOrthographicProjectionMatrix = ortho((-100.0f *(width / height)), (100.0f * (width / height)), -100.0f, 100.0f, -100.0f, 100.0f);
	}*/
	gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height,
		0.1f, 100.0f); //perspective(3rd change)
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}
	// destroy element vbo
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}
	glDetachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
	glDetachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	glDetachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
	glDetachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	glDeleteShader(gVertexShaderObjectPerVertex);
	gVertexShaderObjectPerVertex = 0;
	glDeleteShader(gFragmentShaderObjectPerVertex);
	gFragmentShaderObjectPerVertex = 0;

	glDeleteShader(gVertexShaderObjectPerFragment);
	gVertexShaderObjectPerFragment = 0;
	glDeleteShader(gFragmentShaderObjectPerFragment);
	gFragmentShaderObjectPerFragment = 0;


	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	//Delete the device context
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}


