#include<GL/freeglut.h>
#include <math.h>

static int shoulder = 0;
static int elbow = 0;

#define WIN_WIDTH 600
#define WIN_HEIGHT 600

bool gbFullScreen = false;

int main(int argc, char** argv)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void keyBoard(unsigned char, int, int);
	void reSize(int x, int y);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	//	int width = GetSystemMetrics(SM_CXSCREEN);
	//int height = GetSystemMetrics(SM_CYSCREEN);

	int width = glutGet(GLUT_SCREEN_WIDTH);
	int height = glutGet(GLUT_SCREEN_HEIGHT);

	int windowWidth = width / 2;
	int windowHeight = height / 2;

	int xCordinate = windowWidth / 2;
	int yCordinate = windowHeight / 2;

	//glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
	//glutInitWindowPosition(0, 0);
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition(xCordinate, yCordinate);


	glutCreateWindow("OpenGL GLUT :Robotic Arm");
	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(reSize);
	glutKeyboardFunc(keyBoard);
	glutCloseFunc(uninitialize);

	glutMainLoop();
	//return ((int)msg.wParam);
}

void initialize(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	//glClearDepth(1.0f);
//	glEnable(GL_DEPTH_TEST);
//	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_FLAT);
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void display(void)
{
	void DrawRoboticArm(void);
	glClear(GL_COLOR_BUFFER_BIT );

	DrawRoboticArm();
	glutSwapBuffers();
}
void DrawRoboticArm(void)
{

	glPushMatrix(); //save current context

	glTranslatef(-1.0f, 0.0f, 0.0f); // -1 x axis
	glRotatef((GLfloat)shoulder, 0.0f, 0.0f, 1.0f);
	glTranslatef(1.0f, 0.0f, 0.0f);
	glPushMatrix();
	glScalef(2.0f, 0.4f, 1.0f);
	glutWireCube(1.0f);
	glPopMatrix();

	glTranslatef(1.0f, 0.0f, 0.0f);
	glRotatef((GLfloat)elbow, 0.0f, 0.0f, 1.0f);
	glTranslatef(-1.0f, 0.0f, 0.0f);
	glPushMatrix();
	glScalef(2.0f, 0.4f, 1.0f);
	glutWireCube(1.0f);
	glPopMatrix();

	glPopMatrix();

}
void reSize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(65.0, (GLfloat)width / (GLfloat)height, 0.1f, 20.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -5.0f);
	
}

void uninitialize(void)
{
}

void keyBoard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 's':
		shoulder = (shoulder + 5) % 360;
		glutPostRedisplay();
		break;
	case 'S':
		shoulder = (shoulder - 5) % 360;
		glutPostRedisplay();
		break;
	case 'e':
		elbow = (elbow + 5) % 360;
		glutPostRedisplay();
		break;
	case 'E':
		elbow = (elbow - 5) % 360;
		glutPostRedisplay();
		break;

	case 'f':
	case 'F':
		if (gbFullScreen == false)
		{
			glutFullScreen();
			gbFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			gbFullScreen = false;
		}
		break;
	default:
		break;
	}
}
