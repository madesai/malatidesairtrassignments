#include<GL/freeglut.h>
#include <math.h>

static int year = 0;
static int day = 0;

#define WIN_WIDTH 600
#define WIN_HEIGHT 600

bool gbFullScreen = false;

int main(int argc, char** argv)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void keyBoard(unsigned char, int, int);
	void reSize(int x, int y);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

	//	int width = GetSystemMetrics(SM_CXSCREEN);
	//int height = GetSystemMetrics(SM_CYSCREEN);

	int width = glutGet(GLUT_SCREEN_WIDTH);
	int height = glutGet(GLUT_SCREEN_HEIGHT);

	int windowWidth = width / 2;
	int windowHeight = height / 2;

	int xCordinate = windowWidth / 2;
	int yCordinate = windowHeight / 2;

	//glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
	//glutInitWindowPosition(0, 0);
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition(xCordinate, yCordinate);


	glutCreateWindow("OpenGL GLUT :Planet and Sun");
	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(reSize);
	glutKeyboardFunc(keyBoard);
	glutCloseFunc(uninitialize);

	glutMainLoop();
	//return ((int)msg.wParam);
}

void initialize(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_FLAT);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void display(void)
{
	void DrawPlanetSunSystem(void);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	
	DrawPlanetSunSystem();
	glutSwapBuffers();
}
void DrawPlanetSunSystem(void)
{
	
	glPushMatrix(); //save current context
	
	glColor3f(1.0f, 1.0f, 0.0f);
	glutWireSphere(1.0, 30, 30);
	
	glRotatef((GLfloat)year, 0.0f, 1.0f, 0.0f);
	glTranslatef(2.0f, 0.0f, 0.0f);
	glRotatef((GLfloat)day, 0.0f, 1.0f, 0.0f);
	
	glColor3f(0.0f, 0.0f, 1.0f);
	glutWireSphere(0.2, 15, 15);
	
	glPopMatrix();

}
void reSize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(60.0, (GLfloat)width / (GLfloat)height, 0.1f, 20.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
}

void uninitialize(void)
{
}

void keyBoard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'd':
		day = (day + 10) % 360;
		glutPostRedisplay();
		break;
	case 'D':
		day = (day - 10) % 360;
		glutPostRedisplay();
		break;
	case 'y':
		year = (year + 5) % 360;
		glutPostRedisplay();
		break;
	case 'Y':
		year = (year - 5) % 360;
		glutPostRedisplay();
		break;

	case 'f':
	case 'F':
		if (gbFullScreen == false)
		{
			glutFullScreen();
			gbFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			gbFullScreen = false;
		}
		break;
	default:
		break;
	}
}
