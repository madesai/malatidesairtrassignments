package com.astromedicomp.window_ComboVNFNew;

import android.content.Context; //for drawing context related
import android.opengl.GLSurfaceView;//surface view
import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

//for vbo 
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

//(nio -> non Blocking IO)

import android.opengl.Matrix; //for Matrix math

public class GLESView extends GLSurfaceView implements
GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;
	private GestureDetector gestureDetector;
	
	private int vertexShaderObjectPerVertex;
	private int vertexShaderObjectPerFragment;
	private int fragmentShaderObjectPerVertex;
	private int fragmentShaderObjectPerFragment;
	private int shaderProgramObjectPerVertex;
	private int shaderProgramObjectPerFragment;
	private int shaderProgramObject;
	
	
    private int numElements;
    private int numVertices;

	private int[] vao_sphere = new int[1];
	private int[] vbo_sphere_normal = new int[1];
	private int[] vbo_sphere_position = new int[1];
	private int[] vbo_sphere_element = new int[1];
	
    private float light_ambient[] = {0.0f,0.0f,0.0f,1.0f};
    private float light_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
    private float light_specular[] = {1.0f,1.0f,1.0f,1.0f};
    private float light_position[] = { 100.0f,100.0f,100.0f,1.0f };
    
    private float material_ambient[] = {0.0f,0.0f,0.0f,1.0f};
    private float material_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
    private float material_specular[] = {1.0f,1.0f,1.0f,1.0f};
    private float material_shininess = 50.0f;

	private int modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
	private int laUniform, ldUniform, lsUniform, lightPositionUniform; 
	private int kaUniform, kdUniform, ksUniform, materialShininessUniform;
	
	private int singleTapUniform;
	private int doubleTapUniform;
	
	private float perspectiveProjectionMatrix[] = new float[16];
	//4*4 matrix
	
	private float gAngleCube = 0.0f;

	private int singleTap;
	private int doubleTap;
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context =drawingContext;
		
		setEGLContextClientVersion(3);
		
		setRenderer(this);
		
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	
		gestureDetector = new GestureDetector(context, this, null, false);
		
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	//overriden method of GLSurfaceView.Renderer (init Code)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("MPD: OpenGL-ES Version = "+glesVersion);
			
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("MPD: GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	//overriden method GLSurfaceView.Renderer(change Size)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

    // overriden method of GLSurfaceView.Renderer ( Rendering Code )
    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

	//imp
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventaction = e.getAction();
		if(!gestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return (true);	
	}
	
	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		doubleTap++;
		if(doubleTap > 1)
			doubleTap = 0;
		return (true);
	}
	 
 // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
   // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
		singleTap++;
		if(singleTap > 2)
			singleTap = 1;
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
	
    public boolean onDown(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
		uninitialize();
		System.exit(0);
		return (true);
	}
	
	// abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }
	
	private void initialize(GL10 gl)
	{
		//vertex shader per vertex 
		vertexShaderObjectPerVertex = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCodePerVertex = String.format
		(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;" +
			"in vec3 vNormal;" +
			"uniform mat4 u_model_matrix;" +
			"uniform mat4 u_view_matrix;" +
			"uniform mat4 u_projection_matrix;" +
			"uniform mediump int u_double_tap;" +
			"uniform vec3 u_La;" +
			"uniform vec3 u_Ld;" +
			"uniform vec3 u_Ls;" +
			"uniform vec3 u_Ka;" +
			"uniform vec3 u_Ks;"+
			"uniform vec3 u_Kd;" +
			"uniform vec4 u_light_position;" +
			"uniform float u_material_shininess;"+
			"out vec3 phong_ads_color;" +
			"void main(void)" +
			"{"+
			"if(u_double_tap ==1)" +
			"{"+
			"vec4 eyeCoordinates = u_view_matrix * u_model_matrix  * vPosition;"+
			"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);"+
			"vec3 light_direction  = normalize(vec3(u_light_position) - eyeCoordinates.xyz);" +
			"vec3 ambient = u_La * u_Ka;" +
			"vec3 diffuse = u_Ld * u_Kd * max(dot(light_direction ,transformed_normals), 0.0);" +
			"vec3 reflection_vector = reflect(-light_direction, transformed_normals);"+
			"vec3 viewer_vector = normalize(-eyeCoordinates.xyz);"+
			"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"+
			"phong_ads_color=ambient + diffuse + specular;"+
			"}"+
			"else"+
			"{"+
			"phong_ads_color = vec3(1.0, 1.0, 1.0);"+
			"}"+
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"}"

		);
		
		GLES32.glShaderSource(vertexShaderObjectPerVertex, vertexShaderSourceCodePerVertex);
		
		//compile shader & check for errors
		GLES32.glCompileShader(vertexShaderObjectPerVertex);
		int [] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObjectPerVertex, GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,0);
		
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObjectPerVertex,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObjectPerVertex);
				System.out.println("MPD: Vertex Shader Compilation Log per vertex= " +szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		//vertex shader per fragment
		vertexShaderObjectPerFragment = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCodePerFragment = String.format
		(
		 "#version 320 es"+
         "\n"+
         "in vec4 vPosition;"+
         "in vec3 vNormal;"+
         "uniform mat4 u_model_matrix;"+
         "uniform mat4 u_view_matrix;"+
         "uniform mat4 u_projection_matrix;"+
         "uniform mediump int u_double_tap;"+
         "uniform vec4 u_light_position;"+
         "out vec3 transformed_normals;"+
         "out vec3 light_direction;"+
         "out vec3 viewer_vector;"+
         "void main(void)"+
         "{"+
         "if (u_double_tap == 1)"+
         "{"+
         "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
         "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;"+
         "light_direction = vec3(u_light_position) - eye_coordinates.xyz;"+
         "viewer_vector = -eye_coordinates.xyz;"+
         "}"+
         "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
         "}"
		);
		
		GLES32.glShaderSource(vertexShaderObjectPerFragment, vertexShaderSourceCodePerFragment);
		
		//compile shader & check for errors
		GLES32.glCompileShader(vertexShaderObjectPerFragment);
		iShaderCompiledStatus = new int[1];
		iInfoLogLength = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObjectPerFragment, GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,0);
		
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObjectPerFragment,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObjectPerFragment);
				System.out.println("MPD: Vertex Shader Compilation Log per fragment = " +szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		
		//fragmentShader per vertex
		fragmentShaderObjectPerVertex = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		final String fragmentShaderSourceCodePerVertex = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"in vec3 phong_ads_color;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"FragColor = vec4(phong_ads_color, 1.0);"+
			"}"
		);
	
		
		GLES32.glShaderSource(fragmentShaderObjectPerVertex, fragmentShaderSourceCodePerVertex);
		
		//compile shader & check for errors
		GLES32.glCompileShader(fragmentShaderObjectPerVertex);
		iShaderCompiledStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObjectPerVertex, GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,0);
		
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObjectPerVertex,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObjectPerVertex);
				System.out.println("MPD: Fragment Shader Compilation Log Per Vertex= " +szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
			
		//fragment shader per fragment
		fragmentShaderObjectPerFragment = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		final String fragmentShaderSourceCodePerFragment = String.format
		(
			 "#version 320 es"+
         "\n"+
         "precision highp float;"+
         "in vec3 transformed_normals;"+
         "in vec3 light_direction;"+
         "in vec3 viewer_vector;"+
         "out vec4 FragColor;"+
         "uniform vec3 u_La;"+
         "uniform vec3 u_Ld;"+
         "uniform vec3 u_Ls;"+
         "uniform vec3 u_Ka;"+
         "uniform vec3 u_Kd;"+
         "uniform vec3 u_Ks;"+
         "uniform float u_material_shininess;"+
         "uniform int u_double_tap;"+
         "void main(void)"+
         "{"+
         "vec3 phong_ads_color;"+
         "if(u_double_tap==1)"+
         "{"+
         "vec3 normalized_transformed_normals=normalize(transformed_normals);"+
         "vec3 normalized_light_direction=normalize(light_direction);"+
         "vec3 normalized_viewer_vector=normalize(viewer_vector);"+
         "vec3 ambient = u_La * u_Ka;"+
         "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"+
         "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
         "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
         "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
         "phong_ads_color=ambient + diffuse + specular;"+
         "}"+
         "else"+
         "{"+
         "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
         "}"+
         "FragColor = vec4(phong_ads_color, 1.0);"+
         "}"


		);
	
		
		GLES32.glShaderSource(fragmentShaderObjectPerFragment, fragmentShaderSourceCodePerFragment);
		
		//compile shader & check for errors
		GLES32.glCompileShader(fragmentShaderObjectPerFragment);
		iShaderCompiledStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObjectPerFragment, GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,0);
		
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObjectPerFragment,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObjectPerFragment);
				System.out.println("MPD: Fragment Shader Compilation Log Per Fragment= " +szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		
		
		
		/*shaderProgramObjectPerFragment = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);
		GLES32.glAttachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);
		
		MyLinkProgram(shaderProgramObjectPerFragment);*/
		
		shaderProgramObjectPerVertex = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
		GLES32.glAttachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);
		
		MyLinkProgram(shaderProgramObjectPerVertex);
		/*
		shaderProgramObject = GLES32.glCreateProgram();
		
	//	shaderProgramObject = shaderProgramObjectPerFragment;
		
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.MPD_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.MPD_ATTRIBUTE_NORMAL, "vNormal");
		
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, 
		iShaderProgramLinkStatus, 0);
		
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("MPD: Shader Program Link Log Per Vertex= " + szInfoLog);
					uninitialize();
					System.exit(0);
				}
		}
		*/
		
		
		
		
		Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();


		GLES32.glGenVertexArrays(1, vao_sphere, 0);
		GLES32.glBindVertexArray(vao_sphere[0]);
		
		GLES32.glGenBuffers(1, vbo_sphere_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_position[0]);
		
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length  * 4);
		
		byteBuffer.order(ByteOrder.nativeOrder());
		
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(sphere_vertices);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							sphere_vertices.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_VERTEX,
										3, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_VERTEX);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		//color
		//GLES32.glVertexAttrib3f(GLESMacros.MPD_ATTRIBUTE_COLOR, 0.39f, 0.58f, 0.9294f); //cornflower Blue 
	
		//GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_VERTEX);
		GLES32.glGenBuffers(1, vbo_sphere_normal, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_normal[0]);
		
		byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);

		byteBuffer.order(ByteOrder.nativeOrder());
		
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(sphere_normals);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							sphere_normals.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_NORMAL,
										3, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_NORMAL);

		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);

		
		
		// enable depth testing
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        // depth test to do
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        // We will always cull back faces for better performance
        GLES32.glEnable(GLES32.GL_CULL_FACE);
        
        // Set the background color
        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
		singleTap = 0;
		doubleTap =0;
		//set projectionMatrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}
	
	private void resize(int width, int height)
    {
        if(height == 0)
			height = 1;
        GLES32.glViewport(0, 0, width, height);
       /* if(width <= height)
				Matrix.orthoM(orthographicProjectionMatrix, 0, -100.0f, 100.0f,
				(100.0f *(height/width)), (100.0f *(height / width)), -100.0f, 100.0f);
				
		else
				Matrix.orthoM(orthographicProjectionMatrix, 0, (-100.0f *(width / height)),
				(100.0f * (width/height)), -100.0f, 100.0f, -100.0f, 100.0f);
		*/		
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, 
					(float)width/(float)height, 0.1f, 100.0f);
	}
	
	public void MyLinkProgram(int iPgmObject)
	{
		System.out.println("MPD: Shader Program ");
		shaderProgramObject = GLES32.glCreateProgram();
		
		shaderProgramObject = iPgmObject;
		
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.MPD_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.MPD_ATTRIBUTE_NORMAL, "vNormal");
		
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, 
		iShaderProgramLinkStatus, 0);
		
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("MPD: Shader Program Link Log Per Vertex= " + szInfoLog);
					uninitialize();
					System.exit(0);
				}
		}
		
		modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
		viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
		
		projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
		
		doubleTapUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_double_tap");
		singleTapUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_single_tap");
		laUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La");
		ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld");
		lsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls");
		kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ka");
		kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
		ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ks");
		lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");
		
        materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_material_shininess");
	}
	private void update()
	{
		
		/*gAngleCube = gAngleCube + 1.5f;
		if (gAngleCube >= 360.0f)
			gAngleCube = 0.0f;

		gAnglePyramid = gAnglePyramid + 1.5f;
		if (gAnglePyramid >= 360.0f)
			gAnglePyramid = 0.0f;*/
	}

	public void display()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		if((singleTap %2 )==0)
		{
			System.out.println("MPD: In Per Vertex " );
			/*shaderProgramObject = shaderProgramObjectPerVertex;*/
			shaderProgramObjectPerVertex = GLES32.glCreateProgram();
		
			GLES32.glAttachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
			GLES32.glAttachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);
		
			MyLinkProgram(shaderProgramObjectPerVertex);
			
			GLES32.glUseProgram(shaderProgramObjectPerVertex);
			if(doubleTap == 1)
			{
				System.out.println("MPD: In Per Vertex In Double Tap" );
			GLES32.glUniform1i(doubleTapUniform, 1);
			
			GLES32.glUniform3fv(ldUniform, 1, light_ambient, 0);
			GLES32.glUniform3fv(ldUniform, 1, light_diffuse, 0);
            GLES32.glUniform3fv(lsUniform, 1, light_specular, 0);

			GLES32.glUniform4fv(lightPositionUniform, 1, light_position, 0);
			
			GLES32.glUniform3fv(kaUniform, 1, material_ambient, 0);
            GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
            GLES32.glUniform3fv(ksUniform, 1, material_specular, 0);
            GLES32.glUniform1f(materialShininessUniform, material_shininess);

			}			
			else
			{
				System.out.println("MPD: In Per Vertex In DoubleTap = 0 " );
			GLES32.glUniform1i(doubleTapUniform, 0);
			//GLES32.glUniform1i(singleTapUniform, 0);
			}
		}
		else
		{
			System.out.println("MPD: In Per Fragment " );
			//shaderProgramObject = shaderProgramObjectPerFragment;
			//LinkProgram(shaderProgramObjectPerFragment);
			
			shaderProgramObjectPerFragment = GLES32.glCreateProgram();
		
			GLES32.glAttachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);
			GLES32.glAttachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);
		
			MyLinkProgram(shaderProgramObjectPerFragment);
			GLES32.glUseProgram(shaderProgramObjectPerFragment);
		
			if(doubleTap == 1)
			{
				System.out.println("MPD: In Per Fragment in Double Tap" );
			GLES32.glUniform1i(doubleTapUniform, 1);
			
			GLES32.glUniform3fv(ldUniform, 1, light_ambient, 0);
			GLES32.glUniform3fv(ldUniform, 1, light_diffuse, 0);
            GLES32.glUniform3fv(lsUniform, 1, light_specular, 0);

			GLES32.glUniform4fv(lightPositionUniform, 1, light_position, 0);
			
			GLES32.glUniform3fv(kaUniform, 1, material_ambient, 0);
            GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
            GLES32.glUniform3fv(ksUniform, 1, material_specular, 0);
            GLES32.glUniform1f(materialShininessUniform, material_shininess);

			}			
			else
			{
				System.out.println("MPD: In Per Fragment In Double Tap = 0" );
			GLES32.glUniform1i(doubleTapUniform, 0);
			//GLES32.glUniform1i(singleTapUniform, 0);
			}
			
		}
		
		
		
		float modelMatrix[] = new float[16];
		float viewMatrix[] = new float[16];
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix,0);	
		
		
		Matrix.translateM(modelMatrix, 0, 0.0f,0.0f,-1.5f);//perspective change
		
		
		//multiply modelView and Projection matrix -> modelViewProjection matrix
		//Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
		
		//TriangleBlock
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]);
		
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//unbind vao
		GLES32.glBindVertexArray(0);
		
		GLES32.glUseProgram(0);
	
		
		//flush
		requestRender();
	
	}
	
	 void uninitialize()
    {
        // code
        // destroy vao
        
        
		if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
		
		

        // destroy vao
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
		if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
		if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }
        if(shaderProgramObject != 0)
        {
            if(vertexShaderObjectPerVertex != 0)
            {
                // detach vertex shader from shader program object
                GLES32.glDetachShader(shaderProgramObject, vertexShaderObjectPerVertex);
                // delete vertex shader object
                GLES32.glDeleteShader(vertexShaderObjectPerVertex);
                vertexShaderObjectPerVertex = 0;
            }
            
            if(fragmentShaderObjectPerVertex != 0)
            {
                // detach fragment  shader from shader program object
                GLES32.glDetachShader(shaderProgramObject, fragmentShaderObjectPerVertex);
                // delete fragment shader object
                GLES32.glDeleteShader(fragmentShaderObjectPerVertex);
                fragmentShaderObjectPerVertex = 0;
            }
        }

        // delete shader program object
        if(shaderProgramObject != 0)
        {
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }
}
