package com.astromedicomp.win_landscape;

import android.app.Activity;
import android.os.Bundle;

import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;

import android.widget.TextView;
import android.graphics.Color;
import android.view.Gravity;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	
	    //make fullscreen
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
	
	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

	this.getWindow().getDecorView().setBackgroundColor(Color.rgb(0,0,0));

	TextView myTextView = new TextView(this);

	myTextView.setText("Hello World !!!");
	myTextView.setTextSize(60);
	myTextView.setTextColor(Color.GREEN);
	myTextView.setGravity(Gravity.CENTER);

	setContentView(myTextView);

    }
}
