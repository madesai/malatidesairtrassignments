package com.astromedicomp.win_app_Landscape;

import android.app.Activity;
import android.os.Bundle;

import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;

import android.widget.TextView;
import android.graphics.Color;
import android.view.Gravity;

public class MainActivity extends Activity {

	private MyView myView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

	requestWindowFeature(Window.FEATURE_NO_TITLE);

	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			WindowManager.LayoutParams.FLAG_FULLSCREEN);

	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

	getWindow().getDecorView().setBackgroundColor(Color.rgb(0,0,0));

	myView = new MyView(this);

        setContentView(myView);
	
    }
}
