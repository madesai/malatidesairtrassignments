package com.astromedicomp.window_Rotate3LightOnSphere;

import android.content.Context; //for drawing context related
import android.opengl.GLSurfaceView;//surface view
import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

//for vbo 
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

//(nio -> non Blocking IO)

import android.opengl.Matrix; //for Matrix math
import java.lang.Math;

//import java.math;
public class GLESView extends GLSurfaceView implements
GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
    private int numElements;
    private int numVertices;

	private int[] vao_sphere = new int[1];
	private int[] vbo_sphere_normal = new int[1];
	private int[] vbo_sphere_position = new int[1];
	private int[] vbo_sphere_element = new int[1];
	
    /*private float light_ambient[] = {0.0f,0.0f,0.0f,1.0f};
    private float light_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
    private float light_specular[] = {1.0f,1.0f,1.0f,1.0f};
    private float light_position[] = { 100.0f,100.0f,100.0f,1.0f };*/
	
	private float lightAmbient0[] = { 0.0f,0.0f,0.0f,1.0f };
	private float lightAmbient1[] = { 0.0f,0.0f,0.0f,1.0f };
	private float lightAmbient2[] = { 0.0f,0.0f,0.0f,1.0f };

	private float lightDiffuse0[] = { 1.0f,0.0f,0.0f,0.0f };//red
	private float lightDiffuse1[] = { 0.0f,1.0f,0.0f,0.0f };//green
	private float lightDiffuse2[] = { 0.0f,0.0f,1.0f,0.0f };//blue

	private float lightSpecular0[] = { 1.0f,0.0f,0.0f,1.0f };
	private float lightSpecular1[] = { 0.0f,1.0f,0.0f,1.0f };
	private float lightSpecular2[] = { 0.0f,0.0f,1.0f,1.0f };

	private float lightPosition0[] = { 100.0f,100.0f,100.0f,0.0f };
	private float lightPosition1[] = { -100.0f,100.0f,100.0f,0.0f };
	private float lightPosition2[] = { 0.0f,0.0f,100.0f,0.0f };


    
    private float material_ambient[] = {0.0f,0.0f,0.0f,1.0f};
    private float material_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
    private float material_specular[] = {1.0f,1.0f,1.0f,1.0f};
    private float material_shininess = 50.0f;

	private int modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
	private int la0Uniform, ld0Uniform, ls0Uniform, lightPositionUniform0; 
	private int la1Uniform, ld1Uniform, ls1Uniform, lightPositionUniform1;
	private int la2Uniform, ld2Uniform, ls2Uniform, lightPositionUniform2;
	private int kaUniform, kdUniform, ksUniform, materialShininessUniform;
	
	private int doubleTapUniform;
	private float M_PI = 3.141f;//3.14159265
	private float perspectiveProjectionMatrix[] = new float[16];
	//4*4 matrix
	
	private float angle_red = 0.0f;

	
	private int doubleTap;
	private boolean gbLight;
	
	private float myRotateMatrix0[] = new float[4];
	private float myRotateMatrix1[] = new float[4];
	private float myRotateMatrix2[] = new float[4];
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context =drawingContext;
		
		setEGLContextClientVersion(3);
		
		setRenderer(this);
		
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	
		gestureDetector = new GestureDetector(context, this, null, false);
		
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	//overriden method of GLSurfaceView.Renderer (init Code)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("MPD: OpenGL-ES Version = "+glesVersion);
			
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("MPD: GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	//overriden method GLSurfaceView.Renderer(change Size)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

    // overriden method of GLSurfaceView.Renderer ( Rendering Code )
    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

	//imp
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventaction = e.getAction();
		if(!gestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return (true);	
	}
	
	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		doubleTap++;
		if(doubleTap > 1)
			doubleTap = 0;
		return (true);
	}
	 
 // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
   // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
		
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
	
    public boolean onDown(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
		uninitialize();
		System.exit(0);
		return (true);
	}
	
	// abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }
	
	private void initialize(GL10 gl)
	{
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;" +
			"in vec3 vNormal;" +
			"uniform mat4 u_model_matrix;" +
			"uniform mat4 u_view_matrix;" +
			"uniform mat4 u_projection_matrix;" +
			"uniform mediump int u_double_tap;" +
			"uniform vec3 u_La0;" +
			"uniform vec3 u_Ld0;" +
			"uniform vec3 u_Ls0;" +
			"uniform vec3 u_La1;" +
			"uniform vec3 u_Ld1;" +
			"uniform vec3 u_Ls1;" +
			"uniform vec3 u_La2;" +
			"uniform vec3 u_Ld2;" +
			"uniform vec3 u_Ls2;" +
			"uniform vec3 u_Ka;" +
			"uniform vec3 u_Ks;"+
			"uniform vec3 u_Kd;" +
			"uniform vec4 u_light_position0;" +
			"uniform vec4 u_light_position1;" +
			"uniform vec4 u_light_position2;" +
			"uniform float u_material_shininess;"+
			"out vec3 phong_ads_color0;" +
			"out vec3 phong_ads_color1;" +
			"out vec3 phong_ads_color2;" +
			"void main(void)" +
			"{"+
			"if(u_double_tap ==1)" +
			"{"+
			"vec4 eyeCoordinates0 = u_view_matrix * u_model_matrix  * vPosition;"+
			"vec3 transformed_normals0 = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);"+
			"vec3 light_direction0  = normalize(vec3(u_light_position0) - eyeCoordinates0.xyz);" +
			"vec3 ambient0 = u_La0 * u_Ka;" +
			"vec3 diffuse0 = u_Ld0 * u_Kd * max(dot(light_direction0 ,transformed_normals0), 0.0);" +
			"vec3 reflection_vector0 = reflect(-light_direction0, transformed_normals0);"+
			"vec3 viewer_vector0 = normalize(-eyeCoordinates0.xyz);"+
			"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, viewer_vector0), 0.0), u_material_shininess);"+
			"phong_ads_color0=ambient0 + diffuse0 + specular0;"+
			
			"vec4 eyeCoordinates1 = u_view_matrix * u_model_matrix  * vPosition;"+
			"vec3 transformed_normals1 = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);"+
			"vec3 light_direction1  = normalize(vec3(u_light_position1) - eyeCoordinates1.xyz);" +
			"vec3 ambient1 = u_La1 * u_Ka;" +
			"vec3 diffuse1 = u_Ld1 * u_Kd * max(dot(light_direction1 ,transformed_normals1), 0.0);" +
			"vec3 reflection_vector1 = reflect(-light_direction1, transformed_normals1);"+
			"vec3 viewer_vector1 = normalize(-eyeCoordinates1.xyz);"+
			"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, viewer_vector1), 0.0), u_material_shininess);"+
			"phong_ads_color1=ambient1 + diffuse1 + specular1;"+
			
			"vec4 eyeCoordinates2 = u_view_matrix * u_model_matrix  * vPosition;"+
			"vec3 transformed_normals2 = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);"+
			"vec3 light_direction2  = normalize(vec3(u_light_position2) - eyeCoordinates2.xyz);" +
			"vec3 ambient2 = u_La2 * u_Ka;" +
			"vec3 diffuse2 = u_Ld2 * u_Kd * max(dot(light_direction2 ,transformed_normals2), 0.0);" +
			"vec3 reflection_vector2 = reflect(-light_direction2, transformed_normals2);"+
			"vec3 viewer_vector2 = normalize(-eyeCoordinates2.xyz);"+
			"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, viewer_vector2), 0.0), u_material_shininess);"+
			"phong_ads_color2=ambient2 + diffuse2 + specular2;"+
			"}"+
			"else"+
			"{"+
			"phong_ads_color0 = vec3(1.0, 1.0, 1.0);"+
			"phong_ads_color1 = vec3(1.0, 1.0, 1.0);"+
			"phong_ads_color2 = vec3(1.0, 1.0, 1.0);"+
			"}"+
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"}"
		);
		
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		//compile shader & check for errors
		GLES32.glCompileShader(vertexShaderObject);
		int [] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,0);
		
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("MPD: Vertex Shader Compilation Log = " +szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		//fragmentShaderObject
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"in vec3 phong_ads_color0;"+
			"in vec3 phong_ads_color1;"+
			"in vec3 phong_ads_color2;"+
			"vec4 FragColor0;" +
			"vec4 FragColor1;" +
			"vec4 FragColor2;" +
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"FragColor0 = vec4(phong_ads_color0, 1.0);" +
			"FragColor1 = vec4(phong_ads_color1, 1.0);" +
			"FragColor2 = vec4(phong_ads_color2, 1.0);" +
			"FragColor = FragColor0 + FragColor1 + FragColor2;"+
			"}"
		);
		
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		
		//compile shader & check for errors
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,0);
		
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("MPD: Fragment Shader Compilation Log = " +szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		shaderProgramObject = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.MPD_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.MPD_ATTRIBUTE_NORMAL, "vNormal");
		
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, 
		iShaderProgramLinkStatus, 0);
		
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("MPD: Shader Program Link Log = " + szInfoLog);
					uninitialize();
					System.exit(0);
				}
		}
		
		modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
		viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
		
		projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
		
		doubleTapUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_double_tap");
		
		la0Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La0");
		ld0Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld0");
		ls0Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls0");
		
		la1Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La1");
		ld1Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld1");
		ls1Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls1");
		
		la2Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La2");
		ld2Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld2");
		ls2Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls2");
		
		kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ka");
		kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
		ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ks");
		
		lightPositionUniform0 = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position0");
		lightPositionUniform1 = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position1");
		lightPositionUniform2 = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position2");
		
        materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_material_shininess");
		
		//Square
		/*float cubeVertices[] = new float[] 
		{ 
			//top
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		//bottom
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		//front
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		//back
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		//left
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		//right
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f
		
		};
		for(int i = 0; i<72; i++)
		{
			if(cubeVertices[i] < 0.0f)
				cubeVertices[i] = cubeVertices[i] + 0.25f;
			else if(cubeVertices[i] > 0.0f)
				cubeVertices[i] = cubeVertices[i] - 0.25f;
			else
				cubeVertices[i] = cubeVertices[i];
		}
		
		float cubeNormals[] = new float[]
		{

			0.0f, 1.0f, 0.0f, //top
			0.0f, 1.0f, 0.0f, //bottom
			0.0f, 1.0f, 0.0f, //front
			0.0f, 1.0f, 0.0f, //back

			0.0f, -1.0f, 0.0f, //bottom
			0.0f, -1.0f, 0.0f, //front
			0.0f, -1.0f, 0.0f, //back
			1.0f, -1.0f, 0.0f, //right

			0.0f, 0.0f, 1.0f, //front
			0.0f, 0.0f, 1.0f, //back
			0.0f, 0.0f, 1.0f, //right
			0.0f, 0.0f, 1.0f, //left

			0.0f, 0.0f, -1.0f, //back
			0.0f, 0.0f, -1.0f, //right
			0.0f, 0.0f, -1.0f, //left
			0.0f, 0.0f, -1.0f, //top

			-1.0f, 0.0f, 0.0f, //right
			-1.0f, 0.0f, 0.0f, //left
			-1.0f, 0.0f, 0.0f, //top
			-1.0f, 0.0f, 0.0f, //bottom

			1.0f, 0.0f, 0.0f, //left
			1.0f, 0.0f, 0.0f, //top
			1.0f, 0.0f, 0.0f, //bottom
			1.0f, 0.0f, 0.0f //front

		};*/
		
		Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();


		GLES32.glGenVertexArrays(1, vao_sphere, 0);
		GLES32.glBindVertexArray(vao_sphere[0]);
		
		GLES32.glGenBuffers(1, vbo_sphere_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_position[0]);
		
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length  * 4);
		
		byteBuffer.order(ByteOrder.nativeOrder());
		
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(sphere_vertices);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							sphere_vertices.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_VERTEX,
										3, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_VERTEX);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		//color
		//GLES32.glVertexAttrib3f(GLESMacros.MPD_ATTRIBUTE_COLOR, 0.39f, 0.58f, 0.9294f); //cornflower Blue 
	
		//GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_VERTEX);
		GLES32.glGenBuffers(1, vbo_sphere_normal, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_normal[0]);
		
		byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);

		byteBuffer.order(ByteOrder.nativeOrder());
		
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(sphere_normals);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							sphere_normals.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_NORMAL,
										3, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_NORMAL);

		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);

		
		
		// enable depth testing
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        // depth test to do
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        // We will always cull back faces for better performance
        GLES32.glEnable(GLES32.GL_CULL_FACE);
        
        // Set the background color
        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
		
		doubleTap =0;
		//set projectionMatrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}
	
	private void resize(int width, int height)
    {
        if(height == 0)
			height = 1;
        GLES32.glViewport(0, 0, width, height);
       /* if(width <= height)
				Matrix.orthoM(orthographicProjectionMatrix, 0, -100.0f, 100.0f,
				(100.0f *(height/width)), (100.0f *(height / width)), -100.0f, 100.0f);
				
		else
				Matrix.orthoM(orthographicProjectionMatrix, 0, (-100.0f *(width / height)),
				(100.0f * (width/height)), -100.0f, 100.0f, -100.0f, 100.0f);
		*/		
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, 
					(float)width/(float)height, 0.1f, 100.0f);
	}
	
	
	private void update()
	{
		
		/*gAngleCube = gAngleCube + 1.5f;
		if (gAngleCube >= 360.0f)
			gAngleCube = 0.0f;

		gAnglePyramid = gAnglePyramid + 1.5f;
		if (gAnglePyramid >= 360.0f)
			gAnglePyramid = 0.0f;*/
		
		angle_red = angle_red + 0.01f;
		if (angle_red >= 360.0f)
			angle_red = 0.0f;
	}

	public void display()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject);
		if(doubleTap ==1)
		{
			GLES32.glUniform1i(doubleTapUniform, 1);
			
			GLES32.glUniform3fv(la0Uniform, 1, lightAmbient0, 0);
			GLES32.glUniform3fv(ld0Uniform, 1, lightDiffuse0, 0);
            GLES32.glUniform3fv(ls0Uniform, 1, lightSpecular0, 0);
			
			GLES32.glUniform3fv(la1Uniform, 1, lightAmbient1, 0);
			GLES32.glUniform3fv(ld1Uniform, 1, lightDiffuse1, 0);
            GLES32.glUniform3fv(ls1Uniform, 1, lightSpecular1, 0);

			GLES32.glUniform3fv(la2Uniform, 1, lightAmbient2, 0);
			GLES32.glUniform3fv(ld2Uniform, 1, lightDiffuse2, 0);
            GLES32.glUniform3fv(ls2Uniform, 1, lightSpecular2, 0);
			
			myRotateMatrix0[0] = 0.0f;
			myRotateMatrix0[1] = 100.0f * (float)Math.cos(2 * M_PI * angle_red);
			myRotateMatrix0[2] = 100f * (float)Math.sin(2 * M_PI * angle_red);
			myRotateMatrix0[3] = 1.0f;
			GLES32.glUniform4fv(lightPositionUniform0, 1, myRotateMatrix0, 0);

			myRotateMatrix1[0] = 100.0f * (float)Math.cos(2 * M_PI * angle_red);
			myRotateMatrix1[1] = 0.0f;
			myRotateMatrix1[2] = 100f * (float)Math.sin(2 * M_PI * angle_red);
			myRotateMatrix1[3] = 1.0f;
			GLES32.glUniform4fv(lightPositionUniform1, 1, myRotateMatrix1, 0);
		
			myRotateMatrix2[0] = 100f * (float)Math.sin(2 * M_PI * angle_red);
			myRotateMatrix2[1] = 100.0f * (float)Math.cos(2 * M_PI * angle_red);
			myRotateMatrix2[2] = -2.0f;
			myRotateMatrix2[3] = 1.0f;
			GLES32.glUniform4fv(lightPositionUniform2, 1, myRotateMatrix2, 0);
			//GLES32.glUniform4fv(lightPositionUniform, 1, light_position, 0);
			
			GLES32.glUniform3fv(kaUniform, 1, material_ambient, 0);
            GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
            GLES32.glUniform3fv(ksUniform, 1, material_specular, 0);
            GLES32.glUniform1f(materialShininessUniform, material_shininess);
			
			

		}
		else
		{
			GLES32.glUniform1i(doubleTapUniform, 0);
		}
		
		float modelMatrix[] = new float[16];
		float viewMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];

		


		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix,0);	
		Matrix.setIdentityM(rotationMatrix, 0);
		
		//rotationMatrix = vmath::rotate(angle_red, 1.0f, 1.0f, 1.0f);
		//Matrix.rotateM(rotationMatrix, 0, angle_red, 1.0f, 1.0f, 1.0f);
		Matrix.translateM(modelMatrix, 0, 0.0f,0.0f,-1.5f);//perspective change
		
		//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);
		//multiply modelView and Projection matrix -> modelViewProjection matrix
		//Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
		
		//TriangleBlock
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]);
		
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//unbind vao
		GLES32.glBindVertexArray(0);
		
		GLES32.glUseProgram(0);
	
		update();
		//flush
		requestRender();
	
	}
	
	 void uninitialize()
    {
        // code
        // destroy vao
        
        
		if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
		
		

        // destroy vao
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
		if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
		if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }
        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0)
            {
                // detach vertex shader from shader program object
                GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
                // delete vertex shader object
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }
            
            if(fragmentShaderObject != 0)
            {
                // detach fragment  shader from shader program object
                GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
                // delete fragment shader object
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        // delete shader program object
        if(shaderProgramObject != 0)
        {
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }
}
