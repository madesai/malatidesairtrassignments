package com.astromedicomp.window_PerFragLight;

public class GLESMacros
{
    // index
    public static final int MPD_ATTRIBUTE_VERTEX=0;
    public static final int MPD_ATTRIBUTE_COLOR=1;
    public static final int MPD_ATTRIBUTE_NORMAL=2;
    public static final int MPD_ATTRIBUTE_TEXTURE0=3;
}
