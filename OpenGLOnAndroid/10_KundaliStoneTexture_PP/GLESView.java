package com.astromedicomp.window_Texture01_PP;

import android.content.Context; //for drawing context related
import android.opengl.GLSurfaceView;//surface view
import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

//for vbo 
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
//(nio -> non Blocking IO)

import android.opengl.Matrix; //for Matrix math

import android.graphics.BitmapFactory; //texture
import android.graphics.Bitmap; // for PNG image
import android.opengl.GLUtils; // for texImage2D()

public class GLESView extends GLSurfaceView implements
GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao_pyramid = new int[1];
	private int[] vao_cube = new int[1];
	private int[] vbo_pyramid_position = new int[1];
	private int[] vbo_pyramid_texture = new int[1];
	private int[] vbo_cube_texture = new int[1];
	private int[] vbo_cube_position = new int[1];

	private int mvpUniform;
	private int texture0_sampler_uniform;
		
	private float perspectiveProjectionMatrix[] = new float[16];
	//4*4 matrix
	
	private int[] texture_kundali = new int[1];
	private int[] texture_stone = new int[1];
	
	private float gAnglePyramid = 0.0f;
	private float gAngleCube = 0.0f;

	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context =drawingContext;
		
		setEGLContextClientVersion(3);
		
		setRenderer(this);
		
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	
		gestureDetector = new GestureDetector(context, this, null, false);
		
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	//overriden method of GLSurfaceView.Renderer (init Code)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("MPD: OpenGL-ES Version = "+glesVersion);
			
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("MPD: GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	//overriden method GLSurfaceView.Renderer(change Size)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

    // overriden method of GLSurfaceView.Renderer ( Rendering Code )
    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

	//imp
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventaction = e.getAction();
		if(!gestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return (true);	
	}
	
	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return (true);
	}
	 
 // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
   // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
		uninitialize();
		System.exit(0);
		return (true);
	}
	
	// abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }
	
	private void initialize(GL10 gl)
	{
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;" +
			"in vec2 vTexture0_Coord;"+
			"out vec2 out_texture0_coord;"+
			"uniform mat4 u_mvp_matrix;" +
			"void main(void)" +
			"{"+
			"gl_Position = u_mvp_matrix * vPosition;" +
			"out_texture0_coord = vTexture0_Coord;" +
			"}"
		);
		
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		//compile shader & check for errors
		GLES32.glCompileShader(vertexShaderObject);
		int [] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,0);
		
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("MPD: Vertex Shader Compilation Log = " +szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		//fragmentShaderObject
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"in vec2 out_texture0_coord;" +
			"uniform highp sampler2D u_texture0_sampler;"+
			"out vec4 FragColor;" +
			"void main(void)" +
			"{"+
			"FragColor = texture(u_texture0_sampler, out_texture0_coord);" +
			"}"
		);
		
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		
		//compile shader & check for errors
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,0);
		
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("MPD: Fragment Shader Compilation Log = " +szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		shaderProgramObject = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.MPD_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.MPD_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
		
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, 
		iShaderProgramLinkStatus, 0);
		
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("MPD: Shader Program Link Log = " + szInfoLog);
					uninitialize();
					System.exit(0);
				}
		}
	
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
		texture0_sampler_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_texture0_sampler");
		
		texture_stone[0] = loadGLTexture(R.raw.stone);
		texture_kundali[0] = loadGLTexture(R.raw.vijay_kundali_horz_inverted);
		
		
		final float pyramidVertices[] = new float[]
		{
			//front face
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		//right face
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		//back face
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		//Left face
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f
		};
		
		final float pyramidTextcoords[] = new float[]
		{
			//front
			0.5f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f,
			
			//right
			0.5f, 1.0f,
			1.0f, 0.0f,
			0.0f, 0.0f,
			
			//back
			0.5f, 1.0f,
			1.0f, 0.0f,
			0.0f, 0.0f,
			
			//left
			0.5f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f,
		};
	
		GLES32.glGenVertexArrays(1, vao_pyramid, 0);
		GLES32.glBindVertexArray(vao_pyramid[0]);
		
		//position
		GLES32.glGenBuffers(1, vbo_pyramid_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_pyramid_position[0]);
		
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(pyramidVertices.length * 4);
		
		byteBuffer.order(ByteOrder.nativeOrder());
		
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(pyramidVertices);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							pyramidVertices.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_VERTEX,
										3, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_VERTEX);

		//texture vbo
		GLES32.glGenBuffers(1, vbo_pyramid_texture, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_pyramid_texture[0]);
		
		byteBuffer = ByteBuffer.allocateDirect(pyramidTextcoords.length * 4);

		byteBuffer.order(ByteOrder.nativeOrder());
		
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(pyramidTextcoords);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							pyramidTextcoords.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_TEXTURE0,
										2, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_TEXTURE0);

		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);
		
		//Cube
		final float cubeVertices[] = new float[] 
		{ 
			//top
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		//bottom
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		//front
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		//back
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		//left
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		//right
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		};
		
		final float cubeTexcoords[] = new float[]
		{
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
			
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
			
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
			
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
			
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
			
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
			
		};
		
		GLES32.glGenVertexArrays(1, vao_cube, 0);
		GLES32.glBindVertexArray(vao_cube[0]);
		
		GLES32.glGenBuffers(1, vbo_cube_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_position[0]);
		
		byteBuffer = ByteBuffer.allocateDirect(cubeVertices.length * 4);
		
		byteBuffer.order(ByteOrder.nativeOrder());
		
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(cubeVertices);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							cubeVertices.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_VERTEX,
										3, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_VERTEX);
		
		//texture
		GLES32.glGenBuffers(1, vbo_cube_texture, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_texture[0]);
		
		byteBuffer = ByteBuffer.allocateDirect(cubeTexcoords.length * 4);

		byteBuffer.order(ByteOrder.nativeOrder());
		
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(cubeTexcoords);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							cubeTexcoords.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_TEXTURE0,
										2, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_TEXTURE0);

		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);
		
		// enable depth testing
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        // depth test to do
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        // We will always cull back faces for better performance
        GLES32.glEnable(GLES32.GL_CULL_FACE);
        
        // Set the background color
        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
		//set projectionMatrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}
	
	private int loadGLTexture(int imageFileResourceID)
	{
		BitmapFactory.Options options = new BitmapFactory.Options(); //iner class
		options.inScaled = false;
		
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), imageFileResourceID, options);
		
		int[] texture = new int[1];
		
		GLES32.glGenTextures(1, texture, 0);
		
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);
		
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
	
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
		
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
		
		return (texture[0]);
	}
	
	private void resize(int width, int height)
    {
        if(height == 0)
			height = 1;
        GLES32.glViewport(0, 0, width, height);
       /* if(width <= height)
				Matrix.orthoM(orthographicProjectionMatrix, 0, -100.0f, 100.0f,
				(100.0f *(height/width)), (100.0f *(height / width)), -100.0f, 100.0f);
				
		else
				Matrix.orthoM(orthographicProjectionMatrix, 0, (-100.0f *(width / height)),
				(100.0f * (width/height)), -100.0f, 100.0f, -100.0f, 100.0f);
		*/		
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, 
					(float)width/(float)height, 0.1f, 100.0f);
	}
	
	
	private void update()
	{
		
		gAngleCube = gAngleCube + 1.5f;
		if (gAngleCube >= 360.0f)
			gAngleCube = 0.0f;

		gAnglePyramid = gAnglePyramid + 1.5f;
		if (gAnglePyramid >= 360.0f)
			gAnglePyramid = 0.0f;
	}

	public void display()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject);
		
		float modelViewMatrix[] = new float[16];
		float modelViewProjectionMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];
		float scaleMatrix[] =new float[16];
		
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(modelViewMatrix, 0, -1.5f,0.0f,-5.0f);//perspective change
		
		Matrix.rotateM(rotationMatrix, 0, gAnglePyramid, 0.0f, 1.0f, 0.0f);
		Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, rotationMatrix, 0);
		
		//multiply modelView and Projection matrix -> modelViewProjection matrix
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
		
		//TriangleBlock
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
	
		GLES32.glBindVertexArray(vao_pyramid[0]);
			
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_stone[0]);
		
		GLES32.glUniform1i(texture0_sampler_uniform, 0);
		
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0 ,12);
		
		GLES32.glBindVertexArray(0);
		
		//Cube code
		
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.scaleM(scaleMatrix, 0, 0.75f, 0.75f, 0.75f);
		Matrix.translateM(modelViewMatrix, 0, 1.5f,0.0f,-5.0f);//perspective change
		
	//	Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 1.0f, 1.0f);
		Matrix.rotateM(rotationMatrix, 0, gAngleCube, gAngleCube, gAngleCube, gAngleCube);
		Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, scaleMatrix, 0);
		Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, rotationMatrix, 0);
		
		//multiply modelView and Projection matrix -> modelViewProjection matrix
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
		
		//TriangleBlock
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
		
		GLES32.glBindVertexArray(vao_cube[0]);

		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_kundali[0]);
		
		GLES32.glUniform1i(texture0_sampler_uniform, 0);
		
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0 ,4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4 ,4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8 ,4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12 ,4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16 ,4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20 ,4);
		
		//unbind vao
		GLES32.glBindVertexArray(0);
		
		GLES32.glUseProgram(0);
	
		update();
		
		//flush
		requestRender();
	
	}
	
	 void uninitialize()
    {
        // code
        // destroy vao
        if(vao_pyramid[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_pyramid, 0);
            vao_pyramid[0]=0;
        }
        
		if(vao_cube[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_cube, 0);
            vao_cube[0]=0;
        }
		
		 // destroy vao
        if(vbo_pyramid_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_pyramid_position, 0);
            vbo_pyramid_position[0]=0;
        }
		
		if(vbo_pyramid_texture[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_pyramid_texture, 0);
            vbo_pyramid_texture[0]=0;
        }

        // destroy vao
        if(vbo_cube_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_cube_position, 0);
            vbo_cube_position[0]=0;
        }
		if(vbo_cube_texture[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_cube_texture, 0);
            vbo_cube_texture[0]=0;
        }
        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0)
            {
                // detach vertex shader from shader program object
                GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
                // delete vertex shader object
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }
            
            if(fragmentShaderObject != 0)
            {
                // detach fragment  shader from shader program object
                GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
                // delete fragment shader object
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        // delete shader program object
        if(shaderProgramObject != 0)
        {
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
		
		if(texture_stone[0] != 0)
		{
			GLES32.glDeleteTextures(1, texture_stone,0);
			texture_stone[0] = 0;
		}
		
		if(texture_kundali[0] != 0)
		{
			GLES32.glDeleteTextures(1, texture_kundali,0);
			texture_kundali[0] = 0;
		}
    }
}
