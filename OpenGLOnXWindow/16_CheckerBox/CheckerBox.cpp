#include<iostream>
#include<stdio.h>
#include<stdlib.h>//exit
#include<memory.h>//memset

#include<X11/Xlib.h>
#include<X11/Xutil.h>//for XVisualInfo structure
#include<X11/XKBlib.h>//Keyboard related functions
#include<X11/keysym.h>

//OpenGL Change
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>//bridging api

#define checkImageWidth 64
#define checkImageHeight 64

using namespace std;

bool bFullScreen = false;

Display *gpMyDisplay = NULL;
XVisualInfo *gpMyXVisualInfo = NULL;
Colormap gMyColormap;
Window gMyWindow;

int giMyWindowWidth = 800;
int giMyWindowHeight = 600;

GLXContext gGLXMyContext; //HGLRC ghrc; in windows

GLuint Texture_CheckerBox;
GLubyte checkImage[checkImageHeight][checkImageWidth][4];

int main(void)
{
	//function prototype
	void CreateWindow(void);
	void uninitialize(void);
	void ToggleFullScreen(void);
	//added for OpenGL
	void initialize(void);
	void resize(int, int);
	void display(void);
	void update(void);

	//variable declaration
	int windowWidth = giMyWindowWidth;
	int windowHeight = giMyWindowHeight;

	bool bDone = false; //For OpenGL
	
	CreateWindow();
	
	initialize(); //For OpenGL

	XEvent myEvent;
	KeySym myKeysym;

	while(bDone == false)
	{
		while(XPending(gpMyDisplay)) // it is blocking call 
	//its phylosophy with PeekMsg is parallel but functionality wise different
//PeekMsg : NonBlocking API
		{
			XNextEvent(gpMyDisplay, &myEvent);
			//1:Connection to Xserver
			//2:return Match Event associated structure
			switch(myEvent.type)
			{
				case MapNotify: //WM_CREATE	
		 		break;
				case KeyPress:
				myKeysym = XkbKeycodeToKeysym(gpMyDisplay, myEvent.xkey.keycode, 0, 0); 
		//1:ConnectionTo XServer
		//2:KeyOf Interest
		//3:Group Of Interest
		//4:Shift LevelOf Interest
		
				switch(myKeysym)
				{	
					case XK_Escape:
						bDone = true; //For OpenGL
						break;
					case XK_F:
					case XK_f:
					if(bFullScreen == false)
					{
						ToggleFullScreen();
						bFullScreen = true;
					}
					else
					{
						ToggleFullScreen();
						bFullScreen = false;
					}
					break;
					default:
						break;
				}
				break;
				case ButtonPress:
				switch(myEvent.xbutton.button)
				{
					case 1://L
						break;
					case 2://M
						break;
					case 3://R
						break;
					default:
						break;
				}
				break;
				case MotionNotify://mouse
				break;
				case ConfigureNotify://WM_SIZE
				windowWidth = myEvent.xconfigure.width;
				windowHeight = myEvent.xconfigure.height;
				resize(windowWidth, windowHeight); //For OpenGL
				break;
				case Expose://WM_PAINT
				break;
				case DestroyNotify://WM_DESTROY
				break;
				case 33://Window Cross Button
				bDone = true;
				break;
				default:
				break;
			}
		}
//		update();
		display(); //OpenGL single buffer
	}
	return 0;
}

void CreateWindow(void)
{
	//function prototype
	void uninitialize(void);

	static int framebufferAttribs[] = {
			GLX_RGBA,
			GLX_DOUBLEBUFFER,  //Double Buffer
			GLX_RED_SIZE, 8,
			GLX_GREEN_SIZE, 8,
			GLX_BLUE_SIZE, 8,
			GLX_ALPHA_SIZE, 8,
			GLX_DEPTH_SIZE, 24,
		/*	GLX_X_RENDERABLE, True,
			GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
			GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
			GLX_RENDER_TYPE, GLX_RGBA_BIT,*/
			None
	};//OpenGL Single Buffer

	//13 Steps
	gpMyDisplay = XOpenDisplay(NULL);//Set some value to Parameter when u want to do some Remote Connection : NULL: Default Display pointer(127.0.0.1)
	if(gpMyDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display..\nExiting Now...\n" );
		uninitialize();
		exit(1);
	}
	
	
	int myDefaultScreen;
	myDefaultScreen = XDefaultScreen(gpMyDisplay);
	
	int myDefaultDepth ;
	myDefaultDepth = DefaultDepth(gpMyDisplay, myDefaultScreen);

	//allocate memory for XVisualInfo and XMatchVisualInfo here there is only single Function --> glXChooseVisual  ( OpenGL Single Buffer)
	gpMyXVisualInfo = glXChooseVisual(gpMyDisplay, myDefaultScreen, 
					framebufferAttribs);

	XSetWindowAttributes winMyAttribs;
	winMyAttribs.border_pixel = 0;
	winMyAttribs.background_pixmap = 0;
	winMyAttribs.colormap = XCreateColormap(gpMyDisplay,
		RootWindow(gpMyDisplay, gpMyXVisualInfo->screen),//like Init --parent
		gpMyXVisualInfo->visual, AllocNone);//allocNone for No child..If we want to set memory for child windows set this parameter
	
	gMyColormap = winMyAttribs.colormap;
	
	winMyAttribs.background_pixel = BlackPixel(gpMyDisplay, myDefaultScreen);
	
	winMyAttribs.event_mask = ExposureMask | //Expose
					VisibilityChangeMask | //WM_MapNotify
					ButtonPressMask | //buttonPress
					KeyPressMask | //Key_Press
					PointerMotionMask | //Motion
					StructureNotifyMask; //ConfigureNotify
	int styleMask;	
	styleMask =  CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gMyWindow = XCreateWindow(gpMyDisplay, 
				RootWindow(gpMyDisplay, gpMyXVisualInfo->screen),
				0, //x
				0, //y
				giMyWindowWidth,
				giMyWindowHeight,
				0,//borderWidth
				gpMyXVisualInfo->depth,
				InputOutput,
				gpMyXVisualInfo->visual,
				styleMask,//Specifies which window attributes are defined in the attributes argument. 
				&winMyAttribs);//Specifies the structure from which the values (as specified by the value mask) are to be taken. The value mask should have the appropriate bits set to indicate which attributes have been set in the structure.
	if(!gMyWindow)
	{
		printf("ERROR: Failed to Create Main Window..\n Exiting now...\n" );
		uninitialize();
		exit(1);
	}

	XStoreName(gpMyDisplay, gMyWindow, "My First XWindow" );
	
	//for Handling cross (33) on Window
	//Atom is immutable processed strings
	Atom windowManagerDelete = XInternAtom(gpMyDisplay, "WM_DELETE_WINDOW" , True);//True:Always Create Atom
	
	XSetWMProtocols(gpMyDisplay, gMyWindow, &windowManagerDelete, 1);

	XMapWindow(gpMyDisplay, gMyWindow);
}

void initialize(void)
{
	void resize(int,int);
	void LoadGLTextures(void);
	//similar to windows wglCreateContext
	gGLXMyContext = glXCreateContext(gpMyDisplay, gpMyXVisualInfo,
					 NULL, //shareable context
					 GL_TRUE); //rendering done through direct connection to graphics system (TRUE) or through XServer (False)
	
	glXMakeCurrent(gpMyDisplay, gMyWindow, gGLXMyContext);
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_FLAT);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	/*Texture*/
	glEnable(GL_TEXTURE_2D);
	LoadGLTextures();
	resize(giMyWindowWidth, giMyWindowHeight);	
}

void LoadGLTextures(void)
{

	void MakeCheckImage(void);
	MakeCheckImage();

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glGenTextures(1, &Texture_CheckerBox);
	glBindTexture(GL_TEXTURE_2D, Texture_CheckerBox);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, checkImageWidth, checkImageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, checkImage);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);	
}

void MakeCheckImage(void)
{
	int i, j, c;
	for(i = 0; i< checkImageHeight; i++)
	{
		for(j=0; j <checkImageWidth; j++)
		{
			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;
			checkImage[i][j][0] = (GLubyte)c;
			checkImage[i][j][1] = (GLubyte)c;
			checkImage[i][j][2] = (GLubyte)c;
			checkImage[i][j][3] = (GLubyte)255;
		}
	}
}

void display(void)
{
	void DrawRectangle();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.6f);
	DrawRectangle();
		
//	glFlush();it is for single buffer

	glXSwapBuffers(gpMyDisplay, gMyWindow); //for Double Buffer 
}

void update(void)
{
}
void DrawRectangle()
{
        glBegin(GL_QUADS);

        //straight facing quad
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-2.0f, -1.0f, 0.0f);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-2.0f, 1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(0.0f, 1.0f, 0.0f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(0.0f, -1.0f, 0.0f);
	
	        //tilted quad
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(1.0f, -1.0f, 0.0f);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(1.0f, 1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(2.41421f, 1.0f, -1.41421f);
	glTexCoord2f(1.0f, 0.0f);
        glVertex3f(2.41421f, -1.0f, -1.41421f);

        glEnd();
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	if(width == 0)
		width = 1;
	
	gluPerspective(45.0f, ((GLfloat)width / (GLfloat)height),0.1f, 100.0f);	
}

void uninitialize(void)
{
	glEnable(GL_TEXTURE_2D);
	glDeleteTextures(1, &Texture_CheckerBox);

	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	
	if(currentGLXContext != NULL && currentGLXContext == gGLXMyContext)
	{
		glXMakeCurrent(gpMyDisplay, 0, 0); //release context
	}

	if(gGLXMyContext)
	{
		glXDestroyContext(gpMyDisplay, gGLXMyContext);
	}
	if(gMyWindow)
	{
		XDestroyWindow(gpMyDisplay, gMyWindow);
	}
	if(gMyColormap)
	{
		XFreeColormap(gpMyDisplay, gMyColormap);
	}
	if(gpMyXVisualInfo)
	{
		free(gpMyXVisualInfo);
		gpMyXVisualInfo = NULL;
	}
	if(gpMyDisplay)
	{
		XCloseDisplay(gpMyDisplay);
		gpMyDisplay = NULL;
	}
}
void ToggleFullScreen()
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state = XInternAtom(gpMyDisplay, "_NET_WM_STATE" , False);
	memset(&xev, 0, sizeof(xev));
	
	xev.type = ClientMessage;
	xev.xclient.window = gMyWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format =32;
	xev.xclient.data.l[0] = bFullScreen ? 0: 1;

	fullscreen = XInternAtom(gpMyDisplay, "_NET_WM_STATE_FULLSCREEN" , False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpMyDisplay, 
			RootWindow(gpMyDisplay, gpMyXVisualInfo->screen),
			False,//Only Send to Self And Not to child Windows
			StructureNotifyMask,
			&xev);
}


	

