#include<iostream>
#include<stdio.h>
#include<stdlib.h>//exit
#include<memory.h>//memset

#include<X11/Xlib.h>
#include<X11/Xutil.h>//for XVisualInfo structure
#include<X11/XKBlib.h>//Keyboard related functions
#include<X11/keysym.h> //for keysym

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>

#include "vmath.h"
#include "Sphere.h"

using namespace std;
using namespace vmath;

enum
{
	MALATI_ATTRIBUTE_VERTEX = 0,
	MALATI_ATTRIBUTE_COLOR,
	MALATI_ATTRIBUTE_NORMAL,
	MALATI_ATTRIBUTE_TEXTURE
};

bool bFullScreen = false;

Display *gpMyDisplay = NULL;
XVisualInfo *gpMyXVisualInfo = NULL;
Colormap gMyColormap;
Window gMyWindow;

int giMyWindowWidth = 800;
int giMyWindowHeight = 600;

FILE *gpFile = NULL;

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);

glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;

GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext; 

GLuint gVertexShaderObjectPerVertex;
GLuint gVertexShaderObjectPerFragment;
GLuint gFragmentShaderObjectPerVertex;
GLuint gFragmentShaderObjectPerFragment;
GLuint gShaderProgramObjectPerVertex;
GLuint gShaderProgramObjectPerFragment;
GLuint gShaderProgramObject;


GLuint gVao_Pyramid;
GLuint gVao_Cube;//displayList

GLuint gVbo_Pyramid_Position;
GLuint gVbo_Cube_Position;

GLuint gVbo_Pyramid_Color;
GLuint gVbo_Cube_Color;

//GLuint gMVPUniform;

GLuint gModelMatrixUniform, gViewMatrixUniform, gProjectionMatrixUniform;
GLuint gLd0Uniform, gLa0Uniform, gLs0Uniform;
GLuint gLd1Uniform, gLa1Uniform, gLs1Uniform;
GLuint gLd2Uniform, gLa2Uniform, gLs2Uniform;
GLuint gLightPositionUniform0;
GLuint gLightPositionUniform1;
GLuint gLightPositionUniform2;
GLuint gKdUniform, gKaUniform, gKsUniform;
GLuint gMaterialShininessUniform;
mat4 gPerspectiveProjectionMatrix;
GLuint gNumElements;
GLuint gNumVertices;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLfloat gAngle = 0.0f;
GLuint gLKeyPressedUniform;
GLuint gVKeyPressedUniform;
GLuint gFKeyPressedUniform;

GLfloat lightAmbient0[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightAmbient1[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightAmbient2[] = { 0.0f,0.0f,0.0f,1.0f };

GLfloat lightDiffuse0[] = { 1.0f,0.0f,0.0f,0.0f };//red
GLfloat lightDiffuse1[] = { 0.0f,1.0f,0.0f,0.0f };//green
GLfloat lightDiffuse2[] = { 0.0f,0.0f,1.0f,0.0f };//blue

GLfloat lightSpecular0[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lightSpecular1[] = { 0.0f,1.0f,0.0f,1.0f };
GLfloat lightSpecular2[] = { 0.0f,0.0f,1.0f,1.0f };

GLfloat lightPosition0[] = { 100.0f,100.0f,100.0f,0.0f };
GLfloat lightPosition1[] = { -100.0f,100.0f,100.0f,0.0f };
GLfloat lightPosition2[] = { 0.0f,0.0f,100.0f,0.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };

GLfloat material_shininess = 50.0f;

bool gbLight;
bool gbFrag;
bool gbFragmentLight;
bool gbVertexLight;

GLfloat angle_red = 0.0f;
GLfloat angle_green = 0.0f;
GLfloat angle_blue = 0.0f;

GLfloat myRotateMatrix[4];
GLfloat myRotateMatrix1[4];
GLfloat myRotateMatrix2[4];
int main(void)
{
	//function prototype
	void CreateWindow(void);
	void uninitialize(void);
	void ToggleFullScreen(void);
	
	void initialize(void);
	void resize(int, int);
	void display(void);
	void Update(void);

	static bool bIsLKeyPressed = false;
	static bool bIsVKeyPressed = false;
	static bool bIsFKeyPressed = false;
	
	int windowWidth = giMyWindowWidth;
	int windowHeight = giMyWindowHeight;

	gpFile = fopen("LOG.txt" , "w" );
	if(gpFile == NULL)
	{
		printf("Log File Can't be created..Exiting Now..\n");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File is SuccessFully Opened..\n" );
	}
	 
	CreateWindow();

	initialize();

	XEvent myEvent;
	KeySym myKeysym;
	bool bDone = false;

	while(bDone == false)
	{
		while(XPending(gpMyDisplay))
		{
			XNextEvent(gpMyDisplay, &myEvent);
		//1:Connection to Xserver
		//2:return Match Event associated structure
			switch(myEvent.type)
			{
				case MapNotify: //WM_CREATE	
		 			break;
				case KeyPress:
					myKeysym = XkbKeycodeToKeysym(gpMyDisplay, myEvent.xkey.keycode, 0, 0); 
		//1:ConnectionTo XServer
		//2:KeyOf Interest
		//3:Group Of Interest
		//4:Shift LevelOf Interest
		
				switch(myKeysym)
				{
					case XK_Escape:
						if(bFullScreen == false)
						{
							ToggleFullScreen();
							bFullScreen = true;
						}
						else
						{
							ToggleFullScreen();
							bFullScreen = false;
						}
						break;
					case XK_Q:
					case XK_q:
						bDone = true;
						break;
					case XK_F:
					case XK_f:
						if (bIsFKeyPressed == false)
						{
							gbFrag = true;
							bIsFKeyPressed = true;
							gbLight = false;
                                                        bIsLKeyPressed = false;
						}
						else
						{
							gbFrag = false;
							bIsFKeyPressed = false;
						}
						break;
		/*			case XK_V:
					case XK_v:
						if (bIsVKeyPressed == false)
						{
							gbVertexLight = true;
							bIsVKeyPressed = true;
							gbFragmentLight = false;
							bIsFKeyPressed = false;
						}
						else
						{
							gbVertexLight = false;
							bIsVKeyPressed = false;
						}
						break;*/
					case XK_V:
					case XK_v:
					case XK_L:
					case XK_l:
						if (bIsLKeyPressed == false)
						{
							gbLight = true;
							bIsLKeyPressed = true;
							gbFrag = false;
                                                        bIsFKeyPressed = false;
						}
						else
						{
							gbLight = false;
							bIsLKeyPressed = false;
						}
						break;
					default:
						break;
				}
				break;
				case ButtonPress:
					switch(myEvent.xbutton.button)
					{
						case 1://L
						break;
						case 2://M
						break;
						case 3://R
						break;
						default:
						break;
					}
					break;
				case MotionNotify://mouse
				break;
				case ConfigureNotify://WM_SIZE
					windowWidth = myEvent.xconfigure.width;
					windowHeight = myEvent.xconfigure.height;
					resize(windowWidth, windowHeight);
					break;
				case Expose://WM_PAINT
					break;
				case DestroyNotify://WM_DESTROY
					break;
				case 33://Window Cross Button
					bDone = true;
					break;
				default:
					break;
			}
		}
	Update();		
	display();
	}
	uninitialize();
	return 0;
}
void CreateWindow(void)
{
	//function prototype
	void uninitialize(void);

	GLXFBConfig *pGLXFBConfigs = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumFBConfigs = 0;
	int i;

	static int frameBufferAttributes[] = {
		GLX_X_RENDERABLE, True, //Video Rendering
		GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT, //who is drawable (window)
		GLX_RENDER_TYPE, GLX_RGBA_BIT,//frame which type
		GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, //similar to XMAtchVisualInfo-->true color
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DEPTH_SIZE, 8,
		GLX_STENCIL_SIZE , 8,
		GLX_DOUBLEBUFFER, True,
		//GLX_SAMPLE_BUFFER, 1, 
		//GLX_SAMPLES, 4,
		None}; //array
	//13 Steps
	gpMyDisplay = XOpenDisplay(NULL);//Set some value to Parameter when u want to do some Remote Connection : NULL: Default Display pointer(127.0.0.1)
	if(gpMyDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display..\nExiting Now...\n" );
		uninitialize();
		exit(1);
	}
	
	pGLXFBConfigs = glXChooseFBConfig(gpMyDisplay, DefaultScreen(gpMyDisplay), frameBufferAttributes, &iNumFBConfigs);
	if(pGLXFBConfigs == NULL)
	{
		printf("Failed To Get valid Frame Buffer config. Exiting..\n" );
		uninitialize();
		exit(1);
	}
	
	//pick FB config/visual 
	int bestFramebufferconfig = -1, worstFramebufferConfig=-1, bestNumberOfSamples = -1, worstNumberOfSamples = 999;
	for(i = 0; i< iNumFBConfigs; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpMyDisplay, pGLXFBConfigs[i]);
		if(pTempXVisualInfo)
		{
			int sampleBuffer, samples;
			
			glXGetFBConfigAttrib(gpMyDisplay, pGLXFBConfigs[i], GLX_SAMPLE_BUFFERS, &sampleBuffer);
			glXGetFBConfigAttrib(gpMyDisplay, pGLXFBConfigs[i], GLX_SAMPLES, &samples);
			//printf("Matching FrameBuffer Config=%d : Visual ID=0x%lu: SAMPLE_BUFFERS=%d : SAMPLES=%d\n" , i, pTempXVisualInfo->visualid, sampleBuffer, samples);
		if(bestFramebufferconfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
		{
			bestFramebufferconfig = i;
			bestNumberOfSamples = samples;
		}
		
		if( worstFramebufferConfig < 0 || !sampleBuffer || samples < worstNumberOfSamples)
		{
			worstFramebufferConfig = i;
			worstNumberOfSamples = samples;
		}
	}
	XFree(pTempXVisualInfo);
	}
	bestGLXFBConfig = pGLXFBConfigs[bestFramebufferconfig];
	
	gGLXFBConfig = bestGLXFBConfig;

	XFree(pGLXFBConfigs);
	
	gpMyXVisualInfo = glXGetVisualFromFBConfig(gpMyDisplay, bestGLXFBConfig);
	printf("Choosen Visual ID= 0x%lu\n", gpMyXVisualInfo->visualid);

	XSetWindowAttributes winMyAttribs;
	winMyAttribs.border_pixel = 0;
	winMyAttribs.background_pixmap = 0;
	winMyAttribs.colormap = XCreateColormap(gpMyDisplay,
		RootWindow(gpMyDisplay, gpMyXVisualInfo->screen),//like Init --parent
		gpMyXVisualInfo->visual, AllocNone);//allocNone for No child..If we want to set memory for child windows set this parameter
	
	gMyColormap = winMyAttribs.colormap;
	
	winMyAttribs.event_mask = ExposureMask | //Expose
					VisibilityChangeMask | //WM_MapNotify
					ButtonPressMask | //buttonPress
					KeyPressMask | //Key_Press
					PointerMotionMask | //Motion
					StructureNotifyMask; //ConfigureNotify
	int styleMask;	
	styleMask =  CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gMyWindow = XCreateWindow(gpMyDisplay, 
				RootWindow(gpMyDisplay, gpMyXVisualInfo->screen),
				0, //x
				0, //y
				giMyWindowWidth,
				giMyWindowHeight,
				0,//borderWidth
				gpMyXVisualInfo->depth,
				InputOutput,
				gpMyXVisualInfo->visual,
				styleMask,//Specifies which window attributes are defined in the attributes argument. 
				&winMyAttribs);//Specifies the structure from which the values (as specified by the value mask) are to be taken. The value mask should have the appropriate bits set to indicate which attributes have been set in the structure.
	if(!gMyWindow)
	{
		printf("ERROR: Failed to Create Main Window..\n Exiting now...\n" );
		uninitialize();
		exit(1);
	}

	XStoreName(gpMyDisplay, gMyWindow, "My First XWindow" );
	
	//for Handling cross (33) on Window
	//Atom is immutable processed strings
	Atom windowManagerDelete = XInternAtom(gpMyDisplay, "WM_DELETE_WINDOW" , True);//True:Always Create Atom
	
	XSetWMProtocols(gpMyDisplay, gMyWindow, &windowManagerDelete, 1);

	XMapWindow(gpMyDisplay, gMyWindow);
			
}
void initialize(void)
{
	void MyLinkProgram(GLuint iPgmObject);
	void uninitialize(void);
	void resize(int, int);
	
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc) glXGetProcAddressARB((GLubyte*) "glXCreateContextAttribsARB");

	GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
		GLX_CONTEXT_MINOR_VERSION_ARB, 5,
		GLX_CONTEXT_PROFILE_MASK_ARB,
		GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
		0};

	gGLXContext = glXCreateContextAttribsARB(gpMyDisplay, gGLXFBConfig, 0, True, attribs);
	if(!gGLXContext)
	{
		GLint attribs[] = { 
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			0 };
		printf("Failed To Create GLX 4.5 context. Hence Using OLD GLX Context\n" );
		gGLXContext = glXCreateContextAttribsARB(gpMyDisplay, gGLXFBConfig, 0, True, attribs);
	}
	else
	{
		printf("OpenGL Context 4.5 is Created.\n");
	}
	
	if(!glXIsDirect(gpMyDisplay, gGLXContext))
	{
		printf("Indirect GLX Rendering Context Obtained\n");
	}
	else
	{
		printf("Direct GLX Rendering Context Obtained\n");
	}
	glXMakeCurrent(gpMyDisplay, gMyWindow, gGLXContext);

	GLenum glew_error = glewInit();
	if(glew_error != GLEW_OK)
	{
		glXDestroyContext(gpMyDisplay, gGLXContext);
		XDestroyWindow(gpMyDisplay, gMyWindow);
	}
		
	GLint num, i;
	glGetIntegerv(GL_NUM_EXTENSIONS, &num);
	
/*	for(i = 0; i < num; i++)
	{
		fprintf(gpFile, "%d : MALATI : %s \n" , (i+1), glGetStringi(GL_EXTENSIONS, i)   );
	}*/
	fprintf(gpFile, "%s\n", glGetString(GL_VERSION));

	fprintf(gpFile, "Shading Lang Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));


	
	//VERTEX SHADER
/*	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const GLchar *vertextShaderSourceCode =
	"#version 400" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_LKeyPressed;" \
		"uniform int u_VKeyPressed;" \
		"uniform int u_FKeyPressed;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ls;" \
		"uniform vec4 u_light_position;"\
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Ks;" \
		"uniform vec3 u_Kd;" \
		"uniform float u_material_shininess;" \
		"out vec3 phong_ads_color;" \
		"void main(void)" \
		"{" \
		"if(u_VKeyPressed == 1)" \
		"{" \
		"vec4 eyeCoordinates =  u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 tnorm = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
		"vec3 light_direction  = normalize(vec3(u_light_position) - eyeCoordinates.xyz);" \
		"vec3 ambient = u_La * u_Ka;"\
		"vec3 diffuse_light = u_Ld * u_Kd * max(dot(light_direction, tnorm), 0.0);" \
		"vec3 reflection_vector = reflect(-light_direction, tnorm);"\
		"vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0),u_material_shininess);" \
		"phong_ads_color=ambient + diffuse_light + specular;" \
		"}" \
		"else if(u_FKeyPressed == 1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"}" \
		"else"\
		"{"\
		"phong_ads_color = vec3(1.0, 1.0, 1.0);"\
		"}"\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertextShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//FRAGMENT SHADER
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar *fragmentShaderSourceCode =
		"#version 400" \
		"\n" \
		"in vec3 phong_ads_color;" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_VKeyPressed;" \
		"uniform int u_FKeyPressed;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color1;" \
		"if(u_VKeyPressed == 1)" \
		"{" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}" \
		"else" \
		"{" \
		"if(u_FKeyPressed == 1)" \
		"{" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction=normalize(light_direction);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		"vec3 ambient = u_La * u_Ka;" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color1=ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color1 = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_color1, 1.0);" \
		"}"
		"}";
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(gFragmentShaderObject);
	
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Shader Program
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");
	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");
	gVKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_VKeyPressed");
	gFKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_FKeyPressed");
	gLdUniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	gLaUniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	gLsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");

	gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	gKaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");

	gLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	//position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//normal
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo

	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	
	//color
//	glVertexAttrib3f(MALATI_ATTRIBUTE_COLOR, 0.39f, 0.58f, 0.9294f); //cornflower blue
	*/


	//VERTEX SHADER

	gVertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);



	const GLchar *vertextShaderSourceCodePerVertex =

		"#version 400" \

		"\n" \

		"in vec4 vPosition;" \

		"in vec3 vNormal;" \

		"uniform mat4 u_model_matrix;" \

		"uniform mat4 u_view_matrix;" \

		"uniform mat4 u_projection_matrix;" \



		"uniform int u_LKeyPressed;" \



		"uniform vec3 u_Ld0;" \

		"uniform vec3 u_Ld1;" \

		"uniform vec3 u_Ld2;" \

		"uniform vec3 u_La0;" \

		"uniform vec3 u_La1;" \

		"uniform vec3 u_La2;" \

		"uniform vec3 u_Ls0;" \

		"uniform vec3 u_Ls1;" \

		"uniform vec3 u_Ls2;" \

		"uniform vec4 u_light_position0;"\

		"uniform vec4 u_light_position1;"\

		"uniform vec4 u_light_position2;"\

		"uniform vec3 u_Ka;" \

		"uniform vec3 u_Ks;" \

		"uniform vec3 u_Kd;" \

		"uniform float u_material_shininess;" \



		"out vec3 phong_ads_color0;" \

		"out vec3 phong_ads_color1;" \

		"out vec3 phong_ads_color2;" \

		"void main(void)" \

		"{" \

		"if(u_LKeyPressed == 1)" \

		"{" \

		"vec4 eyeCoordinates =  u_view_matrix * u_model_matrix * vPosition;" \

		"vec3 tnorm = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \

		"vec3 light_direction  = normalize(vec3(u_light_position0) - eyeCoordinates.xyz);" \

		"vec3 ambient0 = u_La0 * u_Ka;"\

		"vec3 diffuse_light0 = u_Ld0 * u_Kd * max(dot(light_direction, tnorm), 0.0);" \

		"vec3 reflection_vector0 = reflect(-light_direction, tnorm);"\

		"vec3 viewer_vector0 = normalize(-eyeCoordinates.xyz);" \

		"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, viewer_vector0), 0.0),u_material_shininess);" \

		"phong_ads_color0=ambient0 + diffuse_light0 + specular0;" \



		"vec4 eyeCoordinates1 =  u_view_matrix * u_model_matrix * vPosition;" \

		"vec3 tnorm1 = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \

		"vec3 light_direction1  = normalize(vec3(u_light_position1) - eyeCoordinates1.xyz);" \

		"vec3 ambient1 = u_La1 * u_Ka;"\

		"vec3 diffuse_light1 = u_Ld1 * u_Kd * max(dot(light_direction1, tnorm1), 0.0);" \

		"vec3 reflection_vector1 = reflect(-light_direction1, tnorm1);"\

		"vec3 viewer_vector1 = normalize(-eyeCoordinates1.xyz);" \

		"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, viewer_vector1), 0.0),u_material_shininess);" \

		"phong_ads_color1=ambient1 + diffuse_light1 + specular1;" \



		"vec4 eyeCoordinates2 =  u_view_matrix * u_model_matrix * vPosition;" \

		"vec3 tnorm2 = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \

		"vec3 light_direction2  = normalize(vec3(u_light_position2) - eyeCoordinates2.xyz);" \

		"vec3 ambient2 = u_La2 * u_Ka;"\

		"vec3 diffuse_light2 = u_Ld2 * u_Kd * max(dot(light_direction2, tnorm2), 0.0);" \

		"vec3 reflection_vector2 = reflect(-light_direction2, tnorm2);"\

		"vec3 viewer_vector2 = normalize(-eyeCoordinates2.xyz);" \

		"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, viewer_vector2), 0.0),u_material_shininess);" \

		"phong_ads_color2=ambient2 + diffuse_light2 + specular2;" \

		"}" \

		"else"\

		"{"\

		"phong_ads_color0 = vec3(1.0, 1.0, 1.0);"\

		"phong_ads_color1 = vec3(1.0, 1.0, 1.0);"\

		"phong_ads_color2 = vec3(1.0, 1.0, 1.0);"\

		"}"\

		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \

		"}";



	glShaderSource(gVertexShaderObjectPerVertex, 1, (const GLchar **)&vertextShaderSourceCodePerVertex, NULL);



	//compile shader

	glCompileShader(gVertexShaderObjectPerVertex);



	GLint iInfoLogLength = 0;

	GLint iShaderCompiledStatus = 0;

	char *szInfoLog = NULL;



	glGetShaderiv(gVertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE)

	{

		glGetShaderiv(gVertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)

		{

			szInfoLog = (char *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)

			{

				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);

				free(szInfoLog);

				uninitialize();

				exit(0);

			}

		}

	}



	///////////////////////////////////PerFragment Vertex Shader///////////////////////////////////////////////

	//VERTEX SHADER

	gVertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);



	const GLchar *vertextShaderSourceCodePerFragment =

		"#version 400" \

		"\n" \

		"in vec4 vPosition;" \

		"in vec3 vNormal;" \

		"uniform mat4 u_model_matrix;" \

		"uniform mat4 u_view_matrix;" \

		"uniform mat4 u_projection_matrix;" \

		"uniform vec4 u_light_position0;"\

		"uniform vec4 u_light_position1;"\

		"uniform vec4 u_light_position2;"\

		"uniform int u_FKeyPressed;" \

		"out vec3 transformed_normals;" \

		"out vec3 light_direction0;" \

		"out vec3 light_direction1;" \

		"out vec3 light_direction2;" \

		"out vec3 viewer_vector;" \

		"void main(void)" \

		"{" \

		"if(u_FKeyPressed == 1)" \

		"{" \

		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \

		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \

		"light_direction0 = vec3(u_light_position0) - eye_coordinates.xyz;" \

		"light_direction1 = vec3(u_light_position1) - eye_coordinates.xyz;" \

		"light_direction2 = vec3(u_light_position2) - eye_coordinates.xyz;" \

		"viewer_vector = -eye_coordinates.xyz;" \

		"}"\

		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \

		"}";



	glShaderSource(gVertexShaderObjectPerFragment, 1, (const GLchar **)&vertextShaderSourceCodePerFragment, NULL);



	//compile shader

	glCompileShader(gVertexShaderObjectPerFragment);



	iInfoLogLength = 0;

	iShaderCompiledStatus = 0;

	//char *szInfoLog = NULL;



	glGetShaderiv(gVertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE)

	{

		glGetShaderiv(gVertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)

		{

			szInfoLog = (char *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)

			{

				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);

				free(szInfoLog);

				uninitialize();

				exit(0);

			}

		}

	}



	//FRAGMENT SHADER



	gFragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);



	const GLchar *fragmentShaderSourceCodePerVertex =

		"#version 400" \

		"\n" \

		"in vec3 phong_ads_color0;" \

		"in vec3 phong_ads_color1;" \

		"in vec3 phong_ads_color2;" \

		"vec4 FragColor0;"

		"vec4 FragColor1;"

		"vec4 FragColor2;"

		"out vec4 FragColor;" \

		"void main(void)" \

		"{" \

		"FragColor0 = vec4(phong_ads_color0, 1.0);" \

		"FragColor1 = vec4(phong_ads_color1, 1.0);" \

		"FragColor2 = vec4(phong_ads_color2, 1.0);" \

		"FragColor = FragColor0 + FragColor1 + FragColor2;" \

		"}";



	glShaderSource(gFragmentShaderObjectPerVertex, 1, (const GLchar **)&fragmentShaderSourceCodePerVertex, NULL);



	glCompileShader(gFragmentShaderObjectPerVertex);



	glGetShaderiv(gFragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE)

	{

		glGetShaderiv(gFragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)

		{

			szInfoLog = (char *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)

			{

				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);

				free(szInfoLog);

				uninitialize();

				exit(0);

			}

		}

	}



	//////////////////////////////Per Fragment FragShader



	gFragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);



	const GLchar *fragmentShaderSourceCodePerFragment =

		"#version 400" \

		"\n" \

		"in vec3 transformed_normals;" \

		"in vec3 light_direction0;" \

		"in vec3 light_direction1;" \

		"in vec3 light_direction2;" \

		"in vec3 viewer_vector;" \

		"out vec4 FragColor;" \

		"uniform vec3 u_La0;" \

		"uniform vec3 u_La1;" \

		"uniform vec3 u_La2;" \

		"uniform vec3 u_Ld0;" \

		"uniform vec3 u_Ld1;" \

		"uniform vec3 u_Ld2;" \

		"uniform vec3 u_Ls0;" \

		"uniform vec3 u_Ls1;" \

		"uniform vec3 u_Ls2;" \

		"uniform vec3 u_Ka;" \

		"uniform vec3 u_Kd;" \

		"uniform vec3 u_Ks;" \

		"uniform float u_material_shininess;" \

		"uniform int u_FKeyPressed;" \

		"void main(void)" \

		"{" \

		"vec3 phong_ads_color0;" \

		"vec3 phong_ads_color1;" \

		"vec3 phong_ads_color2;" \

		"vec3 phong_ads_color;" \

		"if(u_FKeyPressed==1)" \

		"{" \

		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \

		"vec3 normalized_light_direction0=normalize(light_direction0);" \

		"vec3 ambient0 = u_La0 * u_Ka;" \

		"float tn_dot_ld0 = max(dot(normalized_transformed_normals, normalized_light_direction0),0.0);" \

		"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;" \

		"vec3 reflection_vector0 = reflect(-normalized_light_direction0, normalized_transformed_normals);" \

		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \

		"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, normalized_viewer_vector), 0.0), u_material_shininess);" \

		"phong_ads_color0=ambient0 + diffuse0 + specular0;" \



	

		"vec3 normalized_light_direction1=normalize(light_direction1);" \

		"vec3 ambient1 = u_La1 * u_Ka;" \

		"float tn_dot_ld1 = max(dot(normalized_transformed_normals, normalized_light_direction1),0.0);" \

		"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;" \

		"vec3 reflection_vector1 = reflect(-normalized_light_direction1, normalized_transformed_normals);" \

		"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, normalized_viewer_vector), 0.0), u_material_shininess);" \

		"phong_ads_color1=ambient1 + diffuse1 + specular1;" \



		"vec3 normalized_light_direction2=normalize(light_direction2);" \

		"vec3 ambient2 = u_La2 * u_Ka;" \

		"float tn_dot_ld2 = max(dot(normalized_transformed_normals, normalized_light_direction2),0.0);" \

		"vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;" \

		"vec3 reflection_vector2 = reflect(-normalized_light_direction2, normalized_transformed_normals);" \

		"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, normalized_viewer_vector), 0.0), u_material_shininess);" \

		"phong_ads_color2=ambient2 + diffuse2 + specular2;" \

		"}" \

		"else" \

		"{" \

		"phong_ads_color0 = vec3(1.0, 1.0, 1.0);" \

		"phong_ads_color1 = vec3(1.0, 1.0, 1.0);" \

		"phong_ads_color2 = vec3(1.0, 1.0, 1.0);" \

		"}" \

		"phong_ads_color = phong_ads_color0 + phong_ads_color1 + phong_ads_color2;" \

		"FragColor = vec4(phong_ads_color, 1.0);" \

		"}";





	glShaderSource(gFragmentShaderObjectPerFragment, 1, (const GLchar **)&fragmentShaderSourceCodePerFragment, NULL);



	glCompileShader(gFragmentShaderObjectPerFragment);



	glGetShaderiv(gFragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE)

	{

		glGetShaderiv(gFragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)

		{

			szInfoLog = (char *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)

			{

				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);

				free(szInfoLog);

				uninitialize();

				exit(0);

			}

		}

	}



	//Shader Program

	

	gShaderProgramObjectPerVertex = glCreateProgram();



	glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);



	glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);



	MyLinkProgram(gShaderProgramObjectPerVertex);

	/*gShaderProgramObjectPerFragment = glCreateProgram();



	glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);



	glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);



	gShaderProgramObject = gShaderProgramObjectPerVertex;



	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");

	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");



	glLinkProgram(gShaderProgramObject);



	GLint iShaderProgramLinkStatus = 0;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);

	if (iShaderProgramLinkStatus == GL_FALSE)

	{

		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength>0)

		{

			szInfoLog = (char *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)

			{

				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObjectPerVertex, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);

				free(szInfoLog);

				uninitialize();

				exit(0);

			}

		}

	}

	

	gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");

	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");

	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");



	gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");



	gLd0Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld0");

	gLd1Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld1");

	gLd2Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld2");



	gLa0Uniform = glGetUniformLocation(gShaderProgramObject, "u_La0");

	gLa1Uniform = glGetUniformLocation(gShaderProgramObject, "u_La1");

	gLa2Uniform = glGetUniformLocation(gShaderProgramObject, "u_La2");



	gLs0Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls0");

	gLs1Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls1");

	gLs2Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls2");



	gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");

	gKsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");

	gKaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");



	gLightPositionUniform0 = glGetUniformLocation(gShaderProgramObject, "u_light_position0");

	gLightPositionUniform1 = glGetUniformLocation(gShaderProgramObject, "u_light_position1");

	gLightPositionUniform2 = glGetUniformLocation(gShaderProgramObject, "u_light_position2");

	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	*/

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);
	//position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//normal
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//glEnable(GL_CULL_FACE);
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	gPerspectiveProjectionMatrix = mat4::identity();
	
	gbLight = false;

	gbFrag = false;	
	resize(giMyWindowWidth, giMyWindowHeight);
}

void MyLinkProgram(GLuint iPgmObject)
{
	void uninitialize(void);
	gShaderProgramObject = glCreateProgram();

	//glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);

	//glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);
	gShaderProgramObject = iPgmObject;

	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	GLint iInfoLogLength = 0;
	char *szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");
	gFKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_FKeyPressed");

	gLd0Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld0");
	gLd1Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld1");
	gLd2Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld2");

	gLa0Uniform = glGetUniformLocation(gShaderProgramObject, "u_La0");
	gLa1Uniform = glGetUniformLocation(gShaderProgramObject, "u_La1");
	gLa2Uniform = glGetUniformLocation(gShaderProgramObject, "u_La2");

	gLs0Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls0");
	gLs1Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls1");
	gLs2Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls2");

	gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	gKaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	gLightPositionUniform0 = glGetUniformLocation(gShaderProgramObject, "u_light_position0");
	gLightPositionUniform1 = glGetUniformLocation(gShaderProgramObject, "u_light_position1");
	gLightPositionUniform2 = glGetUniformLocation(gShaderProgramObject, "u_light_position2");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

}
void resize(int width , int height)
{
	if(height == 0)
		height =1;
	glViewport(0,0, (GLsizei)width, (GLsizei)height);
	
	gPerspectiveProjectionMatrix =  perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);
	glUseProgram(gShaderProgramObject);
	if (gbLight == true)
	{
		gShaderProgramObjectPerVertex = glCreateProgram();
		glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
		glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);
		MyLinkProgram(gShaderProgramObjectPerVertex);
		glUseProgram(gShaderProgramObjectPerVertex);
		glUniform1i(gLKeyPressedUniform, 1);

		glUniform3fv(gLd0Uniform, 1, lightDiffuse0);//white
		glUniform3fv(gLa0Uniform, 1, lightAmbient0);
		glUniform3fv(gLs0Uniform, 1, lightSpecular0);
		glUniform3fv(gLd1Uniform, 1, lightDiffuse1);//white
		glUniform3fv(gLa1Uniform, 1, lightAmbient1);
		glUniform3fv(gLs1Uniform, 1, lightSpecular1);
		glUniform3fv(gLd2Uniform, 1, lightDiffuse2);//white
		glUniform3fv(gLa2Uniform, 1, lightAmbient2);
		glUniform3fv(gLs2Uniform, 1, lightSpecular2);

		mat4 rotationMatrix = mat4::identity();
		rotationMatrix = vmath::rotate(angle_red, 1.0f, 1.0f, 1.0f);

		/*	myRotateMatrix[0] = rotationMatrix[0][0] * lightPosition0[0] + rotationMatrix[0][1] * lightPosition0[1] + rotationMatrix[0][2] * lightPosition0[2] ;

		myRotateMatrix[1] = rotationMatrix[1][0] * lightPosition0[0] + rotationMatrix[1][1] * lightPosition0[1] + rotationMatrix[1][2] * lightPosition0[2];

		myRotateMatrix[2] = rotationMatrix[2][0] * lightPosition0[0] + rotationMatrix[2][1] * lightPosition0[1] + rotationMatrix[2][2] * lightPosition0[2];

		myRotateMatrix[2] = rotationMatrix[2][0] * lightPosition0[0] + rotationMatrix[2][1] * lightPosition0[1] + rotationMatrix[2][2] * lightPosition0[2];

		*/

		myRotateMatrix[0] = 0.0f;
		myRotateMatrix[1] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix[2] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix[3] = 1.0f;
		glUniform4fv(gLightPositionUniform0, 1, myRotateMatrix);
		myRotateMatrix1[0] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix1[1] = 0.0f;
		myRotateMatrix1[2] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix1[3] = 1.0f;

		glUniform4fv(gLightPositionUniform1, 1, myRotateMatrix1);

		myRotateMatrix2[0] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix2[1] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix2[2] = -2.0f;
		myRotateMatrix2[3] = 1.0f;
		glUniform4fv(gLightPositionUniform2, 1, myRotateMatrix2);

		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

		//	float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };

	}

	else if (gbFrag == true)
	{
		fprintf(gpFile, "Use Program FRAG\n");
		gShaderProgramObjectPerFragment = glCreateProgram();

		glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
		glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

		MyLinkProgram(gShaderProgramObjectPerFragment);
		glUseProgram(gShaderProgramObjectPerFragment);
		glUniform1i(gFKeyPressedUniform, 1);
		glUniform3fv(gLd0Uniform, 1, lightDiffuse0);//white
		glUniform3fv(gLa0Uniform, 1, lightAmbient0);
		glUniform3fv(gLs0Uniform, 1, lightSpecular0);
		glUniform3fv(gLd1Uniform, 1, lightDiffuse1);//white
		glUniform3fv(gLa1Uniform, 1, lightAmbient1);
		glUniform3fv(gLs1Uniform, 1, lightSpecular1);
		glUniform3fv(gLd2Uniform, 1, lightDiffuse2);//white
		glUniform3fv(gLa2Uniform, 1, lightAmbient2);
		glUniform3fv(gLs2Uniform, 1, lightSpecular2);

		mat4 rotationMatrix = mat4::identity();
		rotationMatrix = vmath::rotate(angle_red, 1.0f, 1.0f, 1.0f);

		/*	myRotateMatrix[0] = rotationMatrix[0][0] * lightPosition0[0] + rotationMatrix[0][1] * lightPosition0[1] + rotationMatrix[0][2] * lightPosition0[2] ;

		myRotateMatrix[1] = rotationMatrix[1][0] * lightPosition0[0] + rotationMatrix[1][1] * lightPosition0[1] + rotationMatrix[1][2] * lightPosition0[2];

		myRotateMatrix[2] = rotationMatrix[2][0] * lightPosition0[0] + rotationMatrix[2][1] * lightPosition0[1] + rotationMatrix[2][2] * lightPosition0[2];

		myRotateMatrix[2] = rotationMatrix[2][0] * lightPosition0[0] + rotationMatrix[2][1] * lightPosition0[1] + rotationMatrix[2][2] * lightPosition0[2];

		*/

		myRotateMatrix[0] = 0.0f;
		myRotateMatrix[1] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix[2] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix[3] = 1.0f;

		glUniform4fv(gLightPositionUniform0, 1, myRotateMatrix);
		myRotateMatrix1[0] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix1[1] = 0.0f;
		myRotateMatrix1[2] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix1[3] = 1.0f;
		glUniform4fv(gLightPositionUniform1, 1, myRotateMatrix1);

		myRotateMatrix2[0] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix2[1] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix2[2] = -2.0f;
		myRotateMatrix2[3] = 1.0f;
		glUniform4fv(gLightPositionUniform2, 1, myRotateMatrix2);

		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);
	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
		glUniform1i(gFKeyPressedUniform, 0);
	}

	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
	/*rotationMatrix = vmath::rotate(angle_red, angle_red, angle_red);
	modelMatrix = modelMatrix * rotationMatrix; //imp*/
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);
	//glDrawIndexArrays, glDrawElements
	glUseProgram(0);
	glXSwapBuffers(gpMyDisplay, gMyWindow);
}

void Update(void)
{
	/*gAngleCube = gAngleCube + 0.1f;
	if (gAngleCube >= 360.0f)
		gAngleCube = 0.0f;

	gAnglePyramid = gAnglePyramid + 0.1f;
	if (gAnglePyramid >= 360.0f)
		gAnglePyramid = 0.0f;*/
	angle_red = angle_red + 0.001f;
	if (angle_red >= 360.0f)
		angle_red = 0.0f;
	
	angle_green = angle_green + 0.01f;
	if (angle_green >= 360.0f)
		angle_green = 0.0f;
	
	angle_blue = angle_blue + 0.01f;
	if (angle_blue >= 360.0f)
		angle_blue = 0.0f;
}

void uninitialize(void)
{
	if (gVao_Pyramid)
	{
		glDeleteVertexArrays(1, &gVao_Pyramid);
		gVao_Pyramid = 0;
	}
	if (gVao_Cube)
	{
		glDeleteVertexArrays(1, &gVao_Cube);
		gVao_Cube = 0;
	}
	if (gVbo_Pyramid_Position)
	{
		glDeleteBuffers(1, &gVbo_Pyramid_Position);
		gVbo_Pyramid_Position = 0;
	}
	if (gVbo_Pyramid_Color)
	{
		glDeleteBuffers(1, &gVbo_Pyramid_Color);
		gVbo_Pyramid_Color = 0;
	}
	if (gVbo_Cube_Position)
	{
		glDeleteBuffers(1, &gVbo_Cube_Position);
		gVbo_Cube_Position = 0;
	}
	if (gVbo_Cube_Color)
        {
                glDeleteBuffers(1, &gVbo_Cube_Color);
                gVbo_Cube_Color = 0;
        }

	glDetachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
	glDetachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);
	glDetachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
	glDetachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);
	glDeleteShader(gVertexShaderObjectPerVertex);
	gVertexShaderObjectPerVertex = 0;
	glDeleteShader(gFragmentShaderObjectPerVertex);
	gFragmentShaderObjectPerVertex = 0;
	glDeleteShader(gVertexShaderObjectPerFragment);
	gVertexShaderObjectPerFragment = 0;
	glDeleteShader(gFragmentShaderObjectPerFragment);
	gFragmentShaderObjectPerFragment = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	GLXContext currentContext = glXGetCurrentContext();
	if(currentContext != NULL && currentContext == gGLXContext)
	{
		glXMakeCurrent(gpMyDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpMyDisplay, gGLXContext);
	}
	if(gMyWindow)
	{
		XDestroyWindow(gpMyDisplay, gMyWindow);
	}
	if(gMyColormap)
	{
		XFreeColormap(gpMyDisplay, gMyColormap);
	}
	if(gpMyXVisualInfo)
	{
		free(gpMyXVisualInfo);
		gpMyXVisualInfo = NULL;
	}
	if(gpMyDisplay)
	{
		XCloseDisplay(gpMyDisplay);
		gpMyDisplay = NULL;
	}
	if(gpFile)
	{
		fprintf(gpFile, "Log File is successfully Closed\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
void ToggleFullScreen()
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state = XInternAtom(gpMyDisplay, "_NET_WM_STATE" , False);
	memset(&xev, 0, sizeof(xev));
	
	xev.type = ClientMessage;
	xev.xclient.window = gMyWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format =32;
	xev.xclient.data.l[0] = bFullScreen ? 0: 1;

	fullscreen = XInternAtom(gpMyDisplay, "_NET_WM_STATE_FULLSCREEN" , False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpMyDisplay, 
			RootWindow(gpMyDisplay, gpMyXVisualInfo->screen),
			False,//Only Send to Self And Not to child Windows
			StructureNotifyMask,
			&xev);
}


	

