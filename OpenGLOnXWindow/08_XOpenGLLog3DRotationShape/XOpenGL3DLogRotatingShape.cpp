#include<iostream>
#include<stdio.h>
#include<stdlib.h>//exit
#include<memory.h>//memset

#include<X11/Xlib.h>
#include<X11/Xutil.h>//for XVisualInfo structure
#include<X11/XKBlib.h>//Keyboard related functions
#include<X11/keysym.h>

//OpenGL Change
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>//bridging api

using namespace std;

bool bFullScreen = false;

Display *gpMyDisplay = NULL;
XVisualInfo *gpMyXVisualInfo = NULL;
Colormap gMyColormap;
Window gMyWindow;

int giMyWindowWidth = 800;
int giMyWindowHeight = 600;

GLXContext gGLXMyContext; //HGLRC ghrc; in windows
//float gAngle =0.0f;
GLfloat angle_Pyramid = 0.0f;
GLfloat angle_Cube = 0.0f;

FILE *gpFile = NULL;

int main(void)
{
	//function prototype
	void CreateWindow(void);
	void uninitialize(void);
	void ToggleFullScreen(void);
	//added for OpenGL
	void initialize(void);
	void resize(int, int);
	void display(void);
	void update(void);

	//variable declaration
	int windowWidth = giMyWindowWidth;
	int windowHeight = giMyWindowHeight;

	bool bDone = false; //For OpenGL
	
	string fileName = "OGL.txt" ; 
	gpFile = fopen(fileName.c_str() , "w" );
	if(gpFile == NULL)
	{
		printf("File Can Not Be Created\n" );
		exit(0);
	}

	fprintf(gpFile, "File %s is created successfully\n", fileName.c_str());

	fprintf(gpFile, "Before Calling CreateWindow\n" );	
	CreateWindow();
	fprintf(gpFile, "After calling CreateWindow\n" );

	fprintf(gpFile, "Before Calling initialize\n" );	
	initialize(); //For OpenGL
	fprintf(gpFile, "After Calling initialize\n" );	

	XEvent myEvent;
	KeySym myKeysym;

	fprintf(gpFile, "Before Entering into Gameloop\n" );	
	while(bDone == false)
	{
//		fprintf(gpFile, "Inside First while loop of GameLoop\n" );	
		while(XPending(gpMyDisplay)) // it is blocking call 
	//its phylosophy with PeekMsg is parallel but functionality wise different
//PeekMsg : NonBlocking API
		{
//			fprintf(gpFile, "Inside Second while loop of GameLoop\n" );	
			XNextEvent(gpMyDisplay, &myEvent);
			//1:Connection to Xserver
			//2:return Match Event associated structure
			switch(myEvent.type)
			{
				fprintf(gpFile, "Entering into switch case of Events\n" );	
				case MapNotify: //WM_CREATE	
				fprintf(gpFile, "Inside switch case of MapNotify\n" );	
		 		break;
				case KeyPress:
				fprintf(gpFile, "Inside switch case of KeyPress Event\n" );	
				myKeysym = XkbKeycodeToKeysym(gpMyDisplay, myEvent.xkey.keycode, 0, 0); 
		//1:ConnectionTo XServer
		//2:KeyOf Interest
		//3:Group Of Interest
		//4:Shift LevelOf Interest
		
				switch(myKeysym)
				{	
					fprintf(gpFile, "Entering into switch case of Keyboard Events\n" );	
					case XK_Escape:
						fprintf(gpFile, "XK_Escape keyboard event get called\n" );	
						bDone = true; //For OpenGL
						break;
					case XK_F:
					case XK_f:
						fprintf(gpFile, "key `f` is pressed for fullscreen\n" );	
					if(bFullScreen == false)
					{
						fprintf(gpFile, "previously bFullScreen is false..Before calling ToggleFullScreen\n" );	
						ToggleFullScreen();
						fprintf(gpFile, "After calling ToggleFullScreen..Now Window is become FullScreen\n" );	
						bFullScreen = true;
					}
					else
					{
						fprintf(gpFile, "previously bFullScreen is true..Before calling ToggleFullScreen\n" );	
						ToggleFullScreen();
						fprintf(gpFile, "After calling ToggleFullScreen..Now Window is in Normal mode\n" );	
						bFullScreen = false;
					}
					break;
					default:
						break;
				}
				break;
				case ButtonPress:
				fprintf(gpFile, "In ButtonPress Event\n");
				switch(myEvent.xbutton.button)
				{
					case 1://L
						fprintf(gpFile, "Left mouse button get pressed\n");
						break;
					case 2://M
						fprintf(gpFile, "Middle mouse button get pressed\n");
						break;
					case 3://R
						fprintf(gpFile, "Right mouse button get pressed\n");
						break;
					default:
						break;
				}
				break;
				case MotionNotify://mouse
				fprintf(gpFile, "Mouse Motion Event get Called\n");
				break;
				case ConfigureNotify://WM_SIZE
				fprintf(gpFile, "Configure Notify Ebent get called.\n");
				windowWidth = myEvent.xconfigure.width;
				windowHeight = myEvent.xconfigure.height;
				resize(windowWidth, windowHeight); //For OpenGL
				break;
				case Expose://WM_PAINT
				fprintf(gpFile, "Expose event get called.\n");
				break;
				case DestroyNotify://WM_DESTROY
				fprintf(gpFile, "Destroy Notify event get pressed\n");
				break;
				case 33://Window Cross Button
				fprintf(gpFile, "Cross Button get pressed\n");
				bDone = true;
				break;
				default:
				break;
			}
		}
		update();
		display(); //OpenGL single buffer
	}
	return 0;
}

void CreateWindow(void)
{
	fprintf(gpFile, "Inside CreateWindow function\n");
	//function prototype
	void uninitialize(void);

	static int framebufferAttribs[] = {
			GLX_RGBA,
			GLX_DOUBLEBUFFER, True, //Double Buffer
			GLX_RED_SIZE, 8,
			GLX_GREEN_SIZE, 8,
			GLX_BLUE_SIZE, 8,
			GLX_ALPHA_SIZE, 8,
			GLX_DEPTH_SIZE, 24,
		/*	GLX_X_RENDERABLE, True,
			GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
			GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
			GLX_RENDER_TYPE, GLX_RGBA_BIT,*/
			None
	};//OpenGL Single Buffer

	//13 Steps
	fprintf(gpFile, "Calling XOpenDisplay function\n");
	gpMyDisplay = XOpenDisplay(NULL);//Set some value to Parameter when u want to do some Remote Connection : NULL: Default Display pointer(127.0.0.1)
	if(gpMyDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display..\nExiting Now...\n" );
		uninitialize();
		exit(1);
	}
	
	//First Allocate Memory	Only For Normal Window	
/*	gpMyXVisualInfo = (XVisualInfo*) malloc(sizeof(XVisualInfo));
	if(gpMyXVisualInfo == NULL)
	{
		printf("ERROR : Unable To Allocate Memory For Visual Info.\nExiting Now...\n");
		uninitialize();
		exit(1);
	}*/
	
	int myDefaultScreen;
	fprintf(gpFile, "Calling XDefaultScreen function\n");
	myDefaultScreen = XDefaultScreen(gpMyDisplay);
	
	int myDefaultDepth ;
	fprintf(gpFile, "Calling DefaultDepth function\n");
	myDefaultDepth = DefaultDepth(gpMyDisplay, myDefaultScreen);

	//Similar to ChoosPixelFormat For Normal Window
/*	if(XMatchVisualInfo(gpMyDisplay, myDefaultScreen, myDefaultDepth, TrueColor, gpMyXVisualInfo) == 0)
	{
		if(gpMyXVisualInfo==NULL)
		{
			printf("ERROR : Unable To Get A Visual...\nExiting Now..\n");
			uninitialize();
			exit(1);
		}
	}
	*/

	//allocate memory for XVisualInfo and XMatchVisualInfo here there is only single Function --> glXChooseVisual  ( OpenGL Single Buffer)
	fprintf(gpFile, "Calling glXChooseVisual function\n");
	gpMyXVisualInfo = glXChooseVisual(gpMyDisplay, myDefaultScreen, 
					framebufferAttribs);

	XSetWindowAttributes winMyAttribs;
	winMyAttribs.border_pixel = 0;
	winMyAttribs.background_pixmap = 0;
	fprintf(gpFile, "Calling XCreateColormap function\n");
	winMyAttribs.colormap = XCreateColormap(gpMyDisplay,
		RootWindow(gpMyDisplay, gpMyXVisualInfo->screen),//like Init --parent
		gpMyXVisualInfo->visual, AllocNone);//allocNone for No child..If we want to set memory for child windows set this parameter
	
	gMyColormap = winMyAttribs.colormap;
	
	fprintf(gpFile, "Calling BlackPixel function\n");
	winMyAttribs.background_pixel = BlackPixel(gpMyDisplay, myDefaultScreen);
	
	winMyAttribs.event_mask = ExposureMask | //Expose
					VisibilityChangeMask | //WM_MapNotify
					ButtonPressMask | //buttonPress
					KeyPressMask | //Key_Press
					PointerMotionMask | //Motion
					StructureNotifyMask; //ConfigureNotify
	int styleMask;	
	styleMask =  CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	fprintf(gpFile, "Calling XCreateWindow function\n");
	gMyWindow = XCreateWindow(gpMyDisplay, 
				RootWindow(gpMyDisplay, gpMyXVisualInfo->screen),
				0, //x
				0, //y
				giMyWindowWidth,
				giMyWindowHeight,
				0,//borderWidth
				gpMyXVisualInfo->depth,
				InputOutput,
				gpMyXVisualInfo->visual,
				styleMask,//Specifies which window attributes are defined in the attributes argument. 
				&winMyAttribs);//Specifies the structure from which the values (as specified by the value mask) are to be taken. The value mask should have the appropriate bits set to indicate which attributes have been set in the structure.
	if(!gMyWindow)
	{
		printf("ERROR: Failed to Create Main Window..\n Exiting now...\n" );
		uninitialize();
		exit(1);
	}

	fprintf(gpFile, "Calling XStoreName function\n");
	XStoreName(gpMyDisplay, gMyWindow, "My First XWindow" );
	
	//for Handling cross (33) on Window
	//Atom is immutable processed strings
	fprintf(gpFile, "Calling XInternAtom function\n");
	Atom windowManagerDelete = XInternAtom(gpMyDisplay, "WM_DELETE_WINDOW" , True);//True:Always Create Atom
	
	fprintf(gpFile, "Calling XSetWMProtocols function\n");
	XSetWMProtocols(gpMyDisplay, gMyWindow, &windowManagerDelete, 1);

	fprintf(gpFile, "Calling XMapWindow function\n");
	XMapWindow(gpMyDisplay, gMyWindow);
}

void initialize(void)
{
	void resize(int,int);
	//similar to windows wglCreateContext
	fprintf(gpFile, "Calling glXCreateContext function\n");
	gGLXMyContext = glXCreateContext(gpMyDisplay, gpMyXVisualInfo,
					 NULL, //shareable context
					 GL_TRUE); //rendering done through direct connection to graphics system (TRUE) or through XServer (False)
	
	fprintf(gpFile, "Calling glXMakeCurrent function\n");
	glXMakeCurrent(gpMyDisplay, gMyWindow, gGLXMyContext);
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	fprintf(gpFile, "Calling resize function\n");
	resize(giMyWindowWidth, giMyWindowHeight);	
}

void display(void)
{
	void DrawMultiColoredCube();
	void DrawMultiColoredPyramid();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5f, 0.0f, -6.0f);
	glRotatef(angle_Pyramid, 0.0f, 1.0f, 0.0f);
	fprintf(gpFile, "Calling DrawMultiColoredPramid function\n");
	DrawMultiColoredPyramid();
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 0.0f, -6.0f);
	glScalef(0.75f, 0.75f, 0.75f);
	glRotatef(angle_Cube, 1.0f, 1.0f, 1.0f);
	fprintf(gpFile, "Calling DrawMultiColoredCube function\n");
	DrawMultiColoredCube();	
//	glFlush();it is for single buffer

	glXSwapBuffers(gpMyDisplay, gMyWindow); //for Double Buffer 
}

void update(void)
{
	fprintf(gpFile, "Entering into Update function\n");
	angle_Pyramid = angle_Pyramid + 0.1f;
	if (angle_Pyramid >= 360.0f)
		angle_Pyramid = 0.0f;

	angle_Cube = angle_Cube + 0.1f;
	if (angle_Cube >= 360.0f)
		angle_Cube = 0.0f;
	fprintf(gpFile, "Leaving From Update function\n");
}

void DrawMultiColoredPyramid()
{
	fprintf(gpFile, "Entering into DrawMultiColoredPyramid function\n");
	glBegin(GL_TRIANGLES);

	//front face

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//RIGHT Face
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//Back Face
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//LEFT face

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glEnd();
	fprintf(gpFile, "Leaving from DrawMultiColoredPyramid function\n");
}	
void DrawMultiColoredCube()
{
	fprintf(gpFile, "Entering into DrawMultiColoredCube function\n");
	//for triangle
	glBegin(GL_QUADS);
	//TOP face
	glColor3f(1.0f, 0.0f, 0.0f);//red
	glVertex3f(1.0f, 1.0f, -1.0f); //right top
	glVertex3f(-1.0f, 1.0f, -1.0f); //left top
	glVertex3f(-1.0f, 1.0f, 1.0f); //left bottom of top
	glVertex3f(1.0f, 1.0f, 1.0f); //right bottom
								  //Bottom face
	glColor3f(0.0f, 1.0f, 0.0f);//green
	glVertex3f(1.0f, -1.0f, -1.0f); //right bottom
	glVertex3f(-1.0f, -1.0f, -1.0f); //left bottom
	glVertex3f(-1.0f, -1.0f, 1.0f); //left bottom of top
	glVertex3f(1.0f, -1.0f, 1.0f); //right bottom
								   //front
	glColor3f(0.0f, 0.0f, 1.0f);//blue
	glVertex3f(1.0f, 1.0f, 1.0f); //right-top corner of front face
	glVertex3f(-1.0f, 1.0f, 1.0f); //left-top corner of front face
	glVertex3f(-1.0f, -1.0f, 1.0f); //left-bottom corner of front face
	glVertex3f(1.0f, -1.0f, 1.0f); //right-bottom corner of front
								   //BACK FACE
	glColor3f(0.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(1.0f, 1.0f, -1.0f); //right-top of back face
	glVertex3f(-1.0f, 1.0f, -1.0f); //left-top of back face
	glVertex3f(-1.0f, -1.0f, -1.0f); //left-bottom of back face
	glVertex3f(1.0f, -1.0f, -1.0f); //right-bottom of back face
									//RIGHT FACE
	glColor3f(1.0f, 0.0f, 1.0f); //MAGENTA
	glVertex3f(1.0f, 1.0f, -1.0f); //right-top of right face
	glVertex3f(1.0f, 1.0f, 1.0f); //left-top of right face
	glVertex3f(1.0f, -1.0f, 1.0f); //left-bottom of right face
	glVertex3f(1.0f, -1.0f, -1.0f); //right-bottom of right face
									//LEFT FACE
	glColor3f(1.0f, 1.0f, 0.0f); //YELLOW
	glVertex3f(-1.0f, 1.0f, 1.0f); //right-top of left face
	glVertex3f(-1.0f, 1.0f, -1.0f); //left-top of left face
	glVertex3f(-1.0f, -1.0f, -1.0f); //left-bottom of left face
	glVertex3f(-1.0f, -1.0f, 1.0f); //right-bottom of left face

	glEnd();
	fprintf(gpFile, "Leaving from DrawMultiColoredCube function\n");
}
void resize(int width, int height)
{
	fprintf(gpFile, "Entering into resize function\n");
	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	if(width == 0)
		width = 1;
	
	gluPerspective(45.0f, ((GLfloat)width / (GLfloat)height),0.1f, 100.0f);	
	fprintf(gpFile, "Leaving from DrawMultiColoredCube function\n");
}

void uninitialize(void)
{
	fprintf(gpFile, "Entering into uninitialize function\n");
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	
	if(currentGLXContext != NULL && currentGLXContext == gGLXMyContext)
	{
		glXMakeCurrent(gpMyDisplay, 0, 0); //release context
	}

	if(gGLXMyContext)
	{
		glXDestroyContext(gpMyDisplay, gGLXMyContext);
	}
	if(gMyWindow)
	{
		XDestroyWindow(gpMyDisplay, gMyWindow);
	}
	if(gMyColormap)
	{
		XFreeColormap(gpMyDisplay, gMyColormap);
	}
	if(gpMyXVisualInfo)
	{
		free(gpMyXVisualInfo);
		gpMyXVisualInfo = NULL;
	}
	if(gpMyDisplay)
	{
		XCloseDisplay(gpMyDisplay);
		gpMyDisplay = NULL;
	}
	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
	fprintf(gpFile, "Leaving from Uninitialize function\n");
}
void ToggleFullScreen()
{
	fprintf(gpFile, "Entering into ToggleFullScreen function\n");
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	fprintf(gpFile, "calling XInternAtom function.. for _NET_WM_STATE\n");
	wm_state = XInternAtom(gpMyDisplay, "_NET_WM_STATE" , False);
	memset(&xev, 0, sizeof(xev));
	
	xev.type = ClientMessage;
	xev.xclient.window = gMyWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format =32;
	xev.xclient.data.l[0] = bFullScreen ? 0: 1;

	fprintf(gpFile, "calling XInternAtom function.. for _NET_WM_STATE_FULLSCREEN\n");
	fullscreen = XInternAtom(gpMyDisplay, "_NET_WM_STATE_FULLSCREEN" , False);
	xev.xclient.data.l[1] = fullscreen;

	fprintf(gpFile, "calling XSendEvent function..\n");
	XSendEvent(gpMyDisplay, 
			RootWindow(gpMyDisplay, gpMyXVisualInfo->screen),
			False,//Only Send to Self And Not to child Windows
			StructureNotifyMask,
			&xev);
	fprintf(gpFile, "Leave from ToggleFullScreen function\n");
}


	

