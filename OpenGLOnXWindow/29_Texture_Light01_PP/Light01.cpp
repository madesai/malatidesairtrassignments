#include<iostream>
#include<stdio.h>
#include<stdlib.h>//exit
#include<memory.h>//memset

#include<X11/Xlib.h>
#include<X11/Xutil.h>//for XVisualInfo structure
#include<X11/XKBlib.h>//Keyboard related functions
#include<X11/keysym.h> //for keysym

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

enum
{
	MALATI_ATTRIBUTE_VERTEX = 0,
	MALATI_ATTRIBUTE_COLOR,
	MALATI_ATTRIBUTE_NORMAL,
	MALATI_ATTRIBUTE_TEXTURE
};

bool bFullScreen = false;

Display *gpMyDisplay = NULL;
XVisualInfo *gpMyXVisualInfo = NULL;
Colormap gMyColormap;
Window gMyWindow;

int giMyWindowWidth = 800;
int giMyWindowHeight = 600;

FILE *gpFile = NULL;

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);

glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;

GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext; 

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_Cube;//displayList
GLuint gVbo_Cube_Position;
GLuint gVbo_Cube_normal;

GLuint gModelViewMatrixUniform, gProjectionMatrixUniform;
GLuint gLdUniform, gKdUniform, gLightPositionUniform;

GLuint gLKeyPressedUniform;

mat4 gPerspectiveProjectionMatrix;

GLfloat gAngle = 0.0f;
bool gbAnimate;
bool gbLight;

int main(void)
{
	//function prototype
	void CreateWindow(void);
	void uninitialize(void);
	void ToggleFullScreen(void);
	
	void initialize(void);
	void resize(int, int);
	void display(void);
	void Update(void);

	int windowWidth = giMyWindowWidth;
	int windowHeight = giMyWindowHeight;

	static bool bIsAKeyPressed = false;
        static bool bIsLKeyPressed = false;

	gpFile = fopen("LOG.txt" , "w" );
	if(gpFile == NULL)
	{
		printf("Log File Can't be created..Exiting Now..\n");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File is SuccessFully Opened..\n" );
	}
	 
	CreateWindow();

	initialize();

	XEvent myEvent;
	KeySym myKeysym;
	bool bDone = false;

	while(bDone == false)
	{
		while(XPending(gpMyDisplay))
		{
			XNextEvent(gpMyDisplay, &myEvent);
		//1:Connection to Xserver
		//2:return Match Event associated structure
			switch(myEvent.type)
			{
				case MapNotify: //WM_CREATE	
		 			break;
				case KeyPress:
					myKeysym = XkbKeycodeToKeysym(gpMyDisplay, myEvent.xkey.keycode, 0, 0); 
		//1:ConnectionTo XServer
		//2:KeyOf Interest
		//3:Group Of Interest
		//4:Shift LevelOf Interest
		
				switch(myKeysym)
				{
					case XK_Escape:
						bDone = true;
						break;
					case XK_F:
					case XK_f:
						if(bFullScreen == false)
						{
							ToggleFullScreen();
							bFullScreen = true;
						}
						else
						{
							ToggleFullScreen();
							bFullScreen = false;
						}
						break;
					case XK_A:
					case XK_a:
                        			if (bIsAKeyPressed == false)
                        			{
                                			gbAnimate = true;
                                			bIsAKeyPressed = true;
                        			}
                        			else
                        			{
                                			gbAnimate = false;
                                			bIsAKeyPressed = false;
                        			}
                        			break;
                			case XK_L:
                			case XK_l:
                        			if (bIsLKeyPressed == false)
                        			{
                                			gbLight = true;
                                			bIsLKeyPressed = true;
                        			}
                        			else
                        			{
                                			gbLight = false;
                                			bIsLKeyPressed = false;
                        			}
                        			break;
					default:
						break;
				}
				break;
				case ButtonPress:
					switch(myEvent.xbutton.button)
					{
						case 1://L
						break;
						case 2://M
						break;
						case 3://R
						break;
						default:
						break;
					}
					break;
				case MotionNotify://mouse
				break;
				case ConfigureNotify://WM_SIZE
					windowWidth = myEvent.xconfigure.width;
					windowHeight = myEvent.xconfigure.height;
					resize(windowWidth, windowHeight);
					break;
				case Expose://WM_PAINT
					break;
				case DestroyNotify://WM_DESTROY
					break;
				case 33://Window Cross Button
					bDone = true;
					break;
				default:
					break;
			}
		}
	if(gbAnimate == true)
		Update();		
	display();
	
	}
	uninitialize();
	return 0;
}
void CreateWindow(void)
{
	//function prototype
	void uninitialize(void);

	GLXFBConfig *pGLXFBConfigs = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumFBConfigs = 0;
	int i;

	static int frameBufferAttributes[] = {
		GLX_X_RENDERABLE, True, //Video Rendering
		GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT, //who is drawable (window)
		GLX_RENDER_TYPE, GLX_RGBA_BIT,//frame which type
		GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, //similar to XMAtchVisualInfo-->true color
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DEPTH_SIZE, 8,
		GLX_STENCIL_SIZE , 8,
		GLX_DOUBLEBUFFER, True,
		//GLX_SAMPLE_BUFFER, 1, 
		//GLX_SAMPLES, 4,
		None}; //array
	//13 Steps
	gpMyDisplay = XOpenDisplay(NULL);//Set some value to Parameter when u want to do some Remote Connection : NULL: Default Display pointer(127.0.0.1)
	if(gpMyDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display..\nExiting Now...\n" );
		uninitialize();
		exit(1);
	}
	
	pGLXFBConfigs = glXChooseFBConfig(gpMyDisplay, DefaultScreen(gpMyDisplay), frameBufferAttributes, &iNumFBConfigs);
	if(pGLXFBConfigs == NULL)
	{
		printf("Failed To Get valid Frame Buffer config. Exiting..\n" );
		uninitialize();
		exit(1);
	}
	
	//pick FB config/visual 
	int bestFramebufferconfig = -1, worstFramebufferConfig=-1, bestNumberOfSamples = -1, worstNumberOfSamples = 999;
	for(i = 0; i< iNumFBConfigs; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpMyDisplay, pGLXFBConfigs[i]);
		if(pTempXVisualInfo)
		{
			int sampleBuffer, samples;
			
			glXGetFBConfigAttrib(gpMyDisplay, pGLXFBConfigs[i], GLX_SAMPLE_BUFFERS, &sampleBuffer);
			glXGetFBConfigAttrib(gpMyDisplay, pGLXFBConfigs[i], GLX_SAMPLES, &samples);
			//printf("Matching FrameBuffer Config=%d : Visual ID=0x%lu: SAMPLE_BUFFERS=%d : SAMPLES=%d\n" , i, pTempXVisualInfo->visualid, sampleBuffer, samples);
		if(bestFramebufferconfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
		{
			bestFramebufferconfig = i;
			bestNumberOfSamples = samples;
		}
		
		if( worstFramebufferConfig < 0 || !sampleBuffer || samples < worstNumberOfSamples)
		{
			worstFramebufferConfig = i;
			worstNumberOfSamples = samples;
		}
	}
	XFree(pTempXVisualInfo);
	}
	bestGLXFBConfig = pGLXFBConfigs[bestFramebufferconfig];
	
	gGLXFBConfig = bestGLXFBConfig;

	XFree(pGLXFBConfigs);
	
	gpMyXVisualInfo = glXGetVisualFromFBConfig(gpMyDisplay, bestGLXFBConfig);
	printf("Choosen Visual ID= 0x%lu\n", gpMyXVisualInfo->visualid);

	XSetWindowAttributes winMyAttribs;
	winMyAttribs.border_pixel = 0;
	winMyAttribs.background_pixmap = 0;
	winMyAttribs.colormap = XCreateColormap(gpMyDisplay,
		RootWindow(gpMyDisplay, gpMyXVisualInfo->screen),//like Init --parent
		gpMyXVisualInfo->visual, AllocNone);//allocNone for No child..If we want to set memory for child windows set this parameter
	
	gMyColormap = winMyAttribs.colormap;
	
	winMyAttribs.event_mask = ExposureMask | //Expose
					VisibilityChangeMask | //WM_MapNotify
					ButtonPressMask | //buttonPress
					KeyPressMask | //Key_Press
					PointerMotionMask | //Motion
					StructureNotifyMask; //ConfigureNotify
	int styleMask;	
	styleMask =  CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gMyWindow = XCreateWindow(gpMyDisplay, 
				RootWindow(gpMyDisplay, gpMyXVisualInfo->screen),
				0, //x
				0, //y
				giMyWindowWidth,
				giMyWindowHeight,
				0,//borderWidth
				gpMyXVisualInfo->depth,
				InputOutput,
				gpMyXVisualInfo->visual,
				styleMask,//Specifies which window attributes are defined in the attributes argument. 
				&winMyAttribs);//Specifies the structure from which the values (as specified by the value mask) are to be taken. The value mask should have the appropriate bits set to indicate which attributes have been set in the structure.
	if(!gMyWindow)
	{
		printf("ERROR: Failed to Create Main Window..\n Exiting now...\n" );
		uninitialize();
		exit(1);
	}

	XStoreName(gpMyDisplay, gMyWindow, "My First XWindow" );
	
	//for Handling cross (33) on Window
	//Atom is immutable processed strings
	Atom windowManagerDelete = XInternAtom(gpMyDisplay, "WM_DELETE_WINDOW" , True);//True:Always Create Atom
	
	XSetWMProtocols(gpMyDisplay, gMyWindow, &windowManagerDelete, 1);

	XMapWindow(gpMyDisplay, gMyWindow);
			
}
void initialize(void)
{
	void uninitialize(void);
	void resize(int, int);
	
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc) glXGetProcAddressARB((GLubyte*) "glXCreateContextAttribsARB");

	GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
		GLX_CONTEXT_MINOR_VERSION_ARB, 5,
		GLX_CONTEXT_PROFILE_MASK_ARB,
		GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
		0};

	gGLXContext = glXCreateContextAttribsARB(gpMyDisplay, gGLXFBConfig, 0, True, attribs);
	if(!gGLXContext)
	{
		GLint attribs[] = { 
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			0 };
		printf("Failed To Create GLX 4.5 context. Hence Using OLD GLX Context\n" );
		gGLXContext = glXCreateContextAttribsARB(gpMyDisplay, gGLXFBConfig, 0, True, attribs);
	}
	else
	{
		printf("OpenGL Context 4.5 is Created.\n");
	}
	
	if(!glXIsDirect(gpMyDisplay, gGLXContext))
	{
		printf("Indirect GLX Rendering Context Obtained\n");
	}
	else
	{
		printf("Direct GLX Rendering Context Obtained\n");
	}
	glXMakeCurrent(gpMyDisplay, gMyWindow, gGLXContext);

	GLenum glew_error = glewInit();
	if(glew_error != GLEW_OK)
	{
		glXDestroyContext(gpMyDisplay, gGLXContext);
		XDestroyWindow(gpMyDisplay, gMyWindow);
	}
		
	GLint num, i;
	glGetIntegerv(GL_NUM_EXTENSIONS, &num);
	
/*	for(i = 0; i < num; i++)
	{
		fprintf(gpFile, "%d : MALATI : %s \n" , (i+1), glGetStringi(GL_EXTENSIONS, i)   );
	}*/
	fprintf(gpFile, "%s\n", glGetString(GL_VERSION));

	fprintf(gpFile, "Shading Lang Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));


	
	//VERTEX SHADER
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const GLchar *vertextShaderSourceCode =
		"#version 450" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_LKeyPressed;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Kd;" \
		"uniform vec4 u_light_position;" \
		"out vec3 diffuse_light;" \
		"void main(void)"\
		"{"\
		"if(u_LKeyPressed == 1)" \
                "{" \
                "vec4 eyeCoordinates = u_model_view_matrix * vPosition;"\
		"vec3 tnorm = normalize(mat3(u_model_view_matrix)*vNormal);" \
                "vec3 s = normalize(vec3(u_light_position - eyeCoordinates));"\
 		"diffuse_light = u_Ld * u_Kd * max(dot(s, tnorm), 0.0);"\
		"}" \
                "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertextShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//FRAGMENT SHADER
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar *fragmentShaderSourceCode =
		"#version 450"\
		"\n" \
		"in vec3 diffuse_light;" \
                "out vec4 FragColor;" \
                "uniform int u_LKeyPressed;" \
                "void main(void)" \
                "{" \
                "vec4 color;" \
                "if(u_LKeyPressed == 1)" \
                "{" \
                "color = vec4(diffuse_light, 1.0);" \
                "}" \
                "else" \
                "{" \
                "color = vec4(1.0 ,1.0 ,1.0 ,1.0);" \
                "}" \
                "FragColor = color;" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(gFragmentShaderObject);
	
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Shader Program
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");
	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	gModelViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_view_matrix");
        gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

        gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");

        gLdUniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
        gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");

        gLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	
	//Cube
	GLfloat cubeVertices[] =
	{
		//top
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		//bottom
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		//front
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		//back
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		//right
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		//left
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f
	};
	for (int i = 0; i<72; i++)
        {
                if (cubeVertices[i]<0.0f)
                        cubeVertices[i] = cubeVertices[i] + 0.25f;
                else if (cubeVertices[i]>0.0f)
                        cubeVertices[i] = cubeVertices[i] - 0.25f;
                else
                        cubeVertices[i] = cubeVertices[i];
        }
	
	const GLfloat cubeNormals[] =
        {
                0.0f, 1.0f, 0.0f,
                0.0f, 1.0f, 0.0f,
                0.0f, 1.0f, 0.0f,
                0.0f, 1.0f, 0.0f,

                0.0f, -1.0f, 0.0f,
                0.0f, -1.0f, 0.0f,
                0.0f, -1.0f, 0.0f,
                0.0f, -1.0f, 0.0f,

                0.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f,

                0.0f, 0.0f, -1.0f,
                0.0f, 0.0f, -1.0f,
                0.0f, 0.0f, -1.0f,
                0.0f, 0.0f, -1.0f,

                -1.0f, 0.0f, 0.0f,
                -1.0f, 0.0f, 0.0f,
                -1.0f, 0.0f, 0.0f,
                -1.0f, 0.0f, 0.0f,

                1.0f, 0.0f, 0.0f,
                1.0f, 0.0f, 0.0f,
                1.0f, 0.0f, 0.0f,
                1.0f, 0.0f, 0.0f

        };

	glGenVertexArrays(1, &gVao_Cube);
	glBindVertexArray(gVao_Cube);

	//position
	glGenBuffers(1, &gVbo_Cube_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Cube_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//normal
	glGenBuffers(1, &gVbo_Cube_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Cube_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_STATIC_DRAW);
	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);


	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//color
//	glVertexAttrib3f(MALATI_ATTRIBUTE_COLOR, 0.39f, 0.58f, 0.9294f); //cornflower blue
	
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//glEnable(GL_CULL_FACE);
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	gPerspectiveProjectionMatrix = mat4::identity();
	gbAnimate = false;
        gbLight = false;
	resize(giMyWindowWidth, giMyWindowHeight);
}

void resize(int width , int height)
{
	if(height == 0)
		height =1;
	glViewport(0,0, (GLsizei)width, (GLsizei)height);
	
	gPerspectiveProjectionMatrix =  perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glUseProgram(gShaderProgramObject);
	
	if (gbLight == true)
        {
		fprintf(gpFile, "Light is true");
                glUniform1i(gLKeyPressedUniform, 1);

                glUniform3f(gLdUniform, 1.0f, 1.0f, 1.0f);//white
                glUniform3f(gKdUniform, 0.5f, 0.5f, 0.5f);//grey

                float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };
                glUniform4fv(gLightPositionUniform, 1, (GLfloat *)lightPosition);
        }
        else
        {
                glUniform1i(gLKeyPressedUniform, 0);
        }

	mat4 modelMatrix = mat4::identity();
	mat4 modelViewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(0.0f, 0.0f, -5.0f); 

	rotationMatrix = vmath::rotate(gAngle, gAngle, gAngle);
	modelViewMatrix = modelMatrix * rotationMatrix; //imp

	glUniformMatrix4fv(gModelViewMatrixUniform, 1, GL_FALSE, modelViewMatrix);
	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindVertexArray(gVao_Cube);

	//Square Blockrrays(GL_TRIANGLE_FAN, 0, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

        glBindVertexArray(0);

	glUseProgram(0);
	glXSwapBuffers(gpMyDisplay, gMyWindow);
}

void Update(void)
{
	gAngle = gAngle + 0.3f;
        if (gAngle >= 360.0f)
                gAngle = gAngle - 360.0f;
}

void uninitialize(void)
{
	if (gVao_Cube)
	{
		glDeleteVertexArrays(1, &gVao_Cube);
		gVao_Cube = 0;
	}
	if (gVbo_Cube_Position)
	{
		glDeleteBuffers(1, &gVbo_Cube_Position);
		gVbo_Cube_Position = 0;
	}
	if (gVbo_Cube_normal)
        {
                glDeleteBuffers(1, &gVbo_Cube_normal);
                gVbo_Cube_normal = 0;
        }

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gFragmentShaderObject);
	glDeleteShader(gVertexShaderObject);
	
	gVertexShaderObject = 0;
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	GLXContext currentContext = glXGetCurrentContext();
	if(currentContext != NULL && currentContext == gGLXContext)
	{
		glXMakeCurrent(gpMyDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpMyDisplay, gGLXContext);
	}
	if(gMyWindow)
	{
		XDestroyWindow(gpMyDisplay, gMyWindow);
	}
	if(gMyColormap)
	{
		XFreeColormap(gpMyDisplay, gMyColormap);
	}
	if(gpMyXVisualInfo)
	{
		free(gpMyXVisualInfo);
		gpMyXVisualInfo = NULL;
	}
	if(gpMyDisplay)
	{
		XCloseDisplay(gpMyDisplay);
		gpMyDisplay = NULL;
	}
	if(gpFile)
	{
		fprintf(gpFile, "Log File is successfully Closed\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
void ToggleFullScreen()
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state = XInternAtom(gpMyDisplay, "_NET_WM_STATE" , False);
	memset(&xev, 0, sizeof(xev));
	
	xev.type = ClientMessage;
	xev.xclient.window = gMyWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format =32;
	xev.xclient.data.l[0] = bFullScreen ? 0: 1;

	fullscreen = XInternAtom(gpMyDisplay, "_NET_WM_STATE_FULLSCREEN" , False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpMyDisplay, 
			RootWindow(gpMyDisplay, gpMyXVisualInfo->screen),
			False,//Only Send to Self And Not to child Windows
			StructureNotifyMask,
			&xev);
}


	

