#include <iostream>
#include<dlfcn.h>
#include "Server.h"
#include <cstdlib>

using namespace std;
int main(void)
{
	string path = "/home/malati/RTRAssignments/malatidesairtrassignments/OpenGLOnXWindow/DLL_SO/Server/libServer.so" ;
	
	typedef int (*SumOfTwoIntegersFn) (int, int);
	typedef int (*SubtractionOfTwoIntegersFn) (int, int);
	SumOfTwoIntegersFn pfnSum = NULL;
	SubtractionOfTwoIntegersFn pfnSub = NULL;

	void *hlib = dlopen(path.c_str(), RTLD_LAZY);
	if(!hlib)
	{
		cout << "Dll Not get Loaded" << endl;
		cout << " Error: " << dlerror() << endl;
		exit (0);
	}
	
	char *error;
	pfnSum = (SumOfTwoIntegersFn) dlsym(hlib, "SumOfTwoIntegers");
	if((error = dlerror()) != NULL)
	{
		cout <<	"ERROR in Sum: " << error << endl;
		exit (0);
	}
	int a = 10;
	int b = 20;
	int c = (*pfnSum)(a, b);
	
	cout << " Sum Of two integer is "  << c << endl;
	
	pfnSub = (SubtractionOfTwoIntegersFn) dlsym(hlib, "SubtractionOfTwoIntegers");
	if((error= dlerror()) != NULL)
	{
		cout << "ERROR in Sub: " << error << endl;
		exit (0);
	}
	int x = 20;
	int y = 10;
	int z = (*pfnSub)(x,y);
	
	cout << " Subtraction of two integers is " << z << endl;

	dlclose(hlib);	
	return 0;
}
