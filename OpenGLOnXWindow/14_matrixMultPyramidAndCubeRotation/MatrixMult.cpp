#include<iostream>
#include<stdio.h>
#include<stdlib.h>//exit
#include<memory.h>//memset
#include <math.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>//for XVisualInfo structure
#include<X11/XKBlib.h>//Keyboard related functions
#include<X11/keysym.h>

//OpenGL Change
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>//bridging api

using namespace std;

const float pie = 3.1415926535898f;

bool bFullScreen = false;

Display *gpMyDisplay = NULL;
XVisualInfo *gpMyXVisualInfo = NULL;
Colormap gMyColormap;
Window gMyWindow;

int giMyWindowWidth = 800;
int giMyWindowHeight = 600;

GLXContext gGLXMyContext; //HGLRC ghrc; in windows
//float gAngle =0.0f;

GLfloat glAngle_radian = 0.0f;
GLfloat glAngle_degree = 0.0f;

GLfloat identityMatrix[16];
//translatef
GLfloat translationMatrix[16];
//scalef
GLfloat scaleMatrix[16];
//rotatef
GLfloat xRotationMatrix[16];
GLfloat yRotationMatrix[16];
GLfloat zRotationMatrix[16];

int main(void)
{
	//function prototype
	void CreateWindow(void);
	void uninitialize(void);
	void ToggleFullScreen(void);
	//added for OpenGL
	void initialize(void);
	void resize(int, int);
	void display(void);
	void update(void);

	//variable declaration
	int windowWidth = giMyWindowWidth;
	int windowHeight = giMyWindowHeight;

	bool bDone = false; //For OpenGL
	
	CreateWindow();
	
	initialize(); //For OpenGL

	XEvent myEvent;
	KeySym myKeysym;

	while(bDone == false)
	{
		while(XPending(gpMyDisplay)) // it is blocking call 
	//its phylosophy with PeekMsg is parallel but functionality wise different
//PeekMsg : NonBlocking API
		{
			XNextEvent(gpMyDisplay, &myEvent);
			//1:Connection to Xserver
			//2:return Match Event associated structure
			switch(myEvent.type)
			{
				case MapNotify: //WM_CREATE	
		 		break;
				case KeyPress:
				myKeysym = XkbKeycodeToKeysym(gpMyDisplay, myEvent.xkey.keycode, 0, 0); 
		//1:ConnectionTo XServer
		//2:KeyOf Interest
		//3:Group Of Interest
		//4:Shift LevelOf Interest
		
				switch(myKeysym)
				{	
					case XK_Escape:
						bDone = true; //For OpenGL
						break;
					case XK_F:
					case XK_f:
					if(bFullScreen == false)
					{
						ToggleFullScreen();
						bFullScreen = true;
					}
					else
					{
						ToggleFullScreen();
						bFullScreen = false;
					}
					break;
					default:
						break;
				}
				break;
				case ButtonPress:
				switch(myEvent.xbutton.button)
				{
					case 1://L
						break;
					case 2://M
						break;
					case 3://R
						break;
					default:
						break;
				}
				break;
				case MotionNotify://mouse
				break;
				case ConfigureNotify://WM_SIZE
				windowWidth = myEvent.xconfigure.width;
				windowHeight = myEvent.xconfigure.height;
				resize(windowWidth, windowHeight); //For OpenGL
				break;
				case Expose://WM_PAINT
				break;
				case DestroyNotify://WM_DESTROY
				break;
				case 33://Window Cross Button
				bDone = true;
				break;
				default:
				break;
			}
		}
		update();
		display(); //OpenGL single buffer
	}
	return 0;
}

void CreateWindow(void)
{
	//function prototype
	void uninitialize(void);

	static int framebufferAttribs[] = {
			GLX_RGBA,
			GLX_DOUBLEBUFFER, True,  //Double Buffer
			GLX_RED_SIZE, 8,
			GLX_GREEN_SIZE, 8,
			GLX_BLUE_SIZE, 8,
			GLX_ALPHA_SIZE, 8,
			GLX_DEPTH_SIZE, 24,
		/*	GLX_X_RENDERABLE, True,
			GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
			GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
			GLX_RENDER_TYPE, GLX_RGBA_BIT,*/
			None
	};//OpenGL Single Buffer

	//13 Steps
	gpMyDisplay = XOpenDisplay(NULL);//Set some value to Parameter when u want to do some Remote Connection : NULL: Default Display pointer(127.0.0.1)
	if(gpMyDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display..\nExiting Now...\n" );
		uninitialize();
		exit(1);
	}
	
	//First Allocate Memory	Only For Normal Window	
/*	gpMyXVisualInfo = (XVisualInfo*) malloc(sizeof(XVisualInfo));
	if(gpMyXVisualInfo == NULL)
	{
		printf("ERROR : Unable To Allocate Memory For Visual Info.\nExiting Now...\n");
		uninitialize();
		exit(1);
	}*/
	
	int myDefaultScreen;
	myDefaultScreen = XDefaultScreen(gpMyDisplay);
	
	int myDefaultDepth ;
	myDefaultDepth = DefaultDepth(gpMyDisplay, myDefaultScreen);

	//Similar to ChoosPixelFormat For Normal Window
/*	if(XMatchVisualInfo(gpMyDisplay, myDefaultScreen, myDefaultDepth, TrueColor, gpMyXVisualInfo) == 0)
	{
		if(gpMyXVisualInfo==NULL)
		{
			printf("ERROR : Unable To Get A Visual...\nExiting Now..\n");
			uninitialize();
			exit(1);
		}
	}
	*/

	//allocate memory for XVisualInfo and XMatchVisualInfo here there is only single Function --> glXChooseVisual  ( OpenGL Single Buffer)
	gpMyXVisualInfo = glXChooseVisual(gpMyDisplay, myDefaultScreen, 
					framebufferAttribs);

	XSetWindowAttributes winMyAttribs;
	winMyAttribs.border_pixel = 0;
	winMyAttribs.background_pixmap = 0;
	winMyAttribs.colormap = XCreateColormap(gpMyDisplay,
		RootWindow(gpMyDisplay, gpMyXVisualInfo->screen),//like Init --parent
		gpMyXVisualInfo->visual, AllocNone);//allocNone for No child..If we want to set memory for child windows set this parameter
	
	gMyColormap = winMyAttribs.colormap;
	
	winMyAttribs.background_pixel = BlackPixel(gpMyDisplay, myDefaultScreen);
	
	winMyAttribs.event_mask = ExposureMask | //Expose
					VisibilityChangeMask | //WM_MapNotify
					ButtonPressMask | //buttonPress
					KeyPressMask | //Key_Press
					PointerMotionMask | //Motion
					StructureNotifyMask; //ConfigureNotify
	int styleMask;	
	styleMask =  CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gMyWindow = XCreateWindow(gpMyDisplay, 
				RootWindow(gpMyDisplay, gpMyXVisualInfo->screen),
				0, //x
				0, //y
				giMyWindowWidth,
				giMyWindowHeight,
				0,//borderWidth
				gpMyXVisualInfo->depth,
				InputOutput,
				gpMyXVisualInfo->visual,
				styleMask,//Specifies which window attributes are defined in the attributes argument. 
				&winMyAttribs);//Specifies the structure from which the values (as specified by the value mask) are to be taken. The value mask should have the appropriate bits set to indicate which attributes have been set in the structure.
	if(!gMyWindow)
	{
		printf("ERROR: Failed to Create Main Window..\n Exiting now...\n" );
		uninitialize();
		exit(1);
	}

	XStoreName(gpMyDisplay, gMyWindow, "My First XWindow" );
	
	//for Handling cross (33) on Window
	//Atom is immutable processed strings
	Atom windowManagerDelete = XInternAtom(gpMyDisplay, "WM_DELETE_WINDOW" , True);//True:Always Create Atom
	
	XSetWMProtocols(gpMyDisplay, gMyWindow, &windowManagerDelete, 1);

	XMapWindow(gpMyDisplay, gMyWindow);
}

void initialize(void)
{
	void resize(int,int);
	//similar to windows wglCreateContext
	gGLXMyContext = glXCreateContext(gpMyDisplay, gpMyXVisualInfo,
					 NULL, //shareable context
					 GL_TRUE); //rendering done through direct connection to graphics system (TRUE) or through XServer (False)
	
	glXMakeCurrent(gpMyDisplay, gMyWindow, gGLXMyContext);
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//identityMatrix initialisation

	identityMatrix[0] = 1.0f;
	identityMatrix[1] = 0.0f;
	identityMatrix[2] = 0.0f;
	identityMatrix[3] = 0.0f;
	identityMatrix[4] = 0.0f;
	identityMatrix[5] = 1.0f;
	identityMatrix[6] = 0.0f;
	identityMatrix[7] = 0.0f;
	identityMatrix[8] = 0.0f;
	identityMatrix[9] = 0.0f;
	identityMatrix[10] = 1.0f;
	identityMatrix[11] = 0.0f;
	identityMatrix[12] = 0.0f;
	identityMatrix[13] = 0.0f;
	identityMatrix[14] = 0.0f;
	identityMatrix[15] = 1.0f;

	//TranslationMatrix initialisation

	translationMatrix[0] = 1.0f;
	translationMatrix[1] = 0.0f;
	translationMatrix[2] = 0.0f;
	translationMatrix[3] = 0.0f;
	translationMatrix[4] = 0.0f;
	translationMatrix[5] = 1.0f;
	translationMatrix[6] = 0.0f;
	translationMatrix[7] = 0.0f;
	translationMatrix[8] = 0.0f;
	translationMatrix[9] = 0.0f;
	translationMatrix[10] = 1.0f;
	translationMatrix[11] = 0.0f;
	translationMatrix[12] = 0.0f;
	translationMatrix[13] = 0.0f;
	translationMatrix[14] = -6.0f;
	translationMatrix[15] = 1.0f;

	//Scale Matrix initialisation

	scaleMatrix[0] = 0.75f;
	scaleMatrix[1] = 0;
	scaleMatrix[2] = 0;
	scaleMatrix[3] = 0;
	scaleMatrix[4] = 0;
	scaleMatrix[5] = 0.75f;
	scaleMatrix[6] = 0;
	scaleMatrix[7] = 0;
	scaleMatrix[8] = 0;
	scaleMatrix[9] = 0;
	scaleMatrix[10] = 0.75f;
	scaleMatrix[11] = 0;
	scaleMatrix[12] = 0;
	scaleMatrix[13] = 0;
	scaleMatrix[14] = 0;
	scaleMatrix[15] = 1.0f;

	resize(giMyWindowWidth, giMyWindowHeight);	
}

void display(void)
{
	void DrawMultiColoredCube(void);

	//xRotationMatrix initialization
	xRotationMatrix[0] = 1.0f;
	xRotationMatrix[1] = 0;
	xRotationMatrix[2] = 0;
	xRotationMatrix[3] = 0;
	xRotationMatrix[4] = 0;
	xRotationMatrix[5] = cos(glAngle_radian);
	xRotationMatrix[6] = sin(glAngle_radian);
	xRotationMatrix[7] = 0;
	xRotationMatrix[8] = 0;
	xRotationMatrix[9] =-sin(glAngle_radian);
	xRotationMatrix[10] = cos(glAngle_radian);
	xRotationMatrix[11] = 0;
	xRotationMatrix[12] = 0;
	xRotationMatrix[13] = 0;
	xRotationMatrix[14] = 0;
	xRotationMatrix[15] = 1.0f;
	
	//yRotationMatrix initialization

	yRotationMatrix[0] = cos(glAngle_radian);
	yRotationMatrix[1] = 0;
	yRotationMatrix[2] = -sin(glAngle_radian);
	yRotationMatrix[3] = 0;
	yRotationMatrix[4] = 0;
	yRotationMatrix[5] = 1;
	yRotationMatrix[6] = 0;
	yRotationMatrix[7] = 0;
	yRotationMatrix[8] = sin(glAngle_radian);
	yRotationMatrix[9] = 0;
	yRotationMatrix[10] = cos(glAngle_radian);
	yRotationMatrix[11] = 0;
	yRotationMatrix[12] = 0;
	yRotationMatrix[13] = 0;
	yRotationMatrix[14] = 0;
	yRotationMatrix[15] = 1.0f;

	//zRotationMatrix initialization
	zRotationMatrix[0] = cos(glAngle_radian);
	zRotationMatrix[1] = sin(glAngle_radian);
	zRotationMatrix[2] = 0;
	zRotationMatrix[3] = 0;
	zRotationMatrix[4] = -sin(glAngle_radian);
	zRotationMatrix[5] = cos(glAngle_radian);
	zRotationMatrix[6] = 0;
	zRotationMatrix[7] = 0;
	zRotationMatrix[8] = 0;
	zRotationMatrix[9] = 0;
	zRotationMatrix[10] = 1.0f;
	zRotationMatrix[11] = 0;
	zRotationMatrix[12] = 0;
	zRotationMatrix[13] = 0;
	zRotationMatrix[14] = 0;
	zRotationMatrix[15] = 1.0f;
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();

	glLoadMatrixf(identityMatrix);
	DrawMultiColoredCube();
//	glFlush();it is for single buffer

	glXSwapBuffers(gpMyDisplay, gMyWindow); //for Double Buffer 
}


void DrawMultiColoredCube()
{
	//for triangle
	//glTranslatef(0.0f, 0.0f, -6.0f); removed
	glMultMatrixf(translationMatrix);
	glAngle_radian = glAngle_degree * (M_PI/180.0f);
	glMultMatrixf(xRotationMatrix);
	glMultMatrixf(yRotationMatrix);
	glMultMatrixf(zRotationMatrix);

	//glRotatef(glAngle_degree, 1.0f, 1.0f, 1.0f);//removed

	glBegin(GL_QUADS);
	//TOP face
	glColor3f(1.0f, 0.0f, 0.0f);//red
	glVertex3f(1.0f, 1.0f, -1.0f); //right top
	glVertex3f(-1.0f, 1.0f, -1.0f); //left top
	glVertex3f(-1.0f, 1.0f, 1.0f); //left bottom of top
	glVertex3f(1.0f, 1.0f, 1.0f); //right bottom

	//Bottom face
	glColor3f(0.0f, 1.0f, 0.0f);//green
	glVertex3f(1.0f, -1.0f, -1.0f); //right bottom
	glVertex3f(-1.0f, -1.0f, -1.0f); //left bottom
	glVertex3f(-1.0f, -1.0f, 1.0f); //left bottom of top
	glVertex3f(1.0f, -1.0f, 1.0f); //right bottom
	//front

	glColor3f(0.0f, 0.0f, 1.0f);//blue
	glVertex3f(1.0f, 1.0f, 1.0f); //right-top corner of front face
	glVertex3f(-1.0f, 1.0f, 1.0f); //left-top corner of front face
	glVertex3f(-1.0f, -1.0f, 1.0f); //left-bottom corner of front face
	glVertex3f(1.0f, -1.0f, 1.0f); //right-bottom corner of front
	//BACK FACE

	glColor3f(0.0f, 1.0f, 1.0f); //CYAN
	glVertex3f(1.0f, 1.0f, -1.0f); //right-top of back face
	glVertex3f(-1.0f, 1.0f, -1.0f); //left-top of back face
	glVertex3f(-1.0f, -1.0f, -1.0f); //left-bottom of back face
	glVertex3f(1.0f, -1.0f, -1.0f); //right-bottom of back face

	//RIGHT FACE

	glColor3f(1.0f, 0.0f, 1.0f); //MAGENTA
	glVertex3f(1.0f, 1.0f, -1.0f); //right-top of right face
	glVertex3f(1.0f, 1.0f, 1.0f); //left-top of right face
	glVertex3f(1.0f, -1.0f, 1.0f); //left-bottom of right face
	glVertex3f(1.0f, -1.0f, -1.0f); //right-bottom of right face

	//LEFT FACE

	glColor3f(1.0f, 1.0f, 0.0f); //YELLOW
	glVertex3f(-1.0f, 1.0f, 1.0f); //right-top of left face
	glVertex3f(-1.0f, 1.0f, -1.0f); //left-top of left face
	glVertex3f(-1.0f, -1.0f, -1.0f); //left-bottom of left face
	glVertex3f(-1.0f, -1.0f, 1.0f); //right-bottom of left face
	glEnd();
}


void update(void)
{
	//code
	glAngle_degree = glAngle_degree + 0.1f;
	if (glAngle_degree >= 360.0f)
		glAngle_degree = 0.0f;
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	if(width == 0)
		width = 1;
	
	gluPerspective(45.0f, ((GLfloat)width / (GLfloat)height),0.1f, 100.0f);	

}

void uninitialize(void)
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	
	if(currentGLXContext != NULL && currentGLXContext == gGLXMyContext)
	{
		glXMakeCurrent(gpMyDisplay, 0, 0); //release context
	}

	if(gGLXMyContext)
	{
		glXDestroyContext(gpMyDisplay, gGLXMyContext);
	}
	if(gMyWindow)
	{
		XDestroyWindow(gpMyDisplay, gMyWindow);
	}
	if(gMyColormap)
	{
		XFreeColormap(gpMyDisplay, gMyColormap);
	}
	if(gpMyXVisualInfo)
	{
		free(gpMyXVisualInfo);
		gpMyXVisualInfo = NULL;
	}
	if(gpMyDisplay)
	{
		XCloseDisplay(gpMyDisplay);
		gpMyDisplay = NULL;
	}
}
void ToggleFullScreen()
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state = XInternAtom(gpMyDisplay, "_NET_WM_STATE" , False);
	memset(&xev, 0, sizeof(xev));
	
	xev.type = ClientMessage;
	xev.xclient.window = gMyWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format =32;
	xev.xclient.data.l[0] = bFullScreen ? 0: 1;

	fullscreen = XInternAtom(gpMyDisplay, "_NET_WM_STATE_FULLSCREEN" , False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpMyDisplay, 
			RootWindow(gpMyDisplay, gpMyXVisualInfo->screen),
			False,//Only Send to Self And Not to child Windows
			StructureNotifyMask,
			&xev);
}


	

