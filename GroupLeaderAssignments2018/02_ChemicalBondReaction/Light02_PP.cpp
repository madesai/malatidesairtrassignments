#include <windows.h>

#include <gl\glew.h>//use it before other GL.h

#include <gl/GL.h>

#include "vmath.h"
#include "sphere_javascript.h"
#include "Texture_PP.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
//#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;


GLuint gLetterVertexShaderObject;
GLuint gLetterFragmentShaderObject;
GLuint gLetterShaderProgramObject;


GLuint gNumElements;
GLuint gNumVertices;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
short sphere_elements[2280];

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint gVao_Square;//displayList
GLuint gVbo_Square_Position;
GLuint gVbo_Square_Texture;
GLuint gVbo_Square_Normal;

GLuint gModelMatrixUniform, gViewMatrixUniform, gProjectionMatrixUniform;
GLuint gLetterModelMatrixUniform, gLetterViewMatrixUniform, gLetterProjectionMatrixUniform;

GLuint gLdUniform, gLaUniform, gLsUniform;
GLuint gLightPositionUniform;
GLuint gKdUniform, gKaUniform, gKsUniform;
GLuint gMaterialShininessUniform;

GLuint gLKeyPressedUniform;

mat4 gPerspectiveProjectionMatrix;

GLfloat gAngle = 0.0f;

//bool gbAnimate;
bool gbLight;
GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 25.0f;

GLuint gTexture_sampler_uniform;
GLuint gVbo_Sphere_Texture;
GLuint gTexture_H_letter;
GLuint gTexture_C_letter;

GLuint gLetterTextColorUniform;

GLuint gVao_cube;
GLuint gVbo_cube_position;
GLuint gVbo_cube_normal;


GLuint gdiffValueUniform;

float rightConnectorRotate = 0.0f;
float centralSphereRotation = 0.0f;

bool isMKeyPressed = false;
bool isBKeyPressed = false;
bool isAKeyPressed = false;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void Update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	//code
	// create log file
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	//Parallel to glutInitWindowSize(), glutInitWindowPosition() and glutCreateWindow() all three together
	hwnd = CreateWindow(szClassName,
		TEXT("OpenGL Programmable Pipeline Window"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//initialize
	initialize();

	//Message Loop
	while (bDone == false) //Parallel to glutMainLoop();
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// rendring function
			display();
			Update();
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true) //Continuation to glutLeaveMainLoop();
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0) //if 0, the window is active
			gbActiveWindow = true;
		else //if non-zero, the window is not active
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE: //Parallel to glutReshapeFunc();
		resize(LOWORD(lParam), HIWORD(lParam)); //Parallel to glutReshapeFunc(resize);
		break;
	case WM_KEYDOWN: //Parallel to glutKeyboardFunc();
		switch (wParam)
		{
		case VK_ESCAPE: //case 27
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true; //Parallel to glutLeaveMainLoop();
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C:
			if (bIsLKeyPressed == false)
			{
				gbLight = true;
				bIsLKeyPressed = true;
			}
			else
			{
				gbLight = false;
				bIsLKeyPressed = false;
			}
			break;

			//M
		case 0x4D:
			if (isMKeyPressed == false)
			{
				isMKeyPressed = true;
				isBKeyPressed = false;
				isAKeyPressed = false;
			}
			else
			{
				isMKeyPressed = false;
			}
			break;
			//B
		case 0x42:
			if (isBKeyPressed == false)
			{
				isBKeyPressed = true;
				isMKeyPressed = false;
				isAKeyPressed = false;
			}
			else
			{
				isBKeyPressed = false;
			}
			break;
			//A
		case 0x41:
			if (isAKeyPressed == false)
			{
				isAKeyPressed = true;
				isMKeyPressed = false;
				isBKeyPressed = false;
			}
			else
			{
				isAKeyPressed = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:  //Parallel to glutMouseFunc();
		break;
	case WM_CLOSE: //Parallel to glutCloseFunc();
		uninitialize(); //Parallel to glutCloseFunc(uninitialize);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

//FUNCTION DEFINITIONS
void initialize(void)
{
	//function prototypes
	void uninitialize(void);
	void resize(int, int);
	int LoadGLTextures(GLuint *, TCHAR[]);
	void InitCube();
	
	void ShaderText();
	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	//Parallel to glutInitDisplayMode()
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	//choose a pixel format which best matches with that of 'pfd'
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//set the pixel format chosen above
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//create OpenGL rendering context
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//make the rendering context created above as current n the current hdc
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "%s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "Shading Lang Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));


	//VERTEX SHADER
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertextShaderSourceCode =
		"#version 400" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;"\
		"uniform int u_LKeyPressed;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_LKeyPressed == 1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"}"\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertextShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	//FRAGMENT SHADER

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
		"#version 400" \
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_LKeyPressed;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"if(u_LKeyPressed==1)" \
		"{" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction=normalize(light_direction);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		"vec3 ambient = u_La * u_Ka;" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color=ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Shader Program

	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");

	gLdUniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	gLaUniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	gLsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");

	gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	gKaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");

	gLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");


	InitCube();

	ShaderText();
	Sphere sphere;
	sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = sphere.getNumberOfSphereVertices();
	gNumElements = sphere.getNumberOfSphereElements();



	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	//position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//normal
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);


	glBindBuffer(GL_ARRAY_BUFFER, 0);

	
	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	glBindVertexArray(0);

	const GLfloat rectangleVertices[] =
	{
		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
	};

	const GLfloat rectangleNormals[] =
	{
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,

	};
	const GLfloat rectangleTextcoords[] =
	{
		1.0f,1.0f,
		0.0f,1.0f,
		0.0f,0.0f,
		1.0f,0.0f,
	};

	//Sqaure
	glGenVertexArrays(1, &gVao_Square);
	glBindVertexArray(gVao_Square);

	glGenBuffers(1, &gVbo_Square_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Square_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);


	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &gVbo_Square_Normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Square_Normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleNormals), rectangleNormals, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);


	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &gVbo_Square_Texture);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Square_Texture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleTextcoords), rectangleTextcoords, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_TEXTURE, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_TEXTURE);


	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	glBindVertexArray(0);


	make_cylinder(2.0f, 5.0f, 30, 30);

	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	glEnable(GL_TEXTURE_2D);
	LoadGLTextures(&gTexture_H_letter, MAKEINTRESOURCE(IDBITMAP_H_LETTER));

	LoadGLTextures(&gTexture_C_letter, MAKEINTRESOURCE(IDBITMAP_C_LETTER));

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // blue

	gPerspectiveProjectionMatrix = mat4::identity();

	
	gbLight = false;
	// resize
	resize(WIN_WIDTH, WIN_HEIGHT);
}
void InitCube()
{

	//Square
	GLfloat cubeVertices[] =
	{
		//top
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		//bottom
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		//front
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		//back
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		//right
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		//left
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f

	};
	for (int i = 0; i<72; i++)
	{
		if (cubeVertices[i]<0.0f)
			cubeVertices[i] = cubeVertices[i] + 0.25f;
		else if (cubeVertices[i]>0.0f)
			cubeVertices[i] = cubeVertices[i] - 0.25f;
		else
			cubeVertices[i] = cubeVertices[i];
	}

	const GLfloat cubeNormals[] =
	{
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		0.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,

		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,

		-1.0f, 0.0f, 0.0f,
		-1.0f, 0.0f, 0.0f,
		-1.0f, 0.0f, 0.0f,
		-1.0f, 0.0f, 0.0f,

		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f

	};

	glGenVertexArrays(1, &gVao_cube);
	glBindVertexArray(gVao_cube);

	//position
	glGenBuffers(1, &gVbo_cube_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//normal
	glGenBuffers(1, &gVbo_cube_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);


	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


}
void ShaderText()
{
	void uninitialize();
	//VERTEX SHADER
	gLetterVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertextShaderSourceCode =
		"#version 400" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexture0_Coord;"\
		"out vec2 out_texture0_coord;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \

		"void main(void)" \
		"{" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"out_texture0_coord = vTexture0_Coord;" \
		"}";

	glShaderSource(gLetterVertexShaderObject, 1, (const GLchar **)&vertextShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gLetterVertexShaderObject);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gLetterVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gLetterVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gLetterVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Letter Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	//FRAGMENT SHADER

	gLetterFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
		"#version 400" \
		"\n" \
		"in vec2 out_texture0_coord;" \
		"out vec4 FragColor;" \
		"uniform sampler2D u_texture0_sampler;" \
		"uniform vec3 textColor;" \
		"uniform float diffValue;" \
		"void main(void)" \
		"{" \
		"vec4 color2 = texture(u_texture0_sampler, out_texture0_coord);" \
		
	//	"if(distance(color2.rgb, vec3(1.0, 0.27, 0.0)) > 0.75)" 
		"if(distance(color2.rgb, textColor) > diffValue)" \
		"discard;" \

		//"if(color2.a <= 0.0)" \

		"FragColor = color2;" \
		"FragColor.rgb = textColor;" \
		"}";

	//H color = 0.2, 0.4, 0.6
	//C Color = 0.0f, 0.0f, 0.0f
	//O Color = 
	glShaderSource(gLetterFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	glCompileShader(gLetterFragmentShaderObject);

	glGetShaderiv(gLetterFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gLetterFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gLetterFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Letter Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Shader Program

	gLetterShaderProgramObject = glCreateProgram();

	glAttachShader(gLetterShaderProgramObject, gLetterVertexShaderObject);

	glAttachShader(gLetterShaderProgramObject, gLetterFragmentShaderObject);

	glBindAttribLocation(gLetterShaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gLetterShaderProgramObject, MALATI_ATTRIBUTE_TEXTURE, "vTexture0_Coord");

	glLinkProgram(gLetterShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gLetterShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gLetterShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gLetterShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Letter Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	gLetterModelMatrixUniform = glGetUniformLocation(gLetterShaderProgramObject, "u_model_matrix");
	gLetterViewMatrixUniform = glGetUniformLocation(gLetterShaderProgramObject, "u_view_matrix");
	gLetterProjectionMatrixUniform = glGetUniformLocation(gLetterShaderProgramObject, "u_projection_matrix");

	gLetterTextColorUniform = glGetUniformLocation(gLetterShaderProgramObject, "textColor");
	gdiffValueUniform = glGetUniformLocation(gLetterShaderProgramObject, "diffValue");

	gTexture_sampler_uniform = glGetUniformLocation(gShaderProgramObject, "u_texture0_sampler");

}
int LoadGLTextures(GLuint *texture, TCHAR imageResourceId[])
{
	HBITMAP hBitmap;
	BITMAP bmp;
	int iStatus = FALSE;
	glGenTextures(1, texture);
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		iStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		//		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGB,
			bmp.bmWidth,
			bmp.bmHeight,
			0,
			GL_BGR,
			GL_UNSIGNED_BYTE,
			bmp.bmBits);

		glGenerateMipmap(GL_TEXTURE_2D);
		DeleteObject(hBitmap);
	}
	return (iStatus);
}

void display(void)
{
	void drawMethane();
	void drawBenzene();
	void drawAlcohol();
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	if (isMKeyPressed)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(gShaderProgramObject);
		drawMethane();
		glUseProgram(0);

	}
	else if (isBKeyPressed)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(gShaderProgramObject);
		drawBenzene();
		glUseProgram(0);

	}
	else if (isAKeyPressed)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(gShaderProgramObject);
		drawAlcohol();
		glUseProgram(0);
	}
	//draw(1.0f);
	SwapBuffers(ghdc);
}

void drawCube()
{
	glBindVertexArray(gVao_cube);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

	glBindVertexArray(0);

}

void giveLightValues()
{
	glUniform1i(gLKeyPressedUniform, 1);

	glUniform3fv(gLdUniform, 1, lightDiffuse);//white
	glUniform3fv(gLaUniform, 1, lightAmbient);
	glUniform3fv(gLsUniform, 1, lightSpecular);
	glUniform4fv(gLightPositionUniform, 1, lightPosition);

	glUniform3fv(gKdUniform, 1, material_diffuse);
	glUniform3fv(gKaUniform, 1, material_ambient);
	glUniform3fv(gKsUniform, 1, material_specular);
	glUniform1f(gMaterialShininessUniform, material_shininess);

	float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };
}

void drawBenzene()
{
	

	if (gbLight == true)
	{
		glUniform1i(gLKeyPressedUniform, 1);

		glUniform3fv(gLdUniform, 1, lightDiffuse);//white
		glUniform3fv(gLaUniform, 1, lightAmbient);
		glUniform3fv(gLsUniform, 1, lightSpecular);
		glUniform4fv(gLightPositionUniform, 1, lightPosition);

		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

		float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };

	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
	}

	//Enable Alpha
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();

	mat4 scaleMatrix = mat4::identity();

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 0.0f, 0.0f, 0.0f));

	//1.left

	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	//	static float leftSpherePosition = -7.0f;
	static float leftTranslate = -20.0f;
	modelMatrix = vmath::translate(-2.3f, -1.0f, leftTranslate);
	leftTranslate += 0.05f;
	if (leftTranslate >= -12.0f)
		leftTranslate = -12.0f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);

	//*****************************************
	//1.BONDING 
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-2.8f, -1.2f, -12.0f);

	rotationMatrix = vmath::rotate(27.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	/*
	rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(0.4f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();

	
	///////////////////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();

	static float x1Translate = -16.0f;
	modelMatrix = vmath::translate(x1Translate, -2.0f, -15.0f);
	x1Translate += 0.03f;
	if (x1Translate >= -4.3f)
		x1Translate = -4.3f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);
	//*****************************************
	//1.BONDING BETWEEN Left and Middle
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-2.5f, 0.00f, -12.0f);


	rotationMatrix = vmath::rotate(90.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;

/*	rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(0.7f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();


	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-2.1f, 0.00f, -12.0f);


	rotationMatrix = vmath::rotate(90.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	/*	rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(0.7f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//2nd Upper Left

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 0.0f, 0.0f, 0.0f));

	modelMatrix = mat4::identity();

	static float upperLeftTranslate = -20.0f;
	modelMatrix = vmath::translate(-2.3f, 1.0f, upperLeftTranslate);
	upperLeftTranslate += 0.05f;
	if (upperLeftTranslate >= -12.0f)
		upperLeftTranslate = -12.0f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);

	//*****************************************
	//2.BONDING 
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-2.8f, 1.2f, -12.0f);


	rotationMatrix = vmath::rotate(-30.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	/*
	rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(0.4f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();
	///////////////////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();

	static float x2Translate = -16.0f;
	modelMatrix = vmath::translate(x2Translate, 2.0f, -15.0f);
	x2Translate += 0.03f;
	if (x2Translate >= -4.3f)
		x2Translate = -4.3f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);

	//*****************************************
	//2.BONDING BETWEEN Left and Middle
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-1.2f, 1.8f, -12.0f);


	rotationMatrix = vmath::rotate(30.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	
	/*rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(1.1f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//3rd Top Most Middle
	
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 0.0f, 0.0f, 0.0f));

	modelMatrix = mat4::identity();

	static float topTranslate = -20.0f;
	modelMatrix = vmath::translate(0.0f, 2.5f, topTranslate);
	topTranslate += 0.05f;
	if (topTranslate >= -12.0f)
		topTranslate = -12.0f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);
	
	//*****************************************
	//3.BONDING 
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(0.0f, 3.3f, -12.0f);


	rotationMatrix = vmath::rotate(90.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	/*
	rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(0.4f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();
	///////////////////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	static float x3Translate = 12.0f;
	modelMatrix = vmath::translate(0.0f, x3Translate, -15.0f);
	x3Translate -= 0.05f;
	if (x3Translate <= 5.0f)
		x3Translate = 5.0f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//*****************************************
	//3.BONDING 
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(1.1f, 1.6f, -12.0f);


	rotationMatrix = vmath::rotate(-30.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	/*rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(1.1f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	drawCube();


	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(1.25f, 1.85f, -12.0f);


	rotationMatrix = vmath::rotate(-30.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	/*rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(1.1f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	drawCube();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//4th right upper

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 0.0f, 0.0f, 0.0f));
	modelMatrix = mat4::identity();

	static float rightUpperTranslate = -20.0f;
	modelMatrix = vmath::translate(2.3f, 1.0f, rightUpperTranslate);
	rightUpperTranslate += 0.05f;
	if (rightUpperTranslate >= -12.0f)
		rightUpperTranslate = -12.0f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);

	//*****************************************
	//4.BONDING 
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(2.8f, 1.2f, -12.0f);


	rotationMatrix = vmath::rotate(30.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	/*
	rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(0.4f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();
	///////////////////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	static float x4Translate = 16.0f;
	modelMatrix = vmath::translate(x4Translate, 2.0f, -15.0f);
	x4Translate -= 0.03f;
	if (x4Translate <= 4.3f)
		x4Translate = 4.3f;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);

	//*****************************************
	//4.BONDING 
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(2.3f, 0.00f, -12.0f);


	rotationMatrix = vmath::rotate(90.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	/*rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(0.7f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//5th right bottom
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 0.0f, 0.0f, 0.0f));

	modelMatrix = mat4::identity();

	static float rightBottomTranslate = -20.0f;
	modelMatrix = vmath::translate(2.3f, -1.0f, rightBottomTranslate);
	rightBottomTranslate += 0.05f;
	if (rightBottomTranslate >= -12.0f)
		rightBottomTranslate = -12.0f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);

	//*****************************************
	//4.BONDING 
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(2.8f, -1.2f, -12.0f);


	rotationMatrix = vmath::rotate(-30.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	/*
	rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(0.4f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();
	///////////////////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	static float x5Translate = 16.0f;
	modelMatrix = vmath::translate(x5Translate, -2.0f, -15.0f);
	x5Translate -= 0.03f;
	if (x5Translate <= 4.3f)
		x5Translate = 4.3f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//*****************************************
	//3.BONDING 
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(1.1f, -1.7f, -12.0f);


	rotationMatrix = vmath::rotate(30.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	/*rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(1.1f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(1.3f, -1.9f, -12.0f);


	rotationMatrix = vmath::rotate(30.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	/*rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(1.1f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 0.0f, 0.0f, 0.0f));

	//6th  bottom
	modelMatrix = mat4::identity();
	static float bottomTranslate = -20.0f;
	modelMatrix = vmath::translate(0.0f, -2.5f, bottomTranslate);
	bottomTranslate += 0.05f;
	if (bottomTranslate >= -12.0f)
		bottomTranslate = -12.0f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);
	//*****************************************
	//3.BONDING 
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(0.0f, -3.2f, -12.0f);


	rotationMatrix = vmath::rotate(90.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	/*
	rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(0.4f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();
	///////////////////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();

	static float x6Translate = -12.0f;
	modelMatrix = vmath::translate(0.0f, x6Translate, -15.0f);
	x6Translate += 0.05f;
	if (x6Translate >= -5.0f)
		x6Translate = -5.0f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);
	/*************************************************************************/
	//BONDING
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-1.2f, -1.8f, -12.0f);


	rotationMatrix = vmath::rotate(-30.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	/*rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(1.1f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();
	
}

void drawAlcohol()
{
	if (gbLight == true)
	{
		glUniform1i(gLKeyPressedUniform, 1);

		glUniform3fv(gLdUniform, 1, lightDiffuse);//white
		glUniform3fv(gLaUniform, 1, lightAmbient);
		glUniform3fv(gLsUniform, 1, lightSpecular);
		glUniform4fv(gLightPositionUniform, 1, lightPosition);

		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

		float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };

	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
	}

	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();

	mat4 scaleMatrix = mat4::identity();

	
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	//Lower C Bond
	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(0.0f, -1.0f, -7.0f);
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	////////////////////////////////
	/*************************************************************************/
	//BONDING between Bottom Left "H" and bottom C
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.6f, 0.4f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-0.8f, -2.4f, -12.0f);


	rotationMatrix = vmath::rotate(45.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	/*rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(0.3f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();

	////////////////////////////////
	//bottom left
	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = vmath::translate(-0.5f, -1.2f, -5.0f);
	scaleMatrix = vmath::scale(0.3f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	/*************************************************************************/
	//BONDING between Bottom right "H" and bottom C
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.6f, 0.4f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(0.8f, -2.4f, -12.0f);


	rotationMatrix = vmath::rotate(120.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	/*rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(0.3f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();

	////////////////////////////////

	//bottom right
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(0.7f, -2.2f, -9.0f);
	scaleMatrix = vmath::scale(0.5f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);


	/////////////////////////////////////////////////////////////////////////////////////////
	/*************************************************************************/
	//BONDING between 2 "C"
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.6f, 0.4f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-1.5f, -0.7f, -12.0f);


	rotationMatrix = vmath::rotate(-30.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	/*rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(1.3f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();

	///////////////////////////////////////////////////////////////////////////////////////
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	scaleMatrix = mat4::identity();

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-2.0f, 0.0f, -8.0f);
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	/*************************************************************************/
	//BONDING between Bottom Left "H" and Upper C
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.6f, 0.4f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-3.6f, -0.7f, -12.0f);


	rotationMatrix = vmath::rotate(55.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	/*rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(0.5f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();

	////////////////////////////////

	//upper Sphere bottom left
	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = vmath::translate(-3.0f, -1.0f, -9.0f);
	scaleMatrix = vmath::scale(0.5f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	/*************************************************************************/
	//BONDING between Upper C and upper left H
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.6f, 0.4f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-4.0f, 0.7f, -12.0f);


	rotationMatrix = vmath::rotate(-30.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	/*rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(0.65f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();

	///////////////////////////////////////////////////////////////////////////////////////

	
	//upper Sphere up left
	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = vmath::translate(-2.0f, 0.5f, -5.0f);
	scaleMatrix = vmath::scale(0.3f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	/*************************************************************************/
	//BONDING between Up right "H" and Upper C
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.6f, 0.4f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-2.4f, 0.8f, -12.0f);


	rotationMatrix = vmath::rotate(55.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	
	scaleMatrix = vmath::scale(0.5f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();
	/////////////////////////////////////////////////////////////////////////////////
	//upper Sphere up right
	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = vmath::translate(-1.5f, 1.0f, -9.0f);
	scaleMatrix = vmath::scale(0.5f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	
	//*************************************
	//Bond between red and white sphere

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.6f, 0.4f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(1.5f, -0.2f, -12.0f);


	rotationMatrix = vmath::rotate(50.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;


	scaleMatrix = vmath::scale(1.67f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();

	////////////////////////////////////////////////////////////////////////
	//Red Sphere 
	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 0.0f, 0.0f, 0.0f));

	modelMatrix = vmath::translate(2.0f, 1.0f, -9.0f);
	scaleMatrix = vmath::scale(1.0f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	/*************************************************************************/
	//BONDING between lower right h and red sphere
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.6f, 0.4f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(3.6f, 0.8f, -12.0f);


	rotationMatrix = vmath::rotate(-30.0f, vec3(0.0f, 0.0f, 1.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	/*rotationMatrix = vmath::rotate(-20.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;*/

	scaleMatrix = vmath::scale(1.5f, 0.10f, 0.15f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();

	///////////////////////////////////////////////////////////////////////////////////////

	//right of red sphere

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = vmath::translate(2.0f, 0.0f, -5.0f);
	scaleMatrix = vmath::scale(0.3f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	glUseProgram(0);

	//////////////////////////////////////////////////////////////////////////////////////TEXT RENDERING/////////////////////////////////////
	/*glUseProgram(gLetterShaderProgramObject);


	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	glBindTexture(GL_TEXTURE_2D, gTexture_C_letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(0.0f, 0.0f, 0.0f));

	glUniform1f(gdiffValueUniform, 0.5f);

	modelMatrix = vmath::translate(0.0f, -0.22f, -1.5f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.1f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gLetterModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gLetterViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gLetterProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Square);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //there is no QUAD in PP(4  rows)
	glBindVertexArray(0);

	//////////////////////////
	//Bottom Left H

	glBindTexture(GL_TEXTURE_2D, gTexture_H_letter);
	glUniform1i(gTexture_sampler_uniform, 0);

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-0.38f, -0.85f, -3.5f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.1f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gLetterModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	
	//glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(0.2f, 0.4f, 0.6f));
	glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(1.0f, 1.0f, 1.0f));

	glUniform1f(gdiffValueUniform, 1.0f);

	//	glCullFace(GL_FRONT);
	glBindVertexArray(gVao_Square);


	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //there is no QUAD in PP(4  rows)
	glBindVertexArray(0);

	//////////////////////////
	//Bottom Right H

	glBindTexture(GL_TEXTURE_2D, gTexture_H_letter);
	glUniform1i(gTexture_sampler_uniform, 0);

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(0.26f, -0.85f, -3.5f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.1f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gLetterModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	//glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(0.2f, 0.4f, 0.6f));
	glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(1.0f, 1.0f, 1.0f));

	glUniform1f(gdiffValueUniform, 1.0f);

	//	glCullFace(GL_FRONT);
	glBindVertexArray(gVao_Square);


	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //there is no QUAD in PP(4  rows)
	glBindVertexArray(0);

	////////////////////////
	//Upper C
	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	glBindTexture(GL_TEXTURE_2D, gTexture_C_letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(0.0f, 0.0f, 0.0f));

	glUniform1f(gdiffValueUniform, 0.5f);

	modelMatrix = vmath::translate(-0.42f, 0.0f, -1.7f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.1f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gLetterModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gLetterViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gLetterProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Square);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //there is no QUAD in PP(4  rows)
	glBindVertexArray(0);
	*/

/*example
	glBindTexture(GL_TEXTURE_2D, gTexture_H_letter);
	glUniform1i(gTexture_sampler_uniform, 0);

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-0.45f, 0.0f, -1.5f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.1f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gLetterModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gLetterViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gLetterProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	//glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(0.2f, 0.4f, 0.6f));
	glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(1.0f, 1.0f, 1.0f));

	glUniform1f(gdiffValueUniform, 1.0f);

	//	glCullFace(GL_FRONT);
	glBindVertexArray(gVao_Square);


	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //there is no QUAD in PP(4  rows)
	glBindVertexArray(0);
	
	//////////C Letter

	glBindTexture(GL_TEXTURE_2D, gTexture_C_letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(0.0f, 0.0f, 0.0f));

	glUniform1f(gdiffValueUniform, 0.5f);

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(0.0f, 0.0f, -1.5f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.1f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gLetterModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_Square);


	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //there is no QUAD in PP(4  rows)
	glBindVertexArray(0);

	*/
///////////////////////////////////////////
	/*
	glBindTexture(GL_TEXTURE_2D, gTexture_H_letter);
	glUniform1i(gTexture_sampler_uniform, 0);

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-0.43f, 0.0f, -1.7f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.1f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gLetterModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gLetterViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gLetterProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	//glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(0.2f, 0.4f, 0.6f));
	glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(1.0f, 1.0f, 1.0f));

	glUniform1f(gdiffValueUniform, 1.0f);

	//	glCullFace(GL_FRONT);
	glBindVertexArray(gVao_Square);


	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //there is no QUAD in PP(4  rows)
	glBindVertexArray(0);
	*/

}

void drawMethane()
{

	if (gbLight == true)
	{
		glUniform1i(gLKeyPressedUniform, 1);

		glUniform3fv(gLdUniform, 1, lightDiffuse);//white
		glUniform3fv(gLaUniform, 1, lightAmbient);
		glUniform3fv(gLsUniform, 1, lightSpecular);
		glUniform4fv(gLightPositionUniform, 1, lightPosition);

		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

		float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };

	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
	}

	//Enable Alpha
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();

	mat4 scaleMatrix = mat4::identity();

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	//1.left
	static float leftSpherePosition = -7.0f;
	modelMatrix = vmath::translate(leftSpherePosition, 0.0f, -8.0f);

	leftSpherePosition += 0.01;
	if (leftSpherePosition > -2.3f)
		leftSpherePosition = -2.3f;

	//rotationMatrix = vmath::rotate(gAngle, vec3(0.0, 1.0, 0.0));

	//	modelMatrix = modelMatrix * rotationMatrix; //imp


	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);

	//BONDING BETWEEN Left and Middle
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));


	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-1.09f, 0.00f, -8.1f);
	scaleMatrix = vmath::scale(0.769f, 0.193f, 1.0f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();
	

	//middle Sphere
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(0.0f, 0.0f, -8.0f);
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);


	//BONDING BETWEEN Middle and right
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));


	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(1.125f, 0.0f, -8.1f);
	scaleMatrix = vmath::scale(0.87f, 0.193f, 1.0f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();

	//3.right
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	static float rightSphereTranslate = 7.0f;
	modelMatrix = vmath::translate(rightSphereTranslate, 0.0f, -8.0f);

	rightSphereTranslate -= 0.01f;
	if (rightSphereTranslate < 2.45f)
		rightSphereTranslate = 2.45f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);


	//BONDING BETWEEN middle and up
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-0.20f, 1.06f, -8.1f);
	scaleMatrix = vmath::scale(0.78f, 0.195f, 1.0f);
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 0.0f, 1.0f);

	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(15.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();

	//4.up

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	static float upSphereTranslate = 5.0f;
	modelMatrix = vmath::translate(0.0f, upSphereTranslate, -8.0f);
	upSphereTranslate -= 0.01f;
	if(upSphereTranslate <= 2.3f)
		upSphereTranslate = 2.3f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);


	//BONDING BETWEEN Down and middle
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-0.05f, -1.05f, -8.1f);
	scaleMatrix = vmath::scale(0.78f, 0.195f, 1.0f);
	rotationMatrix = vmath::rotate(-90.0f, 0.0f, 0.0f, 1.0f);

	/*modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(15.0f, 1.0f, 0.0f, 0.0f);*/
	modelMatrix = modelMatrix * rotationMatrix;


	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	drawCube();


	//5.down

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	static float downTranlateY = -5.0f;
	modelMatrix = vmath::translate(0.0f, downTranlateY, -8.0f);
	downTranlateY += 0.01f;
	if (downTranlateY >= -2.3f)
		downTranlateY = -2.3f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);



	//glDrawIndexArrays, glDrawElements

	glUseProgram(0);


	glUseProgram(gLetterShaderProgramObject);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_letter);
	glUniform1i(gTexture_sampler_uniform, 0);

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-0.60f, 0.0f, -2.0f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.1f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gLetterModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gLetterViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gLetterProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	//glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(0.2f, 0.4f, 0.6f));
	glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(1.0f, 1.0f, 1.0f));

	glUniform1f(gdiffValueUniform, 1.0f);

	//	glCullFace(GL_FRONT);
	glBindVertexArray(gVao_Square);


	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //there is no QUAD in PP(4  rows)
	glBindVertexArray(0);

	glBindTexture(GL_TEXTURE_2D, gTexture_C_letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(0.0f, 0.0f, 0.0f));

	glUniform1f(gdiffValueUniform, 0.5f);

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(0.0f, 0.0f, -1.5f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.1f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gLetterModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_Square);


	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //there is no QUAD in PP(4  rows)
	glBindVertexArray(0);


	//right

	glBindTexture(GL_TEXTURE_2D, gTexture_H_letter);
	glUniform1i(gTexture_sampler_uniform, 0);

	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(0.6f, 0.0f, -2.0f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.1f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gLetterModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gLetterViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gLetterProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(1.0f, 1.0f, 1.0f));
	glUniform1f(gdiffValueUniform, 1.0f);
	//	glCullFace(GL_FRONT);
	glBindVertexArray(gVao_Square);


	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //there is no QUAD in PP(4  rows)
	glBindVertexArray(0);


	//up
	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-0.03f, 0.57f, -2.0f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.1f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gLetterModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gLetterViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gLetterProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(1.0f, 1.0f, 1.0f));

	glUniform1f(gdiffValueUniform, 1.0f);
	//	glCullFace(GL_FRONT);
	glBindVertexArray(gVao_Square);


	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //there is no QUAD in PP(4  rows)
	glBindVertexArray(0);


	//down
	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(-0.03f, -0.6f, -2.0f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.1f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gLetterModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glUniformMatrix4fv(gLetterViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gLetterProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glUniformMatrix4fv(gLetterTextColorUniform, 1, GL_FALSE, vec3(1.0f, 1.0f, 1.0f));

	glUniform1f(gdiffValueUniform, 1.0f);
	//	glCullFace(GL_FRONT);
	glBindVertexArray(gVao_Square);


	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //there is no QUAD in PP(4  rows)
	glBindVertexArray(0);



}
void Update(void)
{
	gAngle = gAngle + 0.5f;
	if (gAngle >= 360.0f)
		gAngle = 0.0f;


	centralSphereRotation = centralSphereRotation + 0.1f;
	if (centralSphereRotation >= 360.0f)
		centralSphereRotation = 0.0f;

	rightConnectorRotate += 0.1f;
	if (rightConnectorRotate >= 360.0f)
		rightConnectorRotate = 0.0f;

}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	/*
	if (width <= height)
	{
	gOrthographicProjectionMatrix = ortho(-100.0f, 100.0f, (-100.0f *(height / width)), (100.0f * (height / width)), -100.0f, 100.0f);
	}
	else
	{
	gOrthographicProjectionMatrix = ortho((-100.0f *(width / height)), (100.0f * (width / height)), -100.0f, 100.0f, -100.0f, 100.0f);
	}*/
	gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height,
		0.1f, 100.0f); //perspective(3rd change)
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	if (gVao_Square)
	{
		glDeleteVertexArrays(1, &gVao_Square);
		gVao_Square = 0;
	}

	if (gVbo_Square_Position)
	{
		glDeleteBuffers(1, &gVbo_Square_Position);
		gVbo_Square_Position = 0;
	}
	if (gVbo_Square_Texture)
	{
		glDeleteBuffers(1, &gVbo_Square_Texture);
		gVbo_Square_Texture = 0;
	}
	// destroy element vbo
	if (gVbo_Square_Normal)
	{
		glDeleteBuffers(1, &gVbo_Square_Normal);
		gVbo_Square_Normal = 0;
	}

	/*
	GLuint gVao_Square;//displayList
	GLuint gVbo_Square_Position;
	GLuint gVbo_Square_Texture;
	GLuint gVbo_Square_Normal;

	*/
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}
	// destroy element vbo
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}


	if (gVao_cube)
	{
		glDeleteVertexArrays(1, &gVao_cube);
		gVao_cube = 0;
	}

	if (gVbo_cube_position)
	{
		glDeleteBuffers(1, &gVbo_cube_position);
		gVbo_cube_position = 0;
	}
	if (gVbo_cube_normal)
	{
		glDeleteBuffers(1, &gVbo_cube_normal);
		gVbo_cube_normal = 0;
	}
	
	glDetachShader(gLetterShaderProgramObject, gLetterVertexShaderObject);
	glDetachShader(gLetterShaderProgramObject, gLetterFragmentShaderObject);

	glDeleteShader(gLetterVertexShaderObject);
	gLetterVertexShaderObject = 0;
	glDeleteShader(gLetterFragmentShaderObject);
	gLetterFragmentShaderObject = 0;

	glDeleteProgram(gLetterShaderProgramObject);
	gLetterShaderProgramObject = 0;

	glUseProgram(0);


	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	//Delete the device context
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}


