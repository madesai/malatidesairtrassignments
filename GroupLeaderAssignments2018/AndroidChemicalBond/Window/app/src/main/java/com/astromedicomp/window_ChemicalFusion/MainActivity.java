package com.astromedicomp.window_ChemicalFusion;

import android.app.Activity;
import android.os.Bundle;

import android.view.Window; //Window Class
import android.view.WindowManager;
import android.graphics.Color;
import android.content.pm.ActivityInfo;

public class MainActivity extends Activity {
	
	private GLESView glesView;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		
		//To get Rid of ActionBar
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		//To make Full Screen
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
								WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		//force activity window orientation to Landscape
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		//getWindow().getDecorView().setBackgroundColor(Color.rgb(0,0,0));
		
		glesView = new GLESView(this);
		
        //setContentView(R.layout.activity_main);
		setContentView(glesView);
    }
	
	@Override
    protected void onPause()
    {
        super.onPause();
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
    }

}
