package com.astromedicomp.window_ChemicalFusion;

import android.content.Context; //for drawing context related
import android.opengl.GLSurfaceView;//surface view
import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

//for vbo 
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

//(nio -> non Blocking IO)

import android.opengl.Matrix; //for Matrix math

import android.graphics.BitmapFactory; //texture
import android.graphics.Bitmap; // for PNG image
import android.opengl.GLUtils; // for texImage2D()
public class GLESView extends GLSurfaceView implements
GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
    private int numElements;
    private int numVertices;

	private Cylinder myCylinder;
	private MyStack myStack;
	private MySphere mysphere;
	
	private int[] vao_cube = new int[1];
	private int[] vbo_cube_normal = new int[1];
	private int[] vbo_cube_position = new int[1];
	
	private int[] vao_sphere = new int[1];
	private int[] vbo_sphere_normal = new int[1];
	private int[] vbo_sphere_position = new int[1];
	private int[] vbo_sphere_element = new int[1];
	
	
	private int[] vao_rectangle = new int[1];
	private int[] vbo_rectangle_texture = new int[1];
	private int[] vbo_rectangle_position = new int[1];
	
    private float light_ambient[] = {0.0f,0.0f,0.0f,1.0f};
    private float light_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
    private float light_specular[] = {1.0f,1.0f,1.0f,1.0f};
    private float light_position[] = { 100.0f,100.0f,100.0f,1.0f };
    
    private float material_ambient[] = {0.0f,0.0f,0.0f,1.0f};
    private float material_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
    private float material_specular[] = {1.0f,1.0f,1.0f,1.0f};
    private float material_shininess = 50.0f;

	private int modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
	private int laUniform, ldUniform, lsUniform, lightPositionUniform; 
	private int kaUniform, kdUniform, ksUniform, materialShininessUniform;
	
	private int doubleTapUniform;
	private int singleTapUniform;
	private int longPressUniform;
	
	private int texture0_sampler_uniform;
	
	private float perspectiveProjectionMatrix[] = new float[16];
	//4*4 matrix
	
	private float gAngleCube = 0.0f;

	
	private int doubleTap;
	private int singleTap;
	private int longPress;
	
	private int[] texture_C_letter = new int[1];
	private int[] texture_H_letter = new int[1];
	private int[] texture_O_letter = new int[1];
	private int[] texture_First_Screen = new int[1];
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context =drawingContext;
		
		setEGLContextClientVersion(3);
		
		setRenderer(this);
		
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	
		gestureDetector = new GestureDetector(context, this, null, false);
		
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	
	//overriden method of GLSurfaceView.Renderer (init Code)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("MPD: OpenGL-ES Version = "+glesVersion);
			
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("MPD: GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	//overriden method GLSurfaceView.Renderer(change Size)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

    // overriden method of GLSurfaceView.Renderer ( Rendering Code )
    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

	//imp
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventaction = e.getAction();
		if(!gestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return (true);	
	}
	
	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		doubleTap++;
		if(doubleTap > 3)
			doubleTap = 1;
		return (true);
	}
	 
 // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
   // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
		singleTap++;
		if(singleTap > 1)
			singleTap = 0;
    
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
	
    public boolean onDown(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
		longPress++;
		if(longPress > 1)
			longPress = 0;
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
		uninitialize();
		System.exit(0);
		return (true);
	}
	
	// abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }
	
	private void initialize(GL10 gl)
	{
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		final String vertexShaderSourceCode = String.format
		(
		 "#version 320 es"+
         "\n"+
         "in vec4 vPosition;"+
         "in vec3 vNormal;"+
		 "in vec2 vTexture0_Coord;"+
         "uniform mat4 u_model_matrix;"+
         "uniform mat4 u_view_matrix;"+
         "uniform mat4 u_projection_matrix;"+
         "uniform mediump int u_double_tap;"+
		 "uniform mediump int u_single_tap;"+
		 "uniform mediump int u_long_press;"+
         "uniform vec4 u_light_position;"+
         "out vec3 transformed_normals;"+
         "out vec3 light_direction;"+
         "out vec3 viewer_vector;"+
		 "out vec2 out_texture0_coord;"+
         "void main(void)"+
         "{"+
         "if ((u_double_tap == 1) || (u_single_tap == 1) || (u_long_press == 1))"+
         "{"+
         "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
         "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;"+
         "light_direction = vec3(u_light_position) - eye_coordinates.xyz;"+
         "viewer_vector = -eye_coordinates.xyz;"+
         "}"+
         "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
		 "out_texture0_coord = vTexture0_Coord;" +
         "}"
		);
		
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		//compile shader & check for errors
		GLES32.glCompileShader(vertexShaderObject);
		int [] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,0);
		
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("MPD: Vertex Shader Compilation Log = " +szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		//fragmentShaderObject
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		final String fragmentShaderSourceCode = String.format
		(
		 "#version 320 es"+
         "\n"+
         "precision highp float;"+
         "in vec3 transformed_normals;"+
         "in vec3 light_direction;"+
         "in vec3 viewer_vector;"+
		 "in vec2 out_texture0_coord;" +
         "out vec4 FragColor;"+
         "uniform vec3 u_La;"+
         "uniform vec3 u_Ld;"+
         "uniform vec3 u_Ls;"+
         "uniform vec3 u_Ka;"+
         "uniform vec3 u_Kd;"+
         "uniform vec3 u_Ks;"+
         "uniform float u_material_shininess;"+
         "uniform int u_double_tap;"+
         "uniform int u_single_tap;"+
		 "uniform int u_long_press;"+
		 "uniform highp sampler2D u_texture0_sampler;"+
		 "void main(void)"+
         "{"+
         "vec3 phong_ads_color;"+
		 "vec4 TextureColor;"+
         "if((u_double_tap==1) || (u_single_tap == 1) || (u_long_press == 1))"+
         "{"+
         "vec3 normalized_transformed_normals=normalize(transformed_normals);"+
         "vec3 normalized_light_direction=normalize(light_direction);"+
         "vec3 normalized_viewer_vector=normalize(viewer_vector);"+
         "vec3 ambient = u_La * u_Ka;"+
         "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"+
         "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
         "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
         "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
         "phong_ads_color=ambient + diffuse + specular;"+
		 "TextureColor = texture(u_texture0_sampler, out_texture0_coord);" +
         "}"+
         "else"+
         "{"+
         "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
		 "TextureColor = vec4(1.0, 1.0, 1.0, 1.0);" +
         "}"+
         "FragColor = vec4(phong_ads_color, 1.0) + TextureColor;"+
         "}"

		);
		
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		
		//compile shader & check for errors
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,0);
		
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("MPD: Fragment Shader Compilation Log = " +szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		shaderProgramObject = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.MPD_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.MPD_ATTRIBUTE_NORMAL, "vNormal");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.MPD_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
		
		
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, 
		iShaderProgramLinkStatus, 0);
		
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("MPD: Shader Program Link Log = " + szInfoLog);
					uninitialize();
					System.exit(0);
				}
		}
		
		modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
		viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
		
		projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
		
		doubleTapUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_double_tap");
		singleTapUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_single_tap");
		longPressUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_long_press");
		
		laUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La");
		ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld");
		lsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls");
		kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ka");
		kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
		ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ks");
		lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");
		
        materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_material_shininess");
		
		texture0_sampler_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_texture0_sampler");
		
		myCylinder = new Cylinder();
		myCylinder.InitializeCylinder(0.05f,0.5f,16);


		float cubeVertices[] = new float[] 
		{ 
			//top
		2.0f, 1.0f, -1.0f,
		0.0f, 1.0f, -1.0f,
		0.0f, 1.0f, 1.0f,
		2.0f, 1.0f, 1.0f,
		//bottom
		2.0f, -1.0f, 1.0f,
		0.0f, -1.0f, 1.0f,
		0.0f, -1.0f, -1.0f,
		2.0f, -1.0f, -1.0f,
		//front
		2.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, -1.0f, 1.0f,
		2.0f, -1.0f, 1.0f,
		//back
		2.0f, -1.0f, -1.0f,
		0.0f, -1.0f, -1.0f,
		0.0f, 1.0f, -1.0f,
		2.0f, 1.0f, -1.0f,
		//left
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, -1.0f,
		0.0f, -1.0f, -1.0f,
		0.0f, -1.0f, 1.0f,
		//right
		2.0f, 1.0f, -1.0f,
		2.0f, 1.0f, 1.0f,
		2.0f, -1.0f, 1.0f,
		2.0f, -1.0f, -1.0f
		
		};
			
		//Square
	/*float cubeVertices[] = new float[]
	{
		//top
		2.0f, 1.0f, -1.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 2.0f, 2.0f,
		2.0f, 2.0f, 2.0f,
		//bottom
		2.0f, 0.0f, 2.0f,
		0.0f, 0.0f, 2.0f,
		0.0f, 0.0f, 0.0f,
		2.0f, 0.0f, 0.0f,
		//front
		2.0f, 2.0f, 2.0f,
		0.0f, 2.0f, 2.0f,
		0.0f, 0.0f, 2.0f,
		2.0f, 0.0f, 2.0f,
		//back
		2.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 2.0f, 0.0f,
		2.0f, 2.0f, 0.0f,
		//right
		0.0f, 2.0f, 2.0f,
		0.0f, 2.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 2.0f,
		//left
		2.0f, 2.0f, 0.0f,
		2.0f, 2.0f, 2.0f,
		2.0f, 0.0f, 2.0f,
		2.0f, 0.0f, 0.0f

	};*/

		float cubeVertices1[] = new float[] 
		{ 
			//top
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		//bottom
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		//front
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		//back
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		//left
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		//right
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f
		
		};
		//0.7f, 0.20f, 0.15f
		for(int i = 0; i<72; i++)
		{
			if(cubeVertices[i] < 1.0f)
				cubeVertices[i] = cubeVertices[i] + 0.2f;
			else if(cubeVertices[i] > 1.0f)
				cubeVertices[i] = cubeVertices[i] - 0.2f;
			else
				cubeVertices[i] = cubeVertices[i];

				if(cubeVertices[i+1] < 0.0f)
				cubeVertices[i+1] = cubeVertices[i+1] + 0.8f;
			else if(cubeVertices[i+1] > 0.0f)
				cubeVertices[i+1] = cubeVertices[i+1] - 0.8f;
			else
				cubeVertices[i+1] = cubeVertices[i+1];

				if(cubeVertices[i+2] < 0.0f)
				cubeVertices[i+2] = cubeVertices[i+2] + 0.85f;
			else if(cubeVertices[i+2] > 0.0f)
				cubeVertices[i+2] = cubeVertices[i+2] - 0.85f;
			else
				cubeVertices[i+2] = cubeVertices[i+2];

				i = i + 2;
		}
		
		float cubeNormals[] = new float[]
		{

			0.0f, 1.0f, 0.0f, //top
			0.0f, 1.0f, 0.0f, //bottom
			0.0f, 1.0f, 0.0f, //front
			0.0f, 1.0f, 0.0f, //back

			0.0f, -1.0f, 0.0f, //bottom
			0.0f, -1.0f, 0.0f, //front
			0.0f, -1.0f, 0.0f, //back
			1.0f, -1.0f, 0.0f, //right

			0.0f, 0.0f, 1.0f, //front
			0.0f, 0.0f, 1.0f, //back
			0.0f, 0.0f, 1.0f, //right
			0.0f, 0.0f, 1.0f, //left

			0.0f, 0.0f, -1.0f, //back
			0.0f, 0.0f, -1.0f, //right
			0.0f, 0.0f, -1.0f, //left
			0.0f, 0.0f, -1.0f, //top

			-1.0f, 0.0f, 0.0f, //right
			-1.0f, 0.0f, 0.0f, //left
			-1.0f, 0.0f, 0.0f, //top
			-1.0f, 0.0f, 0.0f, //bottom

			1.0f, 0.0f, 0.0f, //left
			1.0f, 0.0f, 0.0f, //top
			1.0f, 0.0f, 0.0f, //bottom
			1.0f, 0.0f, 0.0f //front

		};
		GLES32.glGenVertexArrays(1, vao_cube, 0);
		GLES32.glBindVertexArray(vao_cube[0]);
		
		GLES32.glGenBuffers(1, vbo_cube_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_position[0]);
		
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(cubeVertices.length * 4);
		
		byteBuffer.order(ByteOrder.nativeOrder());
		
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(cubeVertices);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							cubeVertices.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_VERTEX,
										3, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_VERTEX);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		//color
		//GLES32.glVertexAttrib3f(GLESMacros.MPD_ATTRIBUTE_COLOR, 0.39f, 0.58f, 0.9294f); //cornflower Blue 
	
		//GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_VERTEX);
		GLES32.glGenBuffers(1, vbo_cube_normal, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_normal[0]);
		
		byteBuffer = ByteBuffer.allocateDirect(cubeNormals.length * 4);

		byteBuffer.order(ByteOrder.nativeOrder());
		
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(cubeNormals);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							cubeNormals.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_NORMAL,
										3, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_NORMAL);

		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);
		
		
		myStack = new MyStack();

		Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();


		GLES32.glGenVertexArrays(1, vao_sphere, 0);
		GLES32.glBindVertexArray(vao_sphere[0]);
		
		GLES32.glGenBuffers(1, vbo_sphere_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_position[0]);
		
		byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length  * 4);
		
		byteBuffer.order(ByteOrder.nativeOrder());
		
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(sphere_vertices);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							sphere_vertices.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_VERTEX,
										3, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_VERTEX);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		//color
		//GLES32.glVertexAttrib3f(GLESMacros.MPD_ATTRIBUTE_COLOR, 0.39f, 0.58f, 0.9294f); //cornflower Blue 
	
		//GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_VERTEX);
		GLES32.glGenBuffers(1, vbo_sphere_normal, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_normal[0]);
		
		byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);

		byteBuffer.order(ByteOrder.nativeOrder());
		
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(sphere_normals);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							sphere_normals.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_NORMAL,
										3, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_NORMAL);

		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);

		///////////////////////////////Rectangle
		final float rectangleVertices[] = new float[]
		{
			1.0f, 1.0f, 0.0f,
			-1.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 0.0f,
			1.0f, -1.0f, 0.0f,
		};
		final float rectangleTextcoords[] = new float[]
		{
			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f,
		};
		GLES32.glGenVertexArrays(1, vao_rectangle, 0);
		GLES32.glBindVertexArray(vao_rectangle[0]);
		
		//position
		GLES32.glGenBuffers(1, vbo_rectangle_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_rectangle_position[0]);
		
		byteBuffer = ByteBuffer.allocateDirect(rectangleVertices.length * 4);
		
		byteBuffer.order(ByteOrder.nativeOrder());
		
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(rectangleVertices);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							rectangleVertices.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_VERTEX,
										3, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_VERTEX);

		//texture vbo
		GLES32.glGenBuffers(1, vbo_rectangle_texture, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_rectangle_texture[0]);
		
		byteBuffer = ByteBuffer.allocateDirect(rectangleTextcoords.length * 4);

		byteBuffer.order(ByteOrder.nativeOrder());
		
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(rectangleTextcoords);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							rectangleTextcoords.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_TEXTURE0,
										2, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_TEXTURE0);

		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);
		///////////////////////////////////////////
		mysphere = new MySphere();
		mysphere.uvSphere(2.0f, 40, 40);
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		
		texture_H_letter[0] = loadGLTexture(R.raw.hydrogen);
		texture_C_letter[0] = loadGLTexture(R.raw.carbon);
		texture_O_letter[0] = loadGLTexture(R.raw.o_letter);
		texture_First_Screen[0] = loadGLTexture(R.raw.firstscreen1);
		
		// enable depth testing
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        // depth test to do
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        // We will always cull back faces for better performance
        GLES32.glEnable(GLES32.GL_CULL_FACE);
        
        // Set the background color
        GLES32.glClearColor(0.5f, 0.5f, 0.0f, 1.0f);
        
		
		doubleTap =0;
		singleTap = 0;
		longPress = 0;
		
		
		//set projectionMatrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}
	
	private int loadGLTexture(int imageFileResourceID)
	{
		BitmapFactory.Options options = new BitmapFactory.Options(); //iner class
		options.inScaled = false;
		
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), imageFileResourceID, options);
		
		int[] texture = new int[1];
		
		GLES32.glGenTextures(1, texture, 0);
		
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);
		
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
	
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
		
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
		
		return (texture[0]);
	}
	
	private void resize(int width, int height)
    {
        if(height == 0)
			height = 1;
        GLES32.glViewport(0, 0, width, height);
       /* if(width <= height)
				Matrix.orthoM(orthographicProjectionMatrix, 0, -100.0f, 100.0f,
				(100.0f *(height/width)), (100.0f *(height / width)), -100.0f, 100.0f);
				
		else
				Matrix.orthoM(orthographicProjectionMatrix, 0, (-100.0f *(width / height)),
				(100.0f * (width/height)), -100.0f, 100.0f, -100.0f, 100.0f);
		*/		
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, 
					(float)width/(float)height, 0.1f, 100.0f);
	}
	
	
	private void update()
	{
		
		gAngleCube = gAngleCube + 1.5f;
		if (gAngleCube >= 360.0f)
			gAngleCube = 0.0f;
			/*
		gAnglePyramid = gAnglePyramid + 1.5f;
		if (gAnglePyramid >= 360.0f)
			gAnglePyramid = 0.0f;*/
	}
	void DrawSphere()
	{
	
		GLES32.glBindVertexArray(vao_sphere[0]);
		
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//unbind vao
		GLES32.glBindVertexArray(0);
	}
	void DrawCube()
	{
		GLES32.glBindVertexArray(vao_cube[0]);
		
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0 ,4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4 ,4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8 ,4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12 ,4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16 ,4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20 ,4);
			
		//unbind vao
		GLES32.glBindVertexArray(0);
	}
	
	public void display()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject);
		if(doubleTap ==1)//methane
		{
			GLES32.glUniform1i(doubleTapUniform, 1);
			
			GLES32.glUniform3fv(ldUniform, 1, light_ambient, 0);
			GLES32.glUniform3fv(ldUniform, 1, light_diffuse, 0);
            GLES32.glUniform3fv(lsUniform, 1, light_specular, 0);

			GLES32.glUniform4fv(lightPositionUniform, 1, light_position, 0);
			
			GLES32.glUniform3fv(kaUniform, 1, material_ambient, 0);
            GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
            GLES32.glUniform3fv(ksUniform, 1, material_specular, 0);
            GLES32.glUniform1f(materialShininessUniform, material_shininess);
			
			DrawMethane();
		
			//GLES32.glUniform1i(singleTapUniform, 0);
			//GLES32.glUniform1i(longPressUniform, 0);
		}
		else if(doubleTap == 2)//benzene
		{
			//GLES32.glUniform1i(singleTapUniform, 1);
			GLES32.glUniform1i(doubleTapUniform, 1);
			
			GLES32.glUniform3fv(ldUniform, 1, light_ambient, 0);
			GLES32.glUniform3fv(ldUniform, 1, light_diffuse, 0);
            GLES32.glUniform3fv(lsUniform, 1, light_specular, 0);

			GLES32.glUniform4fv(lightPositionUniform, 1, light_position, 0);
			
			GLES32.glUniform3fv(kaUniform, 1, material_ambient, 0);
            GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
            GLES32.glUniform3fv(ksUniform, 1, material_specular, 0);
            GLES32.glUniform1f(materialShininessUniform, material_shininess);
			
			DrawBenzene();
			
			//GLES32.glUniform1i(doubleTapUniform, 0);
			//GLES32.glUniform1i(longPressUniform, 0);
		}
		else if(doubleTap == 3)
		{
			//GLES32.glUniform1i(longPressUniform, 1);
			GLES32.glUniform1i(doubleTapUniform, 1);
			
			GLES32.glUniform3fv(ldUniform, 1, light_ambient, 0);
			GLES32.glUniform3fv(ldUniform, 1, light_diffuse, 0);
            GLES32.glUniform3fv(lsUniform, 1, light_specular, 0);

			GLES32.glUniform4fv(lightPositionUniform, 1, light_position, 0);
			
			GLES32.glUniform3fv(kaUniform, 1, material_ambient, 0);
            GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
            GLES32.glUniform3fv(ksUniform, 1, material_specular, 0);
            GLES32.glUniform1f(materialShininessUniform, material_shininess);
		
			DrawAlcohol();
			//GLES32.glUniform1i(doubleTapUniform, 0);
			//GLES32.glUniform1i(singleTapUniform, 0);
		}
		else
		{
			GLES32.glUniform1i(doubleTapUniform, 1);
			
			GLES32.glUniform3fv(ldUniform, 1, light_ambient, 0);
			GLES32.glUniform3fv(ldUniform, 1, light_diffuse, 0);
            GLES32.glUniform3fv(lsUniform, 1, light_specular, 0);

			GLES32.glUniform4fv(lightPositionUniform, 1, light_position, 0);
			
			GLES32.glUniform3fv(kaUniform, 1, material_ambient, 0);
            GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
            GLES32.glUniform3fv(ksUniform, 1, material_specular, 0);
            GLES32.glUniform1f(materialShininessUniform, material_shininess);
			
			DrawFirstScreen();
			
			//GLES32.glUniform1i(singleTapUniform, 0);
			//GLES32.glUniform1i(longPressUniform, 0);
		}
		
		GLES32.glUseProgram(0);
	
		update();
		//flush
		requestRender();
	
	}
	
	
	 void uninitialize()
    {
        // code
        // destroy vao
        
        
		if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
		
		if(vao_rectangle[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_rectangle, 0);
            vao_rectangle[0]=0;
        }

        // destroy vao
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
		if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
		if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }
        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0)
            {
                // detach vertex shader from shader program object
                GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
                // delete vertex shader object
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }
            
            if(fragmentShaderObject != 0)
            {
                // detach fragment  shader from shader program object
                GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
                // delete fragment shader object
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        // delete shader program object
        if(shaderProgramObject != 0)
        {
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
		
		if(texture_H_letter[0] != 0)
		{
			GLES32.glDeleteTextures(1, texture_H_letter,0);
			texture_H_letter[0] = 0;
		}
		if(texture_O_letter[0] != 0)
		{
			GLES32.glDeleteTextures(1, texture_O_letter,0);
			texture_O_letter[0] = 0;
		}
		if(texture_C_letter[0] != 0)
		{
			GLES32.glDeleteTextures(1, texture_C_letter,0);
			texture_C_letter[0] = 0;
		}
		
		if(texture_First_Screen[0] != 0)
		{
			GLES32.glDeleteTextures(1, texture_First_Screen,0);
			texture_First_Screen[0] = 0;
		}
    }
	
	
	void DrawFirstScreen()
	{
		float modelMatrix[] = new float[16];
		float viewMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];
		float scaleMatrix[] =new float[16];
	
		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix,0);	
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f,0.0f,-3.0f);//perspective change

		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		
		GLES32.glBindVertexArray(vao_rectangle[0]);
			
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_First_Screen[0]);
		
		GLES32.glUniform1i(texture0_sampler_uniform, 0);
		
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0 ,4);
		
		GLES32.glBindVertexArray(0);
		
	}
	void DrawBenzene()
	{
		float material_diffuseBlack[] = {0.0f,0.0f,0.0f,1.0f};
		float material_diffuseBlue[] = {0.0f,0.0f,1.0f,1.0f};
		
		float modelMatrix[] = new float[16];
		float viewMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];
		float scaleMatrix[] =new float[16];
		
		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix,0);	
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f,0.0f,-15.0f);//perspective change

		Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 1.0f, 1.0f);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);
		
		myStack.pushMatrix(modelMatrix);

		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		
	
		//Draw Upper C Bond
		///////////////////////////////////////////////////
		
		Matrix.translateM(modelMatrix, 0, 0.0f,3.0f,0.0f);//perspective change
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 25.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);
		
		Matrix.scaleM(scaleMatrix, 0, 0.4f, 0.4f, 0.4f);
		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		//TriangleBlock
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		
		GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlack, 0);
		
		
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_C_letter[0]);
		
		GLES32.glUniform1i(texture0_sampler_uniform, 0);
		//Draw Upper Sphere
		mysphere.DrawSphere();
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		//////////////////////////////////////////////////
		
		modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		///////////////////////////////////////////////////
		//CUBE connector ( Upper C and Upper H )
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f, 4.0f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 180.0f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 3.5f, 4.5f, 2.0f);


		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);

			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		//DrawCube();
		myCylinder.DrawCylinder();	
		
		///////////////////////////////////////////////////
			modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		///////////////////////////////////////////////////
	//upper Small sphere
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f, 4.7f,0.0f);//perspective change
		
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 45.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);
		
		Matrix.scaleM(scaleMatrix, 0, 0.25f, 0.25f, 0.25f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		//TriangleBlock
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
	
		GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);
		
		//Draw Middle Sphere
		//DrawSphere();

		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		
		//Draw Upper Sphere
		mysphere.DrawSphere();
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
		//////////////////////////////////////////////////
		
		modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		///////////////////////////////////////////////////
		//CUBE connector ( left and upper  C)
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, -1.5f, 2.5f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 60.0f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 5.0f, 4.5f, 4.0f);

		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		

		GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		
		myCylinder.DrawCylinder();	
		
		///////////////////////////////////////////////////
			modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		///////////////////////////////////////////////////
		//Left Upper C 
		
		Matrix.setIdentityM(scaleMatrix, 0);
	
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, -2.9f,1.8f,0.0f);//perspective change
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 25.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.4f, 0.4f, 0.4f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		//TriangleBlock
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
	
		GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlack, 0);
		
		
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_C_letter[0]);
		
		//Draw Upper Sphere
		mysphere.DrawSphere();
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
///////////////////////////////////////////////////
			modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
////////////////////////////////////////////////////
		
		//CUBE connector ( left Upper C and left H )
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, -4.0f, 2.0f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 120.0f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 3.5f, 4.5f, 2.0f);

		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
//		if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		//DrawCube();
		myCylinder.DrawCylinder();	
		
		///////////////////////////////////////////////////
			modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		

		//////////////////////////////////////////////////
		
		//left Upper Small sphere (H)
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, -4.7f, 2.4f,0.0f);//perspective change
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 45.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		
		Matrix.scaleM(scaleMatrix, 0, 0.25f, 0.25f, 0.25f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		//TriangleBlock
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
	
		GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);
		
		//Draw Middle Sphere
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		
		//Draw Upper Sphere
		mysphere.DrawSphere();
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		///////////////////////////////////////////////////
			modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		

		//////////////////////////////////////////////////
		/////////////////////////////////////////////////
		//CUBE connector ( between Left Bottom and left Upper C)
	
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		//-2.0f,-1.5f,-12.0f
		Matrix.translateM(modelMatrix, 0, -3.2f, 0.0f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 2.0f, 4.5f, 4.7f);

		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		

		GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		
		myCylinder.DrawCylinder();	
		
		/////////////////////////
		//TO DO : IMP
		Matrix.translateM(modelMatrix, 0, 0.2f, 0.0f, 0.0f);
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		

		GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		
		myCylinder.DrawCylinder();	
		
			///////////////////////////////////////////////////
			modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		

		//////////////////////////////////////////////////
		//////////////////////////////////////////////////
		
		//left Upper Small sphere (H)
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, -4.7f, -2.5f,0.0f);//perspective change
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 45.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.25f, 0.25f, 0.25f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		//TriangleBlock
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
	
		GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);
		
		//Draw Middle Sphere
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		
		//Draw Upper Sphere
		mysphere.DrawSphere();
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		///////////////////////////////////////////////////
			modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		

		//////////////////////////////////////////////////
		//////////////////////////////////////////////////
		//Left Lower C Bond
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.setIdentityM(rotationMatrix, 0);
		
		
		Matrix.translateM(modelMatrix, 0, -2.9f,-1.8f,0.0f);//perspective change

		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 25.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

	
		
		Matrix.scaleM(scaleMatrix, 0, 0.4f, 0.4f, 0.4f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);


		//TriangleBlock
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
	
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlack, 0);
		
		//	DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_C_letter[0]);
		
		//Draw Upper Sphere
		mysphere.DrawSphere();
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
	///////////////////////////////////////////////////
			modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		//////////////////////////////////////////////////
		
		//CUBE connector ( left bottom(C) and his left sphere(H) )
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, -4.0f, -2.2f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 60.0f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 3.5f, 4.5f, 2.0f);

	
		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
//		if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		//DrawCube();
		myCylinder.DrawCylinder();	
		///////////////////////////////////////////////////
			modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		//////////////////////////////////////////////////
		
		//CUBE connector ( left Lower C and down C )
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, -1.45f, -2.6f,0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 120.0f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 5.0f, 4.5f, 4.0f);


		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		

			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		
		myCylinder.DrawCylinder();
///////////////////////////////////////////////////
			modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		//////////////////////////////////////////////////
				
		/////////////////////////////////////////////////
		
		//Draw Lower C Bond
		///////////////////////////////////////////////////
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f,-3.0f,0.0f);//perspective change
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 25.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.4f, 0.4f, 0.4f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		//TriangleBlock
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		
		GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlack, 0);
		
		//Draw Upper Sphere
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_C_letter[0]);
		
		//Draw Upper Sphere
		mysphere.DrawSphere();
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
		//////////////////////////////////////////////////
		
		modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		///////////////////////////////////////////////////
		//CUBE connector ( Lower C and Lower H )
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f, -3.7f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 180.0f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 3.5f, 4.5f, 2.0f);


		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);

			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		//DrawCube();
		myCylinder.DrawCylinder();	
		
		///////////////////////////////////////////////////
		modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		///////////////////////////////////////////////////
	//Lower Small sphere
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f, -4.7f,0.0f);//perspective change
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 45.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.25f, 0.25f, 0.25f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		//TriangleBlock
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
	
		GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);
		
		//Draw Middle Sphere
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		
		//Draw Upper Sphere
		mysphere.DrawSphere();
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		//////////////////////////////////////////////////
		///////////////////////////////////////////////////
		modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		///////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////RIGHT////////////////////////////////////////
		
		/////////////////////////////////////////////////////////////////////
		//CUBE connector ( right and upper )
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 1.5f, 2.6f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 120.0f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 2.0f, 4.5f, 4.7f);
//2.0f, 4.5f, 4.7f
		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
//		if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		//DrawCube();
		myCylinder.DrawCylinder();	
		/////////////////////////////
		Matrix.translateM(modelMatrix, 0, 0.2f, 0.0f, 0.0f);
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		myCylinder.DrawCylinder();
		///////////////////////////////////////////////////
		modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		///////////////////////////////////////////////////
			
	//Right Upper Sphere
		Matrix.setIdentityM(scaleMatrix, 0);
			
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 2.9f,1.8f,0.0f);//perspective change
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 25.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.4f, 0.4f, 0.4f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		//TriangleBlock
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
	
	//	float material_diffuseRed[] = {1.0f,0.0f,0.0f,1.0f};
		//Give Red Color to Sphere
		//if(singleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlack, 0);
		
		//Draw Middle Sphere
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_C_letter[0]);
		
		//Draw Upper Sphere
		mysphere.DrawSphere();
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		///////////////////////////////////////////////////
		modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		///////////////////////////////////////////////////
	
		//CUBE connector ( Right )
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		//-2.0f,-1.5f,-12.0f
		Matrix.translateM(modelMatrix, 0, 2.9f, 0.0f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 5.0f, 4.5f, 4.5f);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
//		if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		//DrawCube();
		myCylinder.DrawCylinder();	
		
		///////////////////////////////////////////////////
		modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		///////////////////////////////////////////////////
	
			
		//right down sphere
		Matrix.setIdentityM(scaleMatrix, 0);
	
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 2.9f,-1.8f,0.0f);//perspective change

		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 25.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.4f, 0.4f, 0.4f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);


		//TriangleBlock
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		
		
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlack, 0);
		
		//Draw Middle Sphere
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_C_letter[0]);
		
		//Draw Upper Sphere
		mysphere.DrawSphere();
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		///////////////////////////////////////////////////
		modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		///////////////////////////////////////////////////
	
	
		//CUBE connector ( right lower C and right H )
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 4.0f, -2.1f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 120.0f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 3.5f, 4.5f, 2.0f);

	
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
//		if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		//DrawCube();
		myCylinder.DrawCylinder();	
		
		///////////////////////////////////////////////////
		
		///////////////////////////////////////////////////
		modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		///////////////////////////////////////////////////
	
	
		//CUBE connector ( right upper C and right H )
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 4.0f, 2.1f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 60.0f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 3.5f, 4.5f, 2.0f);

	
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
//		if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		//DrawCube();
		myCylinder.DrawCylinder();	
		
		///////////////////////////////////////////////////
		modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		///////////////////////////////////////////////////
		
		//CUBE connector ( right and down )
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 1.5f, -2.3f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 60.0f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 2.0f, 4.5f, 4.7f);

		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
//		if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		//DrawCube();
		myCylinder.DrawCylinder();	
		
		/////////////////////////////
		Matrix.translateM(modelMatrix, 0, 0.2f, 0.0f, 0.0f);
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		myCylinder.DrawCylinder();	
		///////////////////////////////////////////////////
		modelMatrix = myStack.PopMatrix();

		myStack.pushMatrix(modelMatrix);
		//////////////////////////////////////////////////
		
		//right Upper Small sphere (H)
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 4.7f, 2.4f,0.0f);//perspective change
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 45.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.25f, 0.25f, 0.25f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		//TriangleBlock
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
	
		GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);
		
		//Draw Middle Sphere
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		
		//Draw Upper Sphere
		mysphere.DrawSphere();
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		///////////////////////////////////////////////////
			modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		

		//////////////////////////////////////////////////
		
		//right down Small sphere (H)
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 4.7f, -2.4f,0.0f);//perspective change
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 45.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.25f, 0.25f, 0.25f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		//TriangleBlock
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
	
		GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);
		
		//Draw Middle Sphere
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		
		//Draw Upper Sphere
		mysphere.DrawSphere();
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		///////////////////////////////////////////////////
		modelMatrix = myStack.PopMatrix();

			//push model matrix
		//myStack.pushMatrix(modelMatrix);
		

		//////////////////////////////////////////////////
	}
	void DrawAlcohol()
	{
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		float modelMatrix[] = new float[16];
		float viewMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];
		float scaleMatrix[] =new float[16];
		
		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix,0);	
		Matrix.setIdentityM(rotationMatrix, 0);
	
		//TriangleBlock
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		
		
		Matrix.translateM(modelMatrix, 0, 0.0f,0.0f,-9.0f);//perspective change
		
		//Matrix.rotateM(rotationMatrix, 0, 270.0f, 0.0f, 0.0f, 1.0f);
		Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f,1.0f,1.0f);
		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		myStack.pushMatrix(modelMatrix);
		
		
		Matrix.translateM(modelMatrix, 0, -2.0f,0.0f,0.0f);
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);
		
		myStack.pushMatrix(modelMatrix);
		////////////////////////////////////////////////////////////
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 0.0f,1.0f,0.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);
		
		Matrix.scaleM(scaleMatrix, 0, 0.25f, 0.25f, 0.25f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		float material_diffuseBlue[] = {0.0f,0.0f,1.0f,1.0f};
		
		float material_diffuseBlack[] = {0.0f,0.0f,0.0f,1.0f};
		float material_diffuseOrange[] = {1.0f,0.65f,0.0f,1.0f};
	
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlack, 0);

		//Draw Middle Sphere
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_C_letter[0]);
		//Draw Upper Sphere
		mysphere.DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
		//remove middle position
		modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		

		//Upper Connector
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f, 0.75f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 1.0f, 1.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 1.5f, 2.5f, 3.5f);

		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		//DrawCube();
		myCylinder.DrawCylinder();	
		
		
		/////////////////////////////////////////////////////////////////////
		
		modelMatrix = myStack.PopMatrix();
		
			//push model matrix
		myStack.pushMatrix(modelMatrix);
		


				//CUBE connector right
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.75f, -0.5f, 0.0f);
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);// -19.5 in z-axis
		Matrix.rotateM(rotationMatrix, 0, 109.5f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, -19.5f, 0.0f, 0.0f, 1.0f);
		//Matrix.rotateM(rotationMatrix, 0, -19.5f, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 1.5f, 2.5f, 2.0f);

		
		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		myCylinder.DrawCylinder();	
		////////////////////////////////////////////////////////////

		
		modelMatrix = myStack.PopMatrix();
	
			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		////////////////////////////////////////////////////////////
		//right sphere
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 1.2f, -0.8f, 0.0f);
		
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 0.0f, 1.0f, 0.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		//Matrix.scaleM(scaleMatrix, 0, 0.6f, 0.6f, 0.6f);
		Matrix.scaleM(scaleMatrix, 0, 0.15f, 0.15f, 0.15f);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
		
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		//float material_diffuseBlue[] = {0.0f,0.0f,1.0f,1.0f};
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);

		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		//Draw Upper Sphere
		mysphere.DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
	////////////////////////////////////////////////////////////

		
		modelMatrix = myStack.PopMatrix();
	
			//push model matrix
		myStack.pushMatrix(modelMatrix);
	
		
		////////////////////////////////////////////////////////////
	
////////////////////////////////////////////////////////////

		//CUBE connector (left)
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, -0.5f, -0.6f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 2*109.5f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 1.5f, 2.5f, 2.0f);

		
		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		myCylinder.DrawCylinder();	
		
		
	////////////////////////////////////////////////////////////

		
		modelMatrix = myStack.PopMatrix();
	
			//push model matrix
		myStack.pushMatrix(modelMatrix);
	
		
		////////////////////////////////////////////////////////////
	
		// left sphere
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, -0.9f, -0.9f, 0.0f);
		
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 0.0f, 1.0f, 0.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.15f, 0.15f, 0.15f);
		
	

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
		
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		//float material_diffuseBlue[] = {0.0f,0.0f,1.0f,1.0f};
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);

		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		//Draw Upper Sphere
		mysphere.DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
	
		////////////////////////////////////////////////////////////
	
		
		modelMatrix = myStack.PopMatrix();
	
			//push model matrix
		myStack.pushMatrix(modelMatrix);
	
		
		////////////////////////////////////////////////////////////
	
		//CUBE connector (BACK)
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f, -0.5f, -0.5f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 160.0f, 1.0f, 0.0f, 0.0f);
	
		
		Matrix.scaleM(scaleMatrix, 0, 1.5f, 2.5f, 2.0f);

		
	
		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		myCylinder.DrawCylinder();	
		
		
		////////////////////////////////////////////////////////////
	
		
		modelMatrix = myStack.PopMatrix();
	
			//push model matrix
		myStack.pushMatrix(modelMatrix);
	
		
		/////////////////////////////////////////////////
		////////////////////////////////////////////////////////////
		//BACK sphere
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f, -0.8f, -1.0f);
		
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 0.0f, 1.0f, 0.0f);
	
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.15f, 0.15f, 0.15f);
		
	//	Matrix.setIdentityM(modelViewMat,0);

		//Matrix.translateM(modelViewMat, 0,  0.0f, 0.0f, -1.0f);

		
//		Matrix.multiplyMM(rotationMatrix1, 0, rotationMatrix1, 0, modelViewMat, 0);

	//	Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix1, 0);

	

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
		
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		//float material_diffuseBlue[] = {0.0f,0.0f,1.0f,1.0f};
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);

		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		//Draw Upper Sphere
		mysphere.DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
	
		////////////////////////////////////////////////////////////
	
		
		modelMatrix = myStack.PopMatrix();
	
	//////////////////////////////////////////////////////////****************2nd methane***************//////////////////////////////////////////////
	Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
	Matrix.translateM(modelMatrix, 0, 0.0f,2.0f,0.0f);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);
		
		myStack.pushMatrix(modelMatrix);
		////////////////////////////////////////////////////////////
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 25.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.25f, 0.25f, 0.25f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		//float material_diffuseBlue[] = {0.0f,0.0f,1.0f,1.0f};
		
//		float material_diffuseBlack[] = {0.0f,0.0f,0.0f,1.0f};
	
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlack, 0);

		//Draw Middle Sphere
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_C_letter[0]);
		//Draw Upper Sphere
		mysphere.DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
		//remove middle position
		modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		

		
		

				//CUBE connector right
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.75f, -0.5f, 0.0f);
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);// -19.5 in z-axis
		Matrix.rotateM(rotationMatrix, 0, 109.5f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, -19.5f, 0.0f, 0.0f, 1.0f);
		//Matrix.rotateM(rotationMatrix, 0, -19.5f, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 1.5f, 2.5f, 2.0f);

		
		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		myCylinder.DrawCylinder();	
		////////////////////////////////////////////////////////////

		
		modelMatrix = myStack.PopMatrix();
	
			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		////////////////////////////////////////////////////////////
		//right sphere
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 1.2f, -0.8f, 0.0f);
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 45.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.15f, 0.15f, 0.15f);
			

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
		
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		//float material_diffuseBlue[] = {0.0f,0.0f,1.0f,1.0f};
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);

		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		//Draw Upper Sphere
		mysphere.DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
	////////////////////////////////////////////////////////////

		
		modelMatrix = myStack.PopMatrix();
	
			//push model matrix
		myStack.pushMatrix(modelMatrix);
	
		
		////////////////////////////////////////////////////////////
	
////////////////////////////////////////////////////////////

		//CUBE connector (left)
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, -0.5f, -0.6f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 2*109.5f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 1.5f, 2.5f, 2.0f);

		
		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		myCylinder.DrawCylinder();	
		
		
	////////////////////////////////////////////////////////////

		
		modelMatrix = myStack.PopMatrix();
	
			//push model matrix
		myStack.pushMatrix(modelMatrix);
	
		
		////////////////////////////////////////////////////////////
	
		// left sphere
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, -0.9f, -0.9f, 0.0f);
		

		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 45.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.15f, 0.15f, 0.15f);
		
	

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
		
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		//float material_diffuseBlue[] = {0.0f,0.0f,1.0f,1.0f};
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);

		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		//Draw Upper Sphere
		mysphere.DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
	
		////////////////////////////////////////////////////////////
	
		
		modelMatrix = myStack.PopMatrix();
	
			//push model matrix
		myStack.pushMatrix(modelMatrix);
	
		
		////////////////////////////////////////////////////////////
	
		/////////////////////////////////////////////////////////////////**********O H Part************//////////////////////
		

		//Upper Connector
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f, 0.75f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 1.0f, 1.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 1.5f, 2.5f, 2.0f);

		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		//DrawCube();
		myCylinder.DrawCylinder();	
		
		/////////////////////////////
		/////////////////////////////////////////////////////////////////////
		
		modelMatrix = myStack.PopMatrix();
		
			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		////////////////////////
		
		//Upper sphere (o)
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f, 1.7f, 0.0f);
		
		
	//	Matrix.rotateM(rotationMatrix, 0, -45.0f, 0.0f, 0.0f, 1.0f);
		
		//Matrix.scaleM(scaleMatrix, 0, 0.6f, 0.6f, 0.6f);



	//	Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 45.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.25f, 0.25f, 0.25f);
		
	

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
		
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		//float material_diffuseBlue[] = {0.0f,0.0f,1.0f,1.0f};
	//	if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseOrange, 0);

		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_O_letter[0]);
		//Draw Upper Sphere
		mysphere.DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		/////////////////////////////////////////////////////////////////////
		
		modelMatrix = myStack.PopMatrix();
		
			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		////////////////////////
		//Upper Connector
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.8f, 1.7f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 0.0f, 1.0f, 0.0f);
		
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 1.0f, 1.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 1.0f, 1.5f, 2.0f);

		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		//DrawCube();
		myCylinder.DrawCylinder();	
		
		
		/////////////////////////////////////////////////////////////////////
		
		modelMatrix = myStack.PopMatrix();
		
			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		
		////////////////////////////////////////////////////////////
		//right sphere (H)
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 1.5f, 1.7f, 0.0f);
		
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 45.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.15f, 0.15f, 0.15f);
			

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
		
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		//float material_diffuseBlue[] = {0.0f,0.0f,1.0f,1.0f};
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);

		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		//Draw Upper Sphere
		mysphere.DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
	
		
		///////////////////////////////////////////////
		modelMatrix = myStack.PopMatrix();
	
	modelMatrix = myStack.PopMatrix();
		
	}
	
	void DrawMethane()
	{
		
							
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		float modelMatrix[] = new float[16];
		float viewMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];
		float scaleMatrix[] =new float[16];
		
		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix,0);	
		Matrix.setIdentityM(rotationMatrix, 0);
	
		//TriangleBlock
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		
		
		Matrix.translateM(modelMatrix, 0, 0.0f,0.0f,-5.0f);//perspective change
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 0.0f, 0.0f, 1.0f);
		Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 1.0f, 1.0f);
		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		myStack.pushMatrix(modelMatrix);
		
		
		////////////////////////////////////////////////////////////
		
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 25.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.28f, 0.28f, 0.28f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		float material_diffuseBlue[] = {0.0f,0.0f,1.0f,1.0f};
		
		float material_diffuseBlack[] = {0.0f,0.0f,0.0f,1.0f};
	
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlack, 0);

		//Draw Middle Sphere
		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_C_letter[0]);
		//Draw Upper Sphere
		mysphere.DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
		//remove middle position
		modelMatrix = myStack.PopMatrix();

			//push model matrix
		myStack.pushMatrix(modelMatrix);
		

		//Upper Connector
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f, 0.75f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 1.0f, 1.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 1.5f, 2.5f, 1.5f);

		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
	//	if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		//DrawCube();
		myCylinder.DrawCylinder();	
		
		
		/////////////////////////////////////////////////////////////////////
		
		modelMatrix = myStack.PopMatrix();
		
			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		////////////////////////
		
		//Upper sphere
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f, 1.3f, 0.0f);
		
		
	//	Matrix.rotateM(rotationMatrix, 0, -45.0f, 0.0f, 0.0f, 1.0f);
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 25.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.15f, 0.15f, 0.15f);



		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
		
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		//float material_diffuseBlue[] = {0.0f,0.0f,1.0f,1.0f};
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);

		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		//Draw Upper Sphere
		mysphere.DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		modelMatrix = myStack.PopMatrix();
	
		
			//push model matrix
		myStack.pushMatrix(modelMatrix);
		

				//CUBE connector right
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.75f, -0.5f, 0.0f);
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);// -19.5 in z-axis
		Matrix.rotateM(rotationMatrix, 0, 109.5f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, -19.5f, 0.0f, 0.0f, 1.0f);
		//Matrix.rotateM(rotationMatrix, 0, -19.5f, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 1.5f, 2.5f, 2.0f);

		
		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		myCylinder.DrawCylinder();	
		////////////////////////////////////////////////////////////

		
		modelMatrix = myStack.PopMatrix();
	
			//push model matrix
		myStack.pushMatrix(modelMatrix);
		
		////////////////////////////////////////////////////////////
		//right sphere
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 1.2f, -0.8f, 0.0f);
		

		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 25.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.15f, 0.15f, 0.15f);



		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
		
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		//float material_diffuseBlue[] = {0.0f,0.0f,1.0f,1.0f};
	//	if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);

		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		//Draw Upper Sphere
		mysphere.DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
	////////////////////////////////////////////////////////////

		
		modelMatrix = myStack.PopMatrix();
	
			//push model matrix
		myStack.pushMatrix(modelMatrix);
	
		
		////////////////////////////////////////////////////////////
	
////////////////////////////////////////////////////////////

		//CUBE connector (left)
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, -0.5f, -0.6f, 0.0f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 270.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 2*109.5f, 0.0f, 1.0f, 0.0f);
		//Matrix.rotateM(rotationMatrix, 0, gAngleCube, 1.0f, 0.0f, 0.0f);
		
		Matrix.scaleM(scaleMatrix, 0, 1.5f, 2.5f, 2.0f);

		
		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
	//	if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		myCylinder.DrawCylinder();	
		
		
	////////////////////////////////////////////////////////////

		
		modelMatrix = myStack.PopMatrix();
	
			//push model matrix
		myStack.pushMatrix(modelMatrix);
	
		
		////////////////////////////////////////////////////////////
	
		// left sphere
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, -0.9f, -0.9f, 0.0f);
		

		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 25.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.15f, 0.15f, 0.15f);



		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
		
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		//float material_diffuseBlue[] = {0.0f,0.0f,1.0f,1.0f};
	//	if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);

		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		//Draw Upper Sphere
		mysphere.DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
	
		////////////////////////////////////////////////////////////
	
		
		modelMatrix = myStack.PopMatrix();
	
			//push model matrix
		myStack.pushMatrix(modelMatrix);
	
		
		////////////////////////////////////////////////////////////
	
		//CUBE connector (BACK)
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f, -0.5f, -0.5f);
		
		
		Matrix.rotateM(rotationMatrix, 0, 160.0f, 1.0f, 0.0f, 0.0f);
	
		
		Matrix.scaleM(scaleMatrix, 0, 1.5f, 2.5f, 2.0f);

		
	
		
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
		myCylinder.DrawCylinder();	
		
		
		////////////////////////////////////////////////////////////
	
		
		modelMatrix = myStack.PopMatrix();
	
			//push model matrix
		myStack.pushMatrix(modelMatrix);
	
		
		/////////////////////////////////////////////////
		////////////////////////////////////////////////////////////
		//BACK sphere
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelMatrix, 0, 0.0f, -0.8f, -1.0f);
		

		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, 25.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 0.15f, 0.15f, 0.15f);



		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
		
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		//float material_diffuseBlue[] = {0.0f,0.0f,1.0f,1.0f};
		//if(doubleTap ==1)
			GLES32.glUniform3fv(kdUniform, 1, material_diffuseBlue, 0);

		//DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_H_letter[0]);
		//Draw Upper Sphere
		mysphere.DrawSphere();
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
	
		////////////////////////////////////////////////////////////
	
		
		modelMatrix = myStack.PopMatrix();
	
	
	
	
	}
		
}
