package com.astromedicomp.window_ChemicalFusion;

import java.lang.Math; // for sqrt()
import android.opengl.GLES32;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
// 382 Verticies
// 382 Normals
// 760 Triangles
// 1 Texture Coordinates
public class MySphere
{

	private float model_vertices[]=new float[1146];
    private float model_normals[]=new float[1146];
    private float model_textures[]=new float[764];
    private short model_elements[]=new short[2280];
	
	
	private int[] vao = new int[1];
	private int[] vbo_normal = new int[1];
	private int[] vbo_position = new int[1];
	private int[] vbo_texture = new int[1];
	private int[] vbo_element = new int[1];
	
	private int numElements1;
    private int numVertices1;
	
	// methods
    public MySphere()
    {
        // code
        
        numElements1=0;
        numVertices1=0;
    }
	public void uvSphere(float radius, int slices, int stacks)
	{
		
		
		int vertexCount = (int)((slices+1)*(stacks+1));
		/*float vertices[]= new float[3 * vertexCount];
		float normals[] = new float[3 * vertexCount];
		float texCoords[] = new float[ 2*vertexCount ];
		short indices[] = new short[2*slices*stacks*3 ];*/
		
		model_vertices = new float[3 * vertexCount];
		model_normals = new float[3 * vertexCount];
		model_textures = new float[ 2*vertexCount ];
		model_elements = new short[2*slices*stacks*3 ];
		
	double du = (double)(2*Math.PI)/slices;
   double dv = (double)Math.PI/stacks;
   int i,j;
   //float u,v,x,y,z;
   double u,v;
	float x,y,z;
   int indexV = 0;
   int indexT = 0;
   
   for (i = 0; i <= stacks; i++)
    {
      v = (double)(-Math.PI/2+ i*dv);
      for (j = 0; j <= slices; j++) 
	  {
         u = j*du;
         x = (float)(Math.cos(u)*Math.cos(v));
         y = (float)(Math.sin(u)*Math.cos(v));
         z = (float)Math.sin(v);
         model_vertices[indexV] = radius*x;
         model_normals[indexV++] = x;
         model_vertices[indexV] = radius*y;
         model_normals[indexV++] = y;
         model_vertices[indexV] = radius*z;
         model_normals[indexV++] = z;
         model_textures[indexT++] = ((float)j/(float)slices);
         model_textures[indexT++] = ((float)i/(float)stacks);
      } 
   }
   
   int k = 0;
   for (j = 0; j < stacks; j++) 
   {
      int row1 = (int)(j*(slices+1));
      int row2 = (int)((j+1)*(slices+1));
      for (i = 0; i < slices; i++) 
	  {
          model_elements[k++] = (short)(row1 + i);
          model_elements[k++] = (short)(row2 + i + 1);
          model_elements[k++] = (short)(row2 + i);
          model_elements[k++] = (short)(row1 + i);
          model_elements[k++] = (short)(row1 + i + 1);
          model_elements[k++] = (short)(row2 + i + 1);
      }  
   }
   numElements1 = k;
   System.out.println("MPD: numElements1 = " +numElements1);
   prepareToDraw();
   }
   
   public void prepareToDraw()
   {
		GLES32.glGenVertexArrays(1, vao, 0);
		GLES32.glBindVertexArray(vao[0]);
		
		GLES32.glGenBuffers(1, vbo_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);
		
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(model_vertices.length  * 4);
		
		byteBuffer.order(ByteOrder.nativeOrder());
		
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(model_vertices);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							model_vertices.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_VERTEX,
										3, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_VERTEX);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		//color
		//GLES32.glVertexAttrib3f(GLESMacros.MPD_ATTRIBUTE_COLOR, 0.39f, 0.58f, 0.9294f); //cornflower Blue 
	
		//GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_VERTEX);
		GLES32.glGenBuffers(1, vbo_normal, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_normal[0]);
		
		byteBuffer = ByteBuffer.allocateDirect(model_normals.length * 4);

		byteBuffer.order(ByteOrder.nativeOrder());
		
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(model_normals);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							model_normals.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_NORMAL,
										3, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_NORMAL);

		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		//textures
		GLES32.glGenBuffers(1, vbo_texture, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_texture[0]);
		
		byteBuffer = ByteBuffer.allocateDirect(model_textures.length * 4);

		byteBuffer.order(ByteOrder.nativeOrder());
		
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(model_textures);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
							model_textures.length *4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);
							
		GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_TEXTURE0,
										2, 
										GLES32.GL_FLOAT,
										false, 0, 0);
										
		GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_TEXTURE0);

		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// element vbo
        GLES32.glGenBuffers(1,vbo_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(model_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(model_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            model_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);

   }
   
   public void DrawSphere()
	{
	
		GLES32.glBindVertexArray(vao[0]);
		
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements1, GLES32.GL_UNSIGNED_SHORT, 0);

		//unbind vao
		GLES32.glBindVertexArray(0);
	}
	
	public int getNumberOfSphereVertices1()
    {
        // code
        return(numVertices1);
    }
    
    public int getNumberOfSphereElements1()
    {
        // code
        return(numElements1);
    }
	
}