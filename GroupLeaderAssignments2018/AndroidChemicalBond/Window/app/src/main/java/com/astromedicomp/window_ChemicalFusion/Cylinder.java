package com.astromedicomp.window_ChemicalFusion;

// Refrence:
// http://math.hws.edu/graphicsbook/demos/script/basic-object-models-IFS.js
import android.opengl.GLES32;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class Cylinder
{
    float vertices[];
    float normals[];
    float texCoords[];
    short indices[];

    private int[] vao = new int[1];
    private int[] vbo_position = new int[1];
    private int[] vbo_normal = new int[1];
    private int[] vbo_texture = new int[1];
    private int[] vbo_element = new int[1];

    public void InitializeCylinder( float radius, float height, int slices)
    {
        this.uvCylinder(radius,height,slices);
        // vao
        GLES32.glGenVertexArrays(1,vao,0);
        GLES32.glBindVertexArray(vao[0]);

        // position vbo
        GLES32.glGenBuffers(1,vbo_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position[0]);

        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(vertices);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                vertices.length * 4,
                verticesBuffer,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_VERTEX,
                3,
                GLES32.GL_FLOAT,
                false,0,0);

        GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_VERTEX);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

        // normal vbo
        GLES32.glGenBuffers(1,vbo_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_normal[0]);

        byteBuffer=ByteBuffer.allocateDirect(normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(normals);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                normals.length * 4,
                verticesBuffer,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_NORMAL,
                3,
                GLES32.GL_FLOAT,
                false,0,0);

        GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_NORMAL);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

        // texture vbo
        GLES32.glGenBuffers(1,vbo_texture,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_texture[0]);

        byteBuffer=ByteBuffer.allocateDirect(texCoords.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(texCoords);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                texCoords.length * 4,
                verticesBuffer,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.MPD_ATTRIBUTE_TEXTURE0,
                2,
                GLES32.GL_FLOAT,
                false,0,0);

        GLES32.glEnableVertexAttribArray(GLESMacros.MPD_ATTRIBUTE_TEXTURE0);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

        // element vbo
        GLES32.glGenBuffers(1,vbo_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_element[0]);

        byteBuffer=ByteBuffer.allocateDirect(indices.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(indices);
        elementsBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                indices.length * 2,
                elementsBuffer,
                GLES32.GL_STATIC_DRAW);

        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);
    }

    public void DrawCylinder()
    {
        GLES32.glBindVertexArray(vao[0]);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_element[0]);

        GLES32.glDrawElements(GLES32.GL_TRIANGLES, indices.length, GLES32.GL_UNSIGNED_SHORT, 0);

        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);
        GLES32.glBindVertexArray(0);
    }

    void uvCylinder( float radius, float height, int slices)
    {
        boolean noTop= false, noBottom= false;

        if (radius==0)
        {
            radius = 0.5f;
        }

        if (height==0)
        {
            height= 2.0f* radius;
        }

        if (slices==0)
        {
            slices=32;
        }
        int vertexCount = 2*(slices+1);
        if (!noTop)
            vertexCount += slices + 2;
        if (!noBottom)
            vertexCount += slices + 2;
        int triangleCount = 2*slices;
        if (!noTop)
            triangleCount += slices;
        if (!noBottom)
            triangleCount += slices;
         vertices = new float[vertexCount*3];
         normals = new float[vertexCount*3];
         texCoords = new float[vertexCount*2];
         indices = new short[triangleCount*3];
        double du = 2*Math.PI / slices;
        int kv = 0;
        int kt = 0;
        int k = 0;
        int i,j;
        double  u,v;
        for (i = 0; i <= slices; i++) {
            u = i*du;
            double c = Math.cos(u);
            double s = Math.sin(u);
            vertices[kv] = (float)c*radius;
            normals[kv++] = (float)c;
            vertices[kv] = (float)s*radius;
            normals[kv++] = (float)s;
            vertices[kv] = -height/2;
            normals[kv++] = 0;
            texCoords[kt++] = (float)(i/slices);
            texCoords[kt++] = 0;
            vertices[kv] = (float)c*radius;
            normals[kv++] = (float)c;
            vertices[kv] = (float)s*radius;
            normals[kv++] = (float)s;
            vertices[kv] = height/2;
            normals[kv++] = 0;
            texCoords[kt++] = (float)(i/slices);
            texCoords[kt++] = 1;
        }
        for (i = 0; i < slices; i++) {
            indices[k++] = (short)(2*i);
            indices[k++] = (short)(2*i+3);
            indices[k++] = (short)(2*i+1);
            indices[k++] = (short)(2*i);
            indices[k++] = (short)(2*i+2);
            indices[k++] = (short)(2*i+3);
        }
        float startIndex = kv/3;
        if (!noBottom) {
            vertices[kv] = 0;
            normals[kv++] = 0;
            vertices[kv] = 0;
            normals[kv++] = 0;
            vertices[kv] = -height/2;
            normals[kv++] = -1;
            texCoords[kt++] = 0.5f;
            texCoords[kt++] = 0.5f;
            for (i = 0; i <= slices; i++) {
                u = 2*Math.PI - i*du;
                double c = Math.cos(u);
                double s = Math.sin(u);
                vertices[kv] = (float)c*radius;
                normals[kv++] = 0;
                vertices[kv] = (float)s*radius;
                normals[kv++] = 0;
                vertices[kv] = -height/2;
                normals[kv++] = -1;
                texCoords[kt++] = (float)(0.5f - 0.5f*c);
                texCoords[kt++] = (float)(0.5f + 0.5f*s);
            }
            for (i = 0; i < slices; i++) {
                indices[k++] = (short)startIndex;
                indices[k++] = (short)(startIndex + i + 1);
                indices[k++] = (short)(startIndex + i + 2);
            }
        }
        startIndex = kv/3;
        if (!noTop) {
            vertices[kv] = 0;
            normals[kv++] = 0;
            vertices[kv] = 0;
            normals[kv++] = 0;
            vertices[kv] = height/2;
            normals[kv++] = 1;
            texCoords[kt++] = 0.5f;
            texCoords[kt++] = 0.5f;
            for (i = 0; i <= slices; i++) {
                u = i*du;
                double c = Math.cos(u);
                double s = Math.sin(u);
                vertices[kv] = (float)c*radius;
                normals[kv++] = 0;
                vertices[kv] = (float)s*radius;
                normals[kv++] = 0;
                vertices[kv] = height/2;
                normals[kv++] = 1;
                texCoords[kt++] = (float)(0.5f + 0.5f*c);
                texCoords[kt++] = (float)(0.5f + 0.5f*s);
            }
            for (i = 0; i < slices; i++) {
                indices[k++] = (short)startIndex;
                indices[k++] = (short)(startIndex + i + 1);
                indices[k++] = (short)(startIndex + i + 2);
            }
        }

    }
}
