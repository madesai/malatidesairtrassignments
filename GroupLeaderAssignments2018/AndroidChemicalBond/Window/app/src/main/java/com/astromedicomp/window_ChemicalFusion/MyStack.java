package com.astromedicomp.window_ChemicalFusion;

import java.util.Stack;

/*public class MyStack
{
    Stack<float[]> stkModelData;
    MyStack()
    {
        stkModelData = new Stack();
    }
    public void PushMatrix(float[] modelMatrix)
    {
        stkModelData.push(modelMatrix);
    }

    public float[] PopMatrix()
    {
        return stkModelData.pop();
    }

}
*/

public class MyStack
{
		Stack<float[]> stkModelData;
		MyStack()
		{
			stkModelData = new Stack<float[]>();
		}
		
		public void pushMatrix(float[] modelMatrix)
		{
			float insertModelMatrix[] = new float[16];
			
			for(int i = 0;i < modelMatrix.length; i++)
			{
				insertModelMatrix[i] = modelMatrix[i];
			}
			
			stkModelData.push(insertModelMatrix);
		}
		
		public float[] PopMatrix()
		{
			return stkModelData.pop();
		}
}
