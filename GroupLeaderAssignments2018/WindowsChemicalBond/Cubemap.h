#pragma once
//malati
#include "Common.h"

GLuint gVertexShaderObjectSkyBox;
GLuint gFragmentShaderObjectSkyBox;
GLuint gShaderProgramObjectSkyBox;

GLuint skyboxVAO, skyboxVBO;
GLuint cubemapTexture;
GLuint gProjectionSkyBoxUniform;
GLuint gViewSkyBoxUniform;
GLuint gModelSkyboxUniform;
GLuint gSkyboxUniform;

typedef struct                                      // Create A Structure
{
	GLubyte *imageData;                             // Image Data (Up To 32 Bits)
	GLuint  bpp;                                    // Image Color Depth In Bits Per Pixel
	GLuint  width;                                  // Image Width
	GLuint  height;                                 // Image Height
	GLuint  texID;                                  // Texture ID Used To Select A Texture
} TextureImage;

TextureImage    textures[6];

void LoadSkyBoxShader();
unsigned int loadCubemap(std::vector<std::string> faces);
void LoadSkyBoxShader()
{
	fopen_s(&gpFile, "Log.txt", "a+");

	fprintf(gpFile, "In LoadSkyBoxShader \n");
	fclose(gpFile);
	void uninitialize();
	//VERTEX SHADER
	gVertexShaderObjectSkyBox = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertextShaderSourceCodeSkyBox =
		"#version 460" \
		"\n" \
		"out VS_OUT" \
		"{" \
		"vec3    tc;" \
		"} vs_out;" \
		"uniform mat4 view_matrix;" \
		"void main(void)" \
		"{" \
		"vec3[4] vertices = vec3[4](vec3(-1.0, -1.0, 1.0)," \
		"vec3(1.0, -1.0, 1.0)," \
		"vec3(-1.0, 1.0, 1.0)," \
		"vec3(1.0, 1.0, 1.0)); "\

		"vs_out.tc = mat3(view_matrix) * vertices[gl_VertexID];" \
		"gl_Position = vec4(vertices[gl_VertexID], 1.0);" \
		"}";

	//"in vec4 vPosition;" 
	/*"layout(location = 0) in vec3 aPos;" \
	//"in vec2 vTexture0_Coord;"
	"out vec3 TexCoords;" \
	"uniform mat4 projection;" \
	"uniform mat4 view;" \
	"void main(void)"\
	"{"\
	"TexCoords = aPos;" \
	"vec4 pos = projection * view * vec4(aPos, 1.0);" \
	"gl_Position = pos.xyww;" \
	"}";*/


	glShaderSource(gVertexShaderObjectSkyBox, 1, (const GLchar**)&vertextShaderSourceCodeSkyBox, NULL);

	//compile shader
	glCompileShader(gVertexShaderObjectSkyBox);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObjectSkyBox, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectSkyBox, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectSkyBox, iInfoLogLength, &written, szInfoLog);
				fopen_s(&gpFile, "Log.txt", "a+");
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				fclose(gpFile);


				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	//FRAGMENT SHADER

	gFragmentShaderObjectSkyBox = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCodeSkyBox =
		"#version 460"\
		"\n" \
		"uniform samplerCube tex_cubemap;" \
		"in VS_OUT" \
		"{" \
		"vec3    tc;" \
		"} fs_in;" \
		"layout (location = 0) out vec4 color;" \
		"void main(void)" \
		"{" \
		"color = texture(tex_cubemap, vec3(fs_in.tc.x, -fs_in.tc.y, fs_in.tc.z));" \
		//"color = texture(tex_cubemap, fs_in.tc);" 
		"}";

	/*	"in vec3 TexCoords;" \
	"out vec4 FragColor;" \
	"uniform samplerCube skybox;" \
	"void main(void)"\
	"{"\
	"FragColor = texture(skybox,vec3(TexCoords.x,-TexCoords.y, TexCoords.z));" \
	"}";*/

	glShaderSource(gFragmentShaderObjectSkyBox, 1, (const GLchar **)&fragmentShaderSourceCodeSkyBox, NULL);

	glCompileShader(gFragmentShaderObjectSkyBox);

	glGetShaderiv(gFragmentShaderObjectSkyBox, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectSkyBox, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectSkyBox, iInfoLogLength, &written, szInfoLog);
				fopen_s(&gpFile, "Log.txt", "a+");
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				fclose(gpFile);


				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Shader Program

	gShaderProgramObjectSkyBox = glCreateProgram();

	glAttachShader(gShaderProgramObjectSkyBox, gVertexShaderObjectSkyBox);

	glAttachShader(gShaderProgramObjectSkyBox, gFragmentShaderObjectSkyBox);

	//glBindAttribLocation(gShaderProgramObjectSkyBox, MALATI_ATTRIBUTE_VERTEX, "aPos");
	//glBindAttribLocation(gShaderProgramObjectSkyBox, MALATI_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

	glLinkProgram(gShaderProgramObjectSkyBox);

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObjectSkyBox, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectSkyBox, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectSkyBox, iInfoLogLength, &written, szInfoLog);
				fopen_s(&gpFile, "Log.txt", "a+");
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				fclose(gpFile);


				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//gProjectionSkyBoxUniform = glGetUniformLocation(gShaderProgramObjectSkyBox, "projection");
	gViewSkyBoxUniform = glGetUniformLocation(gShaderProgramObjectSkyBox, "view_matrix");
	gSkyboxUniform = glGetUniformLocation(gShaderProgramObjectSkyBox, "tex_cubemap");


}
unsigned int loadCubemap(std::vector<std::string> faces)
{
	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "In loadCubemap....\n");
	fclose(gpFile);
	TextureImage* LoadTGA(TextureImage *texture, const char *filename, int i);
	bool returnType = false;

	unsigned int textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	int width, height, nrChannels;
	for (unsigned int i = 0; i < faces.size(); i++)
	{
		/*returnType = LoadTGA(&textures[i], faces[i].c_str(), i);
		if (returnType)
		{
		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "LoadTGA Successfull....\n");
		fclose(gpFile);
		}
		else
		{
		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "LoadTGA UnSuccessfull....\n");
		fclose(gpFile);
		}*/
		TextureImage* textureIm = LoadTGA(&textures[i], faces[i].c_str(), i);
		if (textureIm->imageData)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, textureIm->width, textureIm->height, 0, GL_RGB, GL_UNSIGNED_BYTE, textureIm->imageData);
		}
		else
		{
			fopen_s(&gpFile, "Log.txt", "a+");
			fprintf(gpFile, "CubeMap texture failed to load..\n");
			fclose(gpFile);
		}
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	/*glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);*/


	return textureID;
}

void MakeSkyBox()
{
	LoadSkyBoxShader();
	glGenVertexArrays(1, &skyboxVAO);
	glBindVertexArray(skyboxVAO);
	glBindVertexArray(0);

	std::vector<std::string> Faces
	{
		"resources/ely_hills/hills_rt.tga",
		"resources/ely_hills/hills_lf.tga",
		"resources/ely_hills/hills_up.tga",
		"resources/ely_hills/hills_down.tga",
		"resources/ely_hills/hills_ft.tga",
		"resources/ely_hills/hills_bk.tga",
		
/*
		"resources/RotateMirMar/right.tga",
		"resources/RotateMirMar/left.tga",
		"resources/RotateMirMar/new_up.tga",
		"resources/RotateMirMar/dwn.tga",
		"resources/RotateMirMar/front.tga",
		"resources/RotateMirMar/bck.tga",*/
		/*"resources/mp_midnight/midnight-silence_rt.tga",
		"resources/mp_midnight/midnight-silence_lf.tga",
		"resources/mp_midnight/midnight-silence_up.tga",
		"resources/mp_midnight/midnight-silence_dn.tga",
		"resources/mp_midnight/midnight-silence_ft.tga",
		"resources/mp_midnight/midnight-silence_bk.tga",*/
	/*	"resources/mp_overrated/highly-overrated_rt.tga",
		"resources/mp_overrated/highly-overrated_lf.tga",
		"resources/mp_overrated/highly-overrated_up.tga",
		"resources/mp_overrated/highly-overrated_dn.tga",
		"resources/mp_overrated/highly-overrated_ft.tga",
		"resources/mp_overrated/highly-overrated_bk.tga",
		*/
	};
	cubemapTexture = loadCubemap(Faces);

	glUseProgram(gShaderProgramObjectSkyBox);
	glUniform1i(gSkyboxUniform, 0);
}


TextureImage* LoadTGA(TextureImage *texture, const char *filename, int num)                 // Loads A TGA File Into Memory
{
	void _check_gl_error();

	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "In LoadTGA : FileName : %s\n", filename);
	fclose(gpFile);

	GLubyte     TGAheader[12] = { 0,0,2,0,0,0,0,0,0,0,0,0 };           // Uncompressed TGA Header
	GLubyte     TGAcompare[12];                         // Used To Compare TGA Header
	GLubyte     header[6];                          // First 6 Useful Bytes From The Header
	GLuint      bytesPerPixel;                          // Holds Number Of Bytes Per Pixel Used In The TGA File
	GLuint      imageSize;                          // Used To Store The Image Size When Setting Aside Ram
	GLuint      temp;                               // Temporary Variable
	GLuint      type = GL_RGBA;                           // Set The Default GL Mode To RBGA (32 BPP)
	FILE *file = NULL;
	fopen_s(&file, filename, "rb");                     // Open The TGA File

	if (file == NULL ||                               // Does File Even Exist?
		fread(TGAcompare, 1, sizeof(TGAcompare), file) != sizeof(TGAcompare) ||  // Are There 12 Bytes To Read?
		memcmp(TGAheader, TGAcompare, sizeof(TGAheader)) != 0 ||  // Does The Header Match What We Want?
		fread(header, 1, sizeof(header), file) != sizeof(header))            // If So Read Next 6 Header Bytes
	{
		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "Does File EVen Exist..\n");
		fclose(gpFile);

		if (file == NULL)                           // Did The File Even Exist? *Added Jim Strong*
		{
			fopen_s(&gpFile, "Log.txt", "a+");
			fprintf(gpFile, "No..File Not Exists..\n");
			fclose(gpFile);
			return NULL;                           // Return False
		}
		else
		{
			fopen_s(&gpFile, "Log.txt", "a+");
			fprintf(gpFile, "Any Other thing Failed\n");
			fclose(gpFile);
			fclose(file);                           // If Anything Failed, Close The File
			return NULL;                           // Return False
		}
	}
	texture->width = header[1] * 256 + header[0];                   // Determine The TGA Width  (highbyte*256+lowbyte)
	texture->height = header[3] * 256 + header[2];                   // Determine The TGA Height (highbyte*256+lowbyte)
	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "In LoadTGA 100\n");
	fclose(gpFile);
	if (texture->width <= 0 ||                      // Is The Width Less Than Or Equal To Zero
		texture->height <= 0 ||                      // Is The Height Less Than Or Equal To Zero
		(header[4] != 24 && header[4] != 32))                   // Is The TGA 24 or 32 Bit?
	{
		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "In LoadTGA 1\n");
		fclose(gpFile);
		fclose(file);                               // If Anything Failed, Close The File
		return NULL;                               // Return False
	}
	texture->bpp = header[4];                            // Grab The TGA's Bits Per Pixel (24 or 32)
	bytesPerPixel = texture->bpp / 8;                        // Divide By 8 To Get The Bytes Per Pixel
	imageSize = texture->width*texture->height*bytesPerPixel;           // Calculate The Memory Required For The TGA Data
	texture->imageData = (GLubyte *)malloc(imageSize);             // Reserve Memory To Hold The TGA Data
	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "In LoadTGA 101\n");
	fclose(gpFile);
	if (texture->imageData == NULL ||                      // Does The Storage Memory Exist?
		fread(texture->imageData, 1, imageSize, file) != imageSize)        // Does The Image Size Match The Memory Reserved?
	{
		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "In LoadTGA 500\n");
		fclose(gpFile);
		if (texture->imageData != NULL)                     // Was Image Data Loaded
		{
			fopen_s(&gpFile, "Log.txt", "a+");
			fprintf(gpFile, "In LoadTGA 501\n");
			fclose(gpFile);
			free(texture->imageData);                    // If So, Release The Image Data
		}
		fclose(file);                               // Close The File
		return NULL;                               // Return False
	}
	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "In LoadTGA 102\n");
	fclose(gpFile);
	for (GLuint i = 0; i<int(imageSize); i += bytesPerPixel)              // Loop Through The Image Data
	{   // Swaps The 1st And 3rd Bytes ('R'ed and 'B'lue)

		temp = texture->imageData[i];                      // Temporarily Store The Value At Image Data 'i'
		texture->imageData[i] = texture->imageData[i + 2];            // Set The 1st Byte To The Value Of The 3rd Byte
		texture->imageData[i + 2] = temp;                    // Set The 3rd Byte To The Value In 'temp' (1st Byte Value)
	}
	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "In LoadTGA 103\n");
	fclose(gpFile);
	fclose(file);
	/*
	// Build A Texture From The Data
	glGenTextures(1, &texture[0].texID);                        // Generate OpenGL texture IDs

	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "In LoadTGA 104\n");
	fclose(gpFile);
	//glBindTexture(GL_TEXTURE_2D, texture[0].texID);
	// Bind Our Texture
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture[0].texID);

	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);       // Linear Filtered
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);


	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "In LoadTGA 105\n");
	fclose(gpFile);
	// Linear Filtered
	if (texture[0].bpp == 24)                             // Was The TGA 24 Bits
	{
	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "In LoadTGA 503\n");
	fclose(gpFile);
	type = GL_RGB;                                // If So Set The 'type' To GL_RGB
	}

	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "In LoadTGA ..width: %d, height: %d\n", texture[0].width, texture[0].height);
	fclose(gpFile);

	if (texture[0].imageData)
	{
	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf_s(gpFile, "Image Data is present with type : %d\n", type);
	fclose(gpFile);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + num, 0, type, texture[0].width, texture[0].height, 0, type, GL_UNSIGNED_BYTE, texture[0].imageData);
	_check_gl_error();
	}
	else
	{
	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf_s(gpFile, "Image Data is absent\n");
	fclose(gpFile);
	}
	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "In LoadTGA 107\n");
	fclose(gpFile);*/
	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "Exiting LoadTGA Successfully..\n");
	fclose(gpFile);
	return texture;                                    // Texture Building Went Ok, Return True
}