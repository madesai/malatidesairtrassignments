#include "Common.h"

	unsigned short *elements;
	float *verts;
	float *norms;
	float *texCoords;
/*	short elements[2280];
	float verts[1146];
	float norms[1146];
	float texCoords[764];
	*/
	unsigned int numElements=0;
	unsigned int maxElements=0;
	unsigned int numVertices=0;
    
    GLuint vbo_position = 0;
    GLuint vbo_normal = 0;
    GLuint vbo_texture = 0;
    GLuint vbo_index = 0;
    GLuint vao = 0;
	
	const double VDG_PI = 3.14159265358979323846;
	
	void allocate(int numIndices);
	void addTriangle(float **single_vertex, float **single_normal, float **single_texture);
	void normalizeVector(float *v);
	void prepareToDraw();
	void draw();
	bool isFoundIdentical(float val1, float val2, float diff);
	void makeSphere(float fRadius, int iSlices, int iStacks);
	void cleanupMeshData();

	void printVertex()
	{
		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "printing vertex data\n");
		fclose(gpFile);
		for (int i = 0; i < sizeof(verts); ++i)
		{
			fopen_s(&gpFile, "Log.txt", "a+");
			fprintf(gpFile, "%f " ,verts[i]);
			fclose(gpFile);
		}
	}

	void allocate(int numIndices)
	{
		
		cleanupMeshData();
		
		maxElements = numIndices;
		numElements = 0;
        numVertices = 0;
		
		int iNumIndices = numIndices/3;
		
		elements = (unsigned short *)malloc(iNumIndices * 3 * sizeof(elements));//2 is size of short
		verts = (float *) malloc(iNumIndices * 3 * sizeof(verts));//4 is size of float
		norms = (float *) malloc(iNumIndices * 3 * sizeof(norms)); // 3 is x,y,z and 4 is sizeof float
        texCoords = (float *) malloc(iNumIndices * 2 * sizeof(texCoords));
		
	}
	
	void addTriangle(float **single_vertex, float **single_normal, float **single_texture)
	{
		//variable declarations
        const float diff = 0.00001f;
        int i, j;
		
		// normals should be of unit length
        normalizeVector(single_normal[0]);
        normalizeVector(single_normal[1]);
        normalizeVector(single_normal[2]);
		
		
		
		for (i = 0; i < 3; i++)
        {
			for (j = 0; j < numVertices; j++) //for the first ever iteration of 'j', numVertices will be 0 because of it's initialization in the parameterized constructor
            {
				if (isFoundIdentical(verts[j * 3], single_vertex[i][0], diff) &&
                    isFoundIdentical(verts[(j * 3) + 1], single_vertex[i][1], diff) &&
                    isFoundIdentical(verts[(j * 3) + 2], single_vertex[i][2], diff) &&
                    
                    isFoundIdentical(norms[j * 3], single_normal[i][0], diff) &&
                    isFoundIdentical(norms[(j * 3) + 1], single_normal[i][1], diff) &&
                    isFoundIdentical(norms[(j * 3) + 2], single_normal[i][2], diff) &&
                    
                    isFoundIdentical(texCoords[j * 2], single_texture[i][0], diff) &&
                    isFoundIdentical(texCoords[(j * 2) + 1], single_texture[i][1], diff))
                {
                    elements[numElements] = (short)j;
                    numElements++;
                    break;
                }
			}
			
			//If the single vertex, normal and texture do not match with the given, then add the corressponding triangle to the end of the list
            if ((j == numVertices) && (numVertices < maxElements) && (numElements < maxElements))
            {
                verts[numVertices * 3] = single_vertex[i][0];
                verts[(numVertices * 3) + 1] = single_vertex[i][1];
                verts[(numVertices * 3) + 2] = single_vertex[i][2];
                
                norms[numVertices * 3] = single_normal[i][0];
                norms[(numVertices * 3) + 1] = single_normal[i][1];
                norms[(numVertices * 3) + 2] = single_normal[i][2];
                
                texCoords[numVertices * 2] = single_texture[i][0];
                texCoords[(numVertices * 2) + 1] = single_texture[i][1];
                
                elements[numElements] = (short)numVertices; //adding the index to the end of the list of elements/indices
                numElements++; //incrementing the 'end' of the list
                numVertices++; //incrementing coun of vertices
            }
		}

	}
	
	void normalizeVector(float *v)
	{
		float squaredVectorLength = (v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]);
		
		// get square root of above 'squared vector length'
        float squareRootOfSquaredVectorLength= (float)sqrt(squaredVectorLength);
		
		 // scale the vector with 1/squareRootOfSquaredVectorLength
        v[0] = v[0] * 1.0/squareRootOfSquaredVectorLength;
        v[1] = v[1] * 1.0/squareRootOfSquaredVectorLength;
        v[2] = v[2] * 1.0/squareRootOfSquaredVectorLength;

	

    }
	
	
	void draw()
	{
		glBindVertexArray(vao);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
		glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

/*
		glBindVertexArray(gVao_sphere);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
		glBindVertexArray(0);*/
	}
	
	bool isFoundIdentical(float val1, float val2, float diff)
	{
		if(fabsf(val1 - val2) < diff)
            return(true);
        else
            return(false);
	}
	
	void makeSphere(float fRadius, int iSlices, int iStacks)
	{
		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "start makesphere\n");
		fclose(gpFile);

		float drho = (float)VDG_PI / (float)iStacks;
		float dtheta = 2.0f * (float)VDG_PI / (float)iSlices;
		float ds = 1.0f / (float)iSlices;
		float dt = 1.0f / (float)iStacks;
		float t = 1.0f;
		float s = 0.0f;
		int i = 0;
		int j = 0;

		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "In makesphere before allocate stacks : %d, and slices : %d\n", iStacks, iSlices);
		fclose(gpFile);
		allocate(iSlices * iStacks * 6);

		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "In makesphere after allocate stacks : %d, and slices : %d\n", iStacks, iSlices);
		fclose(gpFile);
		for (i = 0; i < iStacks; i++)
		{
			fopen_s(&gpFile, "Log.txt", "a+");
			fprintf(gpFile, "In makesphere for loop %d\n", i);
			fclose(gpFile);
			float rho = (float)(i * drho);
			float srho = sinf(rho);
			float crho = cosf(rho);
			float srhodrho = sinf(rho + drho);
			float crhodrho = cosf(rho + drho);

			s = 0.0;

	

			//float single_vertex[3][3], float single_normal[3][3], float single_texture[3][2]
			
			float **vertex = (float **)malloc(sizeof(float*) * 4);
			for (int a = 0; a < 4; a++)
				vertex[a] = (float *)malloc(sizeof(float) * 3 );

			float **normal = (float **)malloc(sizeof(float*) * 4);
			for (int a = 0; a < 4; a++)
				normal[a] = (float *)malloc(sizeof(float) * 3);

			float **texture = (float **)malloc(sizeof(float*) * 4);
			for (int a = 0; a < 4; a++)
				texture[a] = (float *)malloc(sizeof(float) * 2);

	
			for (j = 0; j < iSlices; j++)
			{
				float theta = (j == iSlices) ? 0.0 : j * dtheta;
				float stheta = (float)(-sin(theta));
				float ctheta = (float)(cos(theta));

				float x = stheta * srho;
				float y = ctheta * srho;
				float z = crho;

				texture[0][0] = s;
				texture[0][1] = t;
				normal[0][0] = x;
				normal[0][1] = y;
				normal[0][2] = z;
				vertex[0][0] = x * fRadius;
				vertex[0][1] = y * fRadius;
				vertex[0][2] = z * fRadius;

				x = stheta * srhodrho;
				y = ctheta * srhodrho;
				z = crhodrho;

				texture[1][0] = s;
				texture[1][1] = t - dt;
				normal[1][0] = x;
				normal[1][1] = y;
				normal[1][2] = z;
				vertex[1][0] = x * fRadius;
				vertex[1][1] = y * fRadius;
				vertex[1][2] = z * fRadius;

				theta = ((j + 1) == iSlices) ? 0.0 : (j + 1) * dtheta;
				stheta = (float)(-sin(theta));
				ctheta = (float)(cos(theta));

				x = stheta * srho;
				y = ctheta * srho;
				z = crho;

				s += ds;
				texture[2][0] = s;
				texture[2][1] = t;
				normal[2][0] = x;
				normal[2][1] = y;
				normal[2][2] = z;
				vertex[2][0] = x * fRadius;
				vertex[2][1] = y * fRadius;
				vertex[2][2] = z * fRadius;

				x = stheta * srhodrho;
				y = ctheta * srhodrho;
				z = crhodrho;

				texture[3][0] = s;
				texture[3][1] = t - dt;
				normal[3][0] = x;
				normal[3][1] = y;
				normal[3][2] = z;
				vertex[3][0] = x * fRadius;
				vertex[3][1] = y * fRadius;
				vertex[3][2] = z * fRadius;


				addTriangle(vertex, normal, texture);

				// Rearrange for next triangle
				vertex[0][0] = vertex[1][0];
				vertex[0][1] = vertex[1][1];
				vertex[0][2] = vertex[1][2];
				normal[0][0] = normal[1][0];
				normal[0][1] = normal[1][1];
				normal[0][2] = normal[1][2];
				texture[0][0] = texture[1][0];
				texture[0][1] = texture[1][1];

				vertex[1][0] = vertex[3][0];
				vertex[1][1] = vertex[3][1];
				vertex[1][2] = vertex[3][2];
				normal[1][0] = normal[3][0];
				normal[1][1] = normal[3][1];
				normal[1][2] = normal[3][2];
				texture[1][0] = texture[3][0];
				texture[1][1] = texture[3][1];

				addTriangle(vertex, normal, texture);
			}
			t -= dt;
		}
		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "End makesphere\n");
		fclose(gpFile);

		prepareToDraw();

	}

	void prepareToDraw()//initialize
	{
		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "In Prepare to draw\n");
		fclose(gpFile);

		//printVertex();

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "In Prepare to draw  numElements : %d\n", maxElements);
		fclose(gpFile);
		//position
		glGenBuffers(1, &vbo_position);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
		glBufferData(GL_ARRAY_BUFFER, (maxElements * 3 * sizeof(GLfloat) / 3 ), verts, GL_STATIC_DRAW);

		glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

		glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		//normal
		glGenBuffers(1, &vbo_normal);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_normal);
		glBufferData(GL_ARRAY_BUFFER, (maxElements * 3 * sizeof(GLfloat) / 3), norms, GL_STATIC_DRAW);
		glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);
		glBindBuffer(GL_ARRAY_BUFFER, 0);


		//texture
		glGenBuffers(1, &vbo_texture);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_texture);
		glBufferData(GL_ARRAY_BUFFER, (maxElements * 2 * sizeof(GLfloat) / 3), texCoords, GL_STATIC_DRAW);

		glVertexAttribPointer(MALATI_ATTRIBUTE_TEXTURE, 2, GL_FLOAT, GL_FALSE, 0, NULL);

		glEnableVertexAttribArray(MALATI_ATTRIBUTE_TEXTURE);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// element vbo
		glGenBuffers(1, &vbo_index);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, (maxElements * 3 * sizeof(GLfloat) / 3), elements, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		glBindVertexArray(0);

		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "In End of Prepare to draw \n");
		fclose(gpFile);

		//cleanupMeshData();
	}

	void cleanupMeshData()
	{
		// code
		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "In cleanupMeshData 1\n");
		fclose(gpFile);
		if (elements != NULL)
		{
			free(elements);
			elements = NULL;
		}
		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "In cleanupMeshData 2\n");
		fclose(gpFile);
		if (verts != NULL)
		{
			fopen_s(&gpFile, "Log.txt", "a+");
			fprintf(gpFile, "In cleanupMeshData 21\n");
			fclose(gpFile);
			free(verts);
			fopen_s(&gpFile, "Log.txt", "a+");
			fprintf(gpFile, "In cleanupMeshData 22\n");
			fclose(gpFile);
			verts = NULL;
			fopen_s(&gpFile, "Log.txt", "a+");
			fprintf(gpFile, "In cleanupMeshData 23\n");
			fclose(gpFile);
		}
		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "In cleanupMeshData 3\n");
		fclose(gpFile);
		if (norms != NULL)
		{
			free(norms);
			norms = NULL;
		}
		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "In cleanupMeshData 4\n");
		fclose(gpFile);
		if (texCoords != NULL)
		{
			free(texCoords);
			texCoords = NULL;
		}
		fopen_s(&gpFile, "Log.txt", "a+");
		fprintf(gpFile, "In cleanupMeshData 5\n");
		fclose(gpFile);
	}