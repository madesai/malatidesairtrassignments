#include "Common.h"
#include "MyCylinder.h"
#include "MyOwnTextureSphere.h"
#include "MyStack.h"
#include "Sphere.h"
#include "Cubemap.h"

#include<MmSystem.h>

#pragma comment (lib, "Winmm.lib")
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gNumElements;
GLuint gNumVertices;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint gModelMatrixUniform, gViewMatrixUniform, gProjectionMatrixUniform;

GLuint gLdUniform, gLaUniform, gLsUniform;
GLuint gLightPositionUniform;
GLuint gKdUniform, gKaUniform, gKsUniform;
GLuint gMaterialShininessUniform;

GLuint gLKeyPressedUniform;
GLuint gTexture_sampler_uniform;
mat4 gPerspectiveProjectionMatrix;

GLfloat gAngle = 0.0f;

bool gbLight;
GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 25.0f;

bool isMKeyPressed = false;
bool isBKeyPressed = false;
bool isAKeyPressed = false;
bool bIsLKeyPressed = false;

GLuint gTexture_C_Letter;
GLuint gTexture_H_Letter;
GLuint gTexture_O_Letter;

bool MTranslateX = 0.0f;
bool MTranslateY = 0.0f;
bool MTranslateZ = 0.0f;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void Update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	//code
	// create log file
	/*	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
	MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
	exit(0);
	}
	else
	{*/
	fopen_s(&gpFile, "Log.txt", "w");
	fprintf(gpFile, "Log File Is Successfully Opened.\n");
	fclose(gpFile);
	//}

	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	//Parallel to glutInitWindowSize(), glutInitWindowPosition() and glutCreateWindow() all three together
	hwnd = CreateWindow(szClassName,
		TEXT("OpenGL Programmable Pipeline Window"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//initialize
	initialize();

	//Message Loop
	while (bDone == false) //Parallel to glutMainLoop();
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// rendring function
			display();
			Update();
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true) //Continuation to glutLeaveMainLoop();
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0) //if 0, the window is active
			gbActiveWindow = true;
		else //if non-zero, the window is not active
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE: //Parallel to glutReshapeFunc();
		resize(LOWORD(lParam), HIWORD(lParam)); //Parallel to glutReshapeFunc(resize);
		break;
	case WM_KEYDOWN: //Parallel to glutKeyboardFunc();
		switch (wParam)
		{
		case VK_ESCAPE: //case 27
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true; //Parallel to glutLeaveMainLoop();
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C:
			if (bIsLKeyPressed == false)
			{
				gbLight = true;
				bIsLKeyPressed = true;
			}
			else
			{
				gbLight = false;
				bIsLKeyPressed = false;
			}
			break;

			//M
		case 0x4D:
			if (isMKeyPressed == false)
			{
				isMKeyPressed = true;
				isBKeyPressed = false;
				isAKeyPressed = false;
			}
			else
			{
				isMKeyPressed = false;
			}
			break;
			//B
		case 0x41:
			if (isAKeyPressed == false)
			{
				isAKeyPressed = true;
				isMKeyPressed = false;
				isBKeyPressed = false;
			}
			else
			{
				isAKeyPressed = false;
			}
			break;
			//B
		case 0x42:
			if (isBKeyPressed == false)
			{
				isBKeyPressed = true;
				isMKeyPressed = false;
				isAKeyPressed = false;
			}
			else
			{
				isBKeyPressed = false;
			}
			break;
		
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:  //Parallel to glutMouseFunc();
		break;
	case WM_CLOSE: //Parallel to glutCloseFunc();
		uninitialize(); //Parallel to glutCloseFunc(uninitialize);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void uninitialize(void);
	void resize(int, int);
	int LoadGLTextures(GLuint *, TCHAR[]);
	void MakeSkyBox();

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	//Parallel to glutInitDisplayMode()
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	//choose a pixel format which best matches with that of 'pfd'
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//set the pixel format chosen above
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//create OpenGL rendering context
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//make the rendering context created above as current n the current hdc
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}


	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "%s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "Shading Lang Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	fclose(gpFile);

	//VERTEX SHADER
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertextShaderSourceCode =
		"#version 400" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"in vec2 vTexture0_Coord;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;"\
		"uniform int u_LKeyPressed;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"out vec2 out_texture0_coord;" \
		"void main(void)" \
		"{" \
		"if(u_LKeyPressed == 1)" \
		"{" \
		"vec4 eye_coordinates=u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_model_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"}"\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"out_texture0_coord = vTexture0_Coord; " \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertextShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fopen_s(&gpFile, "Log.txt", "a+");
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				fclose(gpFile);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	//FRAGMENT SHADER

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
		"#version 400" \
		"\n" \
		"in vec2 out_texture0_coord;" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_LKeyPressed;" \
		"uniform sampler2D u_texture0_sampler;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"vec4 TextureColor;" \
		"if(u_LKeyPressed==1)" \
		"{" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction=normalize(light_direction);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		"vec3 ambient = u_La * u_Ka;" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color=ambient + diffuse + specular;" \
		"TextureColor = texture(u_texture0_sampler, out_texture0_coord);"
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"TextureColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_color, 1.0) + TextureColor;" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fopen_s(&gpFile, "Log.txt", "a+");
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				fclose(gpFile);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Shader Program

	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");
	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_TEXTURE, "vTexture0_Coord");

	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fopen_s(&gpFile, "Log.txt", "a+");
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				fclose(gpFile);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");

	gLdUniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	gLaUniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	gLsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");

	gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	gKaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");

	gLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	gTexture_sampler_uniform = glGetUniformLocation(gShaderProgramObject, "u_texture0_sampler");

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();



	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	//position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//normal
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);


	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	glBindVertexArray(0);

	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "before makesphere\n");
	fclose(gpFile);
	makeSphere(2.0, 40, 40);
	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "after makesphere\n");
	fclose(gpFile);

	make_cylinder(2.0f, 5.0f, 30, 30);

	MakeSkyBox();


	PlaySound(TEXT("D:\\OpenGL-2ndBatch\\GroupLeaderAssignmentOct2018\\WindowsChemicalBond\\resources\\music\\Happy_Life.wav"), NULL, SND_ASYNC);

	/*glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);
	*/
	glEnable(GL_TEXTURE_2D);
	LoadGLTextures(&gTexture_C_Letter, MAKEINTRESOURCE(IDBITMAP_C_LETTER));
	LoadGLTextures(&gTexture_H_Letter, MAKEINTRESOURCE(IDBITMAP_H_LETTER));
	LoadGLTextures(&gTexture_O_Letter, MAKEINTRESOURCE(IDBITMAP_O_LETTER));
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // blue

	gPerspectiveProjectionMatrix = mat4::identity();


	gbLight = false;
	// resize
	resize(WIN_WIDTH, WIN_HEIGHT);
}


int LoadGLTextures(GLuint *texture, TCHAR imageResourceId[])
{
	HBITMAP hBitmap;
	BITMAP bmp;
	int iStatus = FALSE;
	glGenTextures(1, texture);
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		iStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		//		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGB,
			bmp.bmWidth,
			bmp.bmHeight,
			0,
			GL_BGR,
			GL_UNSIGNED_BYTE,
			bmp.bmBits);

		glGenerateMipmap(GL_TEXTURE_2D);
		DeleteObject(hBitmap);
	}
	return (iStatus);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	/*
	if (width <= height)
	{
	gOrthographicProjectionMatrix = ortho(-100.0f, 100.0f, (-100.0f *(height / width)), (100.0f * (height / width)), -100.0f, 100.0f);
	}
	else
	{
	gOrthographicProjectionMatrix = ortho((-100.0f *(width / height)), (100.0f * (width / height)), -100.0f, 100.0f, -100.0f, 100.0f);
	}*/
	gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height,
		0.1f, 100.0f); //perspective(3rd change)
}

void display(void)
{
	void drawMethane();
	void drawBenzene();
	void drawAlcohol();
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	vmath::mat4 modelMatrix = mat4::identity();
	vmath::mat4 scaleMatrix = mat4::identity();
	vmath::mat4 view = mat4::identity();

	//SKYBOX
	glUseProgram(gShaderProgramObjectSkyBox);
	glDisable(GL_DEPTH_TEST);

	//view = glm::mat4(glm::mat3(camera.getViewMatrix()));//remove translation from the view matrix
	glUniformMatrix4fv(gViewSkyBoxUniform, 1, GL_FALSE, view);
	//glUniformMatrix4fv(gProjectionSkyBoxUniform, 1, GL_FALSE, &projection[0][0]);


	// skybox cube
	glBindVertexArray(skyboxVAO);

	//glEnable(GL_DEPTH_TEST);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	//	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LESS); // set depth function back to default

	glUseProgram(0);

	if (isMKeyPressed)
	{
		//glClearColor(0.5f, 0.0f, 0.5f, 0.0f); // blue

		glClear(GL_DEPTH_BUFFER_BIT);

		glUseProgram(gShaderProgramObject);
		drawMethane();
		glUseProgram(0);

	}
	else if (isBKeyPressed)
	{
		//glClearColor(0.5f, 0.0f, 0.5f, 0.0f); // blue

		glClear(GL_DEPTH_BUFFER_BIT);

		glUseProgram(gShaderProgramObject);
		drawBenzene();
		glUseProgram(0);
	}
	else if (isAKeyPressed)
	{
		//glClearColor(0.5f, 0.0f, 0.5f, 0.0f); // blue

		glClear(GL_DEPTH_BUFFER_BIT);

		glUseProgram(gShaderProgramObject);
		drawAlcohol();
		glUseProgram(0);
	}
	SwapBuffers(ghdc);
}

void drawBenzene()
{
	if (gbLight == true)
	{
		glUniform1i(gLKeyPressedUniform, 1);

		glUniform3fv(gLdUniform, 1, lightDiffuse);//white
		glUniform3fv(gLaUniform, 1, lightAmbient);
		glUniform3fv(gLsUniform, 1, lightSpecular);
		glUniform4fv(gLightPositionUniform, 1, lightPosition);

		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

		float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };

	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
	}


	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();
	mat4 scaleMatrix = mat4::identity();

	struct Node *top = NULL;

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 0.0f, 0.0f));

	//1.left

	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);


	modelMatrix = vmath::translate(0.0f, 0.0f, -12.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//modelMatrix = modelMatrix * rotationMatrix;
	//rotationMatrix = mat4::identity();
	mat4 rotationMatrixX = vmath::rotate(gAngle, 1.0f, 0.0f, 0.0f);
	mat4 rotationMatrixY = vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f);
	mat4 rotationMatrixZ = vmath::rotate(gAngle, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrixX * rotationMatrixY * rotationMatrixZ;


	myPush(&top, modelMatrix);

	modelMatrix = modelMatrix * vmath::translate(0.0f, 2.5f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(60.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.28f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_C_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
	/*glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	//1.BONDING (Upper Connector)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	//	modelMatrix = mat4::identity();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	modelMatrix = modelMatrix * vmath::translate(0.0f, 2.9f, 0.0f);
	//modelMatrix = modelMatrix * vmath::translate(0.0f, 2.5f, 0.0f);

	rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(180.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	//rotationMatrix = vmath::rotate(2.0f*109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f,0.0f,1.0f,0.0f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.2f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	
	///////////////////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	//modelMatrix = mat4::identity();

	//static float x1Translate = -16.0f;
	modelMatrix = modelMatrix * vmath::translate(0.0f, 4.0f, 0.0f);
	

	//x1Translate += 0.03f;
	//if (x1Translate >= -4.3f)
		//x1Translate = -4.3f;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(55.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
	/*glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/


	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	
	//*****************************************
	//CUBE connector ( left and upper  C)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	

	modelMatrix = modelMatrix * vmath::translate(-1.7f, 1.4f, 0.0f);


	rotationMatrix = vmath::rotate(270.0f, vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = vmath::rotate(60.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.4f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	
	draw_cylinder();


	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	//Left Upper C 
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 0.0f, 0.0f));
	modelMatrix = modelMatrix * vmath::translate(-1.9f, 1.1f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(55.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.28f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_C_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
	/*glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	
	//CUBE connector ( left Upper C and left H )
	
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

//	static float upperLeftTranslate = -20.0f;
	modelMatrix = modelMatrix * vmath::translate(-3.1f, 1.6f, 0.0f);
	//upperLeftTranslate += 0.05f;
	//if (upperLeftTranslate >= -12.0f)
	//	upperLeftTranslate = -12.0f;
	rotationMatrix = vmath::rotate(270.0f, vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = vmath::rotate(120.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.2f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();


	//*****************************************
	//left Upper Small sphere (H)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(-3.3f, 1.8f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(55.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);


	/*glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	///////////////////////////////////////////////
	//CUBE connector ( between Left Bottom and left Upper C)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));


	//static float x2Translate = -16.0f;
	modelMatrix = modelMatrix * vmath::translate(-2.0f, -1.3f, 0.0f);

	rotationMatrix = vmath::rotate(270.0f, vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.05f, 0.1f, 0.4f);
	modelMatrix = modelMatrix * scaleMatrix;
	//x2Translate += 0.03f;
	//if (x2Translate >= -4.3f)
		//x2Translate = -4.3f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	draw_cylinder();

	//IMP
	modelMatrix = modelMatrix * vmath::translate(5.5f, 0.0f, 0.0f);
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////left Down  sphere (C)

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 0.0f, 0.0f));

	//modelMatrix = mat4::identity();

	//static float topTranslate = -20.0f;
	modelMatrix = modelMatrix * vmath::translate(-1.9f, -1.5f, 0.0f);
	
	//topTranslate += 0.05f;
	//if (topTranslate >= -12.0f)
		//topTranslate = -12.0f;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(55.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.28f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_C_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
	/*glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	//*****************************************
	//CUBE connector ( left bottom(C) and his left sphere(H) )
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = modelMatrix * vmath::translate(-3.1f, -2.2f, 0.0f);
	

	rotationMatrix = vmath::rotate(270.0f, vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = vmath::rotate(60.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.2f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	//left Down Small sphere (H)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(-3.3f, -2.3f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(55.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);


	/*glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	*/


	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	//////////////////////////////////////////////////

	//CUBE connector ( left Lower C and down C )
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = modelMatrix * vmath::translate(-1.7f, -1.7f, 0.0f);


	rotationMatrix = vmath::rotate(270.0f, vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = vmath::rotate(120.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.4f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	/////////////////
	//Draw Lower C Bond

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 0.0f, 0.0f));

	modelMatrix = modelMatrix * vmath::translate(0.0f, -2.6f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(55.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.28f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_C_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
	//glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	/*glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	//1.BONDING (Upper Connector)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	//	modelMatrix = mat4::identity();

	modelMatrix = modelMatrix * vmath::translate(0.0f, -4.0f, 0.0f);
	//modelMatrix = modelMatrix * vmath::translate(0.0f, 2.5f, 0.0f);

	rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(180.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	//rotationMatrix = vmath::rotate(2.0f*109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f,0.0f,1.0f,0.0f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.2f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	///////////////////////////////////////////////
	//Lower H
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	//modelMatrix = mat4::identity();

	//static float x1Translate = -16.0f;
	modelMatrix = modelMatrix * vmath::translate(0.0f, -4.2f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(55.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
	//x1Translate += 0.03f;
	//if (x1Translate >= -4.3f)
	//x1Translate = -4.3f;

	/*glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	/////////////////////////////
	//CUBE connector ( right and upper )

	//*****************************************
	//CUBE connector ( left and upper  C)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));



	modelMatrix = modelMatrix * vmath::translate(0.2f, -2.5f, 0.0f);


	rotationMatrix = vmath::rotate(270.0f, vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = vmath::rotate(60.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.05f, 0.05f, 0.4f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	draw_cylinder();

	modelMatrix = modelMatrix * vmath::translate(5.0f, 0.0f, 0.0f);
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();



	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////right Down  sphere (C)

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 0.0f, 0.0f));

	//modelMatrix = mat4::identity();

	//static float topTranslate = -20.0f;
	modelMatrix = modelMatrix * vmath::translate(2.1f, -1.5f, 0.0f);

	//topTranslate += 0.05f;
	//if (topTranslate >= -12.0f)
	//topTranslate = -12.0f;

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(55.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.28f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_C_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
	/*glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();


	//*****************************************
	//CUBE connector ( right bottom(C) and his right sphere(H) )
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = modelMatrix * vmath::translate(2.4f, -1.7f, 0.0f);

	rotationMatrix = vmath::rotate(270.0f, vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = vmath::rotate(120.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.2f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	//left Down Small sphere (H)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(3.5f, -2.3f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(55.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);

	/*glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/



	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	//CUBE connector ( between right Bottom and right Upper C)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));


	//static float x2Translate = -16.0f;
	modelMatrix = modelMatrix * vmath::translate(2.1f, -1.1f, 0.0f);

	rotationMatrix = vmath::rotate(270.0f, vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.4f);
	modelMatrix = modelMatrix * scaleMatrix;
	//x2Translate += 0.03f;
	//if (x2Translate >= -4.3f)
	//x2Translate = -4.3f;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();


	////right Upper  sphere (C)

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 0.0f, 0.0f));

	//modelMatrix = mat4::identity();

	//static float topTranslate = -20.0f;
	modelMatrix = modelMatrix * vmath::translate(2.1f, 1.2f, 0.0f);

	//topTranslate += 0.05f;
	//if (topTranslate >= -12.0f)
	//topTranslate = -12.0f;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(55.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.28f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_C_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
/*	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();


	//*****************************************
	//CUBE connector ( right Upper(C) and his right sphere(H) )
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = modelMatrix * vmath::translate(2.4f, 1.3f, 0.0f);

	rotationMatrix = vmath::rotate(270.0f, vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = vmath::rotate(60.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.2f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	//right Upper Small sphere (H)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(3.5f, 1.9f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(55.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);

	/*glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/



	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	//CUBE connector ( left and upper  C)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(0.3f, 2.4f, 0.0f);


	rotationMatrix = vmath::rotate(270.0f, vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = vmath::rotate(120.0f, vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.05f, 0.05f, 0.4f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	draw_cylinder();

	modelMatrix = modelMatrix * vmath::translate(5.0f, 0.0f, 0.0f);
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	draw_cylinder();


	modelMatrix = myPop(&top);
	
}

void drawAlcohol()
{
	if (gbLight == true)
	{
		glUniform1i(gLKeyPressedUniform, 1);

		glUniform3fv(gLdUniform, 1, lightDiffuse);//white
		glUniform3fv(gLaUniform, 1, lightAmbient);
		glUniform3fv(gLsUniform, 1, lightSpecular);
		glUniform4fv(gLightPositionUniform, 1, lightPosition);

		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

		float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };

	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
	}

	//Enable Alpha
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();
	mat4 scaleMatrix = mat4::identity();


	struct Node *top = NULL;

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 0.0f, 0.0f));

	

	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);


	modelMatrix = vmath::translate(0.0f, 0.0f, -11.0f);
	myPush(&top, modelMatrix);

	modelMatrix = modelMatrix * vmath::translate(-2.0f, 0.0f, 0.0f);
	rotationMatrix = vmath::rotate(270.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	//rotationMatrix = mat4::identity();
	mat4 rotationMatrixX = vmath::rotate(gAngle, 1.0f, 0.0f, 0.0f);
	mat4 rotationMatrixY = vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f);
	mat4 rotationMatrixZ = vmath::rotate(gAngle, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrixX * rotationMatrixY * rotationMatrixZ;


	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	

	scaleMatrix = vmath::scale(0.28f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_C_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);

/*	scaleMatrix = vmath::scale(1.2f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	//1.BONDING (Upper Connector)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	//	modelMatrix = mat4::identity();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	//modelMatrix = vmath::translate(0.1f, 0.3f, -7.5f);
	modelMatrix = modelMatrix * vmath::translate(0.0f, 2.5f, 0.0f);

	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(2.0f*109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f,0.0f,1.0f,0.0f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.4f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

/*	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(0.0f, 1.5f, 0.0f);

	scaleMatrix = vmath::scale(0.7f, 0.7f, 0.7f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();*/

	//1.BONDING (Right Connector)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));



	modelMatrix = modelMatrix * vmath::translate(0.0f, -0.02f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.4f, 0.0f, 0.0f);

	//working	rotationMatrix = vmath::rotate(2 * 109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f, 0.0f, 1.0f, 0.0f);

	//rotationMatrix = vmath::rotate(109.5f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(195.5f, 1.0f, 0.0f, 0.0f);

	rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	rotationMatrix = rotationMatrix * vmath::rotate(109.5f, 0.0f, 1.0f, 0.0f);


	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.25f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	///////////////////////////////////////////Sphere Right/////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(0.0f, -0.5f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(1.75f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
	/*scaleMatrix = vmath::scale(0.7f, 0.7f, 0.7f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	*/
	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(-0.0f, -0.2f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(-0.4f, 0.0f, 0.0f);

	//working	rotationMatrix = vmath::rotate(2 * 109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f, 0.0f, 1.0f, 0.0f);

	//rotationMatrix = vmath::rotate(109.5f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(195.5f, 0.0f, 0.0f, 1.0f);
	rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	rotationMatrix = rotationMatrix * vmath::rotate(2 * 109.5f, 0.0f, 1.0f, 0.0f);


	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.25f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	///////////////////////////////////////////Sphere left/////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(0.0f, -1.3f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(-1.3f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
/*
	scaleMatrix = vmath::scale(0.7f, 0.7f, 0.7f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//1.BONDING (Right Connector)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(-0.0f, -0.1f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.1f, 0.0f, 0.0f);

	//working	rotationMatrix = vmath::rotate(2 * 109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f, 0.0f, 1.0f, 0.0f);

	rotationMatrix = vmath::rotate(160.5f, 1.0f, 0.0f, 0.0f);

	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.4f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	///////////////////////////////////////////Sphere left/////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(0.0f, -0.81f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.0f, 0.0f, -2.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);

	//rotationMatrix = vmath::rotate(160.5f, 1.0f, 0.0f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
/*	scaleMatrix = vmath::scale(0.7f, 0.7f, 0.7f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);


	//////////////////////////////////////////////////////////////////////////////////////////////////
	//***********************2nd Methane
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 0.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(0.0f, 3.0f, 0.0f);
	rotationMatrix = vmath::rotate(180.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	//rotationMatrix = mat4::identity();
//	mat4 rotationMatrixX = vmath::rotate(gAngle, 1.0f, 0.0f, 0.0f);
//	mat4 rotationMatrixY = vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f);
	//mat4 rotationMatrixZ = vmath::rotate(gAngle, 0.0f, 0.0f, 1.0f);
	//modelMatrix = modelMatrix * rotationMatrixX * rotationMatrixY * rotationMatrixZ;


	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(270.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 0.0f, 1.0f);
	//modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.28f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_C_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
	/*scaleMatrix = vmath::scale(1.2f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	//1.BONDING (Upper Connector)
/*	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	//	modelMatrix = mat4::identity();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	//modelMatrix = vmath::translate(0.1f, 0.3f, -7.5f);
	modelMatrix = modelMatrix * vmath::translate(0.0f, 1.2f, 0.0f);

	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(2.0f*109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f,0.0f,1.0f,0.0f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.2f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(0.0f, 1.5f, 0.0f);

	scaleMatrix = vmath::scale(0.7f, 0.7f, 0.7f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	*/
	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	//1.BONDING (Right Connector)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));



	modelMatrix = modelMatrix * vmath::translate(0.0f, -0.02f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.4f, 0.0f, 0.0f);

	//working	rotationMatrix = vmath::rotate(2 * 109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f, 0.0f, 1.0f, 0.0f);

	//rotationMatrix = vmath::rotate(109.5f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(195.5f, 1.0f, 0.0f, 0.0f);

	rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	rotationMatrix = rotationMatrix * vmath::rotate(109.5f, 0.0f, 1.0f, 0.0f);


	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.25f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	///////////////////////////////////////////Sphere Right/////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(0.0f, -0.5f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(1.7f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
	/*scaleMatrix = vmath::scale(0.7f, 0.7f, 0.7f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(-0.0f, -0.2f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(-0.4f, 0.0f, 0.0f);

	//working	rotationMatrix = vmath::rotate(2 * 109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f, 0.0f, 1.0f, 0.0f);

	//rotationMatrix = vmath::rotate(109.5f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(195.5f, 0.0f, 0.0f, 1.0f);
	rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	rotationMatrix = rotationMatrix * vmath::rotate(2 * 109.05f, 0.0f, 1.0f, 0.0f);


	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.25f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	///////////////////////////////////////////Sphere left/////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(0.0f, -1.2f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(-1.2f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
	/*scaleMatrix = vmath::scale(0.7f, 0.7f, 0.7f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//1.BONDING (Right Connector)
	/*if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(-0.0f, -0.1f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.1f, 0.0f, 0.0f);

	//working	rotationMatrix = vmath::rotate(2 * 109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f, 0.0f, 1.0f, 0.0f);

	rotationMatrix = vmath::rotate(160.5f, 1.0f, 0.0f, 0.0f);

	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.4f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	///////////////////////////////////////////Sphere left/////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(0.0f, -0.81f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.0f, 0.0f, -2.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);

	//rotationMatrix = vmath::rotate(160.5f, 1.0f, 0.0f, 0.0f);
	scaleMatrix = vmath::scale(0.7f, 0.7f, 0.7f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);


	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();*/

	//OH  connector
	


	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(-0.0f, -0.0f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.1f, 0.0f, 0.0f);

	//working	rotationMatrix = vmath::rotate(2 * 109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f, 0.0f, 1.0f, 0.0f);

	rotationMatrix = vmath::rotate(180.0f, 0.0f, 1.0f, 0.0f);

	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.3f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	///////////////////////////////////////////Sphere Upper/////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 0.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(0.0f, -0.0f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.0f, 0.0f, -2.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.28f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_O_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
	/*scaleMatrix = vmath::scale(1.2f, 1.2f, 1.2f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	//Connector
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));


	//modelMatrix = vmath::translate(0.1f, 0.3f, -7.5f);
	modelMatrix = modelMatrix * vmath::translate(0.0f, 0.0f, 0.0f);

	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.0f, 0.0f, -2.0f);
	//rotationMatrix = vmath::rotate(2.0f*109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f,0.0f,1.0f,0.0f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.4f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	//H
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(0.0f, -2.0f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.0f, 0.0f, -2.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);


	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
	/*scaleMatrix = vmath::scale(0.7f, 0.7f, 0.7f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
/*
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(0.0f, -1.0f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.1f, 0.0f, 0.0f);

	//modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.0f, 0.0f, -2.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);

	//rotationMatrix = vmath::rotate(160.5f, 1.0f, 0.0f, 0.0f);
	scaleMatrix = vmath::scale(0.7f, 0.7f, 0.7f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);

}

void drawMethane()
{
	static bool isFirstDraw = false;
	static bool isSecondDraw = false;
	static bool isThirdDraw = false;
	static bool isFourthDraw = false;
	static bool isAllDraw = false;

	if (gbLight == true)
	{
		glUniform1i(gLKeyPressedUniform, 1);

		glUniform3fv(gLdUniform, 1, lightDiffuse);//white
		glUniform3fv(gLaUniform, 1, lightAmbient);
		glUniform3fv(gLsUniform, 1, lightSpecular);
		glUniform4fv(gLightPositionUniform, 1, lightPosition);

		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

		float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };

	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
	}

	//Enable Alpha
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();
	mat4 scaleMatrix = mat4::identity();


	struct Node *top = NULL;

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 0.0f, 0.0f));

	//1.left

	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);


	modelMatrix = vmath::translate(0.0f, 0.0f, -7.5f);
	rotationMatrix = vmath::rotate(270.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	//rotationMatrix = mat4::identity();
	mat4 rotationMatrixX = vmath::rotate(gAngle, 1.0f, 0.0f, 0.0f);
	mat4 rotationMatrixY = vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f);
	mat4 rotationMatrixZ = vmath::rotate(gAngle, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrixX * rotationMatrixY * rotationMatrixZ;


	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	scaleMatrix = scale(0.28f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_C_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);
	isFirstDraw = true;
	/*
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	*/
	//1.BONDING (Upper Connector)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

//	modelMatrix = mat4::identity();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	//modelMatrix = vmath::translate(0.1f, 0.3f, -7.5f);
	modelMatrix = modelMatrix * vmath::translate(0.0f, 1.2f, 0.0f);

	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(2.0f*109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f,0.0f,1.0f,0.0f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.2f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	if(isAllDraw)
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	
	modelMatrix = modelMatrix * vmath::translate(0.0f, 1.5f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(25.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	if (isFirstDraw)
	{
		draw();
		isSecondDraw = true;
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	
	/*glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	
	//1.BONDING (Right Connector)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	

	modelMatrix = modelMatrix * vmath::translate(0.0f, -0.02f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.4f, 0.0f, 0.0f);

	//working	rotationMatrix = vmath::rotate(2 * 109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f, 0.0f, 1.0f, 0.0f);

	//rotationMatrix = vmath::rotate(109.5f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(195.5f, 1.0f, 0.0f, 0.0f);

	rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	rotationMatrix = rotationMatrix * vmath::rotate(109.5f, 0.0f, 1.0f, 0.0f);


	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.25f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	if(isAllDraw)
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	///////////////////////////////////////////Sphere Right/////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	
	modelMatrix = modelMatrix * vmath::translate(0.0f, -0.5f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(1.75f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	if (isSecondDraw)
	{
		draw();
		isThirdDraw = true;
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	/*glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(-0.0f, -0.2f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(-0.4f, 0.0f, 0.0f);

	//working	rotationMatrix = vmath::rotate(2 * 109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f, 0.0f, 1.0f, 0.0f);

	//rotationMatrix = vmath::rotate(109.5f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(195.5f, 0.0f, 0.0f, 1.0f);
	rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	rotationMatrix = rotationMatrix * vmath::rotate(240.0f, 0.0f, 1.0f, 0.0f);


	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.25f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	if(isAllDraw)
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	///////////////////////////////////////////Sphere left/////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));


	modelMatrix = modelMatrix * vmath::translate(0.0f, -0.9f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(-1.7f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	if (isThirdDraw)
	{
		draw();
		isFourthDraw = true;
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	/*
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//1.BONDING (Right Connector)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	
	modelMatrix = modelMatrix * vmath::translate(-0.0f, -0.1f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.1f, 0.0f, 0.0f);

	//working	rotationMatrix = vmath::rotate(2 * 109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f, 0.0f, 1.0f, 0.0f);

	rotationMatrix = vmath::rotate(160.5f, 1.0f, 0.0f, 0.0f);

	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.4f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	if(isAllDraw)
	draw_cylinder();

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	///////////////////////////////////////////Sphere left/////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	
	modelMatrix = modelMatrix * vmath::translate(0.0f, -0.81f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.0f, 0.0f, -2.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);

	//rotationMatrix = vmath::rotate(160.5f, 1.0f, 0.0f, 0.0f);
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(90.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.2f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);
	if (isFourthDraw)
	{
		draw();
		isAllDraw = true;
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	/*glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/
	

}
void drawTempMethane()
{
	if (gbLight == true)
	{
		glUniform1i(gLKeyPressedUniform, 1);

		glUniform3fv(gLdUniform, 1, lightDiffuse);//white
		glUniform3fv(gLaUniform, 1, lightAmbient);
		glUniform3fv(gLsUniform, 1, lightSpecular);
		glUniform4fv(gLightPositionUniform, 1, lightPosition);

		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

		//float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };

	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
	}

	
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();
	mat4 scaleMatrix = mat4::identity();

	struct Node *top = NULL;

	//viewMatrix = lookat(vec3(0, 0, 5), vec3(0, 0, 0), vec3(0, 1, 0));
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);



	modelMatrix = vmath::translate(0.0f, 0.0f, -15.0f);

	rotationMatrix = vmath::rotate(270.0f, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	//rotationMatrix = mat4::identity();
	mat4 rotationMatrixX = vmath::rotate(gAngle, 1.0f, 0.0f, 0.0f);
	mat4 rotationMatrixY = vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f);
	mat4 rotationMatrixZ = vmath::rotate(gAngle, 0.0f, 0.0f, 1.0f);
//	modelMatrix = modelMatrix * rotationMatrixX * rotationMatrixY * rotationMatrixZ;
	

	myPush(&top, modelMatrix);
	
	//MIDDLE SPHERE
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix =  modelMatrix * rotationMatrix;

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(270.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	
	scaleMatrix = vmath::scale(0.5f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindTexture(GL_TEXTURE_2D, gTexture_C_Letter);
	glUniform1i(gTexture_sampler_uniform, 0);

	draw();
	/*glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/

	glBindTexture(GL_TEXTURE_2D, 0);

	modelMatrix = myPop(&top);
	myPush(&top, modelMatrix);

	//1.BONDING (Upper Connector)
	
	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	modelMatrix = modelMatrix * vmath::translate(0.0f, 2.3f, 0.0f);
	rotationMatrix = vmath::rotate(270.0f, 0.0f, 0.0f, 1.0f);
//	rotationMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(2.0f*109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f,0.0f,1.0f,0.0f);
	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.35f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();

	modelMatrix = myPop(&top);

	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

//	modelMatrix = mat4::identity();
	modelMatrix = modelMatrix * vmath::translate(0.0f, 2.5f, 0.0f);

	//MIDDLE SPHERE
	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	rotationMatrix = mat4::identity();
	rotationMatrix = vmath::rotate(270.0f, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	scaleMatrix = vmath::scale(0.3f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);

	draw();

	glBindTexture(GL_TEXTURE_2D, 0);
	
	

	modelMatrix = myPop(&top);

	myPush(&top, modelMatrix);
/*
	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	
	//1.BONDING (Right Connector)
	
	
	modelMatrix = modelMatrix * vmath::translate(0.0f, -0.05f, 0.0f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.4f, 0.0f, 0.0f);

	//working	rotationMatrix = vmath::rotate(2 * 109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f, 0.0f, 1.0f, 0.0f);

	//rotationMatrix = vmath::rotate(109.5f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(195.5f, 1.0f, 0.0f, 0.0f);

	rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);


	scaleMatrix = vmath::scale(0.1f, 0.1f, 0.35f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();

	modelMatrix = myPop(&top);

	myPush(&top, modelMatrix);

	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	///////////////////////////////////////////Sphere Right/////////////////////////////////

	modelMatrix = modelMatrix * vmath::translate(0.0f, -0.8f, 0.0f);

	rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(1.7f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);

	

	scaleMatrix = vmath::scale(0.3f);
	modelMatrix = modelMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glBindTexture(GL_TEXTURE_2D, gTexture_H_Letter);
	draw();
	glBindTexture(GL_TEXTURE_2D, 0);*/
	/*glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);*/
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	modelMatrix = vmath::translate(-0.0f, -0.2f, -7.5f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(-0.4f, 0.0f, 0.0f);

	//working	rotationMatrix = vmath::rotate(2 * 109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f, 0.0f, 1.0f, 0.0f);

	//rotationMatrix = vmath::rotate(109.5f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(195.5f, 0.0f, 0.0f, 1.0f);
	rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	rotationMatrix = rotationMatrix * vmath::rotate(240.0f, 0.0f, 1.0f, 0.0f);


	scaleMatrix = vmath::scale(0.05f, 0.1f, 0.25f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();

	///////////////////////////////////////////Sphere left/////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	modelMatrix = vmath::translate(0.0f, -0.9f, -7.5f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(-1.7f, 0.0f, 0.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);


	scaleMatrix = vmath::scale(0.7f, 0.7f, 0.7f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//1.BONDING (Right Connector)
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(1.0f, 1.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	modelMatrix = vmath::translate(-0.0f, -0.1f, -7.5f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.1f, 0.0f, 0.0f);

	//working	rotationMatrix = vmath::rotate(2 * 109.5f, 1.0f, 0.0f, 0.0f) * vmath::rotate(19.5f, 0.0f, 1.0f, 0.0f);
	rotationMatrix = vmath::rotate(160.5f, 1.0f, 0.0f, 0.0f);

	scaleMatrix = vmath::scale(0.05f, 0.1f, 0.4f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	//modelMatrix
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	//drawCube();
	draw_cylinder();
	///////////////////////////////////////////Sphere left/////////////////////////////////
	if (gbLight)
		glUniform3fv(gKdUniform, 1, vec4(0.0f, 0.0f, 1.0f, 0.0f));

	modelMatrix = mat4::identity();
	modelMatrix = vmath::translate(0.0f, -0.81f, -7.5f);

	modelMatrix = modelMatrix * vmath::rotate(gAngle, 0.0f, 1.0f, 0.0f) * vmath::translate(0.0f, 0.0f, -2.0f);
	//rotationMatrix = vmath::rotate(270.0f, 1.0f, 0.0f, 0.0f);
	//rotationMatrix = rotationMatrix * vmath::rotate(120.0f, 0.0f, 1.0f, 0.0f);

	//rotationMatrix = vmath::rotate(160.5f, 1.0f, 0.0f, 0.0f);
	scaleMatrix = vmath::scale(0.7f, 0.7f, 0.7f);
	modelMatrix = modelMatrix * rotationMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
*/
	glUseProgram(0);
}
void Update(void)
{
	void UpdateMethane();
	gAngle += 0.5f;
	if (gAngle >= 360.0f)
		gAngle = 0.0f;

	UpdateMethane();
	
}

void UpdateMethane()
{
	MTranslateX += 0.1f;
	if (MTranslateX >= 360.0f)
		MTranslateX = 0.0f;

	MTranslateY += 0.1f;
	if (MTranslateY >= 360.0f)
		MTranslateY = 0.0f;

	MTranslateZ += 0.1f;
	if (MTranslateZ >= 360.0f)
		MTranslateZ = 0.0f;
}
void uninitialize(void)
{
	//UNINITIALIZATION CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	//Delete the device context
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	//	if (gpFile)
	//{
	fopen_s(&gpFile, "Log.txt", "a+");
	fprintf(gpFile, "Log File Is Successfully Closed.\n");
	fclose(gpFile);
	gpFile = NULL;
	//}
}
