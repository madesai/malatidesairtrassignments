#pragma once

#include <windows.h>

#include <gl\glew.h>//use it before other GL.h

#include <gl/GL.h>

#include "vmath.h"

#include <stdio.h> // for FILE I/O
#include<vector>
FILE *gpFile = NULL;

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

enum
{
	MALATI_ATTRIBUTE_VERTEX = 0,
	MALATI_ATTRIBUTE_COLOR,
	MALATI_ATTRIBUTE_NORMAL,
	MALATI_ATTRIBUTE_TEXTURE
};

#define WIN_WIDTH 800
#define WIN_HEIGHT 600


using namespace vmath;

#define IDBITMAP_H_LETTER	100

#define IDBITMAP_C_LETTER	101

#define IDBITMAP_O_LETTER 102


