#pragma once

#include <stdio.h>
#include <stdlib.h>


#include "vmath.h"

using namespace vmath;



struct Node
{
	//int data;//integer data
	mat4 modelMatrix;
	struct Node* next;		//pointer to next node
};

void myPush(struct Node **top, mat4& tempModelMatrix)
{
	struct Node* node = NULL;
	node = (struct Node*)malloc(sizeof(struct Node));

	if (!node)
	{
		fprintf(gpFile, "Heap OverFlow\n");
		exit(1);
	}

	//fprintf(gpFile, "Inserting %d\n", x);

	node->modelMatrix = tempModelMatrix;

	//Set Next pointer of new node
	node->next = *top;

	//update top pointer
	*top = node;
}

int isEmpty(struct Node* top)
{
	return top == NULL;
}

mat4 peek(struct Node *top)
{
	if (!isEmpty(top))
	{
		return top->modelMatrix;
	}
	else
	{
		exit(EXIT_FAILURE);
	}
}

mat4 myPop(struct Node** top)
{
	struct Node *node;
	mat4 tempMatrix = mat4::identity();
	if (*top == NULL)
	{
		fprintf(gpFile, "Stack UnderFlow\n");
		exit(1);
	}

	//fprintf(gpFile, "Removing %d\n", peek(*top));

	node = *top;
	tempMatrix = node->modelMatrix;
	*top = (*top)->next;

	free(node);
	return tempMatrix;
}