//headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import<QuartzCore/CVDisplayLink.h>
#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import "vmath.h"
#import "sphere.h"

enum
{
	MALATI_ATTRIBUTE_VERTEX=0,
	MALATI_ATTRIBUTE_COLOR, 
	MALATI_ATTRIBUTE_NORMAL,
	MALATI_ATTRIBUTE_TEXTURE0,
};

//'c' style global function declaration
CVReturn MyDisplayLinkCallBack(CVDisplayLinkRef , const CVTimeStamp *, const CVTimeStamp *,
	CVOptionFlags , CVOptionFlags *, void *);

//void MyLinkProgram(GLuint);
FILE *gpFile = NULL;

//interface declarations
@interface AppDeligate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

//Entry Point Function
int main(int argc, const char * argv[])
{
	//code
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];	

	[NSApp setDelegate : [[AppDeligate alloc] init]];

	[NSApp run];

	[pPool release];

	return (0);
}

//interface implementation
@implementation AppDeligate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching : (NSNotification *)aNotification	//WM_CREATE
{
	//code
	//window
	
	//for LOG file
	 NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
		printf("Can Not Create Log File.\nExitting...\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile, "Program Is Started Successfully...\n");
	
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	//create simple window
	window = [[NSWindow alloc] initWithContentRect:win_rect 
				styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
				 backing:NSBackingStoreBuffered
				 defer:NO];

	[window setTitle:@"macOS Window"];
	[window center];

	NSLog(@"%@\n", window);
	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];

}

-(void)applicationWillTerminate:(NSNotification *)notification //WM_CLOSE OR WM_DESTROY
{
	//code
	fprintf(gpFile, "Program Is Terminate Successfully\n");
	
	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

-(void)windowWillClose:(NSNotification *)notification
{
	//code
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];
	
	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displayLink;
	
	GLuint gVertexShaderObjectPerVertex;
GLuint gVertexShaderObjectPerFragment;
GLuint gFragmentShaderObjectPerVertex;
GLuint gFragmentShaderObjectPerFragment;
GLuint gShaderProgramObjectPerVertex;
GLuint gShaderProgramObjectPerFragment;
GLuint gShaderProgramObject;

	
	/*GLuint gVao_Pyramid;
	GLuint gVao_Cube;
	GLuint gVbo_Pyramid_Position;
	GLuint gVbo_Cube_Position;

	GLuint gVbo_Pyramid_Color;
	GLuint gVbo_Cube_Color;
*/
	GLuint gNumElements;
	GLuint gNumVertices;
	float sphere_vertices[1146];
	float sphere_normals[1146];
	float sphere_textures[764];
	short sphere_elements[2280];

	//GLuint gVbo_color;
	//GLuint gMVPUniform;
	GLuint gVao_sphere;
	GLuint gVbo_sphere_position;
	GLuint gVbo_sphere_normal;
	GLuint gVbo_sphere_element;

GLuint gModelMatrixUniform, gViewMatrixUniform, gProjectionMatrixUniform;
GLuint gLd0Uniform, gLa0Uniform, gLs0Uniform;
GLuint gLd1Uniform, gLa1Uniform, gLs1Uniform;
GLuint gLd2Uniform, gLa2Uniform, gLs2Uniform;
GLuint gLightPositionUniform0;
GLuint gLightPositionUniform1;
GLuint gLightPositionUniform2;
GLuint gKdUniform, gKaUniform, gKsUniform;
GLuint gMaterialShininessUniform;
GLuint gLKeyPressedUniform;
GLuint gFKeyPressedUniform;

	vmath::mat4 gPerspectiveProjectionMatrix;
	
	bool gbLight;
	bool gbFrag;

	bool bIsLKeyPressed;
	bool bIsFKeyPressed;
GLfloat lightAmbient0[4] ;
GLfloat lightAmbient1[4] ;
GLfloat lightAmbient2[4] ;
GLfloat lightDiffuse0[4];
GLfloat lightDiffuse1[4];
GLfloat lightDiffuse2[4];
GLfloat lightSpecular0[4] ;
GLfloat lightSpecular1[4] ;
GLfloat lightSpecular2[4] ;
GLfloat lightPosition0[4] ;
GLfloat lightPosition1[4] ;
GLfloat lightPosition2[4] ;

GLfloat material_ambient[4];
GLfloat material_diffuse[4] ;
GLfloat material_specular[4] ;
GLfloat material_shininess ;

GLfloat myRotateMatrix[4];
GLfloat myRotateMatrix1[4];
GLfloat myRotateMatrix2[4];

}

-(id)initWithFrame:(NSRect)frame;
{
	self = [super initWithFrame:frame];
	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery, 
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat == nil)
		{
			fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available. Exitting..");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];
				
	}
	return (self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return (kCVReturnSuccess);
}


-(void)prepareOpenGL
{
	fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version	: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	
	//***VERTEX SHADER***
	gVertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertextShaderSourceCodePerVertex =
		"#version 400" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \

		"uniform int u_LKeyPressed;" \

		"uniform vec3 u_Ld0;" \
		"uniform vec3 u_Ld1;" \
		"uniform vec3 u_Ld2;" \
		"uniform vec3 u_La0;" \
		"uniform vec3 u_La1;" \
		"uniform vec3 u_La2;" \
		"uniform vec3 u_Ls0;" \
		"uniform vec3 u_Ls1;" \
		"uniform vec3 u_Ls2;" \
		"uniform vec4 u_light_position0;"\
		"uniform vec4 u_light_position1;"\
		"uniform vec4 u_light_position2;"\
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Ks;" \
		"uniform vec3 u_Kd;" \
		"uniform float u_material_shininess;" \

		"out vec3 phong_ads_color0;" \
		"out vec3 phong_ads_color1;" \
		"out vec3 phong_ads_color2;" \
		"void main(void)" \
		"{" \
		"if(u_LKeyPressed == 1)" \
		"{" \
		"vec4 eyeCoordinates =  u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 tnorm = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
		"vec3 light_direction  = normalize(vec3(u_light_position0) - eyeCoordinates.xyz);" \
		"vec3 ambient0 = u_La0 * u_Ka;"\
		"vec3 diffuse_light0 = u_Ld0 * u_Kd * max(dot(light_direction, tnorm), 0.0);" \
		"vec3 reflection_vector0 = reflect(-light_direction, tnorm);"\
		"vec3 viewer_vector0 = normalize(-eyeCoordinates.xyz);" \
		"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, viewer_vector0), 0.0),u_material_shininess);" \
		"phong_ads_color0=ambient0 + diffuse_light0 + specular0;" \

		"vec4 eyeCoordinates1 =  u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 tnorm1 = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
		"vec3 light_direction1  = normalize(vec3(u_light_position1) - eyeCoordinates1.xyz);" \
		"vec3 ambient1 = u_La1 * u_Ka;"\
		"vec3 diffuse_light1 = u_Ld1 * u_Kd * max(dot(light_direction1, tnorm1), 0.0);" \
		"vec3 reflection_vector1 = reflect(-light_direction1, tnorm1);"\
		"vec3 viewer_vector1 = normalize(-eyeCoordinates1.xyz);" \
		"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, viewer_vector1), 0.0),u_material_shininess);" \
		"phong_ads_color1=ambient1 + diffuse_light1 + specular1;" \

		"vec4 eyeCoordinates2 =  u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 tnorm2 = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
		"vec3 light_direction2  = normalize(vec3(u_light_position2) - eyeCoordinates2.xyz);" \
		"vec3 ambient2 = u_La2 * u_Ka;"\
		"vec3 diffuse_light2 = u_Ld2 * u_Kd * max(dot(light_direction2, tnorm2), 0.0);" \
		"vec3 reflection_vector2 = reflect(-light_direction2, tnorm2);"\
		"vec3 viewer_vector2 = normalize(-eyeCoordinates2.xyz);" \
		"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, viewer_vector2), 0.0),u_material_shininess);" \
		"phong_ads_color2=ambient2 + diffuse_light2 + specular2;" \
		"}" \
		"else"\
		"{"\
		"phong_ads_color0 = vec3(1.0, 1.0, 1.0);"\
		"phong_ads_color1 = vec3(1.0, 1.0, 1.0);"\
		"phong_ads_color2 = vec3(1.0, 1.0, 1.0);"\
		"}"\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";


	glShaderSource(gVertexShaderObjectPerVertex, 1, (const GLchar**)&vertextShaderSourceCodePerVertex, NULL);

	//compile shader
	glCompileShader(gVertexShaderObjectPerVertex);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader (per Vertex) Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				//uninitialize();
				//exit(0);
				[self release];
				[NSApp terminate:self];
			}
		}
	}
	
	
	//Vertex Shader Per Frag
	
	gVertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertextShaderSourceCodePerFragment =
		"#version 400" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position0;"\
		"uniform vec4 u_light_position1;"\
		"uniform vec4 u_light_position2;"\
		"uniform int u_FKeyPressed;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction0;" \
		"out vec3 light_direction1;" \
		"out vec3 light_direction2;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_FKeyPressed == 1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction0 = vec3(u_light_position0) - eye_coordinates.xyz;" \
		"light_direction1 = vec3(u_light_position1) - eye_coordinates.xyz;" \
		"light_direction2 = vec3(u_light_position2) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"}"\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObjectPerFragment, 1, (const GLchar **)&vertextShaderSourceCodePerFragment, NULL);

	//compile shader
	glCompileShader(gVertexShaderObjectPerFragment);

	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	//char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				//uninitialize();
				//exit(0);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


	//FRAGMENT SHADER (Per Vertex)
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;
	
	gFragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCodePerVertex =
    "#version 400" \
		"\n" \
		"in vec3 phong_ads_color0;" \
		"in vec3 phong_ads_color1;" \
		"in vec3 phong_ads_color2;" \
		"vec4 FragColor0;"
		"vec4 FragColor1;"
		"vec4 FragColor2;"
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor0 = vec4(phong_ads_color0, 1.0);" \
		"FragColor1 = vec4(phong_ads_color1, 1.0);" \
		"FragColor2 = vec4(phong_ads_color2, 1.0);" \
		"FragColor = FragColor0 + FragColor1 + FragColor2;" \
		"}";

    
	glShaderSource(gFragmentShaderObjectPerVertex, 1, (const GLchar **)&fragmentShaderSourceCodePerVertex, NULL);

	glCompileShader(gFragmentShaderObjectPerVertex);
	
	glGetShaderiv(gFragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Frag Shader (Per Vertex) Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				//uninitialize();
				//exit(0);
				[self release];
				[NSApp terminate:self];
			}
		}
	}
	
	//Frag Shader Per Frag
	gFragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCodePerFragment =
		"#version 400" \
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction0;" \
		"in vec3 light_direction1;" \
		"in vec3 light_direction2;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La0;" \
		"uniform vec3 u_La1;" \
		"uniform vec3 u_La2;" \
		"uniform vec3 u_Ld0;" \
		"uniform vec3 u_Ld1;" \
		"uniform vec3 u_Ld2;" \
		"uniform vec3 u_Ls0;" \
		"uniform vec3 u_Ls1;" \
		"uniform vec3 u_Ls2;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_FKeyPressed;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color0;" \
		"vec3 phong_ads_color1;" \
		"vec3 phong_ads_color2;" \
		"vec3 phong_ads_color;" \
		"if(u_FKeyPressed==1)" \
		"{" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction0=normalize(light_direction0);" \
		"vec3 ambient0 = u_La0 * u_Ka;" \
		"float tn_dot_ld0 = max(dot(normalized_transformed_normals, normalized_light_direction0),0.0);" \
		"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;" \
		"vec3 reflection_vector0 = reflect(-normalized_light_direction0, normalized_transformed_normals);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color0=ambient0 + diffuse0 + specular0;" \

	
		"vec3 normalized_light_direction1=normalize(light_direction1);" \
		"vec3 ambient1 = u_La1 * u_Ka;" \
		"float tn_dot_ld1 = max(dot(normalized_transformed_normals, normalized_light_direction1),0.0);" \
		"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;" \
		"vec3 reflection_vector1 = reflect(-normalized_light_direction1, normalized_transformed_normals);" \
		"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color1=ambient1 + diffuse1 + specular1;" \

		"vec3 normalized_light_direction2=normalize(light_direction2);" \
		"vec3 ambient2 = u_La2 * u_Ka;" \
		"float tn_dot_ld2 = max(dot(normalized_transformed_normals, normalized_light_direction2),0.0);" \
		"vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;" \
		"vec3 reflection_vector2 = reflect(-normalized_light_direction2, normalized_transformed_normals);" \
		"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color2=ambient2 + diffuse2 + specular2;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color0 = vec3(1.0, 1.0, 1.0);" \
		"phong_ads_color1 = vec3(1.0, 1.0, 1.0);" \
		"phong_ads_color2 = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"phong_ads_color = phong_ads_color0 + phong_ads_color1 + phong_ads_color2;" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";


	glShaderSource(gFragmentShaderObjectPerFragment, 1, (const GLchar **)&fragmentShaderSourceCodePerFragment, NULL);

	glCompileShader(gFragmentShaderObjectPerFragment);

	glGetShaderiv(gFragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
			//	uninitialize();
			//	exit(0);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


	//Shader Program

	gShaderProgramObjectPerVertex = glCreateProgram();

	glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);

	glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

    [self MyLinkProgram:gShaderProgramObjectPerVertex];
	/*
	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");
	
	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				//uninitialize();
				//exit(0);
				[self release];
				[NSApp terminate:self];
			}
		}
	}

	//gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
		gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");

	gLdUniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	gLaUniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	gLsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");

	gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	gKaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
gLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
*/
	Sphere sphere;
	sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = sphere.getNumberOfSphereVertices();
	gNumElements = sphere.getNumberOfSphereElements();


	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	//position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//normal
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);


	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	glBindVertexArray(0);

	//glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);

	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // blue

	gPerspectiveProjectionMatrix = vmath::mat4::identity();
	

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallBack, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displayLink);
	
	gbLight = false;
	gbFrag = false;
	
}

-(void) MyLinkProgram:(GLuint) iPgmObject
{
    gShaderProgramObject = glCreateProgram();
    
    //glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
    
    //glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);
    
    gShaderProgramObject = iPgmObject;
    
    glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
    glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");
    
    glLinkProgram(gShaderProgramObject);
    
    GLint iShaderProgramLinkStatus = 0;
    GLint iInfoLogLength = 0;
    
    char *szInfoLog = NULL;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength>0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
                free(szInfoLog);
                //uninitialize();
                //exit(0);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
    gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
    gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
    
    gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");
    gFKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_FKeyPressed");
    
    gLd0Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld0");
    gLd1Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld1");
    gLd2Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld2");
    
    gLa0Uniform = glGetUniformLocation(gShaderProgramObject, "u_La0");
    gLa1Uniform = glGetUniformLocation(gShaderProgramObject, "u_La1");
    gLa2Uniform = glGetUniformLocation(gShaderProgramObject, "u_La2");
    
    gLs0Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls0");
    gLs1Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls1");
    gLs2Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls2");
    
    gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
    gKsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
    gKaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
    
    gLightPositionUniform0 = glGetUniformLocation(gShaderProgramObject, "u_light_position0");
    gLightPositionUniform1 = glGetUniformLocation(gShaderProgramObject, "u_light_position1");
    gLightPositionUniform2 = glGetUniformLocation(gShaderProgramObject, "u_light_position2");
    gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
    
    
}

-(void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height= rect.size.height;

	if(height == 0)
	height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	/*if(width <= height)
		gOrthographicProjectionMatrix = vmath::ortho(-100.0f, 100.0f, (-100.0f * (height / width)), (100.0f * (height / width)), -100.0f, 100.0f);
	else
		gOrthographicProjectionMatrix = vmath::ortho((-100.0f * (width / height)), (100.0f * (width / height)), -100.0f, 100.0f, -100.0f, 100.0f);
	*/
	
	gPerspectiveProjectionMatrix =  vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height,
		0.1f, 100.0f); 
	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

}

-(void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
	[self Update];
}

-(void)Update
{
	/*gAngleCube = gAngleCube + 0.1f;
	if (gAngleCube >= 360.0f)
		gAngleCube = 0.0f;

	gAnglePyramid = gAnglePyramid + 0.1f;
	if (gAnglePyramid >= 360.0f)
		gAnglePyramid = 0.0f;*/
}

-(void)drawView
{

	static float angle_red = 0.0f;
	//static float gAngleCube = 0.0f;

	lightAmbient0[0] = 0.0f;
	lightAmbient0[1] = 0.0f;
	lightAmbient0[2] = 0.0f;
	lightAmbient0[3] = 1.0f;
	lightAmbient1[0] = 0.0f;
	lightAmbient1[1] = 0.0f;
	lightAmbient1[2] = 0.0f;
	lightAmbient1[3] = 1.0f;
	lightAmbient2[0] = 0.0f;
	lightAmbient2[1] = 0.0f;
	lightAmbient2[2] = 0.0f;
	lightAmbient2[3] = 1.0f;
	
	lightDiffuse0[0] = 1.0f;
	lightDiffuse0[1] = 0.0f;
	lightDiffuse0[2] = 0.0f;
	lightDiffuse0[3] = 1.0f;
	lightDiffuse1[0] = 0.0f;
	lightDiffuse1[1] = 1.0f;
	lightDiffuse1[2] = 0.0f;
	lightDiffuse1[3] = 1.0f;
	lightDiffuse2[0] = 0.0f;
	lightDiffuse2[1] = 0.0f;
	lightDiffuse2[2] = 1.0f;
	lightDiffuse2[3] = 1.0f;
	
	lightSpecular0[0] = 1.0f;
	lightSpecular0[1] = 0.0f;
	lightSpecular0[2] = 0.0f;
	lightSpecular0[3] = 1.0f;
	lightSpecular1[0] = 0.0f;
	lightSpecular1[1] = 1.0f;
	lightSpecular1[2] = 0.0f;
	lightSpecular1[3] = 1.0f;
	lightSpecular2[0] = 0.0f;
	lightSpecular2[1] = 0.0f;
	lightSpecular2[2] = 1.0f;
	lightSpecular2[3] = 1.0f;
	
	lightPosition0[0] = 100.0f;
	lightPosition0[1] = 100.0f;
	lightPosition0[2] = 100.0f;
	lightPosition0[3] = 0.0f;
	lightPosition1[0] = -100.0f;
	lightPosition1[1] = 100.0f;
	lightPosition1[2] = 100.0f;
	lightPosition1[3] = 0.0f;
	lightPosition2[0] = 0.0f;
	lightPosition2[1] = 0.0f;
	lightPosition2[2] = 100.0f;
	lightPosition2[3] = 0.0f;

	material_ambient[0] = 0.0f;
	material_ambient[1] = 0.0f;
	material_ambient[2] = 0.0f;
	material_ambient[3] = 1.0f;
	material_diffuse[0] = 1.0f;
	material_diffuse[1] = 1.0f;
	material_diffuse[2] = 1.0f;
	material_diffuse[3] = 1.0f;
	material_specular[0] = 1.0f;
	material_specular[1] = 1.0f;
	material_specular[2] = 1.0f;
	material_specular[3] = 1.0f;
	material_shininess = 50.0f;

	[[self openGLContext]makeCurrentContext];

	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	if (gbLight == true)
	{
		gShaderProgramObjectPerVertex = glCreateProgram();

		glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);

		glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

        [self MyLinkProgram:gShaderProgramObjectPerVertex];
		glUseProgram(gShaderProgramObjectPerVertex);

		glUniform1i(gLKeyPressedUniform, 1);

		glUniform3fv(gLd0Uniform, 1, lightDiffuse0);//white
		glUniform3fv(gLa0Uniform, 1, lightAmbient0);
		glUniform3fv(gLs0Uniform, 1, lightSpecular0);
		glUniform3fv(gLd1Uniform, 1, lightDiffuse1);//white
		glUniform3fv(gLa1Uniform, 1, lightAmbient1);
		glUniform3fv(gLs1Uniform, 1, lightSpecular1);
		glUniform3fv(gLd2Uniform, 1, lightDiffuse2);//white
		glUniform3fv(gLa2Uniform, 1, lightAmbient2);
		glUniform3fv(gLs2Uniform, 1, lightSpecular2);

		myRotateMatrix[0] = 0.0f;
		myRotateMatrix[1] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix[2] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix[3] = 1.0f;
		glUniform4fv(gLightPositionUniform0, 1, myRotateMatrix);

		myRotateMatrix1[0] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix1[1] = 0.0f;
		myRotateMatrix1[2] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix1[3] = 1.0f;
		glUniform4fv(gLightPositionUniform1, 1, myRotateMatrix1);

		myRotateMatrix2[0] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix2[1] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix2[2] = -2.0f;
		myRotateMatrix2[3] = 1.0f;
		glUniform4fv(gLightPositionUniform2, 1, myRotateMatrix2);

		//glUniform4fv(gLightPositionUniform, 1, lightPosition);

		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

		//float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };
		
	}
	else if(gbFrag == true)
	{
		gShaderProgramObjectPerFragment = glCreateProgram();

		glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);

		glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

        [self MyLinkProgram:gShaderProgramObjectPerFragment];
		glUseProgram(gShaderProgramObjectPerFragment);
		glUniform1i(gFKeyPressedUniform, 1);

		glUniform3fv(gLd0Uniform, 1, lightDiffuse0);//white
		glUniform3fv(gLa0Uniform, 1, lightAmbient0);
		glUniform3fv(gLs0Uniform, 1, lightSpecular0);
		glUniform3fv(gLd1Uniform, 1, lightDiffuse1);//white
		glUniform3fv(gLa1Uniform, 1, lightAmbient1);
		glUniform3fv(gLs1Uniform, 1, lightSpecular1);
		glUniform3fv(gLd2Uniform, 1, lightDiffuse2);//white
		glUniform3fv(gLa2Uniform, 1, lightAmbient2);
		glUniform3fv(gLs2Uniform, 1, lightSpecular2);

		myRotateMatrix[0] = 0.0f;
		myRotateMatrix[1] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix[2] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix[3] = 1.0f;
		glUniform4fv(gLightPositionUniform0, 1, myRotateMatrix);

		myRotateMatrix1[0] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix1[1] = 0.0f;
		myRotateMatrix1[2] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix1[3] = 1.0f;
		glUniform4fv(gLightPositionUniform1, 1, myRotateMatrix1);

		myRotateMatrix2[0] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix2[1] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix2[2] = -2.0f;
		myRotateMatrix2[3] = 1.0f;
		glUniform4fv(gLightPositionUniform2, 1, myRotateMatrix2);



		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

	

	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
		glUniform1i(gFKeyPressedUniform, 0);
	}


	vmath::mat4 modelMatrix = vmath::mat4::identity();
	vmath::mat4 viewMatrix = vmath::mat4::identity();

	//vmath::mat4 rotationMatrix = vmath::mat4::identity();
	
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f); 
	//rotationMatrix = vmath::rotate(gAnglePyramid, 0.0f, 1.0f, 0.0f);
	
	//modelViewMatrix = modelViewMatrix * rotationMatrix;
	
	//modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

	//glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);
	
	//Cube Block
	/*modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();
	vmath::mat4 scaleMatrix = vmath::mat4::identity();
	
	modelViewMatrix = vmath::translate(1.5f, 0.0f, -6.0f);
	scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);

	rotationMatrix = vmath::rotate(gAngleCube, 1.0f, 0.0f, 0.0f);
	modelViewMatrix = modelViewMatrix * scaleMatrix;
	modelViewMatrix = modelViewMatrix * rotationMatrix;
	
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(gVao_Cube);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	
	glBindVertexArray(0);*/

	glUseProgram(0);
	
	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	angle_red = angle_red + 0.1f;
	//gAnglePyramid = gAnglePyramid + 0.1f;
}

-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return (YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27://Esc
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			if (bIsFKeyPressed == false)
			{
				gbLight = false;
				gbFrag = true;
				bIsFKeyPressed = true;
				bIsLKeyPressed = false;

			}
			else
			{
				gbFrag = false;
				//gbFrag = false;
				bIsFKeyPressed = false;
			}
			break;
		case 'L':
		case 'l':
			if (bIsLKeyPressed == false)
			{
				gbLight = true;
				gbFrag = false;
				bIsLKeyPressed = true;
				bIsFKeyPressed = false;

			}
			else
			{
				gbLight = false;
				//gbFrag = false;

				bIsLKeyPressed = false;
			}
			break;
		case 'Q':
		case 'q':
			[[self window]toggleFullScreen:self];
			break;	
		default:
			break;
	}	
}

-(void)mouseDown:(NSEvent *)theEvent
{
}

-(void)mouseDragged:(NSEvent *)theEvent
{
}

-(void)rightMouseDown:(NSEvent *)theEvent
{

}

-(void)dealloc
{

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}
	
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);

	glDetachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
	glDetachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	glDetachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
	glDetachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	glDeleteShader(gVertexShaderObjectPerVertex);
	gVertexShaderObjectPerVertex = 0;
	glDeleteShader(gFragmentShaderObjectPerVertex);
	gFragmentShaderObjectPerVertex = 0;

	glDeleteShader(gVertexShaderObjectPerFragment);
	gVertexShaderObjectPerFragment = 0;
	glDeleteShader(gFragmentShaderObjectPerFragment);
	gFragmentShaderObjectPerFragment = 0;


	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;


	
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallBack(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime,
	CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
	CVReturn result = [ (GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return (result);
}



