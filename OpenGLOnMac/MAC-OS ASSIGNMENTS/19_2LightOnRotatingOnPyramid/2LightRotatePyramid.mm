//headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import<QuartzCore/CVDisplayLink.h>
#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import "vmath.h"

enum
{
	MALATI_ATTRIBUTE_VERTEX=0,
	MALATI_ATTRIBUTE_COLOR, 
	MALATI_ATTRIBUTE_NORMAL,
	MALATI_ATTRIBUTE_TEXTURE0,
};

//'c' style global function declaration
CVReturn MyDisplayLinkCallBack(CVDisplayLinkRef , const CVTimeStamp *, const CVTimeStamp *,
	CVOptionFlags , CVOptionFlags *, void *);

FILE *gpFile = NULL;

//interface declarations
@interface AppDeligate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

//Entry Point Function
int main(int argc, const char * argv[])
{
	//code
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];	

	[NSApp setDelegate : [[AppDeligate alloc] init]];

	[NSApp run];

	[pPool release];

	return (0);
}

//interface implementation
@implementation AppDeligate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching : (NSNotification *)aNotification	//WM_CREATE
{
	//code
	//window
	
	//for LOG file
	 NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
		printf("Can Not Create Log File.\nExitting...\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile, "Program Is Started Successfully...\n");
	
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	//create simple window
	window = [[NSWindow alloc] initWithContentRect:win_rect 
				styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
				 backing:NSBackingStoreBuffered
				 defer:NO];

	[window setTitle:@"macOS Window"];
	[window center];

	NSLog(@"%@\n", window);
	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];

}

-(void)applicationWillTerminate:(NSNotification *)notification //WM_CLOSE OR WM_DESTROY
{
	//code
	fprintf(gpFile, "Program Is Terminate Successfully\n");
	
	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

-(void)windowWillClose:(NSNotification *)notification
{
	//code
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];
	
	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displayLink;
	
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;
	
	GLuint gVao_Pyramid;
	GLuint gVbo_Pyramid_Position;
	GLuint gVbo_Pyramid_Normal;
	
	GLuint gModelMatrixUniform, gViewMatrixUniform, gProjectionMatrixUniform;

	GLuint gLdRedUniform, gLdBlueUniform, gLaUniform, gLsUniform, gKdUniform, gKaUniform, gKsUniform, gRedLightPositionUniform, gBlueLightPositionUniform, gMaterialShininessUniform;
	GLuint gLKeyPressedUniform;
	bool gbLight;

	vmath::mat4 gPerspectiveProjectionMatrix;
	
		bool bIsLKeyPressed;
GLfloat lightAmbient[4] ;
GLfloat lightDiffuseRed[4];
GLfloat lightDiffuseBlue[4];
GLfloat lightSpecular[4] ;
GLfloat lightPositionRed[4] ;
GLfloat lightPositionBlue[4] ;

GLfloat material_ambient[4];
GLfloat material_diffuse[4] ;
GLfloat material_specular[4] ;
GLfloat material_shininess ;
}

-(id)initWithFrame:(NSRect)frame;
{
	self = [super initWithFrame:frame];
	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery, 
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat == nil)
		{
			fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available. Exitting..");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];
				
	}
	return (self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version	: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	
	//***VERTEX SHADER***
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertextShaderSourceCode =
		/*"#version 410" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;"\
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \

		"uniform int u_LKeyPressed;" \

		"uniform vec3 u_LdRed;" \
		"uniform vec3 u_LdBlue;" \

		"uniform vec3 u_La;" \
		"uniform vec3 u_Ls;" \

		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Ks;" \

		"uniform vec4 u_light_positionRed;"\
		"uniform vec4 u_light_positionBlue;"\

		"uniform float u_material_shininess;" \

		"out vec3 phong_ads_red_color;" \
		"out vec3 phong_ads_blue_color;" \
        "vec3 light_direction_blue;" \
        "vec3 light_direction_red;" \
		"vec4 eyeCoordinates;" \
		"vec4 eyeCoordinates1;" \
		"vec3 blue_light;"\
		"vec3 red_light;"\
		"vec3 tnorm;"\
		"vec3 tnorm1;"\
		"vec3 reflection_vector;"\
		"vec3 reflection_vector1;"\
		"vec3 viewer_vector;" \
		"vec3 viewer_vector1;" \
		"vec3 specular;" \
		"vec3 specular1;" \
		"void main(void)"\
		"{"\
		"if(u_LKeyPressed == 1)" \
		"{" \
		"eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
		"tnorm = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"light_direction_blue = normalize(vec3(u_light_positionBlue - eyeCoordinates.xyz));" \
		"vec3 ambient = u_La * u_Ka;"\
		"blue_light = u_LdBlue * u_Kd * max(dot(light_direction_blue, tnorm), 0.0);" \
		"reflection_vector = reflect(-light_direction_blue, tnorm);"\
		"viewer_vector = normalize(-eyeCoordinates.xyz);" \
		"specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0),u_material_shininess);" \
		"phong_ads_blue_color=ambient + blue_light + specular;" \
		"light_direction_red = normalize(vec3(u_light_positionRed - eyeCoordinates.xyz));" \
		"red_light = u_LdRed * u_Kd * max(dot(light_direction_red, tnorm), 0.0);" \
		"reflection_vector1 = reflect(-light_direction_red, tnorm);"\
		"viewer_vector1 = normalize(-eyeCoordinates1.xyz);" \
		"specular1 = u_Ls * u_Ks * pow(max(dot(reflection_vector1, viewer_vector1), 0.0),u_material_shininess);" \
		"phong_ads_red_color=ambient + red_light + specular1;" \
		"}" \
		"else"\
		"{"\
		"phong_ads_red_color = vec3(1.0, 1.0, 1.0);"\
		"phong_ads_blue_color = vec3(1.0, 1.0, 1.0);"\
		"}"\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
         */
    
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_model_matrix;" \
    "uniform mat4 u_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    
    "uniform int u_LKeyPressed;" \
    
    "uniform vec3 u_LdRed;" \
    "uniform vec3 u_LdBlue;" \
    "uniform vec3 u_La;" \
    "uniform vec3 u_Ls;" \
    "uniform vec4 u_light_positionRed;"\
    "uniform vec4 u_light_positionBlue;"\
    "uniform vec3 u_Ka;" \
    "uniform vec3 u_Ks;" \
    "uniform vec3 u_Kd;" \
    "uniform float u_material_shininess;" \
    
    "out vec3 phong_ads_red_color;" \
    "out vec3 phong_ads_blue_color;" \
    "void main(void)" \
    "{" \
    "if(u_LKeyPressed == 1)" \
    "{" \
    "vec4 eyeCoordinates =  u_view_matrix * u_model_matrix * vPosition;" \
    "vec3 tnorm = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
    "vec3 ambient = u_La * u_Ka;"\
    "vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
    
    "vec3 light_directionRed  = normalize(vec3(u_light_positionRed) - eyeCoordinates.xyz);" \
    "vec3 diffuse_light1 = u_LdRed * u_Kd * max(dot(light_directionRed, tnorm), 0.0);" \
    "vec3 reflection_vector1 = reflect(-light_directionRed, tnorm);"\
    
    "vec3 specular1 = u_Ls * u_Ks * pow(max(dot(reflection_vector1, viewer_vector), 0.0),u_material_shininess);" \
    "phong_ads_red_color=ambient + diffuse_light1 + specular1;" \
    
    "vec3 light_directionBlue  = normalize(vec3(u_light_positionBlue) - eyeCoordinates.xyz);" \
    "vec3 diffuse_light2 = u_LdBlue * u_Kd * max(dot(light_directionBlue, tnorm), 0.0);" \
    "vec3 reflection_vector2 = reflect(-light_directionBlue, tnorm);"\
    "vec3 specular2 = u_Ls * u_Ks * pow(max(dot(reflection_vector2, viewer_vector), 0.0),u_material_shininess);" \
    "phong_ads_blue_color=ambient + diffuse_light2 + specular2;" \
    "}" \
    "else"\
    "{"\
    "phong_ads_red_color = vec3(1.0, 1.0, 1.0);"\
    "phong_ads_blue_color = vec3(1.0,1.0,1.0);"\
    "}"\
    "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
    "}";


	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertextShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				//uninitialize();
				//exit(0);
				[self release];
				[NSApp terminate:self];
			}
		}
	}
	//FRAGMENT SHADER
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;
	
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
		"#version 410"\
		"\n" \
		"in vec3 phong_ads_red_color;" \
		"in vec3 phong_ads_blue_color;" \
		"vec4 RedFragColor;" \
		"vec4 BlueFragColor;"\
		"out vec4 FragColor;"\
		"uniform int u_LKeyPressed;" \
		"void main(void)"\
		"{"\
		"RedFragColor = vec4(phong_ads_red_color, 1.0);" \
		"BlueFragColor = vec4(phong_ads_blue_color, 1.0);" \
		"FragColor = RedFragColor + BlueFragColor; "\
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	glCompileShader(gFragmentShaderObject);
	
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				//uninitialize();
				//exit(0);
				[self release];
				[NSApp terminate:self];
			}
		}
	}
	//Shader Program

	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");
	
	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				//uninitialize();
				//exit(0);
				[self release];
				[NSApp terminate:self];
			}
		}
	}

	gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");

	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");
	gLdBlueUniform = glGetUniformLocation(gShaderProgramObject, "u_LdBlue");
	gLdRedUniform = glGetUniformLocation(gShaderProgramObject, "u_LdRed");
	gLaUniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	gLsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");

	gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	gKaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	gRedLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_positionRed");
	gBlueLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_positionBlue");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	const GLfloat PyramidVertices[] =
	{	//front face
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		//right face
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		//back face
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		//Left face
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f

	};

	const GLfloat pyramidNormal[] =
	{
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,

		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,

		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,

		-0.894427f, 0.447214f, 0.0f
		- 0.894427f, 0.447214f, 0.0f
		- 0.894427f, 0.447214f, 0.0f

	};

	glGenVertexArrays(1, &gVao_Pyramid);
	glBindVertexArray(gVao_Pyramid);

	glGenBuffers(1, &gVbo_Pyramid_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Pyramid_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(PyramidVertices), PyramidVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	//color
glGenBuffers(1, &gVbo_Pyramid_Normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Pyramid_Normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormal), pyramidNormal, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);


	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	//glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);

	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // blue

	gPerspectiveProjectionMatrix = vmath::mat4::identity();
	

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallBack, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height= rect.size.height;

	if(height == 0)
	height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	/*if(width <= height)
		gOrthographicProjectionMatrix = vmath::ortho(-100.0f, 100.0f, (-100.0f * (height / width)), (100.0f * (height / width)), -100.0f, 100.0f);
	else
		gOrthographicProjectionMatrix = vmath::ortho((-100.0f * (width / height)), (100.0f * (width / height)), -100.0f, 100.0f, -100.0f, 100.0f);
	*/
	
	gPerspectiveProjectionMatrix =  vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height,
		0.1f, 100.0f); 
	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

}

-(void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
	[self Update];
}

-(void)Update
{
	/*gAngleCube = gAngleCube + 0.1f;
	if (gAngleCube >= 360.0f)
		gAngleCube = 0.0f;

	gAnglePyramid = gAnglePyramid + 0.1f;
	if (gAnglePyramid >= 360.0f)
		gAnglePyramid = 0.0f;*/
}

-(void)drawView
{

	static float gAnglePyramid = 0.0f;

	lightAmbient[0] = 0.0f;
	lightAmbient[1] = 0.0f;
	lightAmbient[2] = 0.0f;
	lightAmbient[3] = 1.0f;
	
	lightDiffuseRed[0] = 1.0f;
	lightDiffuseRed[1] = 0.0f;
	lightDiffuseRed[2] = 0.0f;
	lightDiffuseRed[3] = 0.0f;
	lightDiffuseBlue[0] = 0.0f;
	lightDiffuseBlue[1] = 0.0f;
	lightDiffuseBlue[2] = 1.0f;
	lightDiffuseBlue[3] = 0.0f;
	
	lightSpecular[0] = 1.0f;
	lightSpecular[1] = 1.0f;
	lightSpecular[2] = 1.0f;
	lightSpecular[3] = 1.0f;
	lightPositionRed[0] = -100.0f;
	lightPositionRed[1] = 0.0f;
	lightPositionRed[2] = 0.0f;
	lightPositionRed[3] = 1.0f;
	lightPositionBlue[0] = 100.0f;
	lightPositionBlue[1] = 0.0f;
	lightPositionBlue[2] = 0.0f;
	lightPositionBlue[3] = 1.0f;

	material_ambient[0] = 0.0f;
	material_ambient[1] = 0.0f;
	material_ambient[2] = 0.0f;
	material_ambient[3] = 1.0f;
	material_diffuse[0] = 1.0f;
	material_diffuse[1] = 1.0f;
	material_diffuse[2] = 1.0f;
	material_diffuse[3] = 1.0f;
	material_specular[0] = 1.0f;
	material_specular[1] = 1.0f;
	material_specular[2] = 1.0f;
	material_specular[3] = 1.0f;
	material_shininess = 50.0f;
	
	[[self openGLContext]makeCurrentContext];

	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	if (gbLight == true)
	{
		

		glUniform1i(gLKeyPressedUniform, 1);
		glUniform3fv(gLdBlueUniform, 1, lightDiffuseBlue);
		glUniform3fv(gLdRedUniform, 1, lightDiffuseRed);
		glUniform3fv(gLaUniform, 1, lightAmbient);
		glUniform3fv(gLsUniform, 1, lightSpecular);
		
		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

		//float lightPosition1[] = { -3.0f, 0.0f, -1.0f, 1.0f };
		glUniform4fv(gRedLightPositionUniform, 1, lightPositionRed);
		//float lightPosition[] = { 3.0f, 0.0f, 1.0f, 1.0f };
		glUniform4fv(gBlueLightPositionUniform, 1, lightPositionBlue);
		
	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
	}
vmath::mat4 modelMatrix = vmath::mat4::identity();
	vmath::mat4 viewMatrix = vmath::mat4::identity();
	vmath::mat4 rotationMatrix = vmath::mat4::identity();

	modelMatrix = vmath::translate(0.0f, 0.0f, -5.0f); //for perspective(4rth change)

	rotationMatrix = vmath::rotate(gAnglePyramid, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix; //imp


	//triangle Block
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	
	glBindVertexArray(gVao_Pyramid);

	glDrawArrays(GL_TRIANGLES, 0, 12); //Imp
	glBindVertexArray(0);
	glUseProgram(0);

	
	
	
	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	
    gAnglePyramid = gAnglePyramid + 0.5f;
}

-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return (YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27://Esc
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
        case 'L':
        case 'l':
            if (bIsLKeyPressed == false)
            {
                gbLight = true;
                bIsLKeyPressed = true;
            }
            else
            {
                gbLight = false;
                bIsLKeyPressed = false;
            }
            break;
		default:
			break;
	}	
}

-(void)mouseDown:(NSEvent *)theEvent
{
}

-(void)mouseDragged:(NSEvent *)theEvent
{
}

-(void)rightMouseDown:(NSEvent *)theEvent
{

}

-(void)dealloc
{

	if (gVao_Pyramid)
	{
		glDeleteVertexArrays(1, &gVao_Pyramid);
		gVao_Pyramid = 0;
	}
	
	if (gVbo_Pyramid_Position)
	{
		glDeleteBuffers(1, &gVbo_Pyramid_Position);
		gVbo_Pyramid_Position = 0;
	}
	if (gVbo_Pyramid_Normal)
	{
		glDeleteBuffers(1, &gVbo_Pyramid_Normal);
		gVbo_Pyramid_Normal = 0;
	}

	

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallBack(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime,
	CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
	CVReturn result = [ (GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return (result);
}
