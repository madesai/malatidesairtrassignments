//headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

//interface declarations
@interface AppDeligate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface MyView : NSView
@end

//Entry Point Function
int main(int argc, const char * argv[])
{
	//code
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];	

	[NSApp setDelegate : [[AppDeligate alloc] init]];

	[NSApp run];

	[pPool release];

	return (0);
}

//interface implementation
@implementation AppDeligate
{
@private
	NSWindow *window;
	MyView *view;
}

- (void)applicationDidFinishLaunching : (NSNotification *)aNotification	//WM_CREATE
{
	//code
	//window
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	//create simple window
	window = [[NSWindow alloc] initWithContentRect:win_rect 
				styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
				 backing:NSBackingStoreBuffered
				 defer:NO];

	[window setTitle:@"macOS Window"];
	[window center];

	NSLog(@"%@\n", window);
	view = [[MyView alloc]initWithFrame:win_rect];

	[window setContentView:view];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];

}

-(void)applicationWillTerminate:(NSNotification *)notification //WM_CLOSE OR WM_DESTROY
{
	//code
}

-(void)windowWillClose:(NSNotification *)notification
{
	//code
	[NSApp terminate:self];
}

-(void)dealloc
{
	[view release];
	
	[window release];

	[super dealloc];
}
@end

@implementation MyView
{
	NSString	*centralText;
}

-(id)initWithFrame:(NSRect)frame;
{
	self = [super initWithFrame:frame];
	if(self)
	{
		[[self window]setContentView:self];
		//NSLog(@"%@\n", self window);
		centralText = @"Hello World !!!";
	}
	return (self);
}

-(void)drawRect:(NSRect)dirtyRect
{

	//code
	//black background
	NSColor *fillColor=[NSColor blackColor];
	[fillColor set];
	NSRectFill(dirtyRect);
	
	//dictionary with kvc
	NSDictionary *dictionaryForTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
												[NSFont fontWithName:@"Helvetica" size:32], NSFontAttributeName,
												[NSColor greenColor], NSForegroundColorAttributeName,
												nil];
	
	NSSize textSize = [centralText sizeWithAttributes:dictionaryForTextAttributes];
	
	NSPoint point;
	point.x = (dirtyRect.size.width/2) - (textSize.width/2);
	point.y = (dirtyRect.size.height/2)-(textSize.height/2)+12;
	
	[centralText drawAtPoint:point withAttributes:dictionaryForTextAttributes];
}
	
-(BOOL)acceptFirstResponder
{
	[[self window]makeFirstResponder:self];
	return (YES);
}
	
-(void)keyDown:(NSEvent *)theEvent
{
	int key=(int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27:
			[self release];
			[NSApp terminate:self];
			break;
		case 'f':
		case 'F':
			centralText = @"'F' or 'f' Key Is pressed";
			[[self window]toggleFullScreen:self];
			break;
		default:
			break;
	}
}
	
-(void)mouseDown:(NSEvent *)theEvent
{
	centralText=@"Left Mouse Button Is Clicked";
	[self setNeedsDisplay:YES];
}

-(void)mouseDragged:(NSEvent *)theEvent
{
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	centralText=@"Right Mouse Button Is Clicked";
	[self setNeedsDisplay:YES];
}
	
-(void)dealloc
{
		[super dealloc];
}

@end
