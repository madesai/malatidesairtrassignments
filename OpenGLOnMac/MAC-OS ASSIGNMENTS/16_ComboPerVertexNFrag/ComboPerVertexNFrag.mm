//headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import<QuartzCore/CVDisplayLink.h>
#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import "vmath.h"
#import "sphere.h"

enum
{
	MALATI_ATTRIBUTE_VERTEX=0,
	MALATI_ATTRIBUTE_COLOR, 
	MALATI_ATTRIBUTE_NORMAL,
	MALATI_ATTRIBUTE_TEXTURE0,
};

//'c' style global function declaration
CVReturn MyDisplayLinkCallBack(CVDisplayLinkRef , const CVTimeStamp *, const CVTimeStamp *,
	CVOptionFlags , CVOptionFlags *, void *);

FILE *gpFile = NULL;

//interface declarations
@interface AppDeligate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

//Entry Point Function
int main(int argc, const char * argv[])
{
	//code
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];	

	[NSApp setDelegate : [[AppDeligate alloc] init]];

	[NSApp run];

	[pPool release];

	return (0);
}

//interface implementation
@implementation AppDeligate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching : (NSNotification *)aNotification	//WM_CREATE
{
	//code
	//window
	
	//for LOG file
	 NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
		printf("Can Not Create Log File.\nExitting...\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile, "Program Is Started Successfully...\n");
	
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	//create simple window
	window = [[NSWindow alloc] initWithContentRect:win_rect 
				styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
				 backing:NSBackingStoreBuffered
				 defer:NO];

	[window setTitle:@"macOS Window"];
	[window center];

	NSLog(@"%@\n", window);
	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];

}

-(void)applicationWillTerminate:(NSNotification *)notification //WM_CLOSE OR WM_DESTROY
{
	//code
	fprintf(gpFile, "Program Is Terminate Successfully\n");
	
	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

-(void)windowWillClose:(NSNotification *)notification
{
	//code
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];
	
	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displayLink;
	
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;
	
	/*GLuint gVao_Pyramid;
	GLuint gVao_Cube;
	GLuint gVbo_Pyramid_Position;
	GLuint gVbo_Cube_Position;

	GLuint gVbo_Pyramid_Color;
	GLuint gVbo_Cube_Color;
*/
	GLuint gNumElements;
	GLuint gNumVertices;
	float sphere_vertices[1146];
	float sphere_normals[1146];
	float sphere_textures[764];
	short sphere_elements[2280];

	//GLuint gVbo_color;
	//GLuint gMVPUniform;
	GLuint gVao_sphere;
	GLuint gVbo_sphere_position;
	GLuint gVbo_sphere_normal;
	GLuint gVbo_sphere_element;

GLuint gModelMatrixUniform, gViewMatrixUniform, gProjectionMatrixUniform;
GLuint gLdUniform, gLaUniform, gLsUniform;
GLuint gLightPositionUniform;
GLuint gKdUniform, gKaUniform, gKsUniform;
GLuint gMaterialShininessUniform;
GLuint gLKeyPressedUniform;
    GLuint gVKeyPressedUniform;
    GLuint gFKeyPressedUniform;
    
	vmath::mat4 gPerspectiveProjectionMatrix;
	
	bool gbLight;
    bool gbFragmentLight;
    bool gbVertexLight;
    
	bool bIsLKeyPressed;
    bool bIsFKeyPressed;
    bool bIsVKeyPressed;
    
GLfloat lightAmbient[4] ;
GLfloat lightDiffuse[4];
GLfloat lightSpecular[4] ;
GLfloat lightPosition[4] ;

GLfloat material_ambient[4];
GLfloat material_diffuse[4] ;
GLfloat material_specular[4] ;
GLfloat material_shininess ;

}

-(id)initWithFrame:(NSRect)frame;
{
	self = [super initWithFrame:frame];
	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery, 
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat == nil)
		{
			fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available. Exitting..");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];
				
	}
	return (self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version	: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	
	//***VERTEX SHADER***
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertextShaderSourceCode =
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    
    "out vec3 transformed_normals;" \
    "out vec3 light_direction;" \
    "out vec3 viewer_vector;" \
    
    "uniform mat4 u_model_matrix;" \
    "uniform mat4 u_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    
    "uniform int u_LKeyPressed;" \
    "uniform int u_VKeyPressed;" \
    "uniform int u_FKeyPressed;" \
    
    "uniform vec3 u_Ld;" \
    "uniform vec3 u_La;" \
    "uniform vec3 u_Ls;" \
    "uniform vec4 u_light_position;"\
    "uniform vec3 u_Ka;" \
    "uniform vec3 u_Ks;" \
    "uniform vec3 u_Kd;" \
    "uniform float u_material_shininess;" \
    
    "out vec3 phong_ads_color;" \
    "void main(void)" \
    "{" \
    "if(u_VKeyPressed == 1)" \
    "{" \
    "vec4 eyeCoordinates =  u_view_matrix * u_model_matrix * vPosition;" \
    "vec3 tnorm = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
    "vec3 light_direction  = normalize(vec3(u_light_position) - eyeCoordinates.xyz);" \
    "vec3 ambient = u_La * u_Ka;"\
    "vec3 diffuse_light = u_Ld * u_Kd * max(dot(light_direction, tnorm), 0.0);" \
    "vec3 reflection_vector = reflect(-light_direction, tnorm);"\
    "vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
    "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0),u_material_shininess);" \
    "phong_ads_color=ambient + diffuse_light + specular;" \
    "}" \
    "else if(u_FKeyPressed == 1)" \
    "{" \
    "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
    "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
    "light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
    "viewer_vector = -eye_coordinates.xyz;" \
    "}" \
    "else"\
    "{"\
    "phong_ads_color = vec3(1.0, 1.0, 1.0);"\
    "}"\
    "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
    "}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertextShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				//uninitialize();
				//exit(0);
				[self release];
				[NSApp terminate:self];
			}
		}
	}
	//FRAGMENT SHADER
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;
	
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
    "#version 410" \
    "\n" \
    "in vec3 phong_ads_color;" \
    
    "in vec3 transformed_normals;" \
    "in vec3 light_direction;" \
    "in vec3 viewer_vector;" \
    
    "out vec4 FragColor;" \
    "uniform vec3 u_La;" \
    "uniform vec3 u_Ld;" \
    "uniform vec3 u_Ls;" \
    "uniform vec3 u_Ka;" \
    "uniform vec3 u_Kd;" \
    "uniform vec3 u_Ks;" \
    "uniform float u_material_shininess;" \
    "uniform int u_VKeyPressed;" \
    "uniform int u_FKeyPressed;" \
    "void main(void)" \
    "{" \
    "vec3 phong_ads_color1;" \
    "if(u_VKeyPressed == 1)" \
    "{" \
    "FragColor = vec4(phong_ads_color, 1.0);" \
    "}" \
    "else" \
    "{" \
    "if(u_FKeyPressed == 1)" \
    "{" \
    "vec3 normalized_transformed_normals=normalize(transformed_normals);" \
    "vec3 normalized_light_direction=normalize(light_direction);" \
    "vec3 normalized_viewer_vector=normalize(viewer_vector);" \
    "vec3 ambient = u_La * u_Ka;" \
    "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
    "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
    "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
    "phong_ads_color1=ambient + diffuse + specular;" \
    "}" \
    "else" \
    "{" \
    "phong_ads_color1 = vec3(1.0, 1.0, 1.0);" \
    "}" \
    "FragColor = vec4(phong_ads_color1, 1.0);" \
    "}"
    "}";
    
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	glCompileShader(gFragmentShaderObject);
	
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				//uninitialize();
				//exit(0);
				[self release];
				[NSApp terminate:self];
			}
		}
	}
	//Shader Program

	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");
	
	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				//uninitialize();
				//exit(0);
				[self release];
				[NSApp terminate:self];
			}
		}
	}

	//gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
		gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");
    gVKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_VKeyPressed");
    gFKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_FKeyPressed");
    

	gLdUniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	gLaUniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	gLsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");

	gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	gKaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
gLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	Sphere sphere;
	sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = sphere.getNumberOfSphereVertices();
	gNumElements = sphere.getNumberOfSphereElements();


	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	//position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//normal
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);


	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	glBindVertexArray(0);

	/*const GLfloat PyramidVertices[] =
	{	//front face
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		//right face
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		//back face
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		//Left face
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f

	};

	const GLfloat PyramidColors[] =
	{
		//front
		1.0f, 0.0f, 0.0f,//r
		0.0f, 1.0f, 0.0f,//g
		0.0f, 0.0f, 1.0f,//b
		//right
		1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f,
		//back
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f,
		//left
		1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f

	};
	glGenVertexArrays(1, &gVao_Pyramid);
	glBindVertexArray(gVao_Pyramid);

	glGenBuffers(1, &gVbo_Pyramid_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Pyramid_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(PyramidVertices), PyramidVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	//color
	glGenBuffers(1, &gVbo_Pyramid_Color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Pyramid_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(PyramidColors), PyramidColors, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_COLOR);


	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	//Cube
	const GLfloat CubeVertices[] =
	{
		//top
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		//bottom
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		//front
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		//back
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		//right
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		//left
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f

	};

	const GLfloat cubeColors[] =
	{
//		1.0f, 0.0f, 0.0f, //top
	//	0.0f, 1.0f, 0.0f, //bottom
		//0.0f, 0.0f, 1.0f, //front
		//0.0f, 1.0f, 1.0f, //back
		//1.0f, 0.0f, 1.0f, //right
		//1.0f, 1.0f, 0.0f //left

		1.0f, 0.0f, 0.0f, //top
		0.0f, 1.0f, 0.0f, //bottom
		0.0f, 0.0f, 1.0f, //front
		0.0f, 1.0f, 1.0f, //back

		0.0f, 1.0f, 0.0f, //bottom
		0.0f, 0.0f, 1.0f, //front
		0.0f, 1.0f, 1.0f, //back
		1.0f, 0.0f, 1.0f, //right

		0.0f, 0.0f, 1.0f, //front
		0.0f, 1.0f, 1.0f, //back
		1.0f, 0.0f, 1.0f, //right
		1.0f, 1.0f, 0.0f, //left

		0.0f, 1.0f, 1.0f, //back
		1.0f, 0.0f, 1.0f, //right
		1.0f, 1.0f, 0.0f, //left
		1.0f, 0.0f, 0.0f, //top

		1.0f, 0.0f, 1.0f, //right
		1.0f, 1.0f, 0.0f, //left
		1.0f, 0.0f, 0.0f, //top
		0.0f, 1.0f, 0.0f, //bottom

		1.0f, 1.0f, 0.0f, //left
		1.0f, 0.0f, 0.0f, //top
		0.0f, 1.0f, 0.0f, //bottom
		0.0f, 0.0f, 1.0f //front

	};

	glGenVertexArrays(1, &gVao_Cube);
	glBindVertexArray(gVao_Cube);

	glGenBuffers(1, &gVbo_Cube_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Cube_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(CubeVertices), CubeVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	//color
	glGenBuffers(1, &gVbo_Cube_Color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Cube_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeColors), cubeColors, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_COLOR);

	
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//glVertexAttrib3f(MALATI_ATTRIBUTE_COLOR, 0.39f, 0.58f, 0.9294f);
	//glEnableVertexAttribArray(MALATI_ATTRIBUTE_COLOR);
	
	glBindVertexArray(0);
*/
	//glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);

	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // blue

	gPerspectiveProjectionMatrix = vmath::mat4::identity();
	

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallBack, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height= rect.size.height;

	if(height == 0)
	height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	/*if(width <= height)
		gOrthographicProjectionMatrix = vmath::ortho(-100.0f, 100.0f, (-100.0f * (height / width)), (100.0f * (height / width)), -100.0f, 100.0f);
	else
		gOrthographicProjectionMatrix = vmath::ortho((-100.0f * (width / height)), (100.0f * (width / height)), -100.0f, 100.0f, -100.0f, 100.0f);
	*/
	
	gPerspectiveProjectionMatrix =  vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height,
		0.1f, 100.0f); 
	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

}

-(void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
	[self Update];
}

-(void)Update
{
	/*gAngleCube = gAngleCube + 0.1f;
	if (gAngleCube >= 360.0f)
		gAngleCube = 0.0f;

	gAnglePyramid = gAnglePyramid + 0.1f;
	if (gAnglePyramid >= 360.0f)
		gAnglePyramid = 0.0f;*/
}

-(void)drawView
{

	static float gAnglePyramid = 0.0f;
	static float gAngleCube = 0.0f;

	lightAmbient[0] = 0.0f;
	lightAmbient[1] = 0.0f;
	lightAmbient[2] = 0.0f;
	lightAmbient[3] = 1.0f;
	lightDiffuse[0] = 1.0f;
	lightDiffuse[1] = 1.0f;
	lightDiffuse[2] = 1.0f;
	lightDiffuse[3] = 1.0f;
	lightSpecular[0] = 1.0f;
	lightSpecular[1] = 1.0f;
	lightSpecular[2] = 1.0f;
	lightSpecular[3] = 1.0f;
	lightPosition[0] = 100.0f;
	lightPosition[1] = 100.0f;
	lightPosition[2] = 100.0f;
	lightPosition[3] = 1.0f;
	

	material_ambient[0] = 0.0f;
	material_ambient[1] = 0.0f;
	material_ambient[2] = 0.0f;
	material_ambient[3] = 1.0f;
	material_diffuse[0] = 1.0f;
	material_diffuse[1] = 1.0f;
	material_diffuse[2] = 1.0f;
	material_diffuse[3] = 1.0f;
	material_specular[0] = 1.0f;
	material_specular[1] = 1.0f;
	material_specular[2] = 1.0f;
	material_specular[3] = 1.0f;
	material_shininess = 50.0f;

	[[self openGLContext]makeCurrentContext];

	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	if (gbFragmentLight == true)
	{
		glUniform1i(gFKeyPressedUniform, 1);

		glUniform3fv(gLdUniform, 1, lightDiffuse);//white
		glUniform3fv(gLaUniform, 1, lightAmbient);
		glUniform3fv(gLsUniform, 1, lightSpecular);
		glUniform4fv(gLightPositionUniform, 1, lightPosition);

		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

		float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };
		
	}
	else
	{
		glUniform1i(gFKeyPressedUniform, 0);
	}
    if (gbVertexLight == true)
    {
        glUniform1i(gVKeyPressedUniform, 1);
        
        glUniform3fv(gLdUniform, 1, lightDiffuse);//white
        glUniform3fv(gLaUniform, 1, lightAmbient);
        glUniform3fv(gLsUniform, 1, lightSpecular);
        glUniform4fv(gLightPositionUniform, 1, lightPosition);
        
        glUniform3fv(gKdUniform, 1, material_diffuse);
        glUniform3fv(gKaUniform, 1, material_ambient);
        glUniform3fv(gKsUniform, 1, material_specular);
        glUniform1f(gMaterialShininessUniform, material_shininess);
        
        float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };
        
    }
    else
    {
        glUniform1i(gVKeyPressedUniform, 0);
    }

	vmath::mat4 modelMatrix = vmath::mat4::identity();
	vmath::mat4 viewMatrix = vmath::mat4::identity();

	//vmath::mat4 rotationMatrix = vmath::mat4::identity();
	
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f); 
	//rotationMatrix = vmath::rotate(gAnglePyramid, 0.0f, 1.0f, 0.0f);
	
	//modelViewMatrix = modelViewMatrix * rotationMatrix;
	
	//modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

	//glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);
	
	//Cube Block
	/*modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();
	vmath::mat4 scaleMatrix = vmath::mat4::identity();
	
	modelViewMatrix = vmath::translate(1.5f, 0.0f, -6.0f);
	scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);

	rotationMatrix = vmath::rotate(gAngleCube, 1.0f, 0.0f, 0.0f);
	modelViewMatrix = modelViewMatrix * scaleMatrix;
	modelViewMatrix = modelViewMatrix * rotationMatrix;
	
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(gVao_Cube);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	
	glBindVertexArray(0);*/

	glUseProgram(0);
	
	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	gAngleCube = gAngleCube + 0.1f;
	gAnglePyramid = gAnglePyramid + 0.1f;
}

-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return (YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27://Esc
			/*[self release];
			[NSApp terminate:self];
			break;*/
            [[self window]toggleFullScreen:self];
            break;
		case 'F':
		case 'f':
            if (bIsFKeyPressed == false)
            {
                gbFragmentLight = true;
                bIsFKeyPressed = true;
                gbVertexLight = false;
                bIsVKeyPressed = false;
            }
            else
            {
                gbFragmentLight = false;
                bIsFKeyPressed = false;
            }
            break;
		case 'L':
		case 'l':
        case 'v':
        case 'V':
			if (bIsVKeyPressed == false)
			{
                gbVertexLight = true;
                bIsVKeyPressed = true;
                gbFragmentLight = false;
                bIsFKeyPressed = false;			}
			else
			{
                gbVertexLight = false;
                bIsVKeyPressed = false;
            }
			break;
		default:
			break;
	}	
}

-(void)mouseDown:(NSEvent *)theEvent
{
}

-(void)mouseDragged:(NSEvent *)theEvent
{
}

-(void)rightMouseDown:(NSEvent *)theEvent
{

}

-(void)dealloc
{

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}
	
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallBack(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime,
	CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
	CVReturn result = [ (GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return (result);
}
