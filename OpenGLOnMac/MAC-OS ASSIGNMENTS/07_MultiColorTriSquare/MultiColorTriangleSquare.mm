//headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import<QuartzCore/CVDisplayLink.h>
#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import "vmath.h"

enum
{
	MALATI_ATTRIBUTE_VERTEX=0,
	MALATI_ATTRIBUTE_COLOR, 
	MALATI_ATTRIBUTE_NORMAL,
	MALATI_ATTRIBUTE_TEXTURE0,
};

//'c' style global function declaration
CVReturn MyDisplayLinkCallBack(CVDisplayLinkRef , const CVTimeStamp *, const CVTimeStamp *,
	CVOptionFlags , CVOptionFlags *, void *);

FILE *gpFile = NULL;

//interface declarations
@interface AppDeligate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

//Entry Point Function
int main(int argc, const char * argv[])
{
	//code
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];	

	[NSApp setDelegate : [[AppDeligate alloc] init]];

	[NSApp run];

	[pPool release];

	return (0);
}

//interface implementation
@implementation AppDeligate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching : (NSNotification *)aNotification	//WM_CREATE
{
	//code
	//window
	
	//for LOG file
	 NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
		printf("Can Not Create Log File.\nExitting...\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile, "Program Is Started Successfully...\n");
	
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	//create simple window
	window = [[NSWindow alloc] initWithContentRect:win_rect 
				styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
				 backing:NSBackingStoreBuffered
				 defer:NO];

	[window setTitle:@"macOS Window"];
	[window center];

	NSLog(@"%@\n", window);
	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];

}

-(void)applicationWillTerminate:(NSNotification *)notification //WM_CLOSE OR WM_DESTROY
{
	//code
	fprintf(gpFile, "Program Is Terminate Successfully\n");
	
	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

-(void)windowWillClose:(NSNotification *)notification
{
	//code
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];
	
	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displayLink;
	
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;
	
	GLuint gVao_Triangle;
	GLuint gVao_Square;
	GLuint gVbo_Triangle_Position;
	GLuint gVbo_Square_Position;

	GLuint gVbo_Triangle_Color;
	GLuint gVbo_Square_Color;

	//GLuint gVbo_color;
	GLuint gMVPUniform;
	
	vmath::mat4 gPerspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame;
{
	self = [super initWithFrame:frame];
	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery, 
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat == nil)
		{
			fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available. Exitting..");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];
				
	}
	return (self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version	: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	
	//***VERTEX SHADER***
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertextShaderSourceCode =
		"#version 410" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)"\
		"{"\
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertextShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				//uninitialize();
				//exit(0);
				[self release];
				[NSApp terminate:self];
			}
		}
	}
	//FRAGMENT SHADER
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;
	
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
		"#version 410"\
		"\n" \
		"in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)"\
		"{"\
		"FragColor = out_color;" \
		"}";
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	glCompileShader(gFragmentShaderObject);
	
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				//uninitialize();
				//exit(0);
				[self release];
				[NSApp terminate:self];
			}
		}
	}
	//Shader Program

	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, MALATI_ATTRIBUTE_COLOR, "vColor");
	
	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				//uninitialize();
				//exit(0);
				[self release];
				[NSApp terminate:self];
			}
		}
	}

	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	const GLfloat triangleVertices[] =
	{	0.0f, 1.0f, 0.0f, // Apex
		-1.0f, -1.0f, 0.0f, // left bottom
		1.0f, -1.0f, 0.0f // right bottom
	};

	const GLfloat triangleColors[] =
	{
		1.0f, 0.0f, 0.0f,//r
		0.0f, 1.0f, 0.0f,//g
		0.0f, 0.0f, 1.0f//b
	};
	glGenVertexArrays(1, &gVao_Triangle);
	glBindVertexArray(gVao_Triangle);

	glGenBuffers(1, &gVbo_Triangle_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Triangle_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	//color
	glGenBuffers(1, &gVbo_Triangle_Color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Triangle_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColors), triangleColors, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_COLOR);


	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	//Square
	const GLfloat squareVertices[] =
	{
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		1.0f, 1.0f, 0.0f 
	};

	glGenVertexArrays(1, &gVao_Square);
	glBindVertexArray(gVao_Square);

	glGenBuffers(1, &gVbo_Square_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Square_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glVertexAttrib3f(MALATI_ATTRIBUTE_COLOR, 0.39f, 0.58f, 0.9294f);
	glEnableVertexAttribArray(MALATI_ATTRIBUTE_COLOR);
	//glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);

	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // blue

	gPerspectiveProjectionMatrix = vmath::mat4::identity();
	

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallBack, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height= rect.size.height;

	if(height == 0)
	height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	/*if(width <= height)
		gOrthographicProjectionMatrix = vmath::ortho(-100.0f, 100.0f, (-100.0f * (height / width)), (100.0f * (height / width)), -100.0f, 100.0f);
	else
		gOrthographicProjectionMatrix = vmath::ortho((-100.0f * (width / height)), (100.0f * (width / height)), -100.0f, 100.0f, -100.0f, 100.0f);
	*/
	
	gPerspectiveProjectionMatrix =  vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height,
		0.1f, 100.0f); 
	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

}

-(void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
}

-(void)drawView
{

	[[self openGLContext]makeCurrentContext];

	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	vmath::mat4 modelViewMatrix = vmath::mat4::identity();
	vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();

	modelViewMatrix = vmath::translate(-1.5f, 0.0f, -6.0f); 
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(gVao_Triangle);

	glDrawArrays(GL_TRIANGLES, 0, 3);

	//glBindVertexArray(0);
	
	//Square Block
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	modelViewMatrix = vmath::translate(1.5f, 0.0f, -6.0f);
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(gVao_Square);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //there is no QUAD in PP(4  rows)

	//glBindVertexArray(0);

	glUseProgram(0);
	
	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return (YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27://Esc
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
		default:
			break;
	}	
}

-(void)mouseDown:(NSEvent *)theEvent
{
}

-(void)mouseDragged:(NSEvent *)theEvent
{
}

-(void)rightMouseDown:(NSEvent *)theEvent
{

}

-(void)dealloc
{

	if (gVao_Triangle)
	{
		glDeleteVertexArrays(1, &gVao_Triangle);
		gVao_Triangle = 0;
	}
	if (gVao_Square)
	{
		glDeleteVertexArrays(1, &gVao_Square);
		gVao_Square = 0;
	}
	if (gVbo_Triangle_Position)
	{
		glDeleteBuffers(1, &gVbo_Triangle_Position);
		gVbo_Triangle_Position = 0;
	}
	if (gVbo_Triangle_Color)
	{
		glDeleteBuffers(1, &gVbo_Triangle_Color);
		gVbo_Triangle_Color = 0;
	}

	if (gVbo_Square_Position)
	{
		glDeleteBuffers(1, &gVbo_Square_Position);
		gVbo_Square_Position = 0;
	}
	if (gVbo_Square_Color)
	{
		glDeleteBuffers(1, &gVbo_Square_Color);
		gVbo_Square_Color = 0;
	}

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallBack(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime,
	CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
	CVReturn result = [ (GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return (result);
}
