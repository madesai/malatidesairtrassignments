//global var
var canvas=null;
var gl=null; //context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	MPD_ATTRIBUTE_VERTEX:0,
	MPD_ATTRIBUTE_COLOR:1,
	MPD_ATTRIBUTE_NORMAL:2,
	MPD_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var light_ambient1=[0.0,0.0,0.0];
var light_ambient2=[0.0,0.0,0.0];
var light_ambient3=[0.0,0.0,0.0];

var light_diffuse1=[1.0,0.0,0.0]; //red
var light_diffuse2=[0.0,1.0,0.0]; //green
var light_diffuse3=[0.0,0.0,1.0]; //blue

var light_specular1=[1.0,0.0,0.0];
var light_specular2=[0.0,1.0,0.0];
var light_specular3=[0.0,0.0,1.0];

var light_position1=[100.0,100.0,100.0,1.0];
var light_position2=[-100.0,100.0,100.0,1.0];
var light_position3=[0.0,0.0,100.0,1.0];

var material_ambient= [0.0,0.0,0.0];
var material_diffuse= [1.0,1.0,1.0];
var material_specular= [1.0,1.0,1.0];
var material_shininess= 50.0;

var sphere = null;

var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;

var la1Uniform, ld1Uniform, ls1Uniform, lightPositionUniform1;
var la2Uniform, ld2Uniform, ls2Uniform, lightPositionUniform2;
var la3Uniform, ld3Uniform, ls3Uniform, lightPositionUniform3;

var kaUniform, kdUniform, ksUniform, materialShininessUniform;

var lKeyPressedUniform;
var bLKeyPressed = false;

var PerspectiveProjectionMatrix;//1st Change

//var myRotationMatrix1 = [];
//new Float32Array();
//var myRotationMatrix2 = [];
//new Float32Array();
//var myRotationMatrix3 = [];
//new Float32Array();

/*var triangleVertices = new Float32Array( [ 0.0, 1.0, 0.0, 
												-1.0, -1.0, 0.0,
												1.0, -1.0, 0.0
												]);*/
var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ;
	
var cancelAnimationFrame = 
	window.cancelAnimationFrame ||
	window.webkitCancelRequestAnimationFrame ||
	window.webkitCancelAnimationFrame ||
	window.mozCancelRequestAnimationFrame ||
	window.mozCancelAnimationFrame ||
	window.oCancelRequestAnimationFrame ||
	window.oCancelAnimationFrame ||
	window.msCancelRequestAnimationFrame ||
	window.msCancelAnimationFrame;

var angle_red = 0.0;


//onload fuction
function myMain()
{
	//get <canvas> element
	canvas = document.getElementById("MPD");
	if(!canvas)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	//print canvas width and height on console
	console.log("Canvas Width : " + canvas.width + " And Canvas Height : " + canvas.height);
	
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);//false:No Capture
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize();
	draw();
}

function init()
{
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	var vertexShaderSourceCode = 
			"#version 300 es" +
			"\n" +
			"in vec4 vPosition;" +
			"in vec3 vNormal;" +
			"uniform mat4 u_model_matrix;" +
			"uniform mat4 u_view_matrix;" +
			"uniform mat4 u_projection_matrix;"+
			"uniform mediump int u_LKeyPressed;"+
			"uniform vec4 u_light_position1;"+
			"uniform vec4 u_light_position2;"+
			"uniform vec4 u_light_position3;"+
			"out vec3 transformed_normals;"+
			"out vec3 light_direction1;"+
			"out vec3 light_direction2;"+
			"out vec3 light_direction3;"+
			"out vec3 viewer_vector;"+
			"void main(void)" +
			"{"+
			"if(u_LKeyPressed == 1)" +
			"{"+
			"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
			"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"+
			"light_direction1 = vec3(u_light_position1) - eye_coordinates.xyz;"+
			"light_direction2 = vec3(u_light_position2) - eye_coordinates.xyz;"+
			"light_direction3 = vec3(u_light_position3) - eye_coordinates.xyz;"+
			"viewer_vector = -eye_coordinates.xyz;"+
			"}"+
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"}";
			
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	var fragmentShaderSourceCode = 
			"#version 300 es" +
			"\n" +
			"precision highp float;"+
         "in vec3 transformed_normals;"+
         "in vec3 light_direction1;"+
		 "in vec3 light_direction2;"+
		 "in vec3 light_direction3;"+
         "in vec3 viewer_vector;"+
         "out vec4 FragColor;"+
         "uniform vec3 u_La1;"+
		 "uniform vec3 u_La2;"+
		 "uniform vec3 u_La3;"+
         "uniform vec3 u_Ld1;"+
		 "uniform vec3 u_Ld2;"+
		 "uniform vec3 u_Ld3;"+
         "uniform vec3 u_Ls1;"+
		 "uniform vec3 u_Ls2;"+
		 "uniform vec3 u_Ls3;"+
         "uniform vec3 u_Ka;"+
         "uniform vec3 u_Kd;"+
         "uniform vec3 u_Ks;"+
         "uniform float u_material_shininess;"+
         "uniform int u_LKeyPressed;"+
         "void main(void)"+
         "{"+
         "vec3 phong_ads_color1;"+
		 "vec3 phong_ads_color2;"+
		 "vec3 phong_ads_color3;"+
		 "vec3 phong_ads_color;"+
         "if(u_LKeyPressed==1)"+
         "{"+
         "vec3 normalized_transformed_normals1=normalize(transformed_normals);"+
         "vec3 normalized_light_direction1=normalize(light_direction1);"+
         "vec3 normalized_viewer_vector1=normalize(viewer_vector);"+
         "vec3 ambient1 = u_La1 * u_Ka;"+
         "float tn_dot_ld1 = max(dot(normalized_transformed_normals1, normalized_light_direction1),0.0);"+
         "vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;"+
         "vec3 reflection_vector1 = reflect(-normalized_light_direction1, normalized_transformed_normals1);"+
         "vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, normalized_viewer_vector1), 0.0), u_material_shininess);"+
         "phong_ads_color1=ambient1 + diffuse1 + specular1;"+
		 
		 "vec3 normalized_transformed_normals2=normalize(transformed_normals);"+
         "vec3 normalized_light_direction2=normalize(light_direction2);"+
         "vec3 normalized_viewer_vector2=normalize(viewer_vector);"+
         "vec3 ambient2 = u_La2 * u_Ka;"+
         "float tn_dot_ld2 = max(dot(normalized_transformed_normals2, normalized_light_direction2),0.0);"+
         "vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;"+
         "vec3 reflection_vector2 = reflect(-normalized_light_direction2, normalized_transformed_normals2);"+
         "vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, normalized_viewer_vector2), 0.0), u_material_shininess);"+
         "phong_ads_color2=ambient2 + diffuse2 + specular2;"+
		 
		 "vec3 normalized_transformed_normals3=normalize(transformed_normals);"+
         "vec3 normalized_light_direction3=normalize(light_direction3);"+
         "vec3 normalized_viewer_vector3=normalize(viewer_vector);"+
         "vec3 ambient3 = u_La3 * u_Ka;"+
         "float tn_dot_ld3 = max(dot(normalized_transformed_normals3, normalized_light_direction3),0.0);"+
         "vec3 diffuse3 = u_Ld3 * u_Kd * tn_dot_ld3;"+
         "vec3 reflection_vector3 = reflect(-normalized_light_direction3, normalized_transformed_normals3);"+
         "vec3 specular3 = u_Ls3 * u_Ks * pow(max(dot(reflection_vector3, normalized_viewer_vector3), 0.0), u_material_shininess);"+
         "phong_ads_color3=ambient3 + diffuse3 + specular3;"+
		 "phong_ads_color = phong_ads_color1 + phong_ads_color2 +phong_ads_color3;"+
         "}"+
         "else"+
         "{"+
         "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
         "}"+
         "FragColor = vec4(phong_ads_color, 1.0);"+
         "}"

			
			
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.MPD_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.MPD_ATTRIBUTE_NORMAL, "vNormal");
	
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
	
	lKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_LKeyPressed");
	
	la1Uniform = gl.getUniformLocation(shaderProgramObject, "u_La1");
	la2Uniform = gl.getUniformLocation(shaderProgramObject, "u_La2");
	la3Uniform = gl.getUniformLocation(shaderProgramObject, "u_La3");
	ld1Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld1");
	ld2Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld2");
	ld3Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld3");
	ls1Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls1");
	ls2Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls2");
	ls3Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls3");
	kaUniform = gl.getUniformLocation(shaderProgramObject, "u_Ka");
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
	ksUniform = gl.getUniformLocation(shaderProgramObject, "u_Ks");
	
	lightPositionUniform1 = gl.getUniformLocation(shaderProgramObject, "u_light_position1");
	lightPositionUniform2 = gl.getUniformLocation(shaderProgramObject, "u_light_position2");
	lightPositionUniform3 = gl.getUniformLocation(shaderProgramObject, "u_light_position3");
	materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_material_shininess");
	
	sphere = new Mesh();
	makeSphere(sphere, 2.0, 30, 30);
	
	
	gl.clearColor(0.0,0.0,0.0,1.0);
	gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
	gl.enable(gl.CULL_FACE);
	PerspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	
	gl.viewport(0,0,canvas.width, canvas.height);
	
	//if(canvas.width <= canvas.height)
	//	mat4.ortho(orthographicProjectionMatrix, -100.0, 100.0, (-100.0 * (canvas.height / canvas.width)), 
	//			(100.0 * (canvas.height / canvas.width)), -100.0, 100.0);
		
	//else
	//	mat4.ortho(orthographicProjectionMatrix, (-100.0 * (canvas.width/canvas.height)) , (100.0*(canvas.width/canvas.height)), -100.0 , 
		//		100.0 , -100.0, 100.0);
		mat4.perspective(PerspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}



function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	if(bLKeyPressed == true)
	{
		gl.uniform1i(lKeyPressedUniform, 1);
		
		gl.uniform3fv(la1Uniform, light_ambient1);
		gl.uniform3fv(la2Uniform, light_ambient2);
		gl.uniform3fv(la3Uniform, light_ambient3);
		gl.uniform3fv(ls1Uniform, light_specular1);
		gl.uniform3fv(ls2Uniform, light_specular2);
		gl.uniform3fv(ls3Uniform, light_specular3);
		gl.uniform3fv(ld1Uniform, light_diffuse1);
		gl.uniform3fv(ld2Uniform, light_diffuse2);
		gl.uniform3fv(ld3Uniform, light_diffuse3);
		
		gl.uniform3fv(kaUniform, material_ambient);
		gl.uniform3fv(ksUniform, material_specular);
		gl.uniform3fv(kdUniform, material_diffuse);
		
		var modelViewMatrix1 = mat4.create();
		var modelViewMatrix2 = mat4.create();
		var modelViewMatrix3 = mat4.create();
		
		//var lightPosition = [0.0, 0.0, 2.0, 1.0];
		/*myRotationMatrix1[0] = 0.0;
		myRotationMatrix1[1] = 100.0 * Math.cos(2 * VDG_PI * angle_red);
		myRotationMatrix1[2] = 100.0 * Math.sin(2 * VDG_PI * angle_red);
		myRotationMatrix1[3] = 1.0;*/
        
		var myRotationMatrix1 = [0.0, 100.0 * Math.cos(2 * VDG_PI * degToRad(angle_red)), 100.0 * Math.sin(2 * VDG_PI * degToRad(angle_red)), 1.0];
		//mat4.rotateX(myRotationMatrix1, myRotationMatrix1 , degToRad(angle_red));
		//mat4.rotateX(modelViewMatrix1, modelViewMatrix1 , degToRad(angle_red));
		gl.uniform4fv(lightPositionUniform1, myRotationMatrix1);
		
		/*myRotationMatrix2[0] = 100.0 * Math.cos(2 * VDG_PI * angle_red);
		myRotationMatrix2[1] = 0.0;
		myRotationMatrix2[2] = 100.0 * Math.sin(2 * VDG_PI * angle_red);
		myRotationMatrix2[3] = 1.0;
		*/
		var myRotationMatrix2 = [100.0 * Math.cos(2 * VDG_PI * degToRad(angle_red)), 0.0, 100.0 * Math.sin(2 * VDG_PI * degToRad(angle_red)), 1.0];

		//mat4.rotateX(myRotationMatrix2, myRotationMatrix2 , degToRad(angle_red));
		//mat4.rotateX(modelViewMatrix2, modelViewMatrix2 , degToRad(angle_red));
		gl.uniform4fv(lightPositionUniform2, myRotationMatrix2);
		
	/*	myRotationMatrix3[0] = 100.0 * Math.sin(2 * VDG_PI * angle_red);
		myRotationMatrix3[1] = 100.0 * Math.cos(2 * VDG_PI * angle_red);
		myRotationMatrix3[2] = -2.0;
		myRotationMatrix3[3] = 1.0;*/

		var myRotationMatrix3 = [100.0 * Math.sin(2 * VDG_PI * degToRad(angle_red)), 100.0 * Math.cos(2 * VDG_PI * degToRad(angle_red)), -2.0, 1.0];

		//mat4.rotateX(myRotationMatrix3, myRotationMatrix3 , degToRad(angle_red));
		//mat4.rotateX(modelViewMatrix3, modelViewMatrix3 , degToRad(angle_red));
		gl.uniform4fv(lightPositionUniform3, myRotationMatrix3);
		gl.uniform1f(materialShininessUniform, material_shininess);
	}
	else
	{
			gl.uniform1i(lKeyPressedUniform, 0);
	}
	
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	mat4.translate(modelMatrix,modelMatrix, [0.0, 0.0, -6.0]);
	
	/*mat4.rotateX(modelViewMatrix, modelViewMatrix , degToRad(angleCube));
	mat4.rotateY(modelViewMatrix, modelViewMatrix , degToRad(angleCube));
	mat4.rotateZ(modelViewMatrix, modelViewMatrix , degToRad(angleCube));
	*/
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, PerspectiveProjectionMatrix);
	
	sphere.draw();
	
	gl.useProgram(null);
	update();
	requestAnimationFrame(draw, canvas);
}

/*function degreeToRad(angle)
{
	return (angle * Math.PI/180.0);
}*/
function update()
{
	angle_red = angle_red +1.0;
	if(angle_red >= 360.0)
		angle_red -= 360.0;
	/*
	anglePyramid = anglePyramid +2.0;
	if(anglePyramid >= 360.0)
		anglePyramid -= 360.0;*/
}
function toggleFullScreen()
{
	var fullscreen_element = 
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;
	
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		
		bFullscreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullscreen = false;
	}
}	
function keyDown(event)
{
	//alert ("A Key is Pressed");
	switch(event.keyCode)
	{
		case 27:
			uninitialize();
			window.close();
			break;
		 case 76: // for 'L' or 'l'
            if(bLKeyPressed==false)
                bLKeyPressed=true;
            else
                bLKeyPressed=false;
            break;
		case 70:
			toggleFullScreen();
	//		drawText("Hello World !!!");
			break;
		default:
			
			break;
	}
}

function mouseDown(event)
{
	//alert("Mouse is clicked");
}

function uninitialize()
{

	if(sphere)
    {
        sphere.deallocate();
        sphere=null;
    }
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI / 180);
}
