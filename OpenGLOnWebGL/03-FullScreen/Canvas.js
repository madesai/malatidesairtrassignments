//global var
var canvas=null;
var context=null;

//onload fuction
function myMain()
{
	//get <canvas> element
	canvas = document.getElementById("MPD");
	if(!canvas)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");
	
	//print canvas width and height on console
	console.log("Canvas Width : " + canvas.width + " And Canvas Height : " + canvas.height);
	
	//get 2D context
	context = canvas.getContext("2d");
	if(!context)
		console.log("Obtaining 2D Context Failed\n");
	else
		console.log("Obtaining 2D Context Succeeded\n");
	
	//fill canvas with black colorDepth
	context.fillStyle="black";
	context.fillRect(0,0,canvas.width,canvas.height);
	
	drawText("Hello World !!!");
	/*
	//center the text
	context.textAlign = "center";
	context.textBaseline = "middle";
	
	var str = "Hello World !!! \n Along with KeyDown And MouseDown Event.\n";
	
	context.font="48px sans-serif";
	
	//text color
	context.fillStyle="white";
	
	context.fillText(str, canvas.width/2, canvas.height/2);
	
	*/
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);//false:No Capture
	
}

function drawText(text)
{
	//center the text
	context.textAlign = "center";
	context.textBaseline = "middle";
	
//	var str = "Hello World !!! \n Along with KeyDown And MouseDown Event.\n";
	
	context.font="48px sans-serif";
	
	//text color
	context.fillStyle="white";
	
	context.fillText(text, canvas.width/2, canvas.height/2);
	
}

function toggleFullScreen()
{
	var fullscreen_element = 
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;
		
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
	}
}	
function keyDown(event)
{
	//alert ("A Key is Pressed");
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			drawText("Hello World !!!");
			break;
	}
}

function mouseDown(event)
{
	//alert("Mouse is clicked");
}