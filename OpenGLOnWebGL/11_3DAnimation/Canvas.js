//global var
var canvas=null;
var gl=null; //context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	MPD_ATTRIBUTE_VERTEX:0,
	MPD_ATTRIBUTE_COLOR:1,
	MPD_ATTRIBUTE_NORMAL:2,
	MPD_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_Pyramid;
var vao_Cube;

var vbo_Pyramid_position;
var vbo_Cube_position;
var vbo_Pyramid_color;
var vbo_Cube_color;
var mvpUniform;

var PerspectiveProjectionMatrix;//1st Change

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ;
	
var cancelAnimationFrame = 
	window.cancelAnimationFrame ||
	window.webkitCancelRequestAnimationFrame ||
	window.webkitCancelAnimationFrame ||
	window.mozCancelRequestAnimationFrame ||
	window.mozCancelAnimationFrame ||
	window.oCancelRequestAnimationFrame ||
	window.oCancelAnimationFrame ||
	window.msCancelRequestAnimationFrame ||
	window.msCancelAnimationFrame;

var anglePyramid = 0.0;
var angleCube = 0.0;	
//onload fuction
function myMain()
{
	//get <canvas> element
	canvas = document.getElementById("MPD");
	if(!canvas)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	//print canvas width and height on console
	console.log("Canvas Width : " + canvas.width + " And Canvas Height : " + canvas.height);
	/*
	//get 2D context
	context = canvas.getContext("2d");
	if(!context)
		console.log("Obtaining 2D Context Failed\n");
	else
		console.log("Obtaining 2D Context Succeeded\n");
	
	//fill canvas with black colorDepth
	context.fillStyle="black";
	context.fillRect(0,0,canvas.width,canvas.height);
	
	drawText("Hello World !!!");*/
	/*
	//center the text
	context.textAlign = "center";
	context.textBaseline = "middle";
	
	var str = "Hello World !!! \n Along with KeyDown And MouseDown Event.\n";
	
	context.font="48px sans-serif";
	
	//text color
	context.fillStyle="white";
	
	context.fillText(str, canvas.width/2, canvas.height/2);
	
	*/
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);//false:No Capture
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize();
	draw();
}

function init()
{
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	var vertexShaderSourceCode = 
			"#version 300 es" +
			"\n" +
			"in vec4 vPosition;" +
			"in vec4 vColor;" +
			"uniform mat4 u_mvp_matrix;" +
			"out vec4 out_color;"+
			"void main(void)" +
			"{"+
			"gl_Position = u_mvp_matrix * vPosition;" +
			"out_color = vColor;"+
			"}";
			
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	var fragmentShaderSourceCode = 
			"#version 300 es" +
			"\n" +
			"precision highp float;" +
			"out vec4 FragColor;" +
			"in vec4 out_color;" +
			"void main(void)" +
			"{"+
			"FragColor = out_color;" +
			"}"
			
			
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.MPD_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.MPD_ATTRIBUTE_COLOR, "vColor");
	
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	
	var PyramidVertices = new Float32Array( [ 0.0, 1.0, 0.0, 
												-1.0, -1.0, 1.0,
												1.0, -1.0, 1.0,
												
												0.0,1.0,0.0,
												1.0,-1.0,1.0,
												1.0,-1.0,-1.0,
												
												0.0,1.0,0.0,
												1.0,-1.0,-1.0,
												-1.0,-1.0,-1.0,
												
												0.0,1.0,0.0,
												-1.0,-1.0,-1.0,
												-1.0,-1.0,1.0
												]);
												
	var PyramidColors = new Float32Array( [ 1.0, 0.0, 0.0, 
												0.0, 1.0, 0.0,
												0.0, 0.0, 1.0,
												
												1.0, 0.0, 0.0, 
												0.0, 0.0, 1.0,
												0.0, 1.0, 0.0,
												
												1.0, 0.0, 0.0,
												0.0, 1.0, 0.0,
												0.0, 0.0, 1.0,
												
												1.0, 0.0, 0.0, 
												0.0, 0.0, 1.0,
												0.0, 1.0, 0.0
												]);
	vao_Pyramid = gl.createVertexArray();
	gl.bindVertexArray(vao_Pyramid);
	
	vbo_Pyramid_position= gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Pyramid_position);
	gl.bufferData(gl.ARRAY_BUFFER, PyramidVertices, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.MPD_ATTRIBUTE_VERTEX, 
							3, gl.FLOAT,
							false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.MPD_ATTRIBUTE_VERTEX);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	//color
	vbo_Pyramid_color= gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Pyramid_color);
	gl.bufferData(gl.ARRAY_BUFFER, PyramidColors, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.MPD_ATTRIBUTE_COLOR, 
							3, gl.FLOAT,
							false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.MPD_ATTRIBUTE_COLOR);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
	var CubeVertices = new Float32Array( [ 1.0, 1.0, -1.0,
												-1.0, 1.0, -1.0,
												-1.0, 1.0, 1.0,
												1.0, 1.0, 1.0,
												
												1.0, -1.0, -1.0,
												-1.0, -1.0, -1.0,
												-1.0, -1.0, 1.0,
												1.0,-1.0,1.0,
												
												1.0,1.0,1.0,
												-1.0,1.0,1.0,
												-1.0,-1.0,1.0,
												1.0,-1.0,1.0,
												
												1.0,1.0,-1.0,
												-1.0,1.0,-1.0,
												-1.0,-1.0,-1.0,
												1.0,-1.0,-1.0,
												
												1.0,1.0,-1.0,
												1.0,1.0,1.0,
												1.0,-1.0,1.0,
												1.0,-1.0,-1.0,
												
												-1.0,1.0,1.0,
												-1.0,1.0,-1.0,
												-1.0,-1.0,-1.0,
												-1.0,1.0,1.0
												
												]);
												
	vao_Cube = gl.createVertexArray();
	gl.bindVertexArray(vao_Cube);
	
	vbo_Cube_position= gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Cube_position);
	gl.bufferData(gl.ARRAY_BUFFER, CubeVertices, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.MPD_ATTRIBUTE_VERTEX, 
							3, gl.FLOAT,
							false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.MPD_ATTRIBUTE_VERTEX);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	var CubeColors = new Float32Array( [

		1.0, 0.0, 0.0, //top
		0.0, 1.0, 0.0, //bottom
		0.0, 0.0, 1.0, //front
		0.0, 1.0, 1.0, //back

		0.0, 1.0, 0.0, //bottom
		0.0, 0.0, 1.0, //front
		0.0, 1.0, 1.0, //back
		1.0, 0.0, 1.0, //right

		0.0, 0.0, 1.0, //front
		0.0, 1.0, 1.0, //back
		1.0, 0.0, 1.0, //right
		1.0, 1.0, 0.0, //left

		0.0, 1.0, 1.0, //back
		1.0, 0.0, 1.0, //right
		1.0, 1.0, 0.0, //left
		1.0, 0.0, 0.0, //top

		1.0, 0.0, 1.0, //right
		1.0, 1.0, 0.0, //left
		1.0, 0.0, 0.0, //top
		0.0, 1.0, 0.0, //bottom

		1.0, 1.0, 0.0, //left
		1.0, 0.0, 0.0, //top
		0.0, 1.0, 0.0, //bottom
		0.0, 0.0, 1.0 //front

	]);

	
	vbo_Cube_color= gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Cube_color);
	gl.bufferData(gl.ARRAY_BUFFER, CubeColors, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.MPD_ATTRIBUTE_COLOR, 
							3, gl.FLOAT,
							false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.MPD_ATTRIBUTE_COLOR);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
	//gl.vertexAttrib3f(WebGLMacros.MPD_ATTRIBUTE_COLOR, 0.39, 0.58, 0.9294);
	//gl.enableVertexAttribArray(WebGLMacros.MPD_ATTRIBUTE_COLOR);
	
	
	gl.clearColor(0.0,0.0,0.0,1.0);
	gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
	PerspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	
	gl.viewport(0,0,canvas.width, canvas.height);
	
	//if(canvas.width <= canvas.height)
	//	mat4.ortho(orthographicProjectionMatrix, -100.0, 100.0, (-100.0 * (canvas.height / canvas.width)), 
	//			(100.0 * (canvas.height / canvas.width)), -100.0, 100.0);
		
	//else
	//	mat4.ortho(orthographicProjectionMatrix, (-100.0 * (canvas.width/canvas.height)) , (100.0*(canvas.width/canvas.height)), -100.0 , 
		//		100.0 , -100.0, 100.0);
		mat4.perspective(PerspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

/*function drawText(text)
{
	//center the text
	context.textAlign = "center";
	context.textBaseline = "middle";
	
//	var str = "Hello World !!! \n Along with KeyDown And MouseDown Event.\n";
	
	context.font="48px sans-serif";
	
	//text color
	context.fillStyle="white";
	
	context.fillText(text, canvas.width/2, canvas.height/2);
	
}*/

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	mat4.identity(modelViewProjectionMatrix);
	var translationVector = vec3.create();
	vec3.set(translationVector, -1.5, 0, -6);
	//mat4.translate(modelViewMatrix, modelViewMatrix,[-1.5,0.0,-6.0]);
	mat4.translate(modelViewMatrix, modelViewMatrix, translationVector);
	
	
	var angleInRadians1 = anglePyramid * Math.PI /180;
	//mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians1, [1,0,0]);
mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians1, [0,1,0]);
//mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians1, [0,0,1]);
	//mat4.rotateX(modelViewMatrix, modelViewMatrix, anglePyramid);
	var modelViewProjectionMatrix = mat4.create();
	
	mat4.multiply(modelViewProjectionMatrix, PerspectiveProjectionMatrix, modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_Pyramid);
	gl.drawArrays(gl.TRIANGLES, 0, 12);
	gl.bindVertexArray(null);
	
	modelViewMatrix = mat4.create();
	modelViewProjectionMatrix = mat4.create();
	
	
	mat4.translate(modelViewMatrix, modelViewMatrix,[1.5,0.0,-6.0]);
	
mat4.scale(modelViewMatrix, modelViewMatrix, [0.75, 0.75, 0.75]);
	var angleInRadians = angleCube * Math.PI /180;
	mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians, [1,0,0]);
	mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians, [0,1,0]);
	mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians, [0,0,1]);
	//mat4.rotateX(modelViewMatrix, modelViewMatrix, degreeToRad(angleCube));
	mat4.multiply(modelViewProjectionMatrix, PerspectiveProjectionMatrix, modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_Cube);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	gl.bindVertexArray(null);
	
	gl.useProgram(null);
	update();
	requestAnimationFrame(draw, canvas);
}

/*function degreeToRad(angle)
{
	return (angle * Math.PI/180.0);
}*/
function update()
{
	angleCube = angleCube +1.0;
	if(angleCube >= 360.0)
		angleCube = 0.0;
	
	anglePyramid = anglePyramid +1.0;
	if(anglePyramid >= 360.0)
		anglePyramid = 0.0;
}
function toggleFullScreen()
{
	var fullscreen_element = 
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;
	
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		
		bFullscreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullscreen = false;
	}
}	
function keyDown(event)
{
	//alert ("A Key is Pressed");
	switch(event.keyCode)
	{
		case 27:
			uninitialize();
			window.close();
			break;
		case 70:
			toggleFullScreen();
	//		drawText("Hello World !!!");
			break;
	}
}

function mouseDown(event)
{
	//alert("Mouse is clicked");
}

function uninitialize()
{
	if(vao_Pyramid)
	{
		gl.deleteVertexArray(vao_Pyramid);
		vao_Pyramid = null;
	}
	if(vbo_Pyramid_position)
	{
		gl.deleteBuffer(vbo_Pyramid_position);
		vbo_Pyramid_position = null;
	}
	if(vbo_Pyramid_color)
	{
		gl.deleteBuffer(vbo_Pyramid_color);
		vbo_Pyramid_color = null;
	}
	if(vao_Cube)
	{
		gl.deleteVertexArray(vao_Cube);
		vao_Cube = null;
	}
	if(vbo_Cube_position)
	{
		gl.deleteBuffer(vbo_Cube_position);
		vbo_Cube_position = null;
	}
	if(vbo_Cube_color)
	{
		gl.deleteBuffer(vbo_Cube_color);
		vbo_Cube_color = null;
	}
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gldeleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gldeleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gldeleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}