//global var
var canvas=null;
var gl=null; //context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	MPD_ATTRIBUTE_VERTEX:0,
	MPD_ATTRIBUTE_COLOR:1,
	MPD_ATTRIBUTE_NORMAL:2,
	MPD_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_Pyramid;
var vao_Cube;

var vbo_Pyramid_position;
var vbo_Cube_position;
var vbo_Pyramid_texture;
var vbo_Cube_texture;
var mvpUniform;

var PerspectiveProjectionMatrix;//1st Change

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ;
	
var cancelAnimationFrame = 
	window.cancelAnimationFrame ||
	window.webkitCancelRequestAnimationFrame ||
	window.webkitCancelAnimationFrame ||
	window.mozCancelRequestAnimationFrame ||
	window.mozCancelAnimationFrame ||
	window.oCancelRequestAnimationFrame ||
	window.oCancelAnimationFrame ||
	window.msCancelRequestAnimationFrame ||
	window.msCancelAnimationFrame;

var anglePyramid = 0.0;
var angleCube = 0.0;	

var Texture_sampler_uniform;
var Texture_Kundali =0;
var Texture_Stone = 0;
//onload fuction
function myMain()
{
	//get <canvas> element
	canvas = document.getElementById("MPD");
	if(!canvas)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	//print canvas width and height on console
	console.log("Canvas Width : " + canvas.width + " And Canvas Height : " + canvas.height);
	/*
	//get 2D context
	context = canvas.getContext("2d");
	if(!context)
		console.log("Obtaining 2D Context Failed\n");
	else
		console.log("Obtaining 2D Context Succeeded\n");
	
	//fill canvas with black colorDepth
	context.fillStyle="black";
	context.fillRect(0,0,canvas.width,canvas.height);
	
	drawText("Hello World !!!");*/
	/*
	//center the text
	context.textAlign = "center";
	context.textBaseline = "middle";
	
	var str = "Hello World !!! \n Along with KeyDown And MouseDown Event.\n";
	
	context.font="48px sans-serif";
	
	//text color
	context.fillStyle="white";
	
	context.fillText(str, canvas.width/2, canvas.height/2);
	
	*/
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);//false:No Capture
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize();
	draw();
}

function init()
{
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	var vertexShaderSourceCode = 
			"#version 300 es" +
			"\n" +
			"in vec4 vPosition;" +
			"in vec2 vTexture0_Coord;" +
			"out vec2 out_texture0_coord;"+
			"uniform mat4 u_mvp_matrix;" +
			"void main(void)" +
			"{"+
			"gl_Position = u_mvp_matrix * vPosition;" +
			"out_texture0_coord = vTexture0_Coord;"+
			"}";
			
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	var fragmentShaderSourceCode = 
			"#version 300 es" +
			"\n" +
			"precision highp float;" +
			"in vec2 out_texture0_coord;"+
			"uniform highp sampler2D u_texture0_sampler;"+
			"out vec4 FragColor;" +
			"void main(void)" +
			"{"+
			"FragColor = texture(u_texture0_sampler, out_texture0_coord);"+
			"}"
			
			
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.MPD_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.MPD_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
	
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//same like function present in windows and android as LoadGLTexture()
	Texture_Stone =gl.createTexture();
	Texture_Stone.image = new Image();
	Texture_Stone.image.src="stone.png";
	Texture_Stone.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D, Texture_Stone);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, Texture_Stone.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}
	
	Texture_Kundali =gl.createTexture();
	Texture_Kundali.image = new Image();
	var ImageSrc = "Vijay_Kundali.png";
	//Texture_Kundali.crossOrigin = "Anonymous";
	Texture_Kundali.image.src=ImageSrc;
	Texture_Kundali.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D, Texture_Kundali);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, Texture_Kundali.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}
	
	
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	uniform_texture0_sampler = gl.getUniformLocation(shaderProgramObject, "u_texture0_sampler");
	
	var PyramidVertices = new Float32Array( [ 
												0.0, 1.0, 0.0, 
												-1.0, -1.0, 1.0,
												1.0, -1.0, 1.0,
												
												0.0,1.0,0.0,
												1.0,-1.0,1.0,
												1.0,-1.0,-1.0,
												
												0.0,1.0,0.0,
												1.0,-1.0,-1.0,
												-1.0,-1.0,-1.0,
												
												0.0,1.0,0.0,
												-1.0,-1.0,-1.0,
												-1.0,-1.0,1.0
												]);
												
	var pyramidTexcoords=new Float32Array([
                                           0.5, 1.0, // front-top
                                           0.0, 0.0, // front-left
                                           1.0, 0.0, // front-right
                                           
                                           0.5, 1.0, // right-top
                                           1.0, 0.0, // right-left
                                           0.0, 0.0, // right-right
                                           
                                           0.5, 1.0, // back-top
                                           1.0, 0.0, // back-left
                                           0.0, 0.0, // back-right
                                           
                                           0.5, 1.0, // left-top
                                           0.0, 0.0, // left-left
                                           1.0, 0.0, // left-right
                                           ]);
	vao_Pyramid = gl.createVertexArray();
	gl.bindVertexArray(vao_Pyramid);
	
	vbo_Pyramid_position= gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Pyramid_position);
	gl.bufferData(gl.ARRAY_BUFFER, PyramidVertices, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.MPD_ATTRIBUTE_VERTEX, 
							3, gl.FLOAT,
							false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.MPD_ATTRIBUTE_VERTEX);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	//texture
	vbo_Pyramid_texture= gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Pyramid_texture);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidTexcoords, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.MPD_ATTRIBUTE_TEXTURE0, 
							2, gl.FLOAT,
							false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.MPD_ATTRIBUTE_TEXTURE0);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
	var CubeVertices = new Float32Array( [ 1.0, 1.0, -1.0,
												-1.0, 1.0, -1.0,
												-1.0, 1.0, 1.0,
												1.0, 1.0, 1.0,
												
												1.0, -1.0, -1.0,
												-1.0, -1.0, -1.0,
												-1.0, -1.0, 1.0,
												1.0,-1.0,1.0,
												
												1.0,1.0,1.0,
												-1.0,1.0,1.0,
												-1.0,-1.0,1.0,
												1.0,-1.0,1.0,
												
												1.0,1.0,-1.0,
												-1.0,1.0,-1.0,
												-1.0,-1.0,-1.0,
												1.0,-1.0,-1.0,
											
												-1.0,1.0,1.0,
												-1.0,1.0,-1.0,
												-1.0,-1.0,-1.0,
												-1.0,1.0,1.0,
											
												1.0,1.0,-1.0,
												1.0,1.0,1.0,
												1.0,-1.0,1.0,
												1.0,-1.0,-1.0,
												
												
												]);
	

	var cubeTexcoords=new Float32Array([
                                        0.0,0.0,
                                        1.0,0.0,
                                        1.0,1.0,
                                        0.0,1.0,
                                        
                                        0.0,0.0,
                                        1.0,0.0,
                                        1.0,1.0,
                                        0.0,1.0,
                                        
                                        0.0,0.0,
                                        1.0,0.0,
                                        1.0,1.0,
                                        0.0,1.0,
                                        
                                        0.0,0.0,
                                        1.0,0.0,
                                        1.0,1.0,
                                        0.0,1.0,
                                        
                                        0.0,0.0,
                                        1.0,0.0,
                                        1.0,1.0,
                                        0.0,1.0,
                                        
                                        0.0,0.0,
                                        1.0,0.0,
                                        1.0,1.0,
                                        0.0,1.0,
                                        ]);

	
	for(var i=0;i<72;i++)
    {
        if(CubeVertices[i]<0.0)
            CubeVertices[i]=CubeVertices[i]+0.25;
        else if(CubeVertices[i]>0.0)
            CubeVertices[i]=CubeVertices[i]-0.25;
        else
            CubeVertices[i]=CubeVertices[i]; // no change
    }
	
	vao_Cube = gl.createVertexArray();
	gl.bindVertexArray(vao_Cube);
	
	vbo_Cube_position= gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Cube_position);
	gl.bufferData(gl.ARRAY_BUFFER, CubeVertices, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.MPD_ATTRIBUTE_VERTEX, 
							3, gl.FLOAT,
							false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.MPD_ATTRIBUTE_VERTEX);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	vbo_Cube_texture= gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Cube_texture);
	gl.bufferData(gl.ARRAY_BUFFER, cubeTexcoords, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.MPD_ATTRIBUTE_TEXTURE0, 
							2, gl.FLOAT,
							false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.MPD_ATTRIBUTE_TEXTURE0);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
	//gl.vertexAttrib3f(WebGLMacros.MPD_ATTRIBUTE_COLOR, 0.39, 0.58, 0.9294);
	//gl.enableVertexAttribArray(WebGLMacros.MPD_ATTRIBUTE_COLOR);
	
	
	gl.clearColor(0.0,0.0,0.0,1.0);
	gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
	PerspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	
	gl.viewport(0,0,canvas.width, canvas.height);
	
	//if(canvas.width <= canvas.height)
	//	mat4.ortho(orthographicProjectionMatrix, -100.0, 100.0, (-100.0 * (canvas.height / canvas.width)), 
	//			(100.0 * (canvas.height / canvas.width)), -100.0, 100.0);
		
	//else
	//	mat4.ortho(orthographicProjectionMatrix, (-100.0 * (canvas.width/canvas.height)) , (100.0*(canvas.width/canvas.height)), -100.0 , 
		//		100.0 , -100.0, 100.0);
		mat4.perspective(PerspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

/*function drawText(text)
{
	//center the text
	context.textAlign = "center";
	context.textBaseline = "middle";
	
//	var str = "Hello World !!! \n Along with KeyDown And MouseDown Event.\n";
	
	context.font="48px sans-serif";
	
	//text color
	context.fillStyle="white";
	
	context.fillText(text, canvas.width/2, canvas.height/2);
	
}*/

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	//mat4.identity(modelViewProjectionMatrix);
	//var translationVector = vec3.create();
	//vec3.set(translationVector, -1.5, 0, -6);
	mat4.translate(modelViewMatrix, modelViewMatrix,[-1.5,0.0,-6.0]);
	//mat4.translate(modelViewMatrix, modelViewMatrix, translationVector);
	
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(anglePyramid));
	
	
	//var angleInRadians1 = anglePyramid * Math.PI /180;
	//mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians1, [1,0,0]);
//mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians1, [0,1,0]);
//mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians1, [0,0,1]);
	//mat4.rotateX(modelViewMatrix, modelViewMatrix, anglePyramid);
	//var modelViewProjectionMatrix = mat4.create();
	
	mat4.multiply(modelViewProjectionMatrix, PerspectiveProjectionMatrix, modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	//bind with texture
	gl.bindTexture(gl.TEXTURE_2D, Texture_Stone);
	gl.uniform1i(uniform_texture0_sampler, 0);
	
	gl.bindVertexArray(vao_Pyramid);
	
	gl.drawArrays(gl.TRIANGLES, 0, 12);
	gl.bindVertexArray(null);
	
	//cube
//	modelViewMatrix = mat4.create();
	//modelViewProjectionMatrix = mat4.create();
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);
	
	mat4.translate(modelViewMatrix, modelViewMatrix,[1.5,0.0,-5.0]);
	
//mat4.scale(modelViewMatrix, modelViewMatrix, [0.75, 0.75, 0.75]);
	//var angleInRadians = angleCube * Math.PI /180;
	mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angleCube));
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleCube));
	mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angleCube));
//	mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians, [0,1,0]);
	//mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians, [0,0,1]);
	//mat4.rotateX(modelViewMatrix, modelViewMatrix, degreeToRad(angleCube));
	mat4.multiply(modelViewProjectionMatrix, PerspectiveProjectionMatrix, modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindTexture(gl.TEXTURE_2D, Texture_Kundali);
	gl.uniform1i(uniform_texture0_sampler, 0);
	
	gl.bindVertexArray(vao_Cube);
	
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	gl.bindVertexArray(null);
	
	gl.useProgram(null);
	update();
	requestAnimationFrame(draw, canvas);
}

/*function degreeToRad(angle)
{
	return (angle * Math.PI/180.0);
}*/
function update()
{
	angleCube = angleCube +2.0;
	if(angleCube >= 360.0)
		angleCube -= 360.0;
	
	anglePyramid = anglePyramid +2.0;
	if(anglePyramid >= 360.0)
		anglePyramid -= 360.0;
}
function toggleFullScreen()
{
	var fullscreen_element = 
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;
	
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		
		bFullscreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullscreen = false;
	}
}	
function keyDown(event)
{
	//alert ("A Key is Pressed");
	switch(event.keyCode)
	{
		case 27:
			uninitialize();
			window.close();
			break;
		case 70:
			toggleFullScreen();
	//		drawText("Hello World !!!");
			break;
	}
}

function mouseDown(event)
{
	//alert("Mouse is clicked");
}

function uninitialize()
{
//	var Texture_Kundali =0;
//var Texture_Stone = 0;
	if(Texture_Kundali)
	{
		gl.deleteTexture(Texture_Kundali);
		Texture_Kundali = 0;
	}
	if(Texture_Stone)
	{
		gl.deleteTexture(Texture_Stone);
		Texture_Stone = 0;
	}
	if(vao_Pyramid)
	{
		gl.deleteVertexArray(vao_Pyramid);
		vao_Pyramid = null;
	}
	if(vbo_Pyramid_position)
	{
		gl.deleteBuffer(vbo_Pyramid_position);
		vbo_Pyramid_position = null;
	}
	if(vbo_Pyramid_texture)
	{
		gl.deleteBuffer(vbo_Pyramid_texture);
		vbo_Pyramid_texture = null;
	}
	if(vao_Cube)
	{
		gl.deleteVertexArray(vao_Cube);
		vao_Cube = null;
	}
	if(vbo_Cube_position)
	{
		gl.deleteBuffer(vbo_Cube_position);
		vbo_Cube_position = null;
	}
	if(vbo_Cube_texture)
	{
		gl.deleteBuffer(vbo_Cube_texture);
		vbo_Cube_texture = null;
	}
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI / 180);
}
