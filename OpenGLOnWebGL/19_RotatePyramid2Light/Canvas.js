//global var
var canvas=null;
var gl=null; //context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	MPD_ATTRIBUTE_VERTEX:0,
	MPD_ATTRIBUTE_COLOR:1,
	MPD_ATTRIBUTE_NORMAL:2,
	MPD_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_Pyramid;

var vbo_Pyramid_position;
var vbo_Pyramid_normal;
var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
var laUniform, ldRedUniform, ldBlueUniform, lsRedUniform, lsBlueUniform;
var kaUniform, kdUniform, ksUniform, materialShininessUniform;
var lightPositionUniformBlue, lightPositionUniformRed;


var PerspectiveProjectionMatrix;//1st Change

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ;
	
var cancelAnimationFrame = 
	window.cancelAnimationFrame ||
	window.webkitCancelRequestAnimationFrame ||
	window.webkitCancelAnimationFrame ||
	window.mozCancelRequestAnimationFrame ||
	window.mozCancelAnimationFrame ||
	window.oCancelRequestAnimationFrame ||
	window.oCancelAnimationFrame ||
	window.msCancelRequestAnimationFrame ||
	window.msCancelAnimationFrame;

var anglePyramid = 0.0;
//onload fuction
function myMain()
{
	//get <canvas> element
	canvas = document.getElementById("MPD");
	if(!canvas)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	//print canvas width and height on console
	console.log("Canvas Width : " + canvas.width + " And Canvas Height : " + canvas.height);
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);//false:No Capture
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize();
	draw();
}

function init()
{
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	var vertexShaderSourceCode =
			"#version 300 es" +
			"\n" +
			"in vec4 vPosition;" +
			"in vec3 vNormal;" +
			"uniform mat4 u_model_matrix;" +
			"uniform mat4 u_view_matrix;" +
			"uniform mat4 u_projection_matrix;" +
			"uniform mediump int u_double_tap;" +
			"uniform vec3 u_La;" +
			"uniform vec3 u_LdRed;" +
			"uniform vec3 u_LdBlue;" +
			"uniform vec3 u_LsRed;" +
			"uniform vec3 u_LsBlue;" +
			"uniform vec3 u_Ka;" +
			"uniform vec3 u_Ks;" +
			"uniform vec3 u_Kd;" +
			"uniform vec4 u_light_positionRed;" +
			"uniform vec4 u_light_positionBlue;" +
			"uniform float u_material_shininess;" +
			"out vec3 phong_ads_colorRed;" +
			"out vec3 phong_ads_colorBlue;" +
			"void main(void)" +
			"{" +
			"if(u_double_tap ==1)" +
			"{" +
			"vec4 eyeCoordinates = u_view_matrix * u_model_matrix  * vPosition;" +
			"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" +
			"vec3 ambient = u_La * u_Ka;" +
			"vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" +

			"vec3 light_directionRed  = normalize(vec3(u_light_positionRed) - eyeCoordinates.xyz);" +
			"vec3 diffuseRed = u_LdRed * u_Kd * max(dot(light_directionRed ,transformed_normals), 0.0);" +
			"vec3 reflection_vectorRed = reflect(-light_directionRed, transformed_normals);" +
			"vec3 specularRed = u_LsRed * u_Ks * pow(max(dot(reflection_vectorRed, viewer_vector), 0.0), u_material_shininess);" +
			"phong_ads_colorRed=ambient + diffuseRed + specularRed;" +

			"vec3 light_directionBlue  = normalize(vec3(u_light_positionBlue) - eyeCoordinates.xyz);" +
			"vec3 diffuseBlue = u_LdBlue * u_Kd * max(dot(light_directionBlue ,transformed_normals), 0.0);" +
			"vec3 reflection_vectorBlue = reflect(-light_directionBlue, transformed_normals);" +
			"vec3 specularBlue = u_LsBlue * u_Ks * pow(max(dot(reflection_vectorBlue, viewer_vector), 0.0), u_material_shininess);" +
			"phong_ads_colorBlue=ambient + diffuseBlue + specularBlue;" +
			"}" +
			"else" +
			"{" +
			"phong_ads_colorRed = vec3(1.0, 1.0, 1.0);" +
			"phong_ads_colorBlue = vec3(1.0, 1.0, 1.0);" +
			"}" +
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"}"
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	var fragmentShaderSourceCode = 
			"#version 300 es" +
    		"\n" +
			"precision highp float;" +
			"in vec3 phong_ads_colorRed;" +
			"in vec3 phong_ads_colorBlue;" +
			"vec3 phong_ads_color;" +
			"out vec4 FragColor;" +
			"void main(void)" +
			"{" +
			"phong_ads_color = phong_ads_colorRed + phong_ads_colorBlue;" +
			"FragColor = vec4(phong_ads_color, 1.0);" +
			"}"

			
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.MPD_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.MPD_ATTRIBUTE_COLOR, "vColor");
	
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	
	var PyramidVertices = new Float32Array( [ 0.0, 1.0, 0.0, 
												-1.0, -1.0, 1.0,
												1.0, -1.0, 1.0,
												
												0.0,1.0,0.0,
												1.0,-1.0,1.0,
												1.0,-1.0,-1.0,
												
												0.0,1.0,0.0,
												1.0,-1.0,-1.0,
												-1.0,-1.0,-1.0,
												
												0.0,1.0,0.0,
												-1.0,-1.0,-1.0,
												-1.0,-1.0,1.0
												]);
												
	
	var pyramidNormal = new Float32Array(   [   0.0, 0.447214, 0.894427,
		                                        0.0, 0.447214, 0.894427,
		                                        0.0, 0.447214, 0.894427,
		                                        0.894427, 0.447214, 0.0,
		                                        0.894427, 0.447214, 0.0,
		                                        0.894427, 0.447214, 0.0,
		                                        0.0, 0.447214, -0.894427,
		                                        0.0, 0.447214, -0.894427,
		                                        0.0, 0.447214, -0.894427,
		                                        -0.894427, 0.447214, 0.0,
		                                        -0.894427, 0.447214, 0.0,
		                                        -0.894427, 0.447214, 0.0,
	]);
	
		
	
	vao_Pyramid = gl.createVertexArray();
	gl.bindVertexArray(vao_Pyramid);
	
	vbo_Pyramid_position= gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Pyramid_position);
	gl.bufferData(gl.ARRAY_BUFFER, PyramidVertices, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.MPD_ATTRIBUTE_VERTEX, 
							3, gl.FLOAT,
							false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.MPD_ATTRIBUTE_VERTEX);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	//color
	vbo_Pyramid_normal= gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Pyramid_normal);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidNormal, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.MPD_ATTRIBUTE_NORMAL, 
							3, gl.FLOAT,
							false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.MPD_ATTRIBUTE_NORMAL);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
	
	gl.clearColor(0.0,0.0,0.0,1.0);
	gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
	PerspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	
	gl.viewport(0,0,canvas.width, canvas.height);

	mat4.perspective(PerspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	mat4.identity(modelViewProjectionMatrix);
	var translationVector = vec3.create();
	vec3.set(translationVector, 0, 0, -6);
	//mat4.translate(modelViewMatrix, modelViewMatrix,[-1.5,0.0,-6.0]);
	mat4.translate(modelViewMatrix, modelViewMatrix, translationVector);
	
	
	var angleInRadians1 = anglePyramid * Math.PI /180;
	//mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians1, [1,0,0]);
mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians1, [0,1,0]);
//mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians1, [0,0,1]);
	//mat4.rotateX(modelViewMatrix, modelViewMatrix, anglePyramid);
	var modelViewProjectionMatrix = mat4.create();
	
	mat4.multiply(modelViewProjectionMatrix, PerspectiveProjectionMatrix, modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_Pyramid);
	gl.drawArrays(gl.TRIANGLES, 0, 12);
	gl.bindVertexArray(null);
	
	
	gl.useProgram(null);
	update();
	requestAnimationFrame(draw, canvas);
}

/*function degreeToRad(angle)
{
	return (angle * Math.PI/180.0);
}*/
function update()
{
	
	anglePyramid = anglePyramid +1.0;
	if(anglePyramid >= 360.0)
		anglePyramid = 0.0;
}
function toggleFullScreen()
{
	var fullscreen_element = 
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;
	
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		
		bFullscreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullscreen = false;
	}
}	
function keyDown(event)
{
	//alert ("A Key is Pressed");
	switch(event.keyCode)
	{
		case 27:
			uninitialize();
			window.close();
			break;
		case 70:
			toggleFullScreen();
	//		drawText("Hello World !!!");
			break;
	}
}

function mouseDown(event)
{
	//alert("Mouse is clicked");
}

function uninitialize()
{
	if(vao_Pyramid)
	{
		gl.deleteVertexArray(vao_Pyramid);
		vao_Pyramid = null;
	}
	if(vbo_Pyramid_position)
	{
		gl.deleteBuffer(vbo_Pyramid_position);
		vbo_Pyramid_position = null;
	}
	if(vbo_Pyramid_normal)
	{
		gl.deleteBuffer(vbo_Pyramid_normal);
		vbo_Pyramid_normal = null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gldeleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gldeleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gldeleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}