//global var
var canvas=null;
var context=null;

//onload fuction
function myMain()
{
	//get <canvas> element
	canvas = document.getElementById("MPD");
	if(!canvas)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");
	
	//print canvas width and height on console
	console.log("Canvas Width : " + canvas.width + " And Canvas Height : " + canvas.height);
	
	//get 2D context
	context = canvas.getContext("2d");
	if(!context)
		console.log("Obtaining 2D Context Failed\n");
	else
		console.log("Obtaining 2D Context Succeeded\n");
	
	//fill canvas with black colorDepth
	context.fillStyle="black";
	context.fillRect(0,0,canvas.width,canvas.height);
	
	//center the text
	context.textAlign = "center";
	context.textBaseline = "middle";
	
	var str = "Hello World !!! \n ";
	
	context.font="48px sans-serif";
	
	//text color
	context.fillStyle="white";
	
	context.fillText(str, canvas.width/2, canvas.height/2);
	
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);//false:No Capture
	
}

function keyDown(event)
{
	alert ("A Key is Pressed");
}

function mouseDown(event)
{
	alert("Mouse is clicked");
}