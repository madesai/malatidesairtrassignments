//global var
var canvas=null;
var gl=null; //context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	MPD_ATTRIBUTE_VERTEX:0,
	MPD_ATTRIBUTE_COLOR:1,
	MPD_ATTRIBUTE_NORMAL:2,
	MPD_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_rectangle;

var vbo_rectangle_position;

var vbo_rectangle_texture;

var mvpUniform;

var PerspectiveProjectionMatrix;//1st Change

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ;
	
var cancelAnimationFrame = 
	window.cancelAnimationFrame ||
	window.webkitCancelRequestAnimationFrame ||
	window.webkitCancelAnimationFrame ||
	window.mozCancelRequestAnimationFrame ||
	window.mozCancelAnimationFrame ||
	window.oCancelRequestAnimationFrame ||
	window.oCancelAnimationFrame ||
	window.msCancelRequestAnimationFrame ||
	window.msCancelAnimationFrame;

var angleRec = 0.0;
	

var Texture_sampler_uniform;
var Texture_Smiley =0;

//onload fuction
function myMain()
{
	//get <canvas> element
	canvas = document.getElementById("MPD");
	if(!canvas)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	//print canvas width and height on console
	console.log("Canvas Width : " + canvas.width + " And Canvas Height : " + canvas.height);
	
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);//false:No Capture
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize();
	draw();
}

function init()
{
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	var vertexShaderSourceCode = 
			"#version 300 es" +
			"\n" +
			"in vec4 vPosition;" +
			"in vec2 vTexture0_Coord;" +
			"out vec2 out_texture0_coord;"+
			"uniform mat4 u_mvp_matrix;" +
			"void main(void)" +
			"{"+
			"gl_Position = u_mvp_matrix * vPosition;" +
			"out_texture0_coord = vTexture0_Coord;"+
			"}";
			
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	var fragmentShaderSourceCode = 
			"#version 300 es" +
			"\n" +
			"precision highp float;" +
			"in vec2 out_texture0_coord;"+
			"uniform highp sampler2D u_texture0_sampler;"+
			"out vec4 FragColor;" +
			"void main(void)" +
			"{"+
			"FragColor = texture(u_texture0_sampler, out_texture0_coord);"+
			"}"
			
			
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.MPD_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.MPD_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
	
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//same like function present in windows and android as LoadGLTexture()
	Texture_Smiley =gl.createTexture();
	Texture_Smiley.image = new Image();
	Texture_Smiley.image.src="Smiley.bmp";
	Texture_Smiley.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D, Texture_Smiley);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, Texture_Smiley.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}
	
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	uniform_texture0_sampler = gl.getUniformLocation(shaderProgramObject, "u_texture0_sampler");
										   
		var rectangleVertices = new Float32Array([
			1.0, 1.0, 0.0,
			-1.0, 1.0, 0.0,
			-1.0, -1.0, 0.0,
			1.0, -1.0, 0.0
		]);
		var rectangleTextcoords = new Float32Array([
			1.0,1.0,
			0.0,1.0,
			0.0,0.0,
			1.0,0.0
		]); 
		
	vao_rectangle = gl.createVertexArray();
	gl.bindVertexArray(vao_rectangle);
	
	vbo_rectangle_position= gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_rectangle_position);
	gl.bufferData(gl.ARRAY_BUFFER, rectangleVertices, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.MPD_ATTRIBUTE_VERTEX, 
							3, gl.FLOAT,
							false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.MPD_ATTRIBUTE_VERTEX);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	//texture
	vbo_rectangle_texture= gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_rectangle_texture);
	gl.bufferData(gl.ARRAY_BUFFER, rectangleTextcoords, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.MPD_ATTRIBUTE_TEXTURE0, 
							2, gl.FLOAT,
							false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.MPD_ATTRIBUTE_TEXTURE0);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
	
	//gl.vertexAttrib3f(WebGLMacros.MPD_ATTRIBUTE_COLOR, 0.39, 0.58, 0.9294);
	//gl.enableVertexAttribArray(WebGLMacros.MPD_ATTRIBUTE_COLOR);
	
	
	gl.clearColor(0.0,0.0,0.0,1.0);
	gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
	PerspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	
	gl.viewport(0,0,canvas.width, canvas.height);
	
	//if(canvas.width <= canvas.height)
	//	mat4.ortho(orthographicProjectionMatrix, -100.0, 100.0, (-100.0 * (canvas.height / canvas.width)), 
	//			(100.0 * (canvas.height / canvas.width)), -100.0, 100.0);
		
	//else
	//	mat4.ortho(orthographicProjectionMatrix, (-100.0 * (canvas.width/canvas.height)) , (100.0*(canvas.width/canvas.height)), -100.0 , 
		//		100.0 , -100.0, 100.0);
		mat4.perspective(PerspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}



function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	//mat4.identity(modelViewProjectionMatrix);
	//var translationVector = vec3.create();
	//vec3.set(translationVector, -1.5, 0, -6);
	mat4.translate(modelViewMatrix, modelViewMatrix,[0.0,0.0,-3.0]);
	//mat4.translate(modelViewMatrix, modelViewMatrix, translationVector);
	
	//mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(anglePyramid));
	
	
	//var angleInRadians1 = anglePyramid * Math.PI /180;
	//mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians1, [1,0,0]);
//mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians1, [0,1,0]);
//mat4.rotate(modelViewMatrix, modelViewMatrix, angleInRadians1, [0,0,1]);
	//mat4.rotateX(modelViewMatrix, modelViewMatrix, anglePyramid);
	//var modelViewProjectionMatrix = mat4.create();
	
	mat4.multiply(modelViewProjectionMatrix, PerspectiveProjectionMatrix, modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	//bind with texture
	gl.bindTexture(gl.TEXTURE_2D, Texture_Smiley);
	gl.uniform1i(uniform_texture0_sampler, 0);
	
	gl.bindVertexArray(vao_rectangle);
	
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.bindVertexArray(null);
	
	
	
	gl.useProgram(null);
	//update();
	requestAnimationFrame(draw, canvas);
}

/*function degreeToRad(angle)
{
	return (angle * Math.PI/180.0);
}*/
function update()
{
	/*angleCube = angleCube +2.0;
	if(angleCube >= 360.0)
		angleCube -= 360.0;
	
	anglePyramid = anglePyramid +2.0;
	if(anglePyramid >= 360.0)
		anglePyramid -= 360.0;*/
}
function toggleFullScreen()
{
	var fullscreen_element = 
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;
	
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		
		bFullscreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullscreen = false;
	}
}	
function keyDown(event)
{
	//alert ("A Key is Pressed");
	switch(event.keyCode)
	{
		case 27:
			uninitialize();
			window.close();
			break;
		case 70:
			toggleFullScreen();
	//		drawText("Hello World !!!");
			break;
	}
}

function mouseDown(event)
{
	//alert("Mouse is clicked");
}

function uninitialize()
{
//	var Texture_Kundali =0;
//var Texture_Stone = 0;
	if(Texture_Smiley)
	{
		gl.deleteTexture(Texture_Smiley);
		Texture_Smiley = 0;
	}
	
	if(vao_rectangle)
	{
		gl.deleteVertexArray(vao_rectangle);
		vao_rectangle = null;
	}
	if(vbo_rectangle_position)
	{
		gl.deleteBuffer(vbo_rectangle_position);
		vbo_rectangle_position = null;
	}
	if(vbo_rectangle_texture)
	{
		gl.deleteBuffer(vbo_rectangle_texture);
		vbo_rectangle_texture = null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI / 180);
}
