
var material_ambient1 = [0.0215, 0.1745, 0.0215];
var material_diffuse1 = [0.07568, 0.61424, 0.07568];
var material_specular1 = [0.633, 0.727811, 0.633];
var material_shininess1 = 0.6 * 128;

var material_ambient2 = [0.135, 0.2225, 0.1575];
var material_diffuse2 = [0.54, 0.89, 0.63];
var material_specular2 = [0.316228, 0.316228, 0.316228];
var material_shininess2 = 0.1 * 128;

var material_ambient3 = [0.05375, 0.05, 0.06625];
var material_diffuse3 = [0.18275, 0.17, 0.22525];
var material_specular3 = [0.332741, 0.328634, 0.346435];
var material_shininess3 = (0.3 * 128);

var material_ambient4 = [0.25, 0.20725, 0.20725];
var material_diffuse4 = [1.0, 0.829, 0.829];
var material_specular4 = [0.296648, 0.296648, 0.296648];
var material_shininess4 = (0.088 * 128);

var material_ambient5 = [0.1745, 0.01175, 0.01175];
var material_diffuse5 = [0.61424, 0.04136, 0.04136];
var material_specular5 = [0.727811, 0.626959, 0.626959];
var material_shininess5 = (0.6 * 128);

var material_ambient6 = [0.1, 0.18725, 0.1745];
var material_diffuse6 = [0.396, 0.74151, 0.69102];
var material_specular6 = [0.297254, 0.30829, 0.306678];
var material_shininess6 = (0.1 * 128);

var material_ambient7 = [0.329412, 0.223529, 0.027451];
var material_diffuse7 = [0.780392, 0.568627, 0.113725];
var material_specular7 = [0.992157, 0.941176, 0.807843];
var material_shininess7 = (0.2179872 * 128);

var material_ambient8 = [0.2125, 0.1275, 0.054];
var material_diffuse8 = [0.714, 0.4284, 0.18144];
var material_specular8 = [0.393548, 0.271906, 0.166722];
var material_shininess8 = (0.2 * 128);

//3rd sphere 2nd column
var material_ambient9 = [0.25, 0.25, 0.25];
var material_diffuse9 = [0.4, 0.4, 0.4];
var material_specular9 = [0.774597, 0.774597, 0.774597];
var material_shininess9 = (0.6 * 128);

//4rth sphere 2nd column
var material_ambient10 = [0.19125, 0.0735, 0.0225];
var material_diffuse10 = [0.7038, 0.27048, 0.0828];
var material_specular10 = [0.256777, 0.137622, 0.086014];
var material_shininess10 = (0.1 * 128);

//5th Sphere 2nd column
var material_ambient11 = [0.24725, 0.1995, 0.0745];
var material_diffuse11 = [0.75164, 0.60648, 0.22648];
var material_specular11 = [0.628281, 0.555802, 0.366065];
var material_shininess11 = (0.4 * 128);

//6th Sphere 2nd column
var material_ambient12 = [0.19225, 0.19225, 0.19225];
var material_diffuse12 = [0.50754, 0.50754, 0.50754];
var material_specular12 = [0.508273, 0.508273, 0.508273];
var material_shininess12 = (0.4 * 128);

//1st sphere 3rd column
var material_ambient13 = [0.0, 0.0, 0.0];
var material_diffuse13 = [0.01, 0.01, 0.01];
var material_specular13 = [0.5, 0.5, 0.5];
var material_shininess13 = (0.25 * 128);

//2nd sphere 3rd column
var material_ambient14 = [0.0, 0.1, 0.06];
var material_diffuse14 = [0.0, 0.50980392, 0.50980392];
var material_specular14 = [0.50196078, 0.50196078, 0.50196078];
var material_shininess14 = (0.25 * 128);

//3rd sphere in 3rd column
var material_ambient15 = [0.0, 0.0, 0.0];
var material_diffuse15 = [0.1, 0.35, 0.1];
var material_specular15 = [0.45, 0.55, 0.45];
var material_shininess15 = (0.25 * 128);

//4rth sphere in 3rd column
var material_ambient16 = [0.0, 0.0, 0.0];
var material_diffuse16 = [0.5, 0.0, 0.0];
var material_specular16 = [0.7, 0.6, 0.6];
var material_shininess16 = (0.25 * 128);

//5th sphere in 3rd column
var material_ambient17 = [0.0, 0.0, 0.0];
var material_diffuse17 = [0.55, 0.55, 0.55];
var material_specular17 = [0.70, 0.70, 0.70];
var material_shininess17 = (0.25 * 128);

//6th sphere in 3rd column
var material_ambient18 = [0.0, 0.0, 0.0];
var material_diffuse18 = [0.5, 0.5, 0.0];
var material_specular18 = [0.6, 0.6, 0.5];
var material_shininess18 = (0.25 * 128);

//1st sphere in 4rth column
var material_ambient19 = [0.02, 0.02, 0.02];
var material_diffuse19 = [0.01, 0.01, 0.01];
var material_specular19 = [0.4, 0.4, 0.4];
var material_shininess19 = (0.078125 * 128);

//2nd sphere in 4rth column
var material_ambient20 = [0.0, 0.05, 0.05];
var material_diffuse20 = [0.4, 0.5, 0.5];
var material_specular20 = [0.04, 0.7, 0.7];
var material_shininess20 = (0.078125 * 128);

//3rd sphere in 4rth column
var material_ambient21 = [0.0, 0.05, 0.0];
var material_diffuse21 = [0.4, 0.5, 0.4];
var material_specular21 = [0.04, 0.7, 0.04];
var material_shininess21 = (0.078125 * 128);

//4rth sphere in 4rth column
var material_ambient22 = [0.05, 0.0, 0.0];
var material_diffuse22 = [0.5, 0.4, 0.4];
var material_specular22 = [0.7, 0.04, 0.04];
var material_shininess22 = 0.078125 * 128;

//5th sphere in 4rth column
var material_ambient23 = [0.05, 0.05, 0.05];
var material_diffuse23 = [0.5, 0.5, 0.5];
var material_specular23 = [0.7, 0.7, 0.7];
var material_shininess23 = 0.078125 * 128;

//6th sphere in 4rth column
var material_ambient24 = [0.05, 0.05, 0.0];
var material_diffuse24 = [0.5, 0.5, 0.4];
var material_specular24 = [0.7, 0.7, 0.04];
var material_shininess24 = 0.078125 * 128;

//global var
var canvas=null;
var gl=null; //context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	MPD_ATTRIBUTE_VERTEX:0,
	MPD_ATTRIBUTE_COLOR:1,
	MPD_ATTRIBUTE_NORMAL:2,
	MPD_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var light_ambient=[0.0,0.0,0.0];

var light_diffuse=[1.0,1.0,1.0]; 

var light_specular=[1.0,1.0,1.0];

var light_position=[100.0,100.0,100.0,1.0];

var material_ambient= [0.0,0.0,0.0];
var material_diffuse= [1.0,1.0,1.0];
var material_specular= [1.0,1.0,1.0];
var material_shininess= 50.0;

var sphere = null;

var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;

var laUniform, ldUniform, lsUniform, lightPositionUniform;

var kaUniform, kdUniform, ksUniform, materialShininessUniform;

var lKeyPressedUniform;
var bRotate = 0;
var bLKeyPressed = false;

var PerspectiveProjectionMatrix;//1st Change

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ;
	
var cancelAnimationFrame = 
	window.cancelAnimationFrame ||
	window.webkitCancelRequestAnimationFrame ||
	window.webkitCancelAnimationFrame ||
	window.mozCancelRequestAnimationFrame ||
	window.mozCancelAnimationFrame ||
	window.oCancelRequestAnimationFrame ||
	window.oCancelAnimationFrame ||
	window.msCancelRequestAnimationFrame ||
	window.msCancelAnimationFrame;

var angle_red = 0.0;


//onload fuction
function myMain()
{
	//get <canvas> element
	canvas = document.getElementById("MPD");
	if(!canvas)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	//print canvas width and height on console
	console.log("Canvas Width : " + canvas.width + " And Canvas Height : " + canvas.height);
	
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);//false:No Capture
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize();
	draw();
}

function init()
{
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	var vertexShaderSourceCode = 
			"#version 300 es" +
			"\n" +
			"in vec4 vPosition;" +
			"in vec3 vNormal;" +
			"uniform mat4 u_model_matrix;" +
			"uniform mat4 u_view_matrix;" +
			"uniform mat4 u_projection_matrix;"+
			"uniform mediump int u_LKeyPressed;"+
			"uniform vec4 u_light_position;"+
			"out vec3 transformed_normals;"+
			"out vec3 light_direction;"+
			"out vec3 viewer_vector;"+
			"void main(void)" +
			"{"+
			"if(u_LKeyPressed == 1)" +
			"{"+
			"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
			"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"+
			"light_direction = vec3(u_light_position) - eye_coordinates.xyz;"+
			"viewer_vector = -eye_coordinates.xyz;"+
			"}"+
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"}";
			
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	var fragmentShaderSourceCode = 
			"#version 300 es" +
			"\n" +
			"precision highp float;"+
         "in vec3 transformed_normals;"+
         "in vec3 light_direction;"+
         "in vec3 viewer_vector;"+
         "out vec4 FragColor;"+
         "uniform vec3 u_La;"+
		 "uniform vec3 u_Ld;"+
		 "uniform vec3 u_Ls;"+
	     "uniform vec3 u_Ka;"+
         "uniform vec3 u_Kd;"+
         "uniform vec3 u_Ks;"+
         "uniform float u_material_shininess;"+
         "uniform int u_LKeyPressed;"+
         "void main(void)"+
         "{"+
         "vec3 phong_ads_color;"+
         "if(u_LKeyPressed==1)"+
         "{"+
         "vec3 normalized_transformed_normals =normalize(transformed_normals);"+
         "vec3 normalized_light_direction =normalize(light_direction);"+
         "vec3 normalized_viewer_vector =normalize(viewer_vector);"+
         "vec3 ambient = u_La * u_Ka;"+
         "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"+
         "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
         "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
         "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
         "phong_ads_color=ambient + diffuse + specular;"+
		 "}"+
         "else"+
         "{"+
         "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
         "}"+
         "FragColor = vec4(phong_ads_color, 1.0);"+
         "}"

			
			
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.MPD_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.MPD_ATTRIBUTE_NORMAL, "vNormal");
	
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
	
	lKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_LKeyPressed");
	xKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_XKeyPressed");
	yKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_YKeyPressed");
	zKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_ZKeyPressed");
	
	laUniform = gl.getUniformLocation(shaderProgramObject, "u_La");
	ldUniform = gl.getUniformLocation(shaderProgramObject, "u_Ld");
	lsUniform = gl.getUniformLocation(shaderProgramObject, "u_Ls");
	kaUniform = gl.getUniformLocation(shaderProgramObject, "u_Ka");
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
	ksUniform = gl.getUniformLocation(shaderProgramObject, "u_Ks");
	
	lightPositionUniform1 = gl.getUniformLocation(shaderProgramObject, "u_light_position");
	lightPositionUniform2 = gl.getUniformLocation(shaderProgramObject, "u_light_position");
	lightPositionUniform3 = gl.getUniformLocation(shaderProgramObject, "u_light_position");

	materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_material_shininess");
	
	sphere = new Mesh();
	makeSphere(sphere, 2.0, 30, 30);
	
	
	gl.clearColor(0.0,0.0,0.0,1.0);
	gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
	gl.enable(gl.CULL_FACE);
	PerspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	
	gl.viewport(0,0,canvas.width, canvas.height);
	
	//if(canvas.width <= canvas.height)
	//	mat4.ortho(orthographicProjectionMatrix, -100.0, 100.0, (-100.0 * (canvas.height / canvas.width)), 
	//			(100.0 * (canvas.height / canvas.width)), -100.0, 100.0);
		
	//else
	//	mat4.ortho(orthographicProjectionMatrix, (-100.0 * (canvas.width/canvas.height)) , (100.0*(canvas.width/canvas.height)), -100.0 , 
		//		100.0 , -100.0, 100.0);
		mat4.perspective(PerspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}



function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	var deviceWidth = canvas.width;
	var deviceHeight = canvas.height;

	if(bLKeyPressed == true)
	{
		gl.uniform1i(lKeyPressedUniform, 1);
		
		gl.uniform3fv(laUniform, light_ambient);
		gl.uniform3fv(lsUniform, light_specular);
		gl.uniform3fv(ldUniform, light_diffuse);
		
		//gl.uniform3fv(kaUniform, material_ambient);
		//gl.uniform3fv(ksUniform, material_specular);
		//gl.uniform3fv(kdUniform, material_diffuse);
		
		var modelViewMatrix1 = mat4.create();
		var modelViewMatrix2 = mat4.create();
		var modelViewMatrix3 = mat4.create();
		
		//var lightPosition = [0.0, 0.0, 2.0, 1.0];
		/*myRotationMatrix1[0] = 0.0;
		myRotationMatrix1[1] = 100.0 * Math.cos(2 * VDG_PI * angle_red);
		myRotationMatrix1[2] = 100.0 * Math.sin(2 * VDG_PI * angle_red);
		myRotationMatrix1[3] = 1.0;*/
		if (bRotate == 1) {
		    var myRotationMatrix1 = [0.0, 100.0 * Math.cos(2 * VDG_PI * degToRad(angle_red)), 100.0 * Math.sin(2 * VDG_PI * degToRad(angle_red)), 1.0];
		    //mat4.rotateX(myRotationMatrix1, myRotationMatrix1 , degToRad(angle_red));
		    //mat4.rotateX(modelViewMatrix1, modelViewMatrix1 , degToRad(angle_red));
		    gl.uniform4fv(lightPositionUniform1, myRotationMatrix1);
		}
		/*myRotationMatrix2[0] = 100.0 * Math.cos(2 * VDG_PI * angle_red);
		myRotationMatrix2[1] = 0.0;
		myRotationMatrix2[2] = 100.0 * Math.sin(2 * VDG_PI * angle_red);
		myRotationMatrix2[3] = 1.0;
		*/
		if (bRotate == 2) {
		    var myRotationMatrix2 = [100.0 * Math.cos(2 * VDG_PI * degToRad(angle_red)), 0.0, 100.0 * Math.sin(2 * VDG_PI * degToRad(angle_red)), 1.0];

		    //mat4.rotateX(myRotationMatrix2, myRotationMatrix2 , degToRad(angle_red));
		    //mat4.rotateX(modelViewMatrix2, modelViewMatrix2 , degToRad(angle_red));
		    gl.uniform4fv(lightPositionUniform2, myRotationMatrix2);
		}
	/*	myRotationMatrix3[0] = 100.0 * Math.sin(2 * VDG_PI * angle_red);
		myRotationMatrix3[1] = 100.0 * Math.cos(2 * VDG_PI * angle_red);
		myRotationMatrix3[2] = -2.0;
		myRotationMatrix3[3] = 1.0;*/
		if (bRotate == 3) {
		    var myRotationMatrix3 = [100.0 * Math.sin(2 * VDG_PI * degToRad(angle_red)), 100.0 * Math.cos(2 * VDG_PI * degToRad(angle_red)), -2.0, 1.0];

		    //mat4.rotateX(myRotationMatrix3, myRotationMatrix3 , degToRad(angle_red));
		    //mat4.rotateX(modelViewMatrix3, modelViewMatrix3 , degToRad(angle_red));
		    gl.uniform4fv(lightPositionUniform3, myRotationMatrix3);
		}
		DrawAllSpheres();
	//	gl.uniform1f(materialShininessUniform, material_shininess);
	}
	else
	{
	    gl.uniform1i(lKeyPressedUniform, 0);
	    gl.viewport(0, 0, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(0, deviceHeight / 2 - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(0, deviceHeight - deviceHeight / 2, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(0, deviceHeight - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    //=================================================
	    gl.viewport(deviceWidth / 6, 0, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(deviceWidth / 6, deviceHeight / 2 - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(deviceWidth / 6, deviceHeight - deviceHeight / 2, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(deviceWidth / 6, deviceHeight - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    //===================================================
	    gl.viewport(deviceWidth / 3, 0, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(deviceWidth / 3, deviceHeight / 2 - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(deviceWidth / 3, deviceHeight - deviceHeight / 2, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(deviceWidth / 3, deviceHeight - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    //==================================================
	    gl.viewport(deviceWidth / 2, 0, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(deviceWidth / 2, deviceHeight / 2 - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(deviceWidth / 2, deviceHeight - deviceHeight / 2, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(deviceWidth / 2, deviceHeight - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    //==================================================
	    gl.viewport(2 * deviceWidth / 3, 0, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(2 * deviceWidth / 3, deviceHeight / 2 - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(2 * deviceWidth / 3, deviceHeight - deviceHeight / 2, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(2 * deviceWidth / 3, deviceHeight - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    //===================================================
	    gl.viewport(5 * deviceWidth / 6, 0, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(5 * deviceWidth / 6, deviceHeight / 2 - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(5 * deviceWidth / 6, deviceHeight - deviceHeight / 2, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();
	    gl.viewport(5 * deviceWidth / 6, deviceHeight - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
	    drawSphere();

	    gl.uniform3fv(laUniform, light_ambient);
	    gl.uniform3fv(lsUniform, light_specular);
	    gl.uniform3fv(ldUniform, light_diffuse);

        gl.uniform4fv(lightPositionUniform, light_position);
	    gl.uniform3fv(kaUniform, material_ambient);
	    gl.uniform3fv(kdUniform, material_diffuse);
	    gl.uniform3fv(ksUniform, material_specular);
	    gl.uniform1f(materialShininessUniform, material_shininess);

	}
	
	/*var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	mat4.translate(modelMatrix,modelMatrix, [0.0, 0.0, -6.0]);
	
	/*mat4.rotateX(modelViewMatrix, modelViewMatrix , degToRad(angleCube));
	mat4.rotateY(modelViewMatrix, modelViewMatrix , degToRad(angleCube));
	mat4.rotateZ(modelViewMatrix, modelViewMatrix , degToRad(angleCube));
	
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, PerspectiveProjectionMatrix);
	
	sphere.draw();
	*/
	gl.useProgram(null);
	update();
	requestAnimationFrame(draw, canvas);
}

function DrawAllSpheres()
{
    var deviceWidth = canvas.width;
    var deviceHeight = canvas.height;
    gl.viewport(0, 0, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient1);
    gl.uniform3fv(ksUniform, material_specular1);
    gl.uniform3fv(kdUniform, material_diffuse1);
    gl.uniform1f(materialShininessUniform, material_shininess1);
    drawSphere();

    gl.viewport(0, deviceHeight / 2 - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient2);
    gl.uniform3fv(ksUniform, material_specular2);
    gl.uniform3fv(kdUniform, material_diffuse2);
    gl.uniform1f(materialShininessUniform, material_shininess2);
    drawSphere();

    gl.viewport(0, deviceHeight - deviceHeight / 2, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient3);
    gl.uniform3fv(ksUniform, material_specular3);
    gl.uniform3fv(kdUniform, material_diffuse3);
    gl.uniform1f(materialShininessUniform, material_shininess3);
    drawSphere();

    gl.viewport(0, deviceHeight - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient4);
    gl.uniform3fv(ksUniform, material_specular4);
    gl.uniform3fv(kdUniform, material_diffuse4);
    gl.uniform1f(materialShininessUniform, material_shininess4);
    drawSphere();

    gl.viewport(deviceWidth / 6, 0, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient5);
    gl.uniform3fv(ksUniform, material_specular5);
    gl.uniform3fv(kdUniform, material_diffuse5);
    gl.uniform1f(materialShininessUniform, material_shininess5);
    drawSphere();

    gl.viewport(deviceWidth / 6, deviceHeight / 2 - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient6);
    gl.uniform3fv(ksUniform, material_specular6);
    gl.uniform3fv(kdUniform, material_diffuse6);
    gl.uniform1f(materialShininessUniform, material_shininess6);
    drawSphere();

    gl.viewport(deviceWidth / 6, deviceHeight - deviceHeight / 2, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient7);
    gl.uniform3fv(ksUniform, material_specular7);
    gl.uniform3fv(kdUniform, material_diffuse7);
    gl.uniform1f(materialShininessUniform, material_shininess7);
    drawSphere();

    gl.viewport(deviceWidth / 6, deviceHeight - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient8);
    gl.uniform3fv(ksUniform, material_specular8);
    gl.uniform3fv(kdUniform, material_diffuse8);
    gl.uniform1f(materialShininessUniform, material_shininess8);
    drawSphere();

    gl.viewport(deviceWidth / 3, 0, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient9);
    gl.uniform3fv(ksUniform, material_specular9);
    gl.uniform3fv(kdUniform, material_diffuse9);
    gl.uniform1f(materialShininessUniform, material_shininess9);
    drawSphere();

    gl.viewport(deviceWidth / 3, deviceHeight / 2 - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient10);
    gl.uniform3fv(ksUniform, material_specular10);
    gl.uniform3fv(kdUniform, material_diffuse10);
    gl.uniform1f(materialShininessUniform, material_shininess10);
    drawSphere();

    gl.viewport(deviceWidth / 3, deviceHeight - deviceHeight / 2, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient11);
    gl.uniform3fv(ksUniform, material_specular11);
    gl.uniform3fv(kdUniform, material_diffuse11);
    gl.uniform1f(materialShininessUniform, material_shininess11);
    drawSphere();

    gl.viewport(deviceWidth / 3, deviceHeight - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient12);
    gl.uniform3fv(ksUniform, material_specular12);
    gl.uniform3fv(kdUniform, material_diffuse12);
    gl.uniform1f(materialShininessUniform, material_shininess12);
    drawSphere();

    gl.viewport(deviceWidth / 2, 0, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient13);
    gl.uniform3fv(ksUniform, material_specular13);
    gl.uniform3fv(kdUniform, material_diffuse13);
    gl.uniform1f(materialShininessUniform, material_shininess13);
    drawSphere();

    gl.viewport(deviceWidth / 2, deviceHeight / 2 - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient14);
    gl.uniform3fv(ksUniform, material_specular14);
    gl.uniform3fv(kdUniform, material_diffuse14);
    gl.uniform1f(materialShininessUniform, material_shininess14);
    drawSphere();

    gl.viewport(deviceWidth / 2, deviceHeight - deviceHeight / 2, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient15);
    gl.uniform3fv(ksUniform, material_specular15);
    gl.uniform3fv(kdUniform, material_diffuse15);
    gl.uniform1f(materialShininessUniform, material_shininess15);
    drawSphere();

    gl.viewport(deviceWidth / 2, deviceHeight - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient16);
    gl.uniform3fv(ksUniform, material_specular16);
    gl.uniform3fv(kdUniform, material_diffuse16);
    gl.uniform1f(materialShininessUniform, material_shininess16);
    drawSphere();

    gl.viewport(2 * deviceWidth / 3, 0, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient17);
    gl.uniform3fv(ksUniform, material_specular17);
    gl.uniform3fv(kdUniform, material_diffuse17);
    gl.uniform1f(materialShininessUniform, material_shininess17);
    drawSphere();

    gl.viewport(2 * deviceWidth / 3, deviceHeight / 2 - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient18);
    gl.uniform3fv(ksUniform, material_specular18);
    gl.uniform3fv(kdUniform, material_diffuse18);
    gl.uniform1f(materialShininessUniform, material_shininess18);
    drawSphere();

    gl.viewport(2 * deviceWidth / 3, deviceHeight - deviceHeight / 2, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient19);
    gl.uniform3fv(ksUniform, material_specular19);
    gl.uniform3fv(kdUniform, material_diffuse19);
    gl.uniform1f(materialShininessUniform, material_shininess19);
    drawSphere();

    gl.viewport(2 * deviceWidth / 3, deviceHeight - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient20);
    gl.uniform3fv(ksUniform, material_specular20);
    gl.uniform3fv(kdUniform, material_diffuse20);
    gl.uniform1f(materialShininessUniform, material_shininess20);
    drawSphere();

    gl.viewport(5 * deviceWidth / 6, 0, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient21);
    gl.uniform3fv(ksUniform, material_specular21);
    gl.uniform3fv(kdUniform, material_diffuse21);
    gl.uniform1f(materialShininessUniform, material_shininess21);
    drawSphere();

    gl.viewport(5 * deviceWidth / 6, deviceHeight / 2 - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient22);
    gl.uniform3fv(ksUniform, material_specular22);
    gl.uniform3fv(kdUniform, material_diffuse22);
    gl.uniform1f(materialShininessUniform, material_shininess22);
    drawSphere();

    gl.viewport(5 * deviceWidth / 6, deviceHeight - deviceHeight / 2, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient23);
    gl.uniform3fv(ksUniform, material_specular23);
    gl.uniform3fv(kdUniform, material_diffuse23);
    gl.uniform1f(materialShininessUniform, material_shininess23);
    drawSphere();

    gl.viewport(5 * deviceWidth / 6, deviceHeight - deviceHeight / 4, deviceWidth / 4, deviceHeight / 4);
    gl.uniform3fv(kaUniform, material_ambient24);
    gl.uniform3fv(ksUniform, material_specular24);
    gl.uniform3fv(kdUniform, material_diffuse24);
    gl.uniform1f(materialShininessUniform, material_shininess24);
    drawSphere();

}
function drawSphere()
{
    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniform, false, PerspectiveProjectionMatrix);

    sphere.draw();

}
/*function degreeToRad(angle)
{
	return (angle * Math.PI/180.0);
}*/
function update()
{
	angle_red = angle_red +1.0;
	if(angle_red >= 360.0)
		angle_red -= 360.0;
	/*
	anglePyramid = anglePyramid +2.0;
	if(anglePyramid >= 360.0)
		anglePyramid -= 360.0;*/
}
function toggleFullScreen()
{
	var fullscreen_element = 
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;
	
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		
		bFullscreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullscreen = false;
	}
}	
function keyDown(event)
{
	//alert ("A Key is Pressed");
	switch(event.keyCode)
	{
		case 27:
			uninitialize();
			window.close();
			break;
		 case 76: // for 'L' or 'l'
            if(bLKeyPressed==false)
                bLKeyPressed=true;
            else
                bLKeyPressed=false;
            break;
		case 70:
			toggleFullScreen();
	//		drawText("Hello World !!!");
			break;
	    case 88:
	        bRotate = 1;
	        break;
	    case 89:
	        bRotate = 2;
	        break;
	    case 90:
	        bRotate = 3;
	        break;
		default:
			
			break;
	}
}

function mouseDown(event)
{
	//alert("Mouse is clicked");
}

function uninitialize()
{

	if(sphere)
    {
        sphere.deallocate();
        sphere=null;
    }
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI / 180);
}
