#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"
#include "sphere.h"

//Class GLESView ()

enum
{
    MALATI_ATTRIBUTE_VERTEX = 0,
    MALATI_ATTRIBUTE_COLOR,
    MALATI_ATTRIBUTE_NORMAL,
    MALATI_ATTRIBUTE_TEXTURE0,
};

@implementation GLESView
{
	EAGLContext *eaglContext;
	
	GLuint defaultFramebuffer;
	GLuint colorRenderbuffer;
	GLuint depthRenderbuffer;
	
	id displayLink;
	NSInteger animationFrameInterval;
	BOOL isAnimating;
	
	GLuint vertexShaderObjectPerVertex;
GLuint vertexShaderObjectPerFragment;
GLuint fragmentShaderObjectPerVertex;
GLuint fragmentShaderObjectPerFragment;
GLuint shaderProgramObjectPerVertex;
GLuint shaderProgramObjectPerFragment;
GLuint shaderProgramObject;

  GLuint gNumElements;
GLuint gNumVertices;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
    short sphere_elements[2280];
    float angle_red;
GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint gModelMatrixUniform, gViewMatrixUniform, gProjectionMatrixUniform;
GLuint gLd0Uniform, gLa0Uniform, gLs0Uniform;
GLuint gLd1Uniform, gLa1Uniform, gLs1Uniform;
GLuint gLd2Uniform, gLa2Uniform, gLs2Uniform;
GLuint gLightPositionUniform0;
GLuint gLightPositionUniform1;
GLuint gLightPositionUniform2;

GLuint gKdUniform, gKaUniform, gKsUniform;
GLuint gMaterialShininessUniform;

GLuint gSingleTapUniform;
GLuint gDoubleTapUniform;
    vmath::mat4 perspectiveProjectionMatrix;
	
	//GLfloat gAngleTriangle = 0.0f;
//GLfloat gAngleSquare = 0.0f;

    GLfloat lightAmbient0[4] ;//= { 0.0f,0.0f,0.0f,1.0f };
	GLfloat lightAmbient1[4] ;
	GLfloat lightAmbient2[4] ;
    GLfloat lightDiffuse0[4];// = { 1.0f,1.0f,1.0f,1.0f };
	GLfloat lightDiffuse1[4];
	GLfloat lightDiffuse2[4];
    GLfloat lightSpecular0[4]; //= { 1.0f,1.0f,1.0f,1.0f };
	GLfloat lightSpecular1[4];
	GLfloat lightSpecular2[4];
    GLfloat lightPosition0[4];// = { 100.0f,100.0f,100.0f,1.0f };
	GLfloat lightPosition1[4];
	GLfloat lightPosition2[4];
	
    GLfloat material_ambient[4];// = { 0.0f,0.0f,0.0f,1.0f };
    GLfloat material_diffuse[4] ;//= { 1.0f,1.0f,1.0f,1.0f };
    GLfloat material_specular[4];// = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat material_shininess;// = 50.0f;

    int singleTap ;
	int doubleTap ;
    //bool gbLight;
	
	GLfloat myRotateMatrix[4];
GLfloat myRotateMatrix1[4];
GLfloat myRotateMatrix2[4];

}

-(id) initWithFrame:(CGRect)frame
{
    angle_red = 0.0f;
    lightAmbient0[0] = 0.0f;
    lightAmbient0[1] = 0.0f;
    lightAmbient0[2] = 0.0f;
    lightAmbient0[3] = 1.0f;
	lightAmbient1[0] = 0.0f;
    lightAmbient1[1] = 0.0f;
    lightAmbient1[2] = 0.0f;
    lightAmbient1[3] = 1.0f;
	lightAmbient2[0] = 0.0f;
    lightAmbient2[1] = 0.0f;
    lightAmbient2[2] = 0.0f;
    lightAmbient2[3] = 1.0f;
	
    lightDiffuse0[0] = 1.0f;
    lightDiffuse0[1] = 0.0f;
    lightDiffuse0[2] = 0.0f;
    lightDiffuse0[3] = 1.0f;
	lightDiffuse1[0] = 0.0f;
    lightDiffuse1[1] = 1.0f;
    lightDiffuse1[2] = 0.0f;
    lightDiffuse1[3] = 1.0f;
	 lightDiffuse2[0] = 0.0f;
    lightDiffuse2[1] = 0.0f;
    lightDiffuse2[2] = 1.0f;
    lightDiffuse2[3] = 1.0f;
	
    lightSpecular0[0] = 1.0f;
    lightSpecular0[1] = 0.0f;
    lightSpecular0[2] = 0.0f;
    lightSpecular0[3] = 1.0f;
	lightSpecular1[0] = 0.0f;
    lightSpecular1[1] = 1.0f;
    lightSpecular1[2] = 0.0f;
    lightSpecular1[3] = 1.0f;
	 lightSpecular2[0] = 0.0f;
    lightSpecular2[1] = 0.0f;
    lightSpecular2[2] = 1.0f;
    lightSpecular2[3] = 1.0f;
	
    lightPosition0[0] = 100.0f;
    lightPosition0[1] = 100.0f;
    lightPosition0[2] = 100.0f;
    lightPosition0[3] = 1.0f;
    lightPosition1[0] = -100.0f;
    lightPosition1[1] = 100.0f;
    lightPosition1[2] = 100.0f;
    lightPosition1[3] = 1.0f;
	lightPosition2[0] = 0.0f;
    lightPosition2[1] = 0.0f;
    lightPosition2[2] = 100.0f;
    lightPosition2[3] = 1.0f;
    
    material_ambient[0] = 0.0f;
    material_ambient[1] = 0.0f;
    material_ambient[2] = 0.0f;
    material_ambient[3] = 1.0f;
    material_diffuse[0] = 1.0f;
    material_diffuse[1] = 1.0f;
    material_diffuse[2] = 1.0f;
    material_diffuse[3] = 1.0f;
    material_specular[0] = 1.0f;
    material_specular[1] = 1.0f;
    material_specular[2] = 1.0f;
    material_specular[3] = 1.0f;
    material_shininess = 50.0f;
	self = [super initWithFrame:frame];
	if(self)
	{
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *) super.layer;
		
		eaglLayer.opaque = YES;
		eaglLayer.drawableProperties = [NSDictionary
		dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
		kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, 
		kEAGLDrawablePropertyColorFormat, nil];
		
		eaglContext = [[EAGLContext alloc] 
		initWithAPI:kEAGLRenderingAPIOpenGLES3];
		
		if(eaglContext == nil)
		{
			[self release];
			return (nil);
		}
		[EAGLContext setCurrentContext:eaglContext];
		
		glGenFramebuffers(1, &defaultFramebuffer);
		glGenRenderbuffers(1, &colorRenderbuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
		
		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
		
		GLint backingWidth;
		GLint backingHeight;
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
		
		glGenRenderbuffers(1, &depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
		
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,  GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
		
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed To Create Complete FrameBuffer Object %x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
			glDeleteFramebuffers(1, &defaultFramebuffer);
			glDeleteRenderbuffers(1, &colorRenderbuffer);
			glDeleteRenderbuffers(1, &depthRenderbuffer);
			[self release];
			return (nil);
		}
		
		printf("Renderer : %s | GL Version : %s | GLSL Version :%s \n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
				
		isAnimating = NO;
		
		animationFrameInterval = 60;
		//Vertex Shader
		vertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);
        
		 const GLchar *vertexShaderSourceCodePerFragment =
        "#version 300 es" \
       "\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position0;"\
		"uniform vec4 u_light_position1;"\
		"uniform vec4 u_light_position2;"\
		"out vec3 transformed_normals;" \
		"out vec3 light_direction0;" \
		"out vec3 light_direction1;" \
		"out vec3 light_direction2;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction0 = vec3(u_light_position0) - eye_coordinates.xyz;" \
		"light_direction1 = vec3(u_light_position1) - eye_coordinates.xyz;" \
		"light_direction2 = vec3(u_light_position2) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

        glShaderSource(vertexShaderObjectPerFragment, 1, (const GLchar **)&vertexShaderSourceCodePerFragment, NULL);
        
		glCompileShader(vertexShaderObjectPerFragment);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(vertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Per Fragment Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		//fragment shader
		iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
		
		 fragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);
		 
		 const GLchar *fragmentShaderSourceCodePerFragment =
        "#version 300 es" \
       "\n" \
        "precision highp float;" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction0;" \
		"in vec3 light_direction1;" \
		"in vec3 light_direction2;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La0;" \
		"uniform vec3 u_La1;" \
		"uniform vec3 u_La2;" \
		"uniform vec3 u_Ld0;" \
		"uniform vec3 u_Ld1;" \
		"uniform vec3 u_Ld2;" \
		"uniform vec3 u_Ls0;" \
		"uniform vec3 u_Ls1;" \
		"uniform vec3 u_Ls2;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_double_tap;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color0;" \
		"vec3 phong_ads_color1;" \
		"vec3 phong_ads_color2;" \
		"vec3 phong_ads_color;" \
		"if(u_double_tap==1)" \
		"{" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction0=normalize(light_direction0);" \
		"vec3 ambient0 = u_La0 * u_Ka;" \
		"float tn_dot_ld0 = max(dot(normalized_transformed_normals, normalized_light_direction0),0.0);" \
		"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;" \
		"vec3 reflection_vector0 = reflect(-normalized_light_direction0, normalized_transformed_normals);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color0=ambient0 + diffuse0 + specular0;" \

	
		"vec3 normalized_light_direction1=normalize(light_direction1);" \
		"vec3 ambient1 = u_La1 * u_Ka;" \
		"float tn_dot_ld1 = max(dot(normalized_transformed_normals, normalized_light_direction1),0.0);" \
		"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;" \
		"vec3 reflection_vector1 = reflect(-normalized_light_direction1, normalized_transformed_normals);" \
		"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color1=ambient1 + diffuse1 + specular1;" \

		"vec3 normalized_light_direction2=normalize(light_direction2);" \
		"vec3 ambient2 = u_La2 * u_Ka;" \
		"float tn_dot_ld2 = max(dot(normalized_transformed_normals, normalized_light_direction2),0.0);" \
		"vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;" \
		"vec3 reflection_vector2 = reflect(-normalized_light_direction2, normalized_transformed_normals);" \
		"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color2=ambient2 + diffuse2 + specular2;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color0 = vec3(1.0, 1.0, 1.0);" \
		"phong_ads_color1 = vec3(1.0, 1.0, 1.0);" \
		"phong_ads_color2 = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"phong_ads_color = phong_ads_color0 + phong_ads_color1 + phong_ads_color2;" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";


		/*"in vec3 phong_ads_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";*/


        glShaderSource(fragmentShaderObjectPerFragment, 1, (const GLchar **)&fragmentShaderSourceCodePerFragment, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObjectPerFragment);
        glGetShaderiv(fragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Per Fragment Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		vertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);
		const GLchar *vertexShaderSourceCodePerVertex = 
		  "#version 300 es" \
       "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        
        "uniform int u_double_tap;" \
        
        "uniform vec3 u_Ld0;" \
        "uniform vec3 u_Ld1;" \
        "uniform vec3 u_Ld2;" \
        "uniform vec3 u_La0;" \
        "uniform vec3 u_La1;" \
        "uniform vec3 u_La2;" \
        "uniform vec3 u_Ls0;" \
        "uniform vec3 u_Ls1;" \
        "uniform vec3 u_Ls2;" \
        "uniform vec4 u_light_position0;"\
        "uniform vec4 u_light_position1;"\
        "uniform vec4 u_light_position2;"\
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Ks;" \
        "uniform vec3 u_Kd;" \
        "uniform float u_material_shininess;" \
        
        "out vec3 phong_ads_color0;" \
        "out vec3 phong_ads_color1;" \
        "out vec3 phong_ads_color2;" \
        "void main(void)" \
        "{" \
        "if(u_double_tap == 1)" \
        "{" \
        "vec4 eyeCoordinates =  u_view_matrix * u_model_matrix * vPosition;" \
        "vec3 tnorm = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
        "vec3 light_direction  = normalize(vec3(u_light_position0) - eyeCoordinates.xyz);" \
        "vec3 ambient0 = u_La0 * u_Ka;"\
        "vec3 diffuse_light0 = u_Ld0 * u_Kd * max(dot(light_direction, tnorm), 0.0);" \
        "vec3 reflection_vector0 = reflect(-light_direction, tnorm);"\
        "vec3 viewer_vector0 = normalize(-eyeCoordinates.xyz);" \
        "vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, viewer_vector0), 0.0),u_material_shininess);" \
        "phong_ads_color0=ambient0 + diffuse_light0 + specular0;" \
        
        "vec4 eyeCoordinates1 =  u_view_matrix * u_model_matrix * vPosition;" \
        "vec3 tnorm1 = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
        "vec3 light_direction1  = normalize(vec3(u_light_position1) - eyeCoordinates1.xyz);" \
        "vec3 ambient1 = u_La1 * u_Ka;"\
        "vec3 diffuse_light1 = u_Ld1 * u_Kd * max(dot(light_direction1, tnorm1), 0.0);" \
        "vec3 reflection_vector1 = reflect(-light_direction1, tnorm1);"\
        "vec3 viewer_vector1 = normalize(-eyeCoordinates1.xyz);" \
        "vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, viewer_vector1), 0.0),u_material_shininess);" \
        "phong_ads_color1=ambient1 + diffuse_light1 + specular1;" \
        
        "vec4 eyeCoordinates2 =  u_view_matrix * u_model_matrix * vPosition;" \
        "vec3 tnorm2 = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
        "vec3 light_direction2  = normalize(vec3(u_light_position2) - eyeCoordinates2.xyz);" \
        "vec3 ambient2 = u_La2 * u_Ka;"\
        "vec3 diffuse_light2 = u_Ld2 * u_Kd * max(dot(light_direction2, tnorm2), 0.0);" \
        "vec3 reflection_vector2 = reflect(-light_direction2, tnorm2);"\
        "vec3 viewer_vector2 = normalize(-eyeCoordinates2.xyz);" \
        "vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, viewer_vector2), 0.0),u_material_shininess);" \
        "phong_ads_color2=ambient2 + diffuse_light2 + specular2;" \
        "}" \
        "else"\
        "{"\
        "phong_ads_color0 = vec3(1.0, 1.0, 1.0);"\
        "phong_ads_color1 = vec3(1.0, 1.0, 1.0);"\
        "phong_ads_color2 = vec3(1.0, 1.0, 1.0);"\
        "}"\
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
		/*"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \

		"uniform int u_SingleTapPressed;" \

		"uniform vec3 u_Ld0;" \
		"uniform vec3 u_Ld1" \
		"uniform vec3 u_Ld2;" \
		"uniform vec3 u_La0;" \
		"uniform vec3 u_La1;" \
		"uniform vec3 u_La2;" \
		"uniform vec3 u_Ls0;" \
		"uniform vec3 u_Ls1;" \
		"uniform vec3 u_Ls2;" \
		"uniform vec4 u_light_position0;"\
		"uniform vec4 u_light_position1;"\
		"uniform vec4 u_light_position2;"\
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Ks;" \
		"uniform vec3 u_Kd;" \
		"uniform float u_material_shininess;" \

		"out vec3 phong_ads_color0;" \
		"out vec3 phong_ads_color1;" \
		"out vec3 phong_ads_color2;" \
		"void main(void)" \
		"{" \
		"if(u_SingleTapPressed == 1)" \
		"{" \
		"vec4 eyeCoordinates =  u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 tnorm = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
		"vec3 ambient = u_La * u_Ka;"\
		"vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
		"vec3 light_direction0  = normalize(vec3(u_light_position0) - eyeCoordinates.xyz);" \
		"vec3 diffuse_light0 = u_Ld0 * u_Kd * max(dot(light_direction0, tnorm), 0.0);" \
		"vec3 reflection_vector0 = reflect(-light_direction0, tnorm);"\
		"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, viewer_vector), 0.0),u_material_shininess);" \
		"phong_ads_color0=ambient + diffuse_light0 + specular0;" \
		
		"vec3 light_direction1  = normalize(vec3(u_light_position1) - eyeCoordinates.xyz);" \
		"vec3 diffuse_light1 = u_Ld1 * u_Kd * max(dot(light_direction1, tnorm), 0.0);" \
		"vec3 reflection_vector1 = reflect(-light_direction1, tnorm);"\
		"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, viewer_vector), 0.0),u_material_shininess);" \
		"phong_ads_color1=ambient + diffuse_light1 + specular1;" \
	
		"vec3 light_direction2  = normalize(vec3(u_light_position2) - eyeCoordinates.xyz);" \
		"vec3 diffuse_light2 = u_Ld2 * u_Kd * max(dot(light_direction2, tnorm), 0.0);" \
		"vec3 reflection_vector2 = reflect(-light_direction2, tnorm);"\
		"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, viewer_vector), 0.0),u_material_shininess);" \
		"phong_ads_color2=ambient + diffuse_light2 + specular2;" \
		//"phong_ads_color = phong_ads_color0 + phong_ads_color1 + phong_ads_color2;"
		"}" \
		"else"\
		"{"\
        "phong_ads_color0 = vec3(1.0, 1.0, 1.0);"\
        "phong_ads_color1 = vec3(1.0, 1.0, 1.0);"\
        "phong_ads_color2 = vec3(1.0, 1.0, 1.0);"\
		"}"\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";*/

        glShaderSource(vertexShaderObjectPerVertex, 1, (const GLchar **)&vertexShaderSourceCodePerVertex, NULL);
        
		glCompileShader(vertexShaderObjectPerVertex);
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        glGetShaderiv(vertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		//fragment shader Per Vertex
		iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
		
		 fragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);
		 
		 const GLchar *fragmentShaderSourceCodePerVertex =
        "#version 300 es" \
       "\n" \
        "precision highp float;" \
		"in vec3 phong_ads_color0;"\
        "in vec3 phong_ads_color1;"\
        "in vec3 phong_ads_color2;"\
        "vec3 phong_ads_color;"\
			"out vec4 FragColor;"\
			"void main(void)"\
			"{"\
        "phong_ads_color = phong_ads_color0 + phong_ads_color1 + phong_ads_color2;" \
			"FragColor = vec4(phong_ads_color, 1.0);"\
			"}";


        glShaderSource(fragmentShaderObjectPerVertex, 1, (const GLchar **)&fragmentShaderSourceCodePerVertex, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObjectPerVertex);
        glGetShaderiv(fragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		shaderProgramObjectPerVertex = glCreateProgram();
		
		glAttachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
		glAttachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);
		[self MyFunc:shaderProgramObjectPerVertex];
		
		/*
		glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");

		 glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
	gModelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

	gLKeyPressedUniform = glGetUniformLocation(shaderProgramObject, "u_LKeyPressed");

	gLdUniform = glGetUniformLocation(shaderProgramObject, "u_Ld");
	gLaUniform = glGetUniformLocation(shaderProgramObject, "u_La");
	gLsUniform = glGetUniformLocation(shaderProgramObject, "u_Ls");

	gKdUniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
	gKaUniform = glGetUniformLocation(shaderProgramObject, "u_Ka");

	gLightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
	gMaterialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
*/
        Sphere sphere;
	sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = sphere.getNumberOfSphereVertices();
	gNumElements = sphere.getNumberOfSphereElements();



	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	//position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//normal
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);


	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	glBindVertexArray(0);

        glEnable(GL_DEPTH_TEST);
        
        glDepthFunc(GL_LEQUAL);
        
       // glEnable(GL_CULL_FACE);
        
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			singleTap = 0;
        doubleTap = 0;
		perspectiveProjectionMatrix = vmath::mat4::identity();
		
		UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
		[singleTapGestureRecognizer setNumberOfTapsRequired:1];
		[singleTapGestureRecognizer setNumberOfTouchesRequired:1];
		[singleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:singleTapGestureRecognizer];
		
		UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
		[doubleTapGestureRecognizer setNumberOfTapsRequired:2];
		[doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
		[doubleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:doubleTapGestureRecognizer];
		
		[singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
		
		UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc ] initWithTarget : self action : @selector(onSwipe:)];
		[self addGestureRecognizer:swipeGestureRecognizer];      
		
		UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
		[self addGestureRecognizer:longPressGestureRecognizer];
	}
	return (self);		
}

-(void)MyFunc:(GLuint)iPgmObject
{
    GLint iInfoLogLength = 0;
    //GLint iShaderCompiledStatus = 0;
    char *szInfoLog = NULL;
	shaderProgramObject = glCreateProgram();
	shaderProgramObject = iPgmObject;
	
glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");

		 glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
	gModelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

	gSingleTapUniform = glGetUniformLocation(shaderProgramObject, "u_SingleTap");
	gDoubleTapUniform = glGetUniformLocation(shaderProgramObject, "u_double_tap");

	gLd0Uniform = glGetUniformLocation(shaderProgramObject, "u_Ld0");
	gLd1Uniform = glGetUniformLocation(shaderProgramObject, "u_Ld1");
	gLd2Uniform = glGetUniformLocation(shaderProgramObject, "u_Ld2");
	
	gLa0Uniform = glGetUniformLocation(shaderProgramObject, "u_La0");
	gLa1Uniform = glGetUniformLocation(shaderProgramObject, "u_La1");
	gLa2Uniform = glGetUniformLocation(shaderProgramObject, "u_La2");
	
	gLs0Uniform = glGetUniformLocation(shaderProgramObject, "u_Ls0");
	gLs1Uniform = glGetUniformLocation(shaderProgramObject, "u_Ls1");
	gLs2Uniform = glGetUniformLocation(shaderProgramObject, "u_Ls2");

	gKdUniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
	gKaUniform = glGetUniformLocation(shaderProgramObject, "u_Ka");

	gLightPositionUniform0 = glGetUniformLocation(shaderProgramObject, "u_light_position0");
	gLightPositionUniform1 = glGetUniformLocation(shaderProgramObject, "u_light_position1");
	gLightPositionUniform2 = glGetUniformLocation(shaderProgramObject, "u_light_position2");
	
	gMaterialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
	
}
	/*
	-(void) drawRect:(CGRect)rect
	{
	
	}
	*/
	
	+(Class)layerClass
	{
		return ([CAEAGLLayer class]);
	}
	
	-(void)drawView:(id)sender
	{
		
		[EAGLContext setCurrentContext:eaglContext];
		
		glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		
		glUseProgram(shaderProgramObject);
		if (doubleTap == 1)
		{
       if((singleTap % 2) == 0) //Per Vertex
			{
			shaderProgramObjectPerVertex = glCreateProgram();
			glAttachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
			glAttachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);
		
			[self MyFunc:shaderProgramObjectPerVertex];
			}
			else
			{
				shaderProgramObjectPerFragment = glCreateProgram();
			glAttachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);
			glAttachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);
		
			[self MyFunc:shaderProgramObjectPerFragment];
			}
		glUniform1i(gDoubleTapUniform, 1);
		
		glUniform3fv(gLd0Uniform, 1, lightDiffuse0);
		glUniform3fv(gLd1Uniform, 1, lightDiffuse1);
		glUniform3fv(gLd2Uniform, 1, lightDiffuse2);
		glUniform3fv(gLa0Uniform, 1, lightAmbient0);
		glUniform3fv(gLa1Uniform, 1, lightAmbient1);
		glUniform3fv(gLa2Uniform, 1, lightAmbient2);
		glUniform3fv(gLs0Uniform, 1, lightSpecular0);
		glUniform3fv(gLs1Uniform, 1, lightSpecular1);
		glUniform3fv(gLs2Uniform, 1, lightSpecular2);
		//glUniform4fv(gLightPositionUniform, 1, lightPosition);

		myRotateMatrix[0] = 0.0f;
		myRotateMatrix[1] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix[2] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix[3] = 1.0f;
		glUniform4fv(gLightPositionUniform0, 1, myRotateMatrix);

		myRotateMatrix1[0] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix1[1] = 0.0f;
		myRotateMatrix1[2] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix1[3] = 1.0f;
		glUniform4fv(gLightPositionUniform1, 1, myRotateMatrix1);

		myRotateMatrix2[0] = 100 * sin(2 * M_PI * angle_red);
		myRotateMatrix2[1] = 100.0f * cos(2 * M_PI * angle_red);
		myRotateMatrix2[2] = -2.0f;
		myRotateMatrix2[3] = 1.0f;
		glUniform4fv(gLightPositionUniform2, 1, myRotateMatrix2);



		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

//float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };
		
		}
		else
		{
			glUniform1i(gDoubleTapUniform, 0);
		}


        vmath::mat4 modelMatrix = vmath::mat4::identity();
        vmath::mat4 viewMatrix = vmath::mat4::identity();
	//mat4 rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);

	//rotationMatrix = vmath::rotate(gAngle, gAngle, gAngle);

	//modelViewMatrix = modelMatrix * rotationMatrix; //imp

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	

	glBindVertexArray(0);

	
		glUseProgram(0);
		
		glBindRenderbuffer(GL_RENDERBUFFER , colorRenderbuffer);
		[eaglContext presentRenderbuffer:GL_RENDERBUFFER];
		
        angle_red += 0.1f;
	}
	
	-(void)layoutSubviews
	{
		GLint width;
		GLint height;
	
		glBindRenderbuffer (GL_RENDERBUFFER, colorRenderbuffer);
		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
		
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
		
		glGenRenderbuffers(1, &depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
		
		glViewport(0, 0, width, height);
		
		GLfloat fwidth = (GLfloat)width;
		GLfloat fheight = (GLfloat)height;
		/*if (width <= height)
			perspectiveProjectionMatrix = vmath::ortho(-100.0f, 100.0f, (-100.0f * (fheight / fwidth)), (100.0f * (fheight / fwidth)), -100.0f, 100.0f);
		else
            perspectiveProjectionMatrix = vmath::ortho((-100.0f * (fwidth / fheight)), (100.0f * (fwidth / fheight)), -100.0f, 100.0f, -100.0f, 100.0f);
    */
        perspectiveProjectionMatrix = vmath::perspective(45.0f, fwidth/fheight, 0.1f, 100.0f);
        
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed To Create Complete FrameBuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
		}
		
		[self drawView:nil];
	}
	
	-(void)startAnimation
	{
		if(!isAnimating)
		{
			displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
			
			[displayLink setPreferredFramesPerSecond:animationFrameInterval];
			[displayLink addToRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
			
			isAnimating = YES;
		}
	}
	
	-(void)stopAnimation
	{
		if(isAnimating)
		{
			[displayLink invalidate];
			displayLink = nil;
			isAnimating = NO;
			
		}
	}
	
	//to become first responder
	-(BOOL)acceptsFirstResponder
	{
		return (YES);
	}
	
	-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event
	{
		
	}
	
	-(void) onSingleTap:(UITapGestureRecognizer *)gr
	{
		++singleTap;
		if(singleTap > 2)
			singleTap = 1;
	}
	
	-(void) onDoubleTap:(UITapGestureRecognizer *)gr
	{
	++doubleTap;
		if(doubleTap > 1)
			doubleTap = 0;
	}
	
	-(void) onSwipe:(UISwipeGestureRecognizer *)gr
	{
		[self release];
		exit(0);
	}
	
	-(void)onLongPress:(UILongPressGestureRecognizer *)gr
	{
	
	}
	
	-(void)dealloc
	{
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}
	// destroy element vbo
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

		glDetachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
		
		glDetachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);
		
		glDeleteShader(vertexShaderObjectPerVertex);
		vertexShaderObjectPerVertex = 0;
		
		glDeleteShader(fragmentShaderObjectPerVertex);
		fragmentShaderObjectPerVertex = 0;
		
		glDeleteProgram(shaderProgramObjectPerVertex);
		shaderProgramObjectPerVertex = 0;

			glDetachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);
		
		glDetachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);
		
		glDeleteShader(vertexShaderObjectPerFragment);
		vertexShaderObjectPerFragment = 0;
		
		glDeleteShader(fragmentShaderObjectPerFragment);
		fragmentShaderObjectPerFragment = 0;
		
		glDeleteProgram(shaderProgramObjectPerFragment);
		shaderProgramObjectPerFragment = 0;

		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
		
		if(depthRenderbuffer)
		{
			glDeleteRenderbuffers(1, &depthRenderbuffer);
			depthRenderbuffer = 0;
		}
		
		if(colorRenderbuffer)
		{
			glDeleteRenderbuffers(1, &colorRenderbuffer);
			colorRenderbuffer = 0;
		}
			
		if(defaultFramebuffer)
		{
			glDeleteFramebuffers(1, &defaultFramebuffer);
			defaultFramebuffer = 0;
		}
		
		if([EAGLContext currentContext] == eaglContext)
		{
			[EAGLContext setCurrentContext:nil];
		}
		[eaglContext release];
		eaglContext = nil;
		
		[super dealloc];
	}
	
	@end
