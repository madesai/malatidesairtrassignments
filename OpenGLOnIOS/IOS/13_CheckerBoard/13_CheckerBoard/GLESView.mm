
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"
//Class GLESView ()

#define checkImageWidth 64
#define checkImageHeight 64

enum
{
    MALATI_ATTRIBUTE_VERTEX = 0,
    MALATI_ATTRIBUTE_COLOR,
    MALATI_ATTRIBUTE_NORMAL,
    MALATI_ATTRIBUTE_TEXTURE0,
};

@implementation GLESView
{
	EAGLContext *eaglContext;
	
	GLuint defaultFramebuffer;
	GLuint colorRenderbuffer;
	GLuint depthRenderbuffer;
	
	id displayLink;
	NSInteger animationFrameInterval;
	BOOL isAnimating;
	
	GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
 GLuint gVao_Cube;

GLuint gVbo_Cube_Position;

GLuint gVbo_Cube_Texture;

    GLuint mvpUniform;
    
	
    vmath::mat4 perspectiveProjectionMatrix;
	GLuint gTexture_sampler_uniform;

	GLuint texName;
GLubyte checkImage[checkImageHeight][checkImageWidth][4];

}

-(id) initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if(self)
	{
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *) super.layer;
		
		eaglLayer.opaque = YES;
		eaglLayer.drawableProperties = [NSDictionary
		dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
		kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, 
		kEAGLDrawablePropertyColorFormat, nil];
		
		eaglContext = [[EAGLContext alloc] 
		initWithAPI:kEAGLRenderingAPIOpenGLES3];
		
		if(eaglContext == nil)
		{
			[self release];
			return (nil);
		}
		[EAGLContext setCurrentContext:eaglContext];
		
		glGenFramebuffers(1, &defaultFramebuffer);
		glGenRenderbuffers(1, &colorRenderbuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
		
		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
		
		GLint backingWidth;
		GLint backingHeight;
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
		
		glGenRenderbuffers(1, &depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
		
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,  GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
		
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed To Create Complete FrameBuffer Object %x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
			glDeleteFramebuffers(1, &defaultFramebuffer);
			glDeleteRenderbuffers(1, &colorRenderbuffer);
			glDeleteRenderbuffers(1, &depthRenderbuffer);
			[self release];
			return (nil);
		}
		
		printf("Renderer : %s | GL Version : %s | GLSL Version :%s \n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
				
		isAnimating = NO;
		
		animationFrameInterval = 60;
		//Vertex Shader
		vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
		 const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec2 vTexture0_Coord;" \
        "out vec2 out_texture0_coord;" \
        "uniform mat4 u_mvp_matrix;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_texture0_coord = vTexture0_Coord;" \
        "}";
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
		glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		//fragment shader
		iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
		
		 fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
		 
		 const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
		"in vec2 out_texture0_coord;" \
		"uniform sampler2D u_texture0_sampler;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
		//"vec3 tex=vec3(texture(u_texture0_sampler, out_texture0_coord));"
       // "FragColor = vec4(tex, 1.0f);" 
	    "FragColor = texture(u_texture0_sampler, out_texture0_coord);" \
        "}";
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		shaderProgramObject = glCreateProgram();
		
		glAttachShader(shaderProgramObject, vertexShaderObject);
		glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

		 glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		 // get MVP uniform location
        mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        
		gTexture_sampler_uniform=glGetUniformLocation(shaderProgramObject,"u_texture0_sampler");
        
	//	pyramid_texture=[self loadTextureFromBMPFile:@"Stone" :@"bmp"];
      //  cube_texture=[self loadTextureFromBMPFile:@"Vijay_Kundali" :@"bmp"];
		//gTexture_Smiley = [self loadTextureFromBMPFile:@"Smiley.bmp" :@"bmp"];
		
		[self loadTextureFromBMPFile];
		
	const GLfloat cubeTexcoords[] =
	{
		0.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 1.0f,
		1.0f, 0.0f,
	};

	glGenVertexArrays(1, &gVao_Cube);
	glBindVertexArray(gVao_Cube);

	//position
	glGenBuffers(1, &gVbo_Cube_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Cube_Position);
	glBufferData(GL_ARRAY_BUFFER, 4*12*sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color
	glGenBuffers(1, &gVbo_Cube_Texture);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Cube_Texture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexcoords), cubeTexcoords, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_TEXTURE0);


	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

        glEnable(GL_DEPTH_TEST);
        
        glDepthFunc(GL_LEQUAL);
        
       // glEnable(GL_CULL_FACE);
        glEnable(GL_TEXTURE_2D);
		
		//LoadGLTexture
		//printf("Before MakeCheckImage\n");
		//MakeCheckImage
	/*	int i, j, c;
	printf("Inside MakeCheckImage\n");
	for (i = 0; i < checkImageHeight; i++)
	{
		for (j = 0; j < checkImageWidth; j++)
		{
			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;

			checkImage[i][j][0] = (GLubyte)c;
			checkImage[i][j][1] = (GLubyte)c;
			checkImage[i][j][2] = (GLubyte)c;
			checkImage[i][j][3] = (GLubyte)255;

		}
	}
	printf("exiting MakeCheckImage\n");

	printf(gpFile, "After MakeCheckImage\n");
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glGenTextures(1, &texName);
	glBindTexture(GL_TEXTURE_2D, texName);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, checkImageWidth, checkImageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, checkImage);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
*/
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		perspectiveProjectionMatrix = vmath::mat4::identity();
		
		UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
		[singleTapGestureRecognizer setNumberOfTapsRequired:1];
		[singleTapGestureRecognizer setNumberOfTouchesRequired:1];
		[singleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:singleTapGestureRecognizer];
		
		UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
		[doubleTapGestureRecognizer setNumberOfTapsRequired:2];
		[doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
		[doubleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:doubleTapGestureRecognizer];
		
		[singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
		
		UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc ] initWithTarget : self action : @selector(onSwipe:)];
		[self addGestureRecognizer:swipeGestureRecognizer];      
		
		UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
		[self addGestureRecognizer:longPressGestureRecognizer];
	}
	return (self);		
}

-(void)loadTextureFromBMPFile
{
	/*NSRect rect = [self bounds];

	width = rect.size.width;
	height= rect.size.height;*/
	
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glGenTextures(1, &texName);
	glBindTexture(GL_TEXTURE_2D, texName);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	int i, j, c;
	
	for (i = 0; i < checkImageHeight; i++)
	{
		for (j = 0; j < checkImageWidth; j++)
		{
			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;

			checkImage[i][j][0] = (GLubyte)c;
			checkImage[i][j][1] = (GLubyte)c;
			checkImage[i][j][2] = (GLubyte)c;
			checkImage[i][j][3] = (GLubyte)255;
            printf("%d , %d, %d, %d\n", checkImage[i][j][0], checkImage[i][j][1],checkImage[i][j][2], checkImage[i][j][3]);
		}
	}
    printf("In MakeCheckImage");
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, checkImageWidth, checkImageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, checkImage);
	
	/*NSString *textureFileNameWithPath = [[NSBundle mainBundle]pathForResource:texFileName ofType:extension];
	
	UIImage *bmpImage = [[UIImage alloc]initWithContentsOfFile:textureFileNameWithPath];
	if(!bmpImage)
	{
		NSLog(@"Can't find %@", textureFileNameWithPath);
		return(0);
	}
	
	CGImageRef cgImage = bmpImage.CGImage;
	
	int w = (int)CGImageGetWidth(cgImage);
	int h = (int)CGImageGetHeight(cgImage);
	CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
	void* pixels = (void *)CFDataGetBytePtr(imageData);
	
	GLuint bmpTextures;
	glGenTextures(1, &bmpTextures);
	
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glBindTexture(GL_TEXTURE_2D, bmpTextures);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 w,
                 h,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels);
    
	glGenerateMipmap(GL_TEXTURE_2D);
    
    CFRelease(imageData);
    return(bmpTextures);*/
	
}


	/*
	-(void) drawRect:(CGRect)rect
	{
	
	}
	*/
	
	+(Class)layerClass
	{
		return ([CAEAGLLayer class]);
	}
	
	-(void)drawView:(id)sender
	{
		//static float gAnglePyramid = 0.0f;
		//static float gAngleCube = 0.0f;
		[EAGLContext setCurrentContext:eaglContext];
		
		glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		
		glUseProgram(shaderProgramObject);
		
      
        vmath::mat4 modelViewMatrix = vmath::mat4::identity();
        vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
      //  vmath::mat4 rotationMatrix = vmath::mat4::identity();

	modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.6f); //for perspective(4rth change)

	//rotationMatrix = vmath::rotate(gAnglePyramid, 0.0f, 1.0f, 0.0f);
	//modelViewMatrix = modelViewMatrix * rotationMatrix; //imp

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//triangle Block
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	/*const GLfloat cubeVertices[] =
	{
		-2.0f, -1.0f, 0.0f,
		-2.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, -1.0f, 0.0f
	};*/
	GLfloat quadTexture[12];
	quadTexture[0] = -2.0f;
	quadTexture[1] = -1.0f;
	quadTexture[2] = 0.0f;
	quadTexture[3] = -2.0f;
	quadTexture[4] = 1.0f;
	quadTexture[5] = 0.0f;
	quadTexture[6] = 0.0f;
	quadTexture[7] = 1.0f;
	quadTexture[8] = 0.0f;
	quadTexture[9] = 0.0f;
	quadTexture[10] = -1.0f;
	quadTexture[11] = 0.0f;

	glBindVertexArray(gVao_Cube);

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Cube_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadTexture), quadTexture, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texName);
	glUniform1i(gTexture_sampler_uniform, 0);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //Imp
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

	glBindVertexArray(0);


	GLfloat quadTiltTexture[12];
	quadTiltTexture[0] = 1.0f;
	quadTiltTexture[1] = -1.0f;
	quadTiltTexture[2] = 0.0f;
	quadTiltTexture[3] = 1.0f;
	quadTiltTexture[4] = 1.0f;
	quadTiltTexture[5] = 0.0f;
	quadTiltTexture[6] = 2.41421f;
	quadTiltTexture[7] = 1.0f;
	quadTiltTexture[8] = -1.1421f;
	quadTiltTexture[9] = 2.41421f;
	quadTiltTexture[10] = -1.0f;
	quadTiltTexture[11] = -1.41421f;

	glBindVertexArray(gVao_Cube);

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Cube_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadTiltTexture), quadTiltTexture, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texName);
	glUniform1i(gTexture_sampler_uniform, 0);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //Imp
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

	glBindVertexArray(0);
	glUseProgram(0);

		 printf("In MakeCheckImage11");
		glBindRenderbuffer(GL_RENDERBUFFER , colorRenderbuffer);
		[eaglContext presentRenderbuffer:GL_RENDERBUFFER];
		
	
		
	}
	
	-(void)layoutSubviews
	{
		GLint width;
		GLint height;
	
		glBindRenderbuffer (GL_RENDERBUFFER, colorRenderbuffer);
		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
		
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
		
		glGenRenderbuffers(1, &depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
		
		glViewport(0, 0, width, height);
		
		GLfloat fwidth = (GLfloat)width;
		GLfloat fheight = (GLfloat)height;
		/*if (width <= height)
			perspectiveProjectionMatrix = vmath::ortho(-100.0f, 100.0f, (-100.0f * (fheight / fwidth)), (100.0f * (fheight / fwidth)), -100.0f, 100.0f);
		else
            perspectiveProjectionMatrix = vmath::ortho((-100.0f * (fwidth / fheight)), (100.0f * (fwidth / fheight)), -100.0f, 100.0f, -100.0f, 100.0f);
    */
        perspectiveProjectionMatrix = vmath::perspective(60.0f, fwidth/fheight, 0.1f, 30.0f);
        
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed To Create Complete FrameBuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
		}
		
		[self drawView:nil];
	}
	
	-(void)startAnimation
	{
		if(!isAnimating)
		{
			displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
			
			[displayLink setPreferredFramesPerSecond:animationFrameInterval];
			[displayLink addToRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
			
			isAnimating = YES;
		}
	}
	
	-(void)stopAnimation
	{
		if(isAnimating)
		{
			[displayLink invalidate];
			displayLink = nil;
			isAnimating = NO;
			
		}
	}
	
	//to become first responder
	-(BOOL)acceptsFirstResponder
	{
		return (YES);
	}
	
	-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event
	{
		
	}
	
	-(void) onSingleTap:(UITapGestureRecognizer *)gr
	{
	
	}
	
	-(void) onDoubleTap:(UITapGestureRecognizer *)gr
	{
	
	}
	
	-(void) onSwipe:(UISwipeGestureRecognizer *)gr
	{
		[self release];
		exit(0);
	}
	
	-(void)onLongPress:(UILongPressGestureRecognizer *)gr
	{
	
	}
	
	-(void)dealloc
	{
	if (gVao_Cube)
	{
		glDeleteVertexArrays(1, &gVao_Cube);
		gVao_Cube = 0;
	}
	if (gVbo_Cube_Position)
	{
		glDeleteBuffers(1, &gVbo_Cube_Position);
		gVbo_Cube_Position = 0;
	}
	if (gVbo_Cube_Texture)
	{
		glDeleteBuffers(1, &gVbo_Cube_Texture);
		gVbo_Cube_Texture = 0;
	}
	
		glDetachShader(shaderProgramObject, vertexShaderObject);
		
		glDetachShader(shaderProgramObject, fragmentShaderObject);
		
		glDeleteShader(vertexShaderObject);
		vertexShaderObject = 0;
		
		glDeleteShader(fragmentShaderObject);
		fragmentShaderObject = 0;
		
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;

		if(depthRenderbuffer)
		{
			glDeleteRenderbuffers(1, &depthRenderbuffer);
			depthRenderbuffer = 0;
		}
		
		if(colorRenderbuffer)
		{
			glDeleteRenderbuffers(1, &colorRenderbuffer);
			colorRenderbuffer = 0;
		}
			
		if(defaultFramebuffer)
		{
			glDeleteFramebuffers(1, &defaultFramebuffer);
			defaultFramebuffer = 0;
		}
		
		if([EAGLContext currentContext] == eaglContext)
		{
			[EAGLContext setCurrentContext:nil];
		}
		[eaglContext release];
		eaglContext = nil;
		
		[super dealloc];
	}
	
	@end

   

