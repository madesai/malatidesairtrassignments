#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"
//Class GLESView ()

enum
{
    MALATI_ATTRIBUTE_VERTEX = 0,
    MALATI_ATTRIBUTE_COLOR,
    MALATI_ATTRIBUTE_NORMAL,
    MALATI_ATTRIBUTE_TEXTURE0,
};

@implementation GLESView
{
	EAGLContext *eaglContext;
	
	GLuint defaultFramebuffer;
	GLuint colorRenderbuffer;
	GLuint depthRenderbuffer;
	
	id displayLink;
	NSInteger animationFrameInterval;
	BOOL isAnimating;
	
	GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
   GLuint gVao_Triangle;
GLuint gVao_Square;//displayList

GLuint gVbo_Triangle_Position;
GLuint gVbo_Square_Position;

GLuint gVbo_Triangle_Color;
GLuint gVbo_Square_Color;
    GLuint mvpUniform;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id) initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if(self)
	{
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *) super.layer;
		
		eaglLayer.opaque = YES;
		eaglLayer.drawableProperties = [NSDictionary
		dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
		kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, 
		kEAGLDrawablePropertyColorFormat, nil];
		
		eaglContext = [[EAGLContext alloc] 
		initWithAPI:kEAGLRenderingAPIOpenGLES3];
		
		if(eaglContext == nil)
		{
			[self release];
			return (nil);
		}
		[EAGLContext setCurrentContext:eaglContext];
		
		glGenFramebuffers(1, &defaultFramebuffer);
		glGenRenderbuffers(1, &colorRenderbuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
		
		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
		
		GLint backingWidth;
		GLint backingHeight;
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
		
		glGenRenderbuffers(1, &depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
		
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,  GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
		
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed To Create Complete FrameBuffer Object %x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
			glDeleteFramebuffers(1, &defaultFramebuffer);
			glDeleteRenderbuffers(1, &colorRenderbuffer);
			glDeleteRenderbuffers(1, &depthRenderbuffer);
			[self release];
			return (nil);
		}
		
		printf("Renderer : %s | GL Version : %s | GLSL Version :%s \n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
				
		isAnimating = NO;
		
		animationFrameInterval = 60;
		//Vertex Shader
		vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
		 const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "out vec4 out_color;" \
        "uniform mat4 u_mvp_matrix;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_color = vColor;" \
        "}";
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
		glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		//fragment shader
		iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
		
		 fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
		 
		 const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
		"in vec4 out_color;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = out_color;" \
        "}";
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		shaderProgramObject = glCreateProgram();
		
		glAttachShader(shaderProgramObject, vertexShaderObject);
		glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_COLOR, "vColor");

		 glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		 // get MVP uniform location
        mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        
		const GLfloat triangleVertices[] =
        { 0.0f, 1.0f, 0.0f, // apex
            -1.0f, -1.0f, 0.0f, // left-bottom
            1.0f, -1.0f, 0.0f // right-bottom
        };
        const GLfloat triangleColors[] =
	{
		1.0f, 0.0f, 0.0f,//r
		0.0f, 1.0f, 0.0f,//g
		0.0f, 0.0f, 1.0f//b
	};

        glGenVertexArrays(1, &gVao_Triangle);
        glBindVertexArray(gVao_Triangle);
        
       //position
	glGenBuffers(1, &gVbo_Triangle_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Triangle_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	//color
	glGenBuffers(1, &gVbo_Triangle_Color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Triangle_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColors), triangleColors, GL_STATIC_DRAW);
	glVertexAttribPointer(MALATI_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(MALATI_ATTRIBUTE_COLOR);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        
		//Square
	const GLfloat squareVertices[] =
	{
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		1.0f, 1.0f, 0.0f
	};

	glGenVertexArrays(1, &gVao_Square);
	glBindVertexArray(gVao_Square);

	//position
	glGenBuffers(1, &gVbo_Square_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Square_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//Color (When u want to give single color then don't use vbo
	glVertexAttrib3f(MALATI_ATTRIBUTE_COLOR, 0.39f, 0.58f, 0.9294f); //cornflower Blue 
	glEnableVertexAttribArray(MALATI_ATTRIBUTE_COLOR);

        glEnable(GL_DEPTH_TEST);
        
        glDepthFunc(GL_LEQUAL);
        
        glEnable(GL_CULL_FACE);
        
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		perspectiveProjectionMatrix = vmath::mat4::identity();
		
		UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
		[singleTapGestureRecognizer setNumberOfTapsRequired:1];
		[singleTapGestureRecognizer setNumberOfTouchesRequired:1];
		[singleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:singleTapGestureRecognizer];
		
		UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
		[doubleTapGestureRecognizer setNumberOfTapsRequired:2];
		[doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
		[doubleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:doubleTapGestureRecognizer];
		
		[singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
		
		UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc ] initWithTarget : self action : @selector(onSwipe:)];
		[self addGestureRecognizer:swipeGestureRecognizer];      
		
		UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
		[self addGestureRecognizer:longPressGestureRecognizer];
	}
	return (self);		
}
	/*
	-(void) drawRect:(CGRect)rect
	{
	
	}
	*/
	
	+(Class)layerClass
	{
		return ([CAEAGLLayer class]);
	}
	
	-(void)drawView:(id)sender
	{
		[EAGLContext setCurrentContext:eaglContext];
		
		glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		
		glUseProgram(shaderProgramObject);
		
		vmath::mat4 modelViewMatrix = vmath::mat4::identity();
		vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
	
        modelViewMatrix = vmath::translate(-1.5f, 0.0f, -6.0f);
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix; // ORDER IS IMPORTANT
    
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		
		glBindVertexArray(gVao_Triangle);

		
		glDrawArrays(GL_TRIANGLES, 0, 3);
		
		glBindVertexArray(0);
		
		//Square Block
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();

	modelViewMatrix = vmath::translate(1.5f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(gVao_Square);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //there is no QUAD in PP(4  rows)
glBindVertexArray(0);
		glUseProgram(0);
		
		glBindRenderbuffer(GL_RENDERBUFFER , colorRenderbuffer);
		[eaglContext presentRenderbuffer:GL_RENDERBUFFER];
		
	}
	
	-(void)layoutSubviews
	{
		GLint width;
		GLint height;
	
		glBindRenderbuffer (GL_RENDERBUFFER, colorRenderbuffer);
		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
		
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
		
		glGenRenderbuffers(1, &depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
		
		glViewport(0, 0, width, height);
		
		GLfloat fwidth = (GLfloat)width;
		GLfloat fheight = (GLfloat)height;
		/*if (width <= height)
			perspectiveProjectionMatrix = vmath::ortho(-100.0f, 100.0f, (-100.0f * (fheight / fwidth)), (100.0f * (fheight / fwidth)), -100.0f, 100.0f);
		else
            perspectiveProjectionMatrix = vmath::ortho((-100.0f * (fwidth / fheight)), (100.0f * (fwidth / fheight)), -100.0f, 100.0f, -100.0f, 100.0f);
    */
        perspectiveProjectionMatrix = vmath::perspective(45.0f, fwidth/fheight, 0.1f, 100.0f);
        
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed To Create Complete FrameBuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
		}
		
		[self drawView:nil];
	}
	
	-(void)startAnimation
	{
		if(!isAnimating)
		{
			displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
			
			[displayLink setPreferredFramesPerSecond:animationFrameInterval];
			[displayLink addToRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
			
			isAnimating = YES;
		}
	}
	
	-(void)stopAnimation
	{
		if(isAnimating)
		{
			[displayLink invalidate];
			displayLink = nil;
			isAnimating = NO;
			
		}
	}
	
	//to become first responder
	-(BOOL)acceptsFirstResponder
	{
		return (YES);
	}
	
	-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event
	{
		
	}
	
	-(void) onSingleTap:(UITapGestureRecognizer *)gr
	{
	
	}
	
	-(void) onDoubleTap:(UITapGestureRecognizer *)gr
	{
	
	}
	
	-(void) onSwipe:(UISwipeGestureRecognizer *)gr
	{
		[self release];
		exit(0);
	}
	
	-(void)onLongPress:(UILongPressGestureRecognizer *)gr
	{
	
	}
	
	-(void)dealloc
	{
		if (gVao_Triangle)
	{
		glDeleteVertexArrays(1, &gVao_Triangle);
		gVao_Triangle = 0;
	}
	if (gVao_Square)
	{
		glDeleteVertexArrays(1, &gVao_Square);
		gVao_Square = 0;
	}
	if (gVbo_Triangle_Position)
	{
		glDeleteBuffers(1, &gVbo_Triangle_Position);
		gVbo_Triangle_Position = 0;
	}
	if (gVbo_Triangle_Color)
	{
		glDeleteBuffers(1, &gVbo_Triangle_Color);
		gVbo_Triangle_Color = 0;
	}
	if (gVbo_Square_Position)
	{
		glDeleteBuffers(1, &gVbo_Square_Position);
		gVbo_Square_Position = 0;
	}
		
		glDetachShader(shaderProgramObject, vertexShaderObject);
		
		glDetachShader(shaderProgramObject, fragmentShaderObject);
		
		glDeleteShader(vertexShaderObject);
		vertexShaderObject = 0;
		
		glDeleteShader(fragmentShaderObject);
		fragmentShaderObject = 0;
		
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;

		if(depthRenderbuffer)
		{
			glDeleteRenderbuffers(1, &depthRenderbuffer);
			depthRenderbuffer = 0;
		}
		
		if(colorRenderbuffer)
		{
			glDeleteRenderbuffers(1, &colorRenderbuffer);
			colorRenderbuffer = 0;
		}
			
		if(defaultFramebuffer)
		{
			glDeleteFramebuffers(1, &defaultFramebuffer);
			defaultFramebuffer = 0;
		}
		
		if([EAGLContext currentContext] == eaglContext)
		{
			[EAGLContext setCurrentContext:nil];
		}
		[eaglContext release];
		eaglContext = nil;
		
		[super dealloc];
	}
	
	@end
