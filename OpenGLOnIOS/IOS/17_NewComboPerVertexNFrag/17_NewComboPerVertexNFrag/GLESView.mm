#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"
#include "sphere.h"

//Class GLESView ()

enum
{
    MALATI_ATTRIBUTE_VERTEX = 0,
    MALATI_ATTRIBUTE_COLOR,
    MALATI_ATTRIBUTE_NORMAL,
    MALATI_ATTRIBUTE_TEXTURE0,
};

@implementation GLESView
{
	EAGLContext *eaglContext;
	
	GLuint defaultFramebuffer;
	GLuint colorRenderbuffer;
	GLuint depthRenderbuffer;
	
	id displayLink;
	NSInteger animationFrameInterval;
	BOOL isAnimating;
	
	GLuint vertexShaderObjectPerVertex;
	GLuint vertexShaderObjectPerFragment;

    GLuint fragmentShaderObjectPerVertex;
	 GLuint fragmentShaderObjectPerFragment;

    GLuint shaderProgramObjectPerVertex;
	GLuint shaderProgramObjectPerFragment;
	GLuint shaderProgramObject;

  GLuint gNumElements;
GLuint gNumVertices;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
    short sphere_elements[2280];

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint gModelMatrixUniform, gViewMatrixUniform, gProjectionMatrixUniform;
GLuint gLdUniform, gLaUniform, gLsUniform;
GLuint gLightPositionUniform;
GLuint gKdUniform, gKaUniform, gKsUniform;
GLuint gMaterialShininessUniform;

GLuint gSingleTapUniform;
GLuint gDoubleTapUniform;
    GLuint gDoubleTapPerVertexUniform;
    GLuint gDoubleTapPerFragUniform;
    vmath::mat4 perspectiveProjectionMatrix;
	
	//GLfloat gAngleTriangle = 0.0f;
//GLfloat gAngleSquare = 0.0f;

    GLfloat lightAmbient[4] ;//= { 0.0f,0.0f,0.0f,1.0f };
    GLfloat lightDiffuse[4];// = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat lightSpecular[4]; //= { 1.0f,1.0f,1.0f,1.0f };
    GLfloat lightPosition[4];// = { 100.0f,100.0f,100.0f,1.0f };

    GLfloat material_ambient[4];// = { 0.0f,0.0f,0.0f,1.0f };
    GLfloat material_diffuse[4] ;//= { 1.0f,1.0f,1.0f,1.0f };
    GLfloat material_specular[4];// = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat material_shininess;// = 50.0f;

    int singleTap ;
	int doubleTap ;
}

-(id) initWithFrame:(CGRect)frame
{
    lightAmbient[0] = 0.0f;
    lightAmbient[1] = 0.0f;
    lightAmbient[2] = 0.0f;
    lightAmbient[3] = 1.0f;
    lightDiffuse[0] = 1.0f;
    lightDiffuse[1] = 1.0f;
    lightDiffuse[2] = 1.0f;
    lightDiffuse[3] = 1.0f;
    lightSpecular[0] = 1.0f;
    lightSpecular[1] = 1.0f;
    lightSpecular[2] = 1.0f;
    lightSpecular[3] = 1.0f;
    lightPosition[0] = 100.0f;
    lightPosition[1] = 100.0f;
    lightPosition[2] = 100.0f;
    lightPosition[3] = 1.0f;
    
    
    material_ambient[0] = 0.0f;
    material_ambient[1] = 0.0f;
    material_ambient[2] = 0.0f;
    material_ambient[3] = 1.0f;
    material_diffuse[0] = 1.0f;
    material_diffuse[1] = 1.0f;
    material_diffuse[2] = 1.0f;
    material_diffuse[3] = 1.0f;
    material_specular[0] = 1.0f;
    material_specular[1] = 1.0f;
    material_specular[2] = 1.0f;
    material_specular[3] = 1.0f;
    material_shininess = 50.0f;
	self = [super initWithFrame:frame];
	if(self)
	{
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *) super.layer;
		
		eaglLayer.opaque = YES;
		eaglLayer.drawableProperties = [NSDictionary
		dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
		kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, 
		kEAGLDrawablePropertyColorFormat, nil];
		
		eaglContext = [[EAGLContext alloc] 
		initWithAPI:kEAGLRenderingAPIOpenGLES3];
		
		if(eaglContext == nil)
		{
			[self release];
			return (nil);
		}
		[EAGLContext setCurrentContext:eaglContext];
		
		glGenFramebuffers(1, &defaultFramebuffer);
		glGenRenderbuffers(1, &colorRenderbuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
		
		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
		
		GLint backingWidth;
		GLint backingHeight;
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
		
		glGenRenderbuffers(1, &depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
		
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,  GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
		
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed To Create Complete FrameBuffer Object %x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
			glDeleteFramebuffers(1, &defaultFramebuffer);
			glDeleteRenderbuffers(1, &colorRenderbuffer);
			glDeleteRenderbuffers(1, &depthRenderbuffer);
			[self release];
			return (nil);
		}
		
		printf("Renderer : %s | GL Version : %s | GLSL Version :%s \n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
				
		isAnimating = NO;
		
		animationFrameInterval = 60;
		//Vertex Shader
		vertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);
        
		 const GLchar *vertexShaderSourceCodePerVertex =
        "#version 300 es" \
       "\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \

		"uniform int u_VertexDouble_tap;" \

		"uniform vec3 u_Ld;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ls;" \
		"uniform vec4 u_light_position;"\
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Ks;" \
		"uniform vec3 u_Kd;" \
		"uniform float u_material_shininess;" \

		"out vec3 phong_ads_color;" \
		"void main(void)" \
		"{" \
		"if(u_VertexDouble_tap == 0)" \
		"{" \
		"vec4 eyeCoordinates =  u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 tnorm = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
		"vec3 light_direction  = normalize(vec3(u_light_position) - eyeCoordinates.xyz);" \
		"vec3 ambient = u_La * u_Ka;"\
		"vec3 diffuse_light = u_Ld * u_Kd * max(dot(light_direction, tnorm), 0.0);" \
		"vec3 reflection_vector = reflect(-light_direction, tnorm);"\
		"vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0),u_material_shininess);" \
		"phong_ads_color=ambient + diffuse_light + specular;" \
		"}" \
		"else"\
		"{"\
		"phong_ads_color = vec3(0.0, 1.0, 0.0);"\
		"}"\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

        glShaderSource(vertexShaderObjectPerVertex, 1, (const GLchar **)&vertexShaderSourceCodePerVertex, NULL);
        
		glCompileShader(vertexShaderObjectPerVertex);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(vertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		//Vertex Shader Per Frag
		iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
		
		vertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);
        
		 const GLchar *vertexShaderSourceCodePerFragment =
         "#version 300 es" \
       "\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;"\
		"out vec3 transformed_normals;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

        glShaderSource(vertexShaderObjectPerFragment, 1, (const GLchar **)&vertexShaderSourceCodePerFragment, NULL);
        
		glCompileShader(vertexShaderObjectPerFragment);
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        glGetShaderiv(vertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Per Frag Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		
		//fragment shader
		iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
		
		 fragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);
		 
		 const GLchar *fragmentShaderSourceCodePerVertex =
        "#version 300 es" \
       "\n" \
        "precision highp float;" \
		"in vec3 phong_ads_color;"\
			"out vec4 FragColor;"\
			"void main(void)"\
			"{"\
			"FragColor = vec4(phong_ads_color, 1.0);"\
			"}";


        glShaderSource(fragmentShaderObjectPerVertex, 1, (const GLchar **)&fragmentShaderSourceCodePerVertex, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObjectPerVertex);
        glGetShaderiv(fragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		//fragment shader Per Frag
		iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
		
		 fragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);
		 
		 const GLchar *fragmentShaderSourceCodePerFragment =
        "#version 300 es" \
       "\n" \
        "precision highp float;" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_FragDouble_tap;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"if(u_FragDouble_tap ==0)" \
		"{" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction=normalize(light_direction);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		"vec3 ambient = u_La * u_Ka;" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color=ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 0.0, 0.0);" \
		"}" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";



        glShaderSource(fragmentShaderObjectPerFragment, 1, (const GLchar **)&fragmentShaderSourceCodePerFragment, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObjectPerFragment);
        glGetShaderiv(fragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Per Frag Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		shaderProgramObjectPerVertex = glCreateProgram();
		
		glAttachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
		glAttachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);
		
		[self MyFunc:shaderProgramObjectPerVertex];
		
		/*glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");

		 glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
	gModelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

	gLKeyPressedUniform = glGetUniformLocation(shaderProgramObject, "u_LKeyPressed");

	gLdUniform = glGetUniformLocation(shaderProgramObject, "u_Ld");
	gLaUniform = glGetUniformLocation(shaderProgramObject, "u_La");
	gLsUniform = glGetUniformLocation(shaderProgramObject, "u_Ls");

	gKdUniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
	gKaUniform = glGetUniformLocation(shaderProgramObject, "u_Ka");

	gLightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
	gMaterialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
*/
        Sphere sphere;
	sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = sphere.getNumberOfSphereVertices();
	gNumElements = sphere.getNumberOfSphereElements();



	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	//position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//normal
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);


	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	glBindVertexArray(0);

        glEnable(GL_DEPTH_TEST);
        
        glDepthFunc(GL_LEQUAL);
        
       // glEnable(GL_CULL_FACE);
        
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		singleTap = 0;
		doubleTap = 0;
		perspectiveProjectionMatrix = vmath::mat4::identity();
		
		UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
		[singleTapGestureRecognizer setNumberOfTapsRequired:1];
		[singleTapGestureRecognizer setNumberOfTouchesRequired:1];
		[singleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:singleTapGestureRecognizer];
		
		UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
		[doubleTapGestureRecognizer setNumberOfTapsRequired:2];
		[doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
		[doubleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:doubleTapGestureRecognizer];
		
		[singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
		
		UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc ] initWithTarget : self action : @selector(onSwipe:)];
		[self addGestureRecognizer:swipeGestureRecognizer];      
		
		UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
		[self addGestureRecognizer:longPressGestureRecognizer];
	}
	return (self);		
}

-(void) MyFunc: (GLuint) iProgramObject
{
	 GLint iInfoLogLength = 0;
        char *szInfoLog = NULL;
		
	shaderProgramObject = glCreateProgram();
	
	shaderProgramObject = iProgramObject;
	
	glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");

		 glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
	gModelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

	gSingleTapUniform = glGetUniformLocation(shaderProgramObject, "u_SingleTap");
	gDoubleTapUniform = glGetUniformLocation(shaderProgramObject, "u_double_tap");
	gDoubleTapPerVertexUniform =glGetUniformLocation(shaderProgramObject, "u_VertexDouble_tap");
    gDoubleTapPerFragUniform = glGetUniformLocation(shaderProgramObject, "u_FragDouble_tap");
	gLdUniform = glGetUniformLocation(shaderProgramObject, "u_Ld");
	gLaUniform = glGetUniformLocation(shaderProgramObject, "u_La");
	gLsUniform = glGetUniformLocation(shaderProgramObject, "u_Ls");

	gKdUniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
	gKaUniform = glGetUniformLocation(shaderProgramObject, "u_Ka");

	gLightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
	gMaterialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
}
	
	
	/*
	-(void) drawRect:(CGRect)rect
	{
	
	}
	*/
	
	+(Class)layerClass
	{
		return ([CAEAGLLayer class]);
	}
	
	-(void)drawView:(id)sender
	{
		
		[EAGLContext setCurrentContext:eaglContext];
		
		glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		
		glUseProgram(shaderProgramObject);
		
		
		if (doubleTap == 1)
		{
            printf("In DOuble TAP\n");
			//glUniform1i(gDoubleTapUniform, 1);

			
			if((singleTap % 2) == 0) //Per Vertex
			{
                printf("In Single Tap Per Vertex\n");
				shaderProgramObjectPerVertex = glCreateProgram();
		
				glAttachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
				glAttachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);
		
                [self MyFunc :shaderProgramObjectPerVertex];
				//glUniform1i(gDoubleTapUniform, 1);
                glUniform1i(gDoubleTapPerVertexUniform, 1);
				glUseProgram(shaderProgramObjectPerVertex);
			}
			else //Per Frag
			{
                printf("In Single Tap Per Fragment\n");
				shaderProgramObjectPerFragment = glCreateProgram();
		
				glAttachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);
				glAttachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);
		
                [self MyFunc :shaderProgramObjectPerFragment];
                glUniform1i(gDoubleTapPerFragUniform, 1);
				//glUniform1i(gDoubleTapUniform, 1);
				glUseProgram(shaderProgramObjectPerFragment);
			}
			glUniform3fv(gLdUniform, 1, lightDiffuse);//white
			glUniform3fv(gLaUniform, 1, lightAmbient);
			glUniform3fv(gLsUniform, 1, lightSpecular);
			glUniform4fv(gLightPositionUniform, 1, lightPosition);
        
			glUniform3fv(gKdUniform, 1, material_diffuse);
			glUniform3fv(gKaUniform, 1, material_ambient);
			glUniform3fv(gKsUniform, 1, material_specular);
			glUniform1f(gMaterialShininessUniform, material_shininess);
			
		//float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };
		
		}
		else
		{
            printf("In Else part\n");
			//glUniform1i(gDoubleTapUniform, 0);
            glUniform1i(gDoubleTapPerVertexUniform, 0);
            glUniform1i(gDoubleTapPerFragUniform, 0);
		}
        

        vmath::mat4 modelMatrix = vmath::mat4::identity();
        vmath::mat4 viewMatrix = vmath::mat4::identity();
	//mat4 rotationMatrix = mat4::identity();

	modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);

	//rotationMatrix = vmath::rotate(gAngle, gAngle, gAngle);

	//modelViewMatrix = modelMatrix * rotationMatrix; //imp

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	

	glBindVertexArray(0);

	
		glUseProgram(0);
		
		glBindRenderbuffer(GL_RENDERBUFFER , colorRenderbuffer);
		[eaglContext presentRenderbuffer:GL_RENDERBUFFER];
		
	
		
	}
	
	-(void)layoutSubviews
	{
		GLint width;
		GLint height;
	
		glBindRenderbuffer (GL_RENDERBUFFER, colorRenderbuffer);
		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
		
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
		
		glGenRenderbuffers(1, &depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
		
		glViewport(0, 0, width, height);
		
		GLfloat fwidth = (GLfloat)width;
		GLfloat fheight = (GLfloat)height;
		/*if (width <= height)
			perspectiveProjectionMatrix = vmath::ortho(-100.0f, 100.0f, (-100.0f * (fheight / fwidth)), (100.0f * (fheight / fwidth)), -100.0f, 100.0f);
		else
            perspectiveProjectionMatrix = vmath::ortho((-100.0f * (fwidth / fheight)), (100.0f * (fwidth / fheight)), -100.0f, 100.0f, -100.0f, 100.0f);
    */
        perspectiveProjectionMatrix = vmath::perspective(45.0f, fwidth/fheight, 0.1f, 100.0f);
        
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed To Create Complete FrameBuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
		}
		
		[self drawView:nil];
	}
	
	-(void)startAnimation
	{
		if(!isAnimating)
		{
			displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
			
			[displayLink setPreferredFramesPerSecond:animationFrameInterval];
			[displayLink addToRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
			
			isAnimating = YES;
		}
	}
	
	-(void)stopAnimation
	{
		if(isAnimating)
		{
			[displayLink invalidate];
			displayLink = nil;
			isAnimating = NO;
			
		}
	}
	
	//to become first responder
	-(BOOL)acceptsFirstResponder
	{
		return (YES);
	}
	
	-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event
	{
		
	}
	
	-(void) onSingleTap:(UITapGestureRecognizer *)gr
	{
		++singleTap;
		if(singleTap > 2)
			singleTap = 1;
	}
	
	-(void) onDoubleTap:(UITapGestureRecognizer *)gr
	{
		++doubleTap;
		if(doubleTap > 1)
			doubleTap = 0;
	}
	
	-(void) onSwipe:(UISwipeGestureRecognizer *)gr
	{
		[self release];
		exit(0);
	}
	
	-(void)onLongPress:(UILongPressGestureRecognizer *)gr
	{
	
	}
	
	-(void)dealloc
	{
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}
	// destroy element vbo
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

		glDetachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
		
		glDetachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);
		
		glDeleteShader(vertexShaderObjectPerVertex);
		vertexShaderObjectPerVertex = 0;
		
		glDeleteShader(fragmentShaderObjectPerVertex);
		fragmentShaderObjectPerVertex = 0;
		
		glDeleteProgram(shaderProgramObjectPerVertex);
		shaderProgramObjectPerVertex = 0;

		glDetachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);
		
		glDetachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);
		
		glDeleteShader(vertexShaderObjectPerFragment);
		vertexShaderObjectPerFragment = 0;
		
		glDeleteShader(fragmentShaderObjectPerFragment);
		fragmentShaderObjectPerFragment = 0;
		
		glDeleteProgram(shaderProgramObjectPerFragment);
		shaderProgramObjectPerFragment = 0;
		
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
		if(depthRenderbuffer)
		{
			glDeleteRenderbuffers(1, &depthRenderbuffer);
			depthRenderbuffer = 0;
		}
		
		if(colorRenderbuffer)
		{
			glDeleteRenderbuffers(1, &colorRenderbuffer);
			colorRenderbuffer = 0;
		}
			
		if(defaultFramebuffer)
		{
			glDeleteFramebuffers(1, &defaultFramebuffer);
			defaultFramebuffer = 0;
		}
		
		if([EAGLContext currentContext] == eaglContext)
		{
			[EAGLContext setCurrentContext:nil];
		}
		[eaglContext release];
		eaglContext = nil;
		
		[super dealloc];
	}
	
	@end
