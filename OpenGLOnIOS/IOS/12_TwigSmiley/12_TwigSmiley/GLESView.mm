#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"
//Class GLESView ()

enum
{
    MALATI_ATTRIBUTE_VERTEX = 0,
    MALATI_ATTRIBUTE_COLOR,
    MALATI_ATTRIBUTE_NORMAL,
    MALATI_ATTRIBUTE_TEXTURE0,
};

@implementation GLESView
{
	EAGLContext *eaglContext;
	
	GLuint defaultFramebuffer;
	GLuint colorRenderbuffer;
	GLuint depthRenderbuffer;
	
	id displayLink;
	NSInteger animationFrameInterval;
	BOOL isAnimating;
	
	GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
 GLuint gVao_rectangle;
GLuint gVbo_rectangle_Position;
GLuint gVbo_rectangle_Texture;
GLuint gVbo_rectangle_TexturePlain;


    GLuint mvpUniform;
    
	//GLuint pyramid_texture;
    //GLuint cube_texture;
    vmath::mat4 perspectiveProjectionMatrix;
	GLuint gTexture_sampler_uniform;
GLuint gTexture_Smiley;
GLuint gKey;
	//GLfloat gAngleTriangle = 0.0f;
//GLfloat gAngleSquare = 0.0f;

GLuint texName;
GLubyte checkImage[64][64][4];

}

-(id) initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if(self)
	{
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *) super.layer;
		
		eaglLayer.opaque = YES;
		eaglLayer.drawableProperties = [NSDictionary
		dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
		kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, 
		kEAGLDrawablePropertyColorFormat, nil];
		
		eaglContext = [[EAGLContext alloc] 
		initWithAPI:kEAGLRenderingAPIOpenGLES3];
		
		if(eaglContext == nil)
		{
			[self release];
			return (nil);
		}
		[EAGLContext setCurrentContext:eaglContext];
		
		glGenFramebuffers(1, &defaultFramebuffer);
		glGenRenderbuffers(1, &colorRenderbuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
		
		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
		
		GLint backingWidth;
		GLint backingHeight;
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
		
		glGenRenderbuffers(1, &depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
		
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,  GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
		
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed To Create Complete FrameBuffer Object %x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
			glDeleteFramebuffers(1, &defaultFramebuffer);
			glDeleteRenderbuffers(1, &colorRenderbuffer);
			glDeleteRenderbuffers(1, &depthRenderbuffer);
			[self release];
			return (nil);
		}
		
		printf("Renderer : %s | GL Version : %s | GLSL Version :%s \n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
				
		isAnimating = NO;
		
		animationFrameInterval = 60;
		//Vertex Shader
		vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
		 const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec2 vTexture0_Coord;" \
        "out vec2 out_texture0_coord;" \
        "uniform mat4 u_mvp_matrix;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_texture0_coord = vTexture0_Coord;" \
        "}";
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
		glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		//fragment shader
		iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
		
		 fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
		 
		 const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
		"in vec2 out_texture0_coord;" \
		"uniform sampler2D u_texture0_sampler;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
		"vec3 tex=vec3(texture(u_texture0_sampler, out_texture0_coord));"
        "FragColor = vec4(tex, 1.0f);" \
        "}";
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		shaderProgramObject = glCreateProgram();
		
		glAttachShader(shaderProgramObject, vertexShaderObject);
		glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

		 glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		 // get MVP uniform location
        mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        
		gTexture_sampler_uniform=glGetUniformLocation(shaderProgramObject,"u_texture0_sampler");
        
	//	pyramid_texture=[self loadTextureFromBMPFile:@"Stone" :@"bmp"];
      //  cube_texture=[self loadTextureFromBMPFile:@"Vijay_Kundali" :@"bmp"];
		gTexture_Smiley = [self loadTextureFromBMPFile:@"Smiley" :@"bmp"];
		
	const GLfloat rectangleVertices[] =
	{
		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
	};

/*	const GLfloat rectangleTextcoords[] =
	{
		0.0f,1.0f,
		0.0f,0.0f,
		1.0f,0.0f,
		1.0f,1.0f,
	};*/

	glGenVertexArrays(1, &gVao_rectangle);
	glBindVertexArray(gVao_rectangle);

	//position
	glGenBuffers(1, &gVbo_rectangle_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_rectangle_Position);

	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//texture
	glGenBuffers(1, &gVbo_rectangle_Texture);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_rectangle_Texture);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleTextcoords), rectangleTextcoords, GL_STATIC_DRAW);
glBufferData(GL_ARRAY_BUFFER, 4*2*sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_TEXTURE0);


	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

        glEnable(GL_DEPTH_TEST);
        
        glDepthFunc(GL_LEQUAL);
        
        glEnable(GL_CULL_FACE);
        glEnable(GL_TEXTURE_2D);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		perspectiveProjectionMatrix = vmath::mat4::identity();
		
		UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
		[singleTapGestureRecognizer setNumberOfTapsRequired:1];
		[singleTapGestureRecognizer setNumberOfTouchesRequired:1];
		[singleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:singleTapGestureRecognizer];
		
		UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
		[doubleTapGestureRecognizer setNumberOfTapsRequired:2];
		[doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
		[doubleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:doubleTapGestureRecognizer];
		
		[singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
		
		UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc ] initWithTarget : self action : @selector(onSwipe:)];
		[self addGestureRecognizer:swipeGestureRecognizer];      
		
		UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
		[self addGestureRecognizer:longPressGestureRecognizer];
        
        gKey = 0;
	}
	return (self);		
}

-(GLuint)loadTextureFromBMPFile:(NSString *)texFileName :(NSString *)extension
{
	NSString *textureFileNameWithPath = [[NSBundle mainBundle]pathForResource:texFileName ofType:extension];
	
	UIImage *bmpImage = [[UIImage alloc]initWithContentsOfFile:textureFileNameWithPath];
	if(!bmpImage)
	{
		NSLog(@"Can't find %@", textureFileNameWithPath);
		return(0);
	}
	
	CGImageRef cgImage = bmpImage.CGImage;
	
	int w = (int)CGImageGetWidth(cgImage);
	int h = (int)CGImageGetHeight(cgImage);
	CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
	void* pixels = (void *)CFDataGetBytePtr(imageData);
	
	GLuint bmpTextures;
	glGenTextures(1, &bmpTextures);
	
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glBindTexture(GL_TEXTURE_2D, bmpTextures);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 w,
                 h,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels);
    
	glGenerateMipmap(GL_TEXTURE_2D);
    
    CFRelease(imageData);
    return(bmpTextures);
}


	/*
	-(void) drawRect:(CGRect)rect
	{
	
	}
	*/
	
	+(Class)layerClass
	{
		return ([CAEAGLLayer class]);
	}
	
	-(void)drawView:(id)sender
	{
		//static float gAnglePyramid = 0.0f;
		//static float gAngleCube = 0.0f;
		[EAGLContext setCurrentContext:eaglContext];
		
		glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		
		glUseProgram(shaderProgramObject);
		
      // vmath::mat4 scaleMatrix = vmath::mat4::identity();
		vmath::mat4 modelViewMatrix = vmath::mat4::identity();
		vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
		//vmath::mat4 rotationMatrix = vmath::mat4::identity(); 
		
        modelViewMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
		
		//rotationMatrix = vmath::rotate(gAnglePyramid, 0.0f, 1.0f, 0.0f);
		//modelViewMatrix = modelViewMatrix * rotationMatrix; //imp
		
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix; // ORDER IS IMPORTANT
    
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		
		 glBindTexture(GL_TEXTURE_2D,gTexture_Smiley);
		 
		glBindVertexArray(gVao_rectangle);

		GLfloat quadTexture[8] = {};
	switch (gKey)
	{
	case 1:
		/*quadTexture[0] = 0.5;
		quadTexture[1] = 0.5;
		quadTexture[2] = 0.0;
		quadTexture[3] = 0.5;
		quadTexture[4] = 0.0;
		quadTexture[5] = 0.0;
		quadTexture[6] = 0.5;
		quadTexture[7] = 0.0;*/
            quadTexture[0] = 0.5;
            quadTexture[1] = 0.0;
            quadTexture[2] = 0.0;
            quadTexture[3] = 0.0;
            quadTexture[4] = 0.0;
            quadTexture[5] = 0.5;
            quadTexture[6] = 0.5;
            quadTexture[7] = 0.5;
            
		break;
	case 2:
		/*quadTexture[0] = 1.0;
		quadTexture[1] = 1.0;
		quadTexture[2] = 0.0;
		quadTexture[3] = 1.0;
		quadTexture[4] = 0.0;
		quadTexture[5] = 0.0;
		quadTexture[6] = 1.0;
		quadTexture[7] = 0.0;*/
            quadTexture[0] = 1.0;
            quadTexture[1] = 0.0;
            quadTexture[2] = 0.0;
            quadTexture[3] = 0.0;
            quadTexture[4] = 0.0;
            quadTexture[5] = 1.0;
            quadTexture[6] = 1.0;
            quadTexture[7] = 1.0;
		break;
	case 3:
		quadTexture[0] = 2.0;
		quadTexture[1] = 0.0;
		quadTexture[2] = 0.0;
		quadTexture[3] = 0.0;
		quadTexture[4] = 0.0;
		quadTexture[5] = 2.0;
		quadTexture[6] = 2.0;
		quadTexture[7] = 2.0;
		break;
	case 4:
		quadTexture[0] = 0.5;
		quadTexture[1] = 0.5;
		quadTexture[2] = 0.5;
		quadTexture[3] = 0.5;
		quadTexture[4] = 0.5;
		quadTexture[5] = 0.5;
		quadTexture[6] = 0.5;
		quadTexture[7] = 0.5;

		break;
	default:
		break;
	}
	if ((gKey == 1) || (gKey == 2) || (gKey == 3) || (gKey == 4))
	{
        printf("gKeyValue :  %d" , gKey);
glBindBuffer(GL_ARRAY_BUFFER, gVbo_rectangle_Texture);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadTexture), quadTexture, GL_DYNAMIC_DRAW);
glVertexAttribPointer(MALATI_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(MALATI_ATTRIBUTE_TEXTURE0);
glBindBuffer(GL_ARRAY_BUFFER, 0);

        glActiveTexture(GL_TEXTURE0);
glBindTexture(GL_TEXTURE_2D, gTexture_Smiley);
		glUniform1i(gTexture_sampler_uniform, 0);
		
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	}
	else
	{
		int i, j;
		for (i = 0; i < 64; i++)
		{
			for (j = 0; j < 64; j++)
			{
		//		c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;

				checkImage[i][j][0] = (GLubyte)255;
				checkImage[i][j][1] = (GLubyte)255;
				checkImage[i][j][2] = (GLubyte)255;
				checkImage[i][j][3] = (GLubyte)255;

			}
		}

		//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		//glGenTextures(1, &texName);
		glBindTexture(GL_TEXTURE_2D, texName); //imp

		glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGB,
			64,
			64,
			0,
			GL_RGB,
			GL_UNSIGNED_BYTE,
			checkImage);//imp
		glGenerateMipmap(GL_TEXTURE_2D);//imp
		//glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

		glBindTexture(GL_TEXTURE_2D, 0);
		
		glGenBuffers(1, &gVbo_rectangle_TexturePlain);//IMP
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_rectangle_TexturePlain);//IMP
		//glBufferData(GL_ARRAY_BUFFER, sizeof(quadTexture), quadTexture, GL_DYNAMIC_DRAW);

		//glVertexAttribPointer(MALATI_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

		//glEnableVertexAttribArray(MALATI_ATTRIBUTE_TEXTURE0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //Imp
	}

		glBindVertexArray(0);
		
		glUseProgram(0);
		
		glBindRenderbuffer(GL_RENDERBUFFER , colorRenderbuffer);
		[eaglContext presentRenderbuffer:GL_RENDERBUFFER];
		
	
		
	}
	
	-(void)layoutSubviews
	{
		GLint width;
		GLint height;
	
		glBindRenderbuffer (GL_RENDERBUFFER, colorRenderbuffer);
		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
		
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
		
		glGenRenderbuffers(1, &depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
		
		glViewport(0, 0, width, height);
		
		GLfloat fwidth = (GLfloat)width;
		GLfloat fheight = (GLfloat)height;
		/*if (width <= height)
			perspectiveProjectionMatrix = vmath::ortho(-100.0f, 100.0f, (-100.0f * (fheight / fwidth)), (100.0f * (fheight / fwidth)), -100.0f, 100.0f);
		else
            perspectiveProjectionMatrix = vmath::ortho((-100.0f * (fwidth / fheight)), (100.0f * (fwidth / fheight)), -100.0f, 100.0f, -100.0f, 100.0f);
    */
        perspectiveProjectionMatrix = vmath::perspective(45.0f, fwidth/fheight, 0.1f, 100.0f);
        
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed To Create Complete FrameBuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
		}
		
		[self drawView:nil];
	}
	
	-(void)startAnimation
	{
		if(!isAnimating)
		{
			displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
			
			[displayLink setPreferredFramesPerSecond:animationFrameInterval];
			[displayLink addToRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
			
			isAnimating = YES;
		}
	}
	
	-(void)stopAnimation
	{
		if(isAnimating)
		{
			[displayLink invalidate];
			displayLink = nil;
			isAnimating = NO;
			
		}
	}
	
	//to become first responder
	-(BOOL)acceptsFirstResponder
	{
		return (YES);
	}
	
	-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event
	{
		
	}
	
	-(void) onSingleTap:(UITapGestureRecognizer *)gr
	{
		static int numTimes = 0;
		++numTimes;
		if(numTimes > 4)
		{
			gKey = 0;
			numTimes = 0;
		}
		else if(numTimes == 1)
			gKey = 1;
		else if(numTimes == 2)
			gKey = 2;
		else if(numTimes == 3)
			gKey = 3;
		else if(numTimes ==4)
			gKey = 4;
	}
	
	-(void) onDoubleTap:(UITapGestureRecognizer *)gr
	{
	
	}
	
	-(void) onSwipe:(UISwipeGestureRecognizer *)gr
	{
		[self release];
		exit(0);
	}
	
	-(void)onLongPress:(UILongPressGestureRecognizer *)gr
	{
	
	}
	
	-(void)dealloc
	{
	if (gVao_rectangle)
	{
		glDeleteVertexArrays(1, &gVao_rectangle);
		gVao_rectangle = 0;
	}
	if (gVbo_rectangle_Position)
	{
		glDeleteBuffers(1, &gVbo_rectangle_Position);
		gVbo_rectangle_Position = 0;
	}
	if (gVbo_rectangle_Texture)
	{
		glDeleteBuffers(1, &gVbo_rectangle_Texture);
		gVbo_rectangle_Texture = 0;
	}
	if (gTexture_Smiley)
	{
		glDeleteBuffers(1, &gTexture_Smiley);
		gTexture_Smiley = 0;
	}
		glDetachShader(shaderProgramObject, vertexShaderObject);
		
		glDetachShader(shaderProgramObject, fragmentShaderObject);
		
		glDeleteShader(vertexShaderObject);
		vertexShaderObject = 0;
		
		glDeleteShader(fragmentShaderObject);
		fragmentShaderObject = 0;
		
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;

		if(depthRenderbuffer)
		{
			glDeleteRenderbuffers(1, &depthRenderbuffer);
			depthRenderbuffer = 0;
		}
		
		if(colorRenderbuffer)
		{
			glDeleteRenderbuffers(1, &colorRenderbuffer);
			colorRenderbuffer = 0;
		}
			
		if(defaultFramebuffer)
		{
			glDeleteFramebuffers(1, &defaultFramebuffer);
			defaultFramebuffer = 0;
		}
		
		if([EAGLContext currentContext] == eaglContext)
		{
			[EAGLContext setCurrentContext:nil];
		}
		[eaglContext release];
		eaglContext = nil;
		
		[super dealloc];
	}
	
	@end


