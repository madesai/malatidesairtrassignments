#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"
//Class GLESView ()

enum
{
    MALATI_ATTRIBUTE_VERTEX = 0,
    MALATI_ATTRIBUTE_COLOR,
    MALATI_ATTRIBUTE_NORMAL,
    MALATI_ATTRIBUTE_TEXTURE0,
};

@implementation GLESView
{
	EAGLContext *eaglContext;
	
	GLuint defaultFramebuffer;
	GLuint colorRenderbuffer;
	GLuint depthRenderbuffer;
	
	id displayLink;
	NSInteger animationFrameInterval;
	BOOL isAnimating;
	
	GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
	GLuint gVao_Pyramid;
	GLuint gVbo_Pyramid_Position;
	GLuint gVbo_Pyramid_Normal;


    GLuint mvpUniform;
    
    vmath::mat4 perspectiveProjectionMatrix;
	GLuint gModelMatrixUniform, gViewMatrixUniform, gProjectionMatrixUniform;
	GLuint gLdRedUniform, gLdBlueUniform, gLaUniform, gLsBlueUniform,gLsRedUniform, gKdUniform, gKaUniform, gKsUniform, gRedLightPositionUniform, gBlueLightPositionUniform, gMaterialShininessUniform;
	GLuint gSingleTapUniform;
	GLuint gDoubleTapUniform;
	
	bool doubleTap;
	
	//bool gbLight;
	//vmath::mat4 gPerspectiveProjectionMatrix;//1st change

//GLfloat gAnglePyramid = 0.0f;

	GLfloat lightAmbient[4] ;//= { 0.0f,0.0f,0.0f,1.0f };
    GLfloat lightDiffuseRed[4];
	GLfloat lightDiffuseBlue[4];
    GLfloat lightSpecularRed[4]; //= { 1.0f,1.0f,1.0f,1.0f };
    GLfloat lightSpecularBlue[4];
    GLfloat lightPositionRed[4];// = { 100.0f,100.0f,100.0f,1.0f };
	GLfloat lightPositionBlue[4];
	
    GLfloat material_ambient[4];// = { 0.0f,0.0f,0.0f,1.0f };
    GLfloat material_diffuse[4] ;//= { 1.0f,1.0f,1.0f,1.0f };
    GLfloat material_specular[4];// = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat material_shininess;// = 50.0f;
	
/*
GLfloat lightDiffuseRed[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuseBlue[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPositionRed[] = { -100.0f,0.0f,0.0f,1.0f };
GLfloat lightPositionBlue[] = { 100.0f,0.0f,0.0f,1.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;*/


	//GLfloat gAngleTriangle = 0.0f;
//GLfloat gAngleSquare = 0.0f;
}

-(id) initWithFrame:(CGRect)frame
{
	lightAmbient[0] = 0.0f;
    lightAmbient[1] = 0.0f;
    lightAmbient[2] = 0.0f;
    lightAmbient[3] = 1.0f;
    lightDiffuseRed[0] = 1.0f;
    lightDiffuseRed[1] = 0.0f;
    lightDiffuseRed[2] = 0.0f;
    lightDiffuseRed[3] = 1.0f;
	lightDiffuseBlue[0] = 0.0f;
    lightDiffuseBlue[1] = 0.0f;
    lightDiffuseBlue[2] = 1.0f;
    lightDiffuseBlue[3] = 1.0f;
    lightSpecularRed[0] = 1.0f;
    lightSpecularRed[1] = 0.0f;
    lightSpecularRed[2] = 0.0f;
    lightSpecularRed[3] = 1.0f;
	lightSpecularBlue[0] = 0.0f;
    lightSpecularBlue[1] = 0.0f;
    lightSpecularBlue[2] = 1.0f;
    lightSpecularBlue[3] = 1.0f;
    lightPositionRed[0] = -100.0f;
    lightPositionRed[1] = 0.0f;
    lightPositionRed[2] = 0.0f;
    lightPositionRed[3] = 1.0f;
	lightPositionBlue[0] = 100.0f;
    lightPositionBlue[1] = 0.0f;
    lightPositionBlue[2] = 0.0f;
    lightPositionBlue[3] = 1.0f;
    
    
    material_ambient[0] = 0.0f;
    material_ambient[1] = 0.0f;
    material_ambient[2] = 0.0f;
    material_ambient[3] = 1.0f;
    material_diffuse[0] = 1.0f;
    material_diffuse[1] = 1.0f;
    material_diffuse[2] = 1.0f;
    material_diffuse[3] = 1.0f;
    material_specular[0] = 1.0f;
    material_specular[1] = 1.0f;
    material_specular[2] = 1.0f;
    material_specular[3] = 1.0f;
    material_shininess = 50.0f;
	
	doubleTap = 0;
	
	self = [super initWithFrame:frame];
	if(self)
	{
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *) super.layer;
		
		eaglLayer.opaque = YES;
		eaglLayer.drawableProperties = [NSDictionary
		dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
		kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, 
		kEAGLDrawablePropertyColorFormat, nil];
		
		eaglContext = [[EAGLContext alloc] 
		initWithAPI:kEAGLRenderingAPIOpenGLES3];
		
		if(eaglContext == nil)
		{
			[self release];
			return (nil);
		}
		[EAGLContext setCurrentContext:eaglContext];
		
		glGenFramebuffers(1, &defaultFramebuffer);
		glGenRenderbuffers(1, &colorRenderbuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
		
		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
		
		GLint backingWidth;
		GLint backingHeight;
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
		
		glGenRenderbuffers(1, &depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
		
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,  GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
		
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed To Create Complete FrameBuffer Object %x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
			glDeleteFramebuffers(1, &defaultFramebuffer);
			glDeleteRenderbuffers(1, &colorRenderbuffer);
			glDeleteRenderbuffers(1, &depthRenderbuffer);
			[self release];
			return (nil);
		}
		
		printf("Renderer : %s | GL Version : %s | GLSL Version :%s \n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
				
		isAnimating = NO;
		
		animationFrameInterval = 60;
		//Vertex Shader
		vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
		 const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;"\
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \

		"uniform int u_double_tap;" \

		"uniform vec3 u_LdRed;" \
		"uniform vec3 u_LdBlue;" \

		"uniform vec3 u_La;" \
		"uniform vec3 u_LsRed;" \
        "uniform vec3 u_LsBlue;" \

		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Ks;" \

		"uniform vec4 u_light_positionRed;"\
		"uniform vec4 u_light_positionBlue;"\

		"uniform float u_material_shininess;" \

		"out vec3 phong_ads_red_color;" \
		"out vec3 phong_ads_blue_color;" \

		"vec4 eyeCoordinates;" \
		"vec4 eyeCoordinates1;" \
		"vec3 blue_light;"\
		"vec3 red_light;"\
		"vec3 tnorm;"\
		"vec3 tnorm1;"\
		"vec3 reflection_vector;"\
		"vec3 reflection_vector1;"\
		"vec3 viewer_vector;" \
		"vec3 viewer_vector1;" \
		"vec3 specular;"
		"vec3 specular1;"
		"void main(void)"\
		"{"\
		"if(u_double_tap == 1)" \
		"{" \
		"eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
		"tnorm = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"vec3 light_direction_blue = normalize(vec3(u_light_positionBlue) - eyeCoordinates.xyz);" \
		"vec3 ambient = u_La * u_Ka;"\
		"blue_light = u_LdBlue * u_Kd * max(dot(light_direction_blue, tnorm), 0.0);" \
		"reflection_vector = reflect(-light_direction_blue, tnorm);"\
		"viewer_vector = normalize(-eyeCoordinates.xyz);" \
		"specular = u_LsBlue * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0),u_material_shininess);" \
		"phong_ads_blue_color=ambient + blue_light + specular;" \

		"eyeCoordinates1 = u_view_matrix * u_model_matrix * vPosition;" \
		"tnorm1 = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"vec3 light_direction_red = normalize(vec3(u_light_positionRed) - eyeCoordinates1.xyz);" \
		"red_light = u_LdRed * u_Kd * max(dot(light_direction_red, tnorm1), 0.0);" \
		"reflection_vector1 = reflect(-light_direction_red, tnorm1);"\
		"viewer_vector1 = normalize(-eyeCoordinates1.xyz);" \
		"specular1 = u_LsRed * u_Ks * pow(max(dot(reflection_vector1, viewer_vector1), 0.0),u_material_shininess);" \
		"phong_ads_red_color=ambient + red_light + specular1;" \
		"}" \
		"else"\
		"{"\
		"phong_ads_red_color = vec3(1.0, 1.0, 1.0);"\
		"phong_ads_blue_color = vec3(1.0, 1.0, 1.0);"\
		"}"\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
		/*
        "in vec4 vPosition;" \
		"in vec3 vNormal;"\
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \

		"uniform int u_double_tap;" \

		"uniform vec3 u_LdRed;" \
		"uniform vec3 u_LdBlue;" \

		"uniform vec3 u_La;" \
		"uniform vec3 u_LsRed;" \
		"uniform vec3 u_LsBlue;" \

		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Ks;" \

		"uniform vec4 u_light_positionRed;"\
		"uniform vec4 u_light_positionBlue;"\

		"uniform float u_material_shininess;" \

		"out vec3 phong_ads_red_color;" \
		"out vec3 phong_ads_blue_color;" \
		"if(u_double_tap == 1)" \
			"{" \
			"vec4 eyeCoordinates = u_view_matrix * u_model_matrix  * vPosition;" \
			"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);" \
			"vec3 ambient = u_La * u_Ka;" \
			"vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
			
			"vec3 light_directionRed  = normalize(vec3(u_light_positionRed) - eyeCoordinates.xyz);" \
			"vec3 diffuseRed = u_LdRed * u_Kd * max(dot(light_directionRed ,transformed_normals), 0.0);" \
			"vec3 reflection_vectorRed = reflect(-light_directionRed, transformed_normals);"\
			"vec3 specularRed = u_LsRed * u_Ks * pow(max(dot(reflection_vectorRed, viewer_vector), 0.0), u_material_shininess);"\
			"phong_ads_red_color=ambient + diffuseRed + specularRed;"\
			
			"vec3 light_directionBlue  = normalize(vec3(u_light_positionBlue) - eyeCoordinates.xyz);" \
			"vec3 diffuseBlue = u_LdBlue * u_Kd * max(dot(light_directionBlue ,transformed_normals), 0.0);" \
			"vec3 reflection_vectorBlue = reflect(-light_directionBlue, transformed_normals);"\
			"vec3 specularBlue = u_LsBlue * u_Ks * pow(max(dot(reflection_vectorBlue, viewer_vector), 0.0), u_material_shininess);"\
			"phong_ads_blue_color=ambient + diffuseBlue + specularBlue;"\
			"}"\
		"else"\
		"{"\
		"phong_ads_red_color = vec3(1.0, 1.0, 1.0);"\
		"phong_ads_blue_color = vec3(1.0, 1.0, 1.0);"\
		"}"\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
*/
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
		glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		//fragment shader
		iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
		
		 fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
		 
		 const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec3 phong_ads_red_color;" \
        "in vec3 phong_ads_blue_color;" \
        "vec4 RedFragColor;" \
        "vec4 BlueFragColor;"\
        "out vec4 FragColor;"\
        "void main(void)"\
        "{"\
        "RedFragColor = vec4(phong_ads_red_color, 1.0);" \
        "BlueFragColor = vec4(phong_ads_blue_color, 1.0);" \
        "FragColor = RedFragColor + BlueFragColor; "\
        "}";

        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		shaderProgramObject = glCreateProgram();
		
		glAttachShader(shaderProgramObject, vertexShaderObject);
		glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, MALATI_ATTRIBUTE_NORMAL, "vNormal");

		 glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
		
		 // get MVP uniform location
       // mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        gModelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");

	gProjectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

	gDoubleTapUniform = glGetUniformLocation(shaderProgramObject, "u_double_tap");
	gLdBlueUniform = glGetUniformLocation(shaderProgramObject, "u_LdBlue");
	gLdRedUniform = glGetUniformLocation(shaderProgramObject, "u_LdRed");
	gLaUniform = glGetUniformLocation(shaderProgramObject, "u_La");
	gLsRedUniform = glGetUniformLocation(shaderProgramObject, "u_LsRed");
	gLsBlueUniform = glGetUniformLocation(shaderProgramObject, "u_LsBlue");

	gKdUniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
	gKsUniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
	gKaUniform = glGetUniformLocation(shaderProgramObject, "u_Ka");
	gRedLightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_positionRed");
	gBlueLightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_positionBlue");
	gMaterialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");


	const GLfloat pyramidVertices[] =
	{ 
		//front face
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		//right face
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		//back face
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		//Left face
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f
	};

	const GLfloat pyramidNormal[] =
	{
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,

		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,

		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,

		-0.894427f, 0.447214f, 0.0f
		- 0.894427f, 0.447214f, 0.0f
		- 0.894427f, 0.447214f, 0.0f

	};


	glGenVertexArrays(1, &gVao_Pyramid);
	glBindVertexArray(gVao_Pyramid);

	//position
	glGenBuffers(1, &gVbo_Pyramid_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Pyramid_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	//color
	glGenBuffers(1, &gVbo_Pyramid_Normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Pyramid_Normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormal), pyramidNormal, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);


	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);



        glEnable(GL_DEPTH_TEST);
        
        glDepthFunc(GL_LEQUAL);
        
       // glEnable(GL_CULL_FACE);
        
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		perspectiveProjectionMatrix = vmath::mat4::identity();
		
		UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
		[singleTapGestureRecognizer setNumberOfTapsRequired:1];
		[singleTapGestureRecognizer setNumberOfTouchesRequired:1];
		[singleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:singleTapGestureRecognizer];
		
		UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
		[doubleTapGestureRecognizer setNumberOfTapsRequired:2];
		[doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
		[doubleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:doubleTapGestureRecognizer];
		
		[singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
		
		UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc ] initWithTarget : self action : @selector(onSwipe:)];
		[self addGestureRecognizer:swipeGestureRecognizer];      
		
		UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
		[self addGestureRecognizer:longPressGestureRecognizer];
	}
	return (self);		
}
	/*
	-(void) drawRect:(CGRect)rect
	{
	
	}
	*/
	
	+(Class)layerClass
	{
		return ([CAEAGLLayer class]);
	}
	
	-(void)drawView:(id)sender
	{
		static float gAnglePyramid = 0.0f;
		
		[EAGLContext setCurrentContext:eaglContext];
		
		glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		
		glUseProgram(shaderProgramObject);
		
		if(doubleTap == 1)
		{
		glUniform1i(gDoubleTapUniform, 1);
		glUniform3fv(gLdBlueUniform, 1, lightDiffuseBlue);
		glUniform3fv(gLdRedUniform, 1, lightDiffuseRed);
		glUniform3fv(gLaUniform, 1, lightAmbient);
		glUniform3fv(gLsRedUniform, 1, lightSpecularRed);
		glUniform3fv(gLsBlueUniform, 1, lightSpecularBlue);
		
		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);

		//float lightPosition1[] = { -3.0f, 0.0f, -1.0f, 1.0f };
		glUniform4fv(gRedLightPositionUniform, 1, lightPositionRed);
		//float lightPosition[] = { 3.0f, 0.0f, 1.0f, 1.0f };
		glUniform4fv(gBlueLightPositionUniform, 1, lightPositionBlue);
		
		}
		else
		{
		glUniform1i(gDoubleTapUniform, 0);
		}


      
		vmath::mat4 modelMatrix = vmath::mat4::identity();
		vmath::mat4 viewMatrix = vmath::mat4::identity();

		
		vmath::mat4 rotationMatrix = vmath::mat4::identity(); 
		
        modelMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
		
		rotationMatrix = vmath::rotate(gAnglePyramid, 0.0f, 1.0f, 0.0f);
		modelMatrix = modelMatrix * rotationMatrix; //imp
		
		
    
		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

		
		glBindVertexArray(gVao_Pyramid);

		
		glDrawArrays(GL_TRIANGLES, 0, 12);
		
		glBindVertexArray(0);
		
	
		glUseProgram(0);
		
		glBindRenderbuffer(GL_RENDERBUFFER , colorRenderbuffer);
		[eaglContext presentRenderbuffer:GL_RENDERBUFFER];
		
		
		gAnglePyramid = gAnglePyramid + 1.0f;
		
	}
	
	-(void)layoutSubviews
	{
		GLint width;
		GLint height;
	
		glBindRenderbuffer (GL_RENDERBUFFER, colorRenderbuffer);
		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
		
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
		
		glGenRenderbuffers(1, &depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
		
		glViewport(0, 0, width, height);
		
		GLfloat fwidth = (GLfloat)width;
		GLfloat fheight = (GLfloat)height;
		/*if (width <= height)
			perspectiveProjectionMatrix = vmath::ortho(-100.0f, 100.0f, (-100.0f * (fheight / fwidth)), (100.0f * (fheight / fwidth)), -100.0f, 100.0f);
		else
            perspectiveProjectionMatrix = vmath::ortho((-100.0f * (fwidth / fheight)), (100.0f * (fwidth / fheight)), -100.0f, 100.0f, -100.0f, 100.0f);
    */
        perspectiveProjectionMatrix = vmath::perspective(45.0f, fwidth/fheight, 0.1f, 100.0f);
        
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed To Create Complete FrameBuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
		}
		
		[self drawView:nil];
	}
	
	-(void)startAnimation
	{
		if(!isAnimating)
		{
			displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
			
			[displayLink setPreferredFramesPerSecond:animationFrameInterval];
			[displayLink addToRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
			
			isAnimating = YES;
		}
	}
	
	-(void)stopAnimation
	{
		if(isAnimating)
		{
			[displayLink invalidate];
			displayLink = nil;
			isAnimating = NO;
			
		}
	}
	
	//to become first responder
	-(BOOL)acceptsFirstResponder
	{
		return (YES);
	}
	
	-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event
	{
		
	}
	
	-(void) onSingleTap:(UITapGestureRecognizer *)gr
	{
		
	}
	
	-(void) onDoubleTap:(UITapGestureRecognizer *)gr
	{
			if (doubleTap == false)
			{
				doubleTap = true;
				//bIsLKeyPressed = true;
			}
			else
			{
				doubleTap = false;
				//bIsLKeyPressed = false;
			}
	}
	
	-(void) onSwipe:(UISwipeGestureRecognizer *)gr
	{
		[self release];
		exit(0);
	}
	
	-(void)onLongPress:(UILongPressGestureRecognizer *)gr
	{
	
	}
	
	-(void)dealloc
	{
	if (gVao_Pyramid)
	{
		glDeleteVertexArrays(1, &gVao_Pyramid);
		gVao_Pyramid = 0;
	}
	
	if (gVbo_Pyramid_Position)
	{
		glDeleteBuffers(1, &gVbo_Pyramid_Position);
		gVbo_Pyramid_Position = 0;
	}
	if (gVbo_Pyramid_Normal)
	{
		glDeleteBuffers(1, &gVbo_Pyramid_Normal);
		gVbo_Pyramid_Normal = 0;
	}
	
	
	
		glDetachShader(shaderProgramObject, vertexShaderObject);
		
		glDetachShader(shaderProgramObject, fragmentShaderObject);
		
		glDeleteShader(vertexShaderObject);
		vertexShaderObject = 0;
		
		glDeleteShader(fragmentShaderObject);
		fragmentShaderObject = 0;
		
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;

		if(depthRenderbuffer)
		{
			glDeleteRenderbuffers(1, &depthRenderbuffer);
			depthRenderbuffer = 0;
		}
		
		if(colorRenderbuffer)
		{
			glDeleteRenderbuffers(1, &colorRenderbuffer);
			colorRenderbuffer = 0;
		}
			
		if(defaultFramebuffer)
		{
			glDeleteFramebuffers(1, &defaultFramebuffer);
			defaultFramebuffer = 0;
		}
		
		if([EAGLContext currentContext] == eaglContext)
		{
			[EAGLContext setCurrentContext:nil];
		}
		[eaglContext release];
		eaglContext = nil;
		
		[super dealloc];
	}
	
	@end
