#include <windows.h>
#include <stdio.h> // for FILE I/O

#include <gl\glew.h>//use it before other GL.h

#include <gl/GL.h>
#include "vbm.h"

#include "vmath.h"
#include "Sphere.h"
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define FRUSTUM_DEPTH       800.0f
#define DEPTH_TEXTURE_SIZE  2048


float aspect;
/*GLuint render_light_prog;
struct
{
	GLint model_view_projection_matrix;
} render_light_uniforms;
*/
using namespace vmath;

enum
{
	MALATI_ATTRIBUTE_VERTEX = 0,
	MALATI_ATTRIBUTE_COLOR,
	MALATI_ATTRIBUTE_NORMAL,
	MALATI_ATTRIBUTE_TEXTURE
};
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObjectShadowMapGen;
GLuint gFragmentShaderObjectShadowMapGen;
GLuint gShaderProgramObjectShadowMapGen;

GLuint gVertexShaderObjectShadowMapRender;
GLuint gFragmentShaderObjectShadowMapRender;
GLuint gShaderProgramObjectShadowMapRender;
/*
GLuint gNumElements;
GLuint gNumVertices;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];*/


GLuint gMVPMatrixUniform, gModelMatrixUniform, gViewMatrixUniform, gProjectionMatrixUniform, gShadowMatrixUniform;
GLuint gLightPositionUniform;
GLuint gKdUniform, gKaUniform, gKsUniform, gMaterialShininessUniform;;
//mat4 gPerspectiveProjectionMatrix;

//GLfloat currentTime = 0.0f;
/*
GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular_power[] = { 1.0f,1.0f,1.0f,1.0f };*/
/*
GLuint cubeVAO;
GLuint cubeVBOposition;
GLuint cubeVBOnormal;
GLuint cubeVBOtexture;*/

//GLuint depthMap;
//GLuint depthMapFBO;
GLuint gTexture_sampler_uniform;
/*GLuint 	planeVAO;
GLuint planeVBOposition;
GLuint planeVBOnormal;
GLuint planeVBOtexture;
*/
ULONGLONG gAppStartTime = 0;


GLuint  depth_fbo;
GLuint  depth_texture;



VBObject object;


GLuint  ground_vbo;
GLuint  ground_vao;

GLint current_width;
GLint current_height;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	//void Update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	//code
	// create log file
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	//Parallel to glutInitWindowSize(), glutInitWindowPosition() and glutCreateWindow() all three together
	hwnd = CreateWindow(szClassName,
		TEXT("OpenGL Programmable Pipeline Window"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//initialize
	initialize();

	//Message Loop
	while (bDone == false) //Parallel to glutMainLoop();
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// rendring function
			display();
			//Update();
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true) //Continuation to glutLeaveMainLoop();
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0) //if 0, the window is active
			gbActiveWindow = true;
		else //if non-zero, the window is not active
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE: //Parallel to glutReshapeFunc();
		resize(LOWORD(lParam), HIWORD(lParam)); //Parallel to glutReshapeFunc(resize);
		break;
	case WM_KEYDOWN: //Parallel to glutKeyboardFunc();
		switch (wParam)
		{
		case VK_ESCAPE: //case 27
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true; //Parallel to glutLeaveMainLoop();
			break;

		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:  //Parallel to glutMouseFunc();
		break;
	case WM_CLOSE: //Parallel to glutCloseFunc();
		uninitialize(); //Parallel to glutCloseFunc(uninitialize);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

//FUNCTION DEFINITIONS
void initialize(void)
{
	//function prototypes
	void uninitialize(void);
	void resize(int, int);
	void LoadGLTextures();
	void MyShadowMapGenFun();
	void MyShdowMapRenderFun();
	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	//Parallel to glutInitDisplayMode()
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	//choose a pixel format which best matches with that of 'pfd'
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//set the pixel format chosen above
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//create OpenGL rendering context
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//make the rendering context created above as current n the current hdc
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "%s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "Shading Lang Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	gAppStartTime = ::GetTickCount64();

	MyShadowMapGenFun();
	MyShdowMapRenderFun();

	glUseProgram(gShaderProgramObjectShadowMapRender);
	//glUniform1i(glGetUniformLocation(gShaderProgramObjectShadowMapRender, "depth_texture"), 0);
	glUniform1i(gTexture_sampler_uniform, 0);
	LoadGLTextures();

	static const float ground_vertices[] =
	{
		-500.0f, -50.0f, -500.0f, 1.0f,
		-500.0f, -50.0f,  500.0f, 1.0f,
		500.0f, -50.0f,  500.0f, 1.0f,
		500.0f, -50.0f, -500.0f, 1.0f,
	};

	static const float ground_normals[] =
	{
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f
	};

	glGenVertexArrays(1, &ground_vao);
	glGenBuffers(1, &ground_vbo);
	glBindVertexArray(ground_vao);
	glBindBuffer(GL_ARRAY_BUFFER, ground_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(ground_vertices) + sizeof(ground_normals), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(ground_vertices), ground_vertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(ground_vertices), sizeof(ground_normals), ground_normals);
	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid *)sizeof(ground_vertices));

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);
	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);

	//TO DO :  Load the object
	

	object.LoadFromVBM("armadillo_low.vbm", 0, 1, 2);

/*	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();



	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	//position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//normal
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);


	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	glBindVertexArray(0);
	

	float planeVertices[] = {
		// positions            // normals         // texcoords
		25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
		-25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
		-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,

		25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
		-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
		25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,  25.0f, 25.0f
	};*/
	// plane VAO
	/*
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	//position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);*/
	//unsigned int planeVBO;
	/*glGenVertexArrays(1, &planeVAO);
	glBindVertexArray(planeVAO);

	//position
	glGenBuffers(1, &planeVBOposition);

	glBindBuffer(GL_ARRAY_BUFFER, planeVBOposition);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);


	glVertexAttribPointer(MALATI_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(MALATI_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &planeVBOnormal);
	glBindBuffer(GL_ARRAY_BUFFER, planeVBOnormal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(MALATI_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(MALATI_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &planeVBOtexture);
	glBindBuffer(GL_ARRAY_BUFFER, planeVBOtexture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(MALATI_ATTRIBUTE_TEXTURE, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(MALATI_ATTRIBUTE_TEXTURE);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	
	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	glClearColor(1.0f, 0.0f, 0.0f, 0.0f); // blue

	gPerspectiveProjectionMatrix = mat4::identity();
	*/
	// resize
	resize(WIN_WIDTH, WIN_HEIGHT);

}

unsigned int MyApp_time()
{
	ULONGLONG currentTime = ::GetTickCount64();

	return (unsigned int)(currentTime - gAppStartTime);
}


void display(void)
{
	//code
	void DrawScene(bool depth_only);
	float t = float(MyApp_time() & 0xFFFF) / float(0xFFFF);
	static float q = 0.0f;

	static const vec3 X(1.0f, 0.0f, 0.0f);
	static const vec3 Y(0.0f, 1.0f, 0.0f);
	static const vec3 Z(0.0f, 0.0f, 1.0f);

	vec3 light_position = vec3(sinf(t * 6.0f * 3.141592f) * 300.0f, 200.0f, cosf(t * 4.0f * 3.141592f) * 100.0f + 250.0f);

	// Setup
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);



	// Matrices for rendering the scene
	mat4 scene_model_matrix = rotate(t * 720.0f, Y);
	mat4 scene_view_matrix = translate(0.0f, 0.0f, -300.0f);
	mat4 scene_projection_matrix = frustum(-1.0f, 1.0f, -aspect, aspect, 1.0f, FRUSTUM_DEPTH);
	const mat4 scale_bias_matrix = mat4(vec4(0.5f, 0.0f, 0.0f, 0.0f),
		vec4(0.0f, 0.5f, 0.0f, 0.0f),
		vec4(0.0f, 0.0f, 0.5f, 0.0f),
		vec4(0.5f, 0.5f, 0.5f, 1.0f));




	// Matrices used when rendering from the light's position
	mat4 light_view_matrix = lookat(light_position, vec3(0.0f), Y);
	mat4 light_projection_matrix(frustum(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, FRUSTUM_DEPTH));



	// Now we render from the light's position into the depth buffer.
	// Select the appropriate program

	glUseProgram(gShaderProgramObjectShadowMapGen);
	mat4 light_mvp_matrix = light_projection_matrix * light_view_matrix * scene_model_matrix;
	glUniformMatrix4fv(gMVPMatrixUniform, 1, GL_FALSE, light_mvp_matrix);

	// Bind the 'depth only' FBO and set the viewport to the size of the depth texture
	glBindFramebuffer(GL_FRAMEBUFFER, depth_fbo);
	glViewport(0, 0, DEPTH_TEXTURE_SIZE, DEPTH_TEXTURE_SIZE);

	// Clear
	glClearDepth(1.0f);
	glClear(GL_DEPTH_BUFFER_BIT);

	// Enable polygon offset to resolve depth-fighting isuses
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(2.0f, 4.0f);
	// Draw from the light's point of view
	DrawScene(true);

	glDisable(GL_POLYGON_OFFSET_FILL);

	// Restore the default framebuffer and field of view
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, current_width, current_height);

	// Now render from the viewer's position
	glUseProgram(gShaderProgramObjectShadowMapRender);

	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	// Setup all the matrices
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, scene_model_matrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, scene_view_matrix);
	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, scene_projection_matrix);
	glUniformMatrix4fv(gShadowMatrixUniform, 1, GL_FALSE, scale_bias_matrix *light_projection_matrix * light_view_matrix);
	glUniform3fv(gLightPositionUniform, 1, light_position);

	// Bind the depth texture
	glBindTexture(GL_TEXTURE_2D, depth_texture);
	glGenerateMipmap(GL_TEXTURE_2D);

	// Draw
	DrawScene(false);

	/*glUseProgram(gShaderProgramObjectShadowMapGen);
	vmath::mat4 model_matrix = mat4::identity();
	//model_matrix = translate(20.0f, -0.5f, 0.0f);
	vmath::mat4 viewMatrix = mat4::identity();
	vmath::mat4 light_proj_matrix =
		vmath::frustum(-1.0f, 1.0f, -1.0f, 1.0f,
			1.0f, 1000.0f);
	vmath::mat4 light_mvp_matrix = light_proj_matrix *
		viewMatrix *
		model_matrix;
	glUniformMatrix4fv(gMVPMatrixUniform, 1, GL_FALSE, light_mvp_matrix);
	glBindVertexArray(planeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
	
	 model_matrix = mat4::identity(); 
	vmath::mat4 rotationMatrix = vmath::rotate(currentTime, 0.0f, 1.0f, 0.0f);
	model_matrix = model_matrix * rotationMatrix;

	vmath::mat4 light_view_matrix =
		vmath::lookat(vec3(-2.0f, 4.0f, -1.0f),
			vmath::vec3(0.0f),
			vmath::vec3(0.0f, 1.0f, 0.0f));
	light_proj_matrix =
		vmath::frustum(-1.0f, 1.0f, -1.0f, 1.0f,
			1.0f, 1000.0f);
	const vmath::mat4 scale_bias_matrix =
		vmath::mat4(vmath::vec4(0.5f, 0.0f, 0.0f, 0.0f),
			vmath::vec4(0.0f, 0.5f, 0.0f, 0.0f),
			vmath::vec4(0.0f, 0.0f, 0.5f, 0.0f),
			vmath::vec4(0.5f, 0.5f, 0.5f, 1.0f));

	light_mvp_matrix = light_proj_matrix *
		light_view_matrix *
		model_matrix;
	vmath::mat4 shadow_matrix = scale_bias_matrix *
		light_proj_matrix *
		light_view_matrix *
		model_matrix;
	glUniformMatrix4fv(gMVPMatrixUniform, 1, GL_FALSE, light_mvp_matrix);
	glUniformMatrix4fv(gShadowMatrixUniform, 1, GL_FALSE, shadow_matrix);
/*
	mat4 lightProjection = ortho(-10.0f, 10.0f, -10.0f, 10.0f, 1.0f, 7.5f);
	mat4 lightView = lookat(vec3(-2.0f, 4.0f, -1.0f),
		vec3(0.0f, 0.0f, 0.0f),
		vec3(0.0f, 1.0f, 0.0f));
	mat4 lightSpaceMatrix = lightProjection * lightView;
	glUniformMatrix4fv(gMVPMatrixUniform, 1, GL_FALSE, lightSpaceMatrix);

	glViewport(0, 0, 1024, 1024);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);
	
	//ConfigureShaderAndMatrices
	
	glActiveTexture(GL_TEXTURE0);
	//RenderScene
	//DrawScene
	mat4 model;
	
	model = vmath::translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, model);
	*
	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);
	


	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glViewport(0, 0, WIN_WIDTH, WIN_HEIGHT);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObjectShadowMapRender);
	//ConfigureShaderAndMatrices
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depthMap);

	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);



	//RenderScene
	/*vec3 light_position = vec3(sinf(t * 6.0f * 3.141592f) * 300.0f,
		200.0f,
		cosf(t * 4.0f * 3.141592f) * 100.0f + 250.0f);

	mat4 scene_model_matrix = vmath::rotate(0.0f, t * 720.0f, 0.0f);

	mat4 light_view_matrix = lookat(light_position, vec3(0, 0, 0), vec3(0, 1, 0));
	mat4 light_projection_matrix(frustum(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 100.0f));

	glUseProgram(gShaderProgramObjectShadowMapGen);
	glUniformMatrix4fv(gMVPMatrixUniform, 1, GL_FALSE, light_projection_matrix * light_view_matrix * scene_model_matrix);

	
	
	

	glClearDepth(1.0f);
	glClear(GL_DEPTH_BUFFER_BIT);

	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(2.0f, 4.0f);

	//DrawScene
	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);

	glDisable(GL_POLYGON_OFFSET_FILL);*
	glUseProgram(0);
	*/
	SwapBuffers(ghdc);
}

void DrawScene(bool depth_only)
{

	// Set material properties for the object
	if (!depth_only)
	{
		glUniform3fv(gKaUniform, 1, vec3(0.1f, 0.0f, 0.2f));
		glUniform3fv(gKdUniform, 1, vec3(0.3f, 0.2f, 0.8f));
		glUniform3fv(gKsUniform, 1, vec3(1.0f, 1.0f, 1.0f));
		glUniform1f(gMaterialShininessUniform, 25.0f);
	}
	//TO DO: Draw The Object
	object.Render();

	// render Cube
	/*glBindVertexArray(cubeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);*/

	// Set material properties for the ground
	if (!depth_only)
	{
		glUniform3fv(gKaUniform, 1, vec3(0.1f, 0.1f, 0.1f));
		glUniform3fv(gKdUniform, 1, vec3(0.1f, 0.5f, 0.1f));
		glUniform3fv(gKsUniform, 1, vec3(0.1f, 0.1f, 0.1f));
		glUniform1f(gMaterialShininessUniform, 3.0f);
	}
	glBindVertexArray(ground_vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

}
void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;

	current_width = width;
	current_height = height;

	aspect = float(height) / float(width);

//	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
//	gPerspectiveProjectionMatrix = frustum(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 100.0f);

	//gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height,
	//		0.1f, 100.0f);
}

void LoadGLTextures()
{
	
	// Create FBO to render depth into
	fprintf(gpFile, "In LoadGLTexture\n");

	glGenFramebuffers(1, &depth_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, depth_fbo);

	glGenTextures(1, &depth_texture);
	glBindTexture(GL_TEXTURE_2D, depth_texture);

	glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32, DEPTH_TEXTURE_SIZE, DEPTH_TEXTURE_SIZE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//glBindTexture(GL_TEXTURE_2D, 0);

	
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
		depth_texture, 0);
	
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	
}

/*void Update(void)
{

	currentTime = currentTime + 0.1;
	if (currentTime > 10)
		currentTime = 0.0f;
}*/

void MyShadowMapGenFun()
{
	void uninitialize(void);
	//VERTEX SHADER
	gVertexShaderObjectShadowMapGen = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCodeShadowMapGen =
		"#version 460" \
		"\n" \
		"uniform mat4 model_view_projection_matrix;" \

		"layout (location = 0) in vec4 position;" \

		"void main(void)" \
		"{" \
		"gl_Position = model_view_projection_matrix * position;" \
		"}";

	glShaderSource(gVertexShaderObjectShadowMapGen, 1, (const GLchar **)&vertexShaderSourceCodeShadowMapGen, NULL);
	//compile shader
	glCompileShader(gVertexShaderObjectShadowMapGen);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObjectShadowMapGen, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectShadowMapGen, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectShadowMapGen, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader ShadowMapGen Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	//FRAGMENT SHADER

	gFragmentShaderObjectShadowMapGen = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCodeShadowMapGen =
		"#version 460" \
		"\n" \
		"layout (location = 0) out vec4 color;" \
		
		"void main(void)" \
		"{" \
		"color = vec4(1.0); " \
		"}";

	glShaderSource(gFragmentShaderObjectShadowMapGen, 1, (const GLchar **)&fragmentShaderSourceCodeShadowMapGen, NULL);

	glCompileShader(gFragmentShaderObjectShadowMapGen);

	glGetShaderiv(gFragmentShaderObjectShadowMapGen, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectShadowMapGen, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectShadowMapGen, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader ShadowMapGen Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	//Shader Program

	gShaderProgramObjectShadowMapGen = glCreateProgram();

	glAttachShader(gShaderProgramObjectShadowMapGen, gVertexShaderObjectShadowMapGen);

	glAttachShader(gShaderProgramObjectShadowMapGen, gFragmentShaderObjectShadowMapGen);

	glBindAttribLocation(gShaderProgramObjectShadowMapGen, MALATI_ATTRIBUTE_VERTEX, "position");
	
	glLinkProgram(gShaderProgramObjectShadowMapGen);

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObjectShadowMapGen, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectShadowMapGen, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectShadowMapGen, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link ShadowMapGen Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	gMVPMatrixUniform = glGetUniformLocation(gShaderProgramObjectShadowMapGen, "model_view_projection_matrix");
	
}


void MyShdowMapRenderFun()
{
	void uninitialize(void);
	//VERTEX SHADER
	gVertexShaderObjectShadowMapRender = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCodeShadowMapRender =
		"#version 460" \
		"\n" \
		"uniform mat4 model_matrix;" \
		"uniform mat4 view_matrix;" \
		"uniform mat4 projection_matrix;" \
		"uniform mat4 shadow_matrix;" \

		"layout (location = 0) in vec4 position;" \
		"layout (location = 2) in vec3 normal;" \

		"out VS_FS_INTERFACE" \
		"{" \
		"vec4 shadow_coord;" \
		"vec3 world_coord;" \
		"vec3 eye_coord;" \
		"vec3 normal;" \
		"} vertex;" \

		"void main(void)" \
		"{" \
		"vec4 world_pos = model_matrix * position;" \
		"vec4 eye_pos = view_matrix * world_pos;" \
		"vec4 clip_pos = projection_matrix * eye_pos;" \

		"vertex.world_coord = world_pos.xyz;" \
		"vertex.eye_coord = eye_pos.xyz;" \
		"vertex.shadow_coord = shadow_matrix * world_pos;" \
		"vertex.normal = mat3(view_matrix * model_matrix) * normal;" \

		"gl_Position = clip_pos;" \
		"}";



	glShaderSource(gVertexShaderObjectShadowMapRender, 1, (const GLchar **)&vertexShaderSourceCodeShadowMapRender, NULL);
	//compile shader
	glCompileShader(gVertexShaderObjectShadowMapRender);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObjectShadowMapRender, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectShadowMapRender, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectShadowMapRender, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader ShadowMapRender Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	//FRAGMENT SHADER

	gFragmentShaderObjectShadowMapRender = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCodeShadowMapRender =
		"#version 460" \
		"\n" \
		"uniform sampler2DShadow depth_texture;" \
		"uniform vec3 light_position;" \

		"uniform vec3 material_ambient;" \
		"uniform vec3 material_diffuse;" \
		"uniform vec3 material_specular;" \
		"uniform float material_specular_power;" \

		"layout (location = 0) out vec4 color;" \

		"in VS_FS_INTERFACE" \
		"{" \
		"vec4 shadow_coord;" \
		"vec3 world_coord;" \
		"vec3 eye_coord;" \
		"vec3 normal;" \
		"} fragment;" \

		"void main(void)" \
		"{" \
		"vec3 N = fragment.normal;" \
		"vec3 L = normalize(light_position - fragment.world_coord);" \
		"float LdotN = dot(N, L);" \
		"vec3 R = reflect(-L, N);" \

		"float diffuse = max(LdotN, 0.0);" \
		"float specular = max(pow(dot(normalize(-fragment.eye_coord), R), material_specular_power), 0.0);" \

		"float f = textureProj(depth_texture, fragment.shadow_coord);" \

		"color = vec4(material_ambient + f * (material_diffuse * diffuse + material_specular * specular), 1.0);" \
		
		"}";

	glShaderSource(gFragmentShaderObjectShadowMapRender, 1, (const GLchar **)&fragmentShaderSourceCodeShadowMapRender, NULL);

	glCompileShader(gFragmentShaderObjectShadowMapRender);

	glGetShaderiv(gFragmentShaderObjectShadowMapRender, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectShadowMapRender, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectShadowMapRender, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader ShadowMapRender Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	//Shader Program

	gShaderProgramObjectShadowMapRender = glCreateProgram();

	glAttachShader(gShaderProgramObjectShadowMapRender, gVertexShaderObjectShadowMapRender);

	glAttachShader(gShaderProgramObjectShadowMapRender, gFragmentShaderObjectShadowMapRender);

	glBindAttribLocation(gShaderProgramObjectShadowMapRender, MALATI_ATTRIBUTE_VERTEX, "position");
	glBindAttribLocation(gShaderProgramObjectShadowMapRender, MALATI_ATTRIBUTE_NORMAL, "normal");

	glLinkProgram(gShaderProgramObjectShadowMapRender);

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObjectShadowMapRender, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectShadowMapRender, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectShadowMapRender, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program ShadowMapRender Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	gModelMatrixUniform = glGetUniformLocation(gShaderProgramObjectShadowMapRender, "model_matrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObjectShadowMapRender, "view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObjectShadowMapRender, "projection_matrix");
	gShadowMatrixUniform = glGetUniformLocation(gShaderProgramObjectShadowMapRender, "shadow_matrix");

	gKdUniform = glGetUniformLocation(gShaderProgramObjectShadowMapRender, "material_diffuse");
	gKsUniform = glGetUniformLocation(gShaderProgramObjectShadowMapRender, "material_specular");
	gKaUniform = glGetUniformLocation(gShaderProgramObjectShadowMapRender, "material_ambient");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObjectShadowMapRender, "material_specular_power");

	gLightPositionUniform = glGetUniformLocation(gShaderProgramObjectShadowMapRender, "light_position");
	gTexture_sampler_uniform = glGetUniformLocation(gShaderProgramObjectShadowMapRender, "depth_texture");
}


void uninitialize(void)
{
	//UNINITIALIZATION CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	if (ground_vao)
	{
		glDeleteVertexArrays(1, &ground_vao);
		ground_vao = 0;
	}

	if (ground_vbo)
	{
		glDeleteBuffers(1, &ground_vbo);
		ground_vbo = 0;
	}
	
	glDetachShader(gShaderProgramObjectShadowMapRender, gVertexShaderObjectShadowMapRender);
	glDetachShader(gShaderProgramObjectShadowMapRender, gFragmentShaderObjectShadowMapRender);

	glDeleteShader(gVertexShaderObjectShadowMapRender);
	gVertexShaderObjectShadowMapRender = 0;
	glDeleteShader(gFragmentShaderObjectShadowMapRender);
	gFragmentShaderObjectShadowMapRender = 0;

	glDeleteProgram(gShaderProgramObjectShadowMapRender);
	gShaderProgramObjectShadowMapRender = 0;

	glDetachShader(gShaderProgramObjectShadowMapGen, gVertexShaderObjectShadowMapGen);
	glDetachShader(gShaderProgramObjectShadowMapGen, gFragmentShaderObjectShadowMapGen);

	glDeleteShader(gVertexShaderObjectShadowMapGen);
	gVertexShaderObjectShadowMapGen = 0;
	glDeleteShader(gFragmentShaderObjectShadowMapGen);
	gFragmentShaderObjectShadowMapGen = 0;

	glDeleteProgram(gShaderProgramObjectShadowMapGen);
	gShaderProgramObjectShadowMapGen = 0;


	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	//Delete the device context
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
